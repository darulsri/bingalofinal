package com.bingalollc.com.base;

import androidx.activity.result.ActivityResult;

public interface OnCameraClickedInterface {
    void onCameraCaptured(ActivityResult result);
}
