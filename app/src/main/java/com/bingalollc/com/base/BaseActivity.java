package com.bingalollc.com.base;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.text.style.ForegroundColorSpan;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.TypefaceSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;

import com.bingalollc.com.AppController;
import com.bingalollc.com.R;
import com.bingalollc.com.chat.FullScreenImageView;
import com.bingalollc.com.preference.PreferenceManager;
import com.bingalollc.com.utils.CustomTypefaceSpan;
import com.bingalollc.com.utils.dialog.ProgressDialog;
import com.bingalollc.com.webview.OpenWebView;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

//import dagger.hilt.android.AndroidEntryPoint;
import me.imid.swipebacklayout.lib.SwipeBackLayout;
import me.imid.swipebacklayout.lib.app.SwipeBackActivity;
import pub.devrel.easypermissions.EasyPermissions;

import static android.Manifest.permission.CAMERA;
import static com.bingalollc.com.preference.Common.PermissionConstant.PERMISSION_REQUEST_CODE_CAMERA_MODE;
import static com.bingalollc.com.preference.StringsConstant.CREATED_DATE;

import javax.inject.Inject;

//@AndroidEntryPoint
public class BaseActivity extends SwipeBackActivity {

  //  @Inject
    public PreferenceManager preferenceManager = AppController.getInstance().preferenceManager;

    private AlertDialog testDialog=null;
    public static String lat = "";
    public static String lng = "";
    private SwipeBackLayout mSwipeBackLayout;
    public static final int REQUEST_LOCATION = 104;
    public static final int LOCATION_PERMISSION = 10001;
    private static LocationManager locationManager;
    public String permissionsArrayLocation[] = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    private android.app.AlertDialog alertDialogPermission;

    public SwipeBackLayout importSwipeBack(){
        mSwipeBackLayout = getSwipeBackLayout();
        mSwipeBackLayout.setEdgeTrackingEnabled(SwipeBackLayout.EDGE_LEFT);
        return mSwipeBackLayout;
    }
    public void disableSwipe(){
        mSwipeBackLayout = getSwipeBackLayout();
        mSwipeBackLayout.setEnableGesture(false);
    }

    public static void fullScreenImageDialog(Context context, String imageUrl, int isCustomer) {
        Intent intent=new Intent(context, FullScreenImageView.class);
        intent.putExtra("url",imageUrl);
        intent.putExtra("cust",isCustomer+"");
        context.startActivity(intent);
    }

    public String getBasicAddress(Double latitude, Double longitude, Context context) {
        StringBuilder result = new StringBuilder();
        String cityState ="";
        if (latitude == null) {
            return "";
        }

        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                result.append(address.getFeatureName()).append(", ");
                //  result.append(address.locality).append(", ")
                result.append(address.getCountryName());
                // exactAddress = "Near " + address.getAddressLine(0);
                //cityState = address.getLocality()+", "+ address.getAdminArea();
                String add1 = "";

                if (!TextUtils.isEmpty(address.getLocality())) {
                    add1 = address.getLocality()+",";
                }else if (!TextUtils.isEmpty(address.getSubAdminArea())) {
                    add1 = address.getSubAdminArea()+",";
                }
                cityState = add1+" "+address.getAdminArea();

            }
        } catch (IOException e) {
            Log.e("tag", e.getMessage());
        }
        return cityState;
    }

    public String getAddress(Double latitude, Double longitude, Context context, Boolean onlyCityState){
        StringBuilder result = new StringBuilder();
        String completeAddress ="";

        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses!=null && addresses.size() > 0) {
                Address address = addresses.get(0);
                result.append(address.getFeatureName()).append("\n");
                result.append(address.getLocality()).append("\n");
                result.append(address.getCountryName());
                completeAddress = address.getAddressLine(0);
                if (completeAddress.isEmpty()){
                    completeAddress = address.getAdminArea();
                }
                if (onlyCityState) {
                    String add1 = "";
                    if (!TextUtils.isEmpty(address.getSubAdminArea())) {
                        add1 = address.getSubAdminArea()+",";
                    } else if (!TextUtils.isEmpty(address.getLocality())) {
                        add1 = address.getLocality()+",";
                    }
                    completeAddress = add1+" "+address.getAdminArea();
                }

            }
        }catch (IOException e) {
            Log.e("tag", e.getMessage());
        }
        return completeAddress;
    }



    public void setImageToGlide(){

    }

    public  void preventDoubleTap(View view){
        view.setEnabled(false);
        new Handler().postDelayed(() -> view.setEnabled(true),1000);
    }
    public void showFullScreenBaseColor(int colorNumber){
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                if (colorNumber==0) {
                    window.setStatusBarColor(getColor(R.color.basecolor));
                }else  if (colorNumber==1){
                    window.setStatusBarColor(getColor(R.color.basecolor));
                } else  {
                    window.setStatusBarColor(colorNumber);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public boolean checkGpsOn() {
        locationManager = (LocationManager) getSystemService(this.LOCATION_SERVICE);

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        System.out.print(">>>>>>>>>LOCA ");
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            displayLocationSettingsRequest(this);
            return false;

        } else {
            System.out.print(">>>>>>>>>LOCA ");
            return true;
        }

    }

    public void displayLocationSettingsRequest(Activity context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i("TAG1", "All location settings are satisfied.");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i("TAG2", "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(context, LOCATION_PERMISSION);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i("TAG3", "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i("Tag4", "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }

    public void showLoading(Context context) {
        ProgressDialog.showProgressDialog(context);
    }

    public void showToast(String message,Context context) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
    public void showMessageOKCancel(String message, String title, DialogInterface.OnClickListener okListener, Context context, String positiveBtn, String negativeBtn, Boolean isCancelable) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setMessage(message);
        alertDialog.setTitle(title);
        alertDialog.setPositiveButton(positiveBtn, okListener);
        if (!TextUtils.isEmpty(negativeBtn)){
            alertDialog
                    .setNegativeButton(context.getString(R.string.cancel), null);
        }
        alertDialog.setCancelable(isCancelable);

                alertDialog.create()
                .show();
    }
    public void hideLoading() {
        ProgressDialog.dismissProgressDialog();
    }

    public void hideKeyBoardOfView(Context context, EditText editText) {
        if (context != null && editText != null) {
            InputMethodManager im = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        }
    }

    public void showKeyBoardOfView(Context context, EditText editText) {
        if (context != null && editText != null) {
            InputMethodManager keyboard = (InputMethodManager)
                    context.getSystemService(Context.INPUT_METHOD_SERVICE);
            keyboard.showSoftInput(editText, 0);
        }
    }

    public String getCurrentDateTime(){
        //2021-09-05 12:28:37
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>DATE "+df.format(new Date()));
        return df.format(new Date());
    }

    public void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static String getCameraFilePathName() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                        .toString() + "/picFolder/bingalo_images.jpg";

    }

    public static File setCamerFilePath() {
        String dir=
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                        .toString() + "/picFolder/";
        File newdir = new File(dir);
        newdir.mkdirs();

        String file = dir + "bingalo_images"+ ".jpg";
        File newfile = new File(file);
        if (newfile.exists()) {
            newfile.delete();
        }
        try {
            newfile.createNewFile();
        } catch (IOException e) {
        }
        return newfile;
    }

    public void openImageIntent(Context context, OnCameraClickedInterface onCameraClickedInterface) {
        Uri outputFileUri;

        ActivityResultLauncher<Intent> startActivityIntent = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        // Add same code that you want to add in onActivityResult method
                        onCameraClickedInterface.onCameraCaptured(result);
                    }
                });


        // Determine Uri of camera image to save.
        final File root = new File(Environment.getExternalStorageDirectory() + File.separator + "MyDir" + File.separator);
        root.mkdirs();
        final String fname = System.currentTimeMillis()+"";
        final File sdImageMainDirectory = new File(root, fname);
        outputFileUri = Uri.fromFile(sdImageMainDirectory);

        // Camera.
        final List<Intent> cameraIntents = new ArrayList<Intent>();
        final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = context.getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for(ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            cameraIntents.add(intent);
        }

        // Filesystem.
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");

        // Add the camera options.
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[0]));
        startActivityIntent.launch(chooserIntent);
        //startActivityForResult(chooserIntent, YOUR_SELECT_PICTURE_REQUEST_CODE);


    }


    public void showNormalDialog(Activity context, String headerOne, String headerTwo, String button1, String button2,final OnOkClicked onOkClicked){
        preferenceManager = PreferenceManager.getInstance(context);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        LayoutInflater inflater = context.getLayoutInflater();
        final View add_menu_layout = inflater.inflate(R.layout.dialog_ui,  null);
        TextView header_one = add_menu_layout.findViewById(R.id.header_one);
        TextView header_two = add_menu_layout.findViewById(R.id.header_two);
        TextView cancelBtn = add_menu_layout.findViewById(R.id.cancelBtn);
        TextView logout_btn = add_menu_layout.findViewById(R.id.logout_btn);
        RelativeLayout cancelLayout = add_menu_layout.findViewById(R.id.cancelLayout);
        RelativeLayout logoutLayout = add_menu_layout.findViewById(R.id.logoutLayout);
        header_one.setText(headerOne);
        if (header_one.getText().toString().equals("")) {header_one.setVisibility(View.GONE);}
        header_two.setText(headerTwo);
        cancelBtn.setText(button1);
        if (headerOne.equals(context.getString(R.string.logout_questionmark)) || button2.equals(context.getString(R.string.block))) {
            logout_btn.setTextColor(context.getColor(R.color.colorRed));
        }
        logout_btn.setText(button2);
        alertDialog.setView(add_menu_layout);

        logoutLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testDialog.dismiss();
                onOkClicked.onOkClicked(true);
            }
        });

        cancelLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testDialog.dismiss();
                onOkClicked.onOkClicked(false);
            }
        });

        alertDialog.setView(add_menu_layout);
        alertDialog.setCancelable(false);
        testDialog = alertDialog.create();
        testDialog.show();
        //setMargins(testDialog,  20, 0, 20, 0);
//        if (headerOne.equals(context.getString(R.string.logout_questionmark)) || button2.equals(context.getString(R.string.block))) {
            testDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        }
    }


    public void chooseLanguage(Boolean english) {
        System.out.println(">>>>>>>>>>>>>>LANGUA "+english);
        if (english){

            Configuration config = getBaseContext().getResources().getConfiguration();
            Locale locale = new Locale("en");
            Locale.setDefault(locale);
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());

        }else {
            Configuration config = getBaseContext().getResources().getConfiguration();
            Locale locale = new Locale("iw");
            Locale.setDefault(locale);
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());

        }
    }


    public void showNormalDialogVerticalButton(Activity context, String headerOne, String headerTwo, String button1, String button2,final OnOkClicked onOkClicked, Boolean setCancelable){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        LayoutInflater inflater = context.getLayoutInflater();
        final View add_menu_layout = inflater.inflate(R.layout.dialog_ui_vertical, null);
        TextView header_one = add_menu_layout.findViewById(R.id.header_one);
        View viewLayout = add_menu_layout.findViewById(R.id.viewLayout);
        TextView header_two = add_menu_layout.findViewById(R.id.header_two);
        TextView cancelBtn = add_menu_layout.findViewById(R.id.cancelBtn);
        TextView logout_btn = add_menu_layout.findViewById(R.id.logout_btn);
        if (TextUtils.isEmpty(headerOne)){
            header_one.setVisibility(View.GONE);
            viewLayout.setVisibility(View.GONE);
        }
        if (button1.isEmpty()){
            cancelBtn.setVisibility(View.GONE);
        }
        if (button2.isEmpty()){
            logout_btn.setVisibility(View.GONE);
        }
        if (button2.equals(context.getString(R.string.deactivate))) logout_btn.setTextColor(getColor(R.color.dark_red));
        header_one.setText(headerOne);
        header_two.setText(headerTwo);
        cancelBtn.setText(button1);
        logout_btn.setText(button2);
        alertDialog.setCancelable(setCancelable);
        alertDialog.setView(add_menu_layout);

        logout_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testDialog.dismiss();
                onOkClicked.onOkClicked(true);
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testDialog.dismiss();
                onOkClicked.onOkClicked(false);
            }
        });

        alertDialog.setView(add_menu_layout);
        alertDialog.setCancelable(false);
        testDialog = alertDialog.create();
        testDialog.show();
        testDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    public static boolean checkPermissionForCamera(Context context) {
        int result = ContextCompat.checkSelfPermission(context, CAMERA);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    public static void requestPermissionForCamera(AppCompatActivity context) {
        ActivityCompat.requestPermissions(context, new String[]{CAMERA}, PERMISSION_REQUEST_CODE_CAMERA_MODE);
    }

    private void getLocation(Boolean status) {
        checkGpsOn();
    }
    public void showPermissionRequestDialog(String header_ones,String header_twos){

        final android.app.AlertDialog.Builder alertDialoga = new android.app.AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View add_menu_layout = inflater.inflate(R.layout.item_text_alert, null);
        Button button_back=add_menu_layout.findViewById(R.id.button_back);
        Button button_allow=add_menu_layout.findViewById(R.id.button_allow);
        TextView headerOne=add_menu_layout.findViewById(R.id.headerOne);
        TextView headerTwo=add_menu_layout.findViewById(R.id.headerTwo);
        headerOne.setText(header_ones);
        headerTwo.setText(header_twos);
        alertDialoga.setView(add_menu_layout);
        alertDialoga.setCancelable(false);
        alertDialogPermission = alertDialoga.show();
        button_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogPermission.dismiss();
                finish();
            }
        });
        button_allow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogPermission.dismiss();
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivityForResult(intent,2000);
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull @NotNull String[] permissions, @NonNull @NotNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_LOCATION) {
            if (grantResults.length > 0) {
                if (EasyPermissions.hasPermissions(this, permissionsArrayLocation)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        //getLatitudeLongitude();
                        //  sendNotification(getResources().getString(R.string.sent_you_his_location));
                        getLocation(true);
                    }
                } else {
                    showPermissionRequestDialog(getString(R.string.network_alert_lcoation),getString(R.string.do_u_wish_to_enable));
                }
            }
        }
    }

    public File getCompressed(Context context, String path) throws IOException {
        ExifInterface oldExif = new ExifInterface(path);
        String exifOrientation = oldExif.getAttribute(ExifInterface.TAG_ORIENTATION);
        System.out.println(">>>>>>>>>>>>>>>>>>>>>ORITNEEE1 "+exifOrientation);

        if(context == null)
            throw new NullPointerException("Context must not be null.");
        File cacheDir = context.getExternalCacheDir();
        if(cacheDir == null)
//fall back
            cacheDir = context.getCacheDir();
        String rootDir = cacheDir.getAbsolutePath() + "/images";
        File root = new File(rootDir);
//Create ImageCompressor folder if it doesnt already exists.
        if(!root.exists())
            root.mkdirs();
//decode and resize the original bitmap from @param path.
        Bitmap bitmap = decodeImageFromFiles(path, /* your desired width*/400, /*your desired height*/ 400);
        File compressed = new File(root, System.currentTimeMillis() + ".jpg" /*Your desired format*/);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
        FileOutputStream fileOutputStream = new FileOutputStream(compressed);
        fileOutputStream.write(byteArrayOutputStream.toByteArray());
        fileOutputStream.flush();
        fileOutputStream.close();

        if (exifOrientation != null) {
            ExifInterface newExif = new ExifInterface(compressed.getPath());
            newExif.setAttribute(ExifInterface.TAG_ORIENTATION, exifOrientation);
            newExif.saveAttributes();
        }

        return compressed;
    }

    public File compressBitmap(Context context, Bitmap bitmaps) throws IOException {
        if(context == null)
            throw new NullPointerException("Context must not be null.");
        File cacheDir = context.getExternalCacheDir();
        if(cacheDir == null)
//fall back
            cacheDir = context.getCacheDir();
        String rootDir = cacheDir.getAbsolutePath() + "/images";
        File root = new File(rootDir);
//Create ImageCompressor folder if it doesnt already exists.
        if(!root.exists())
            root.mkdirs();
//decode and resize the original bitmap from @param path.
        Bitmap bitmap =bitmaps;
        File compressed = new File(root, System.currentTimeMillis() + ".jpg" /*Your desired format*/);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
        FileOutputStream fileOutputStream = new FileOutputStream(compressed);
        fileOutputStream.write(byteArrayOutputStream.toByteArray());
        fileOutputStream.flush();
        fileOutputStream.close();
        return compressed;
    }


    public static Bitmap decodeImageFromFiles(String path, int width, int height) {
        BitmapFactory.Options scaleOptions = new BitmapFactory.Options();
        scaleOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, scaleOptions);
        int scale = 1;
        while (scaleOptions.outWidth / scale / 2 >= width
                && scaleOptions.outHeight / scale / 2 >= height) {
            scale *= 2;
        }
// decode with the sample size
        BitmapFactory.Options outOptions = new BitmapFactory.Options();
        outOptions.inSampleSize = scale;
        return BitmapFactory.decodeFile(path, outOptions);
    }

    public interface OnOkClicked{
        void onOkClicked(Boolean isPositiveBtnClick);
    }

    public void setSpannableString(Context context, String text, TextView textView, int startIndex, int endIndex, int color){
        SpannableString content = new SpannableString(text);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View view) {

            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(color);
                //draw underline base on true/false
                ds.setUnderlineText(true);
            }
        };
        content.setSpan(clickableSpan, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setHighlightColor(color);
        textView.setText(content);

    }

    public void setBoldViewAbleSpannableString(Context context, String text, TextView textView,
                                           int startIndex, int endIndex, int color){
        SpannableString content = new SpannableString(text);
        Typeface myTypeface = ResourcesCompat.getFont(context, R.font.avenir_next_bold);

       // content.setSpan(myTypeface, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        content.setSpan(new CustomTypefaceSpan("", myTypeface),
                0,
                endIndex,
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE
        );
        content.setSpan(
                new ForegroundColorSpan(ContextCompat.getColor(context, R.color.basecolor)),
                0,
                endIndex,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        );
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(content);

    }

    public void openPdf(String url, Context mContext, String header) {
        Intent intent = new Intent(mContext, OpenWebView.class);
        intent.putExtra("headers", header);
        intent.putExtra("pdfurl", url);
        startActivity(intent);
    }

    public void openUrl(String url, Context context, String header){
        Intent intent = new Intent(context, OpenWebView.class);
        intent.putExtra("headers", header);
        intent.putExtra("url", url);
        startActivity(intent);
    }

    public void openEmail(String subject, String emailId, Context context){
        Intent intent = new Intent(Intent.ACTION_SEND);
        String[] recipients = {emailId};//Add multiple recipients here
        intent.putExtra(Intent.EXTRA_EMAIL, recipients);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject); //Add Mail Subject
        intent.setType("text/html");
        intent.setPackage("com.google.android.gm");//Added Gmail Package to forcefully open Gmail App
        startActivity(Intent.createChooser(intent, "Send mail"));
    }

    public static String getDateDiff(Date date1, Date date2) {
        long diff = date1.getTime() - date2.getTime();
        long seconds = diff / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;
        if (days>0) {
            return days+" days ago";
        } else if (hours>0) {
            return hours+" hours ago";
        }else if (minutes>0) {
            return minutes+" mins ago";
        } else {
            return seconds+" secs ago";
        }
    }

    public static boolean isProductTodayPosted(Date date1, Date date2) {
        long diff = date1.getTime() - date2.getTime();
        long seconds = diff / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;
        if (days>3) {
            return false;
        }
        else return true;
    }

    public static long getNoOfDays(Date date1, Date date2) {
        long diff = date1.getTime() - date2.getTime();
        long seconds = diff / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        return hours / 24;
    }

    public static Date convertStringToDate(String date) {
        SimpleDateFormat format = new SimpleDateFormat(CREATED_DATE, Locale.ENGLISH);
        format.setTimeZone(TimeZone.getTimeZone("IST+05:30"));
        Date newDate = null;
        try {
            newDate = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate;
    }



}
