package com.bingalollc.com.base.bottom_action_sheet

/**
 * Created by khoiron on 02/06/18.
 */

interface OnClickListener {
    fun onclick(string: String, position: Int)
}