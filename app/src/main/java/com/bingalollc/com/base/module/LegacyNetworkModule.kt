//package com.propagrub.com.di.module
//
//import android.content.Context
//import com.bingalollc.com.preference.PreferenceManager
//import dagger.Module
//import dagger.Provides
//import dagger.hilt.InstallIn
//import dagger.hilt.android.qualifiers.ApplicationContext
//import dagger.hilt.components.SingletonComponent
//import javax.inject.Singleton
//
//@Module
//@InstallIn(SingletonComponent::class)
//object LegacyNetworkModule {
//
//    @Provides
//    @Singleton
//    fun providePreferenceHelper(@ApplicationContext applicationContext: Context): PreferenceManager {
//        return PreferenceManager.getInstance(applicationContext)
//    }
//
//  /*  @Provides
//    @Singleton
//    fun provideNetworkUtility(preferences: PrefHelper, apiService: ApiService): NetworkUtility {
//        return NetworkUtility(preferences, apiService)
//    }*/
//}
