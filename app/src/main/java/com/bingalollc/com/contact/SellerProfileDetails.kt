package com.bingalollc.com.contact

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.databinding.ActivitySellerProfileDetailsBinding
import com.bingalollc.com.homeactivity.homefragment.adapter.ProductAdapter
import com.bingalollc.com.homeactivity.model.ProductModel
import com.bingalollc.com.homeactivity.profilefragment.model.ProductCountOfUser
import com.bingalollc.com.network.RestClient
import com.bingalollc.com.preference.Common
import com.bingalollc.com.preference.PreferenceManager
import com.bingalollc.com.product_details.ProductDetails
import com.bingalollc.com.users.Users
import com.bingalollc.com.utils.ViewBindingActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import jp.wasabeef.glide.transformations.BlurTransformation
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SellerProfileDetails : ViewBindingActivity<ActivitySellerProfileDetailsBinding>(), ProductAdapter.OnItemChoosed {
    private var users : Users.Data? = null
    private var productModeldata: ArrayList<ProductModel.Datum>? = null
    private var productAdapter: ProductAdapter? = null
    private var isSellingDisplayed = true
    private lateinit var preferenceManager: PreferenceManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        productModeldata = ArrayList()
        if (intent.getStringExtra("user_id").isNullOrEmpty().not()) {
            getProfileDetails()
            updateVerifiedBy()
            getCountFromServer()
            initRecyclerView()
            getproductFromServer("0");
        }
       binding.sellingLayout.setOnClickListener {
           fetchSellingProduct()
       }
        binding.soldLayout.setOnClickListener {
            fetchSoldProduct()
        }


    }

    private fun getproductFromServer(status: String) {
        RestClient.getApiInterface().getProductByUserId(intent.getStringExtra("user_id"), status).enqueue(
            object : Callback<ProductModel> {
                override fun onFailure(call: Call<ProductModel>, t: Throwable) {
                    hideLoading()
                    binding.noProductFound.visibility = View.VISIBLE
                    binding.noProductFound.text =
                        getString(R.string.not_able_to_load)
                }

                override fun onResponse(
                    call: Call<ProductModel>,
                    response: Response<ProductModel>
                ) {
                    hideLoading()
                    productModeldata = ArrayList()
                    if (response.body()?.status == 200 && response.body()?.data != null && response.body()?.data?.size!! > 0) {
                        binding.noProductFound.visibility = View.GONE
                        response?.body()?.data?.forEachIndexed { index, datum ->
                            if (Common.blockedUserId.contains(datum.uploadedByUserId.id).not()) {
                                productModeldata?.add(datum)
                            }
                        }
                    } else {
                        binding.noProductFound.visibility = View.VISIBLE
                        binding.noProductFound.text =
                            getString(R.string.no_product_found)
                    }
                    productAdapter?.refreshData(productModeldata)
                }
            })
    }
    private fun initRecyclerView() {
        productAdapter= ProductAdapter(productModeldata, this)
        val linearLayoutManager= GridLayoutManager(this, 2)
        binding.rvProducts?.adapter=productAdapter
        binding.rvProducts?.layoutManager=linearLayoutManager
    }


    private fun updateVerifiedBy() {
        if (users?.verification !=null) {
            users?.verification?.forEachIndexed { index, verification ->
                if (verification.verifiedBy.equals("phone", ignoreCase = true)) {
                    binding.psPhoneButton.visibility = View.VISIBLE
                } else {
                    binding.psPhoneButton.visibility = View.GONE
                }
                if (verification.verifiedBy.equals("email", ignoreCase = true)) {
                    binding.psEmailButton.visibility = View.VISIBLE
                } else {
                    binding.psEmailButton.visibility = View.GONE
                }
            }
        }
        if (users?.googleId.isNullOrEmpty().not()) binding.psGoogleButton.visibility = View.VISIBLE else  binding.psGoogleButton.visibility = View.GONE
        if (users?.facebookId.isNullOrEmpty().not()) binding.psFacebookButton.visibility = View.VISIBLE else  binding.psFacebookButton.visibility = View.GONE
        if (users?.twitterId.isNullOrEmpty().not()) binding.psTwitterButton.visibility = View.VISIBLE else  binding.psTwitterButton.visibility = View.GONE
        
        if (binding.psEmailButton.visibility == View.GONE && binding.psGoogleButton.visibility == View.GONE &&
            binding.psTwitterButton.visibility == View.GONE && binding.psFacebookButton.visibility == View.GONE &&
            binding.psPhoneButton.visibility == View.GONE) {
            binding.notVerfiedText.visibility = View.VISIBLE
        }
    }
    private fun getCountFromServer() {
        RestClient.getApiInterface().getProductCountByUserId(intent.getStringExtra("user_id")).enqueue(
            object : Callback<ProductCountOfUser> {
                override fun onFailure(call: Call<ProductCountOfUser>, t: Throwable) {

                }

                override fun onResponse(
                    call: Call<ProductCountOfUser>,
                    response: Response<ProductCountOfUser>
                ) {

                    if (response.body()?.status == 200) {
                        binding.sellingCounter.setText(response.body()?.data?.get(0)?.unsoldProduct.toString())
                        binding.soldCounter.setText(response.body()?.data?.get(0)?.soldProduct.toString())
                    }

                }
            })
    }

    private fun getProfileDetails() {
        showLoading(this)
        RestClient.getApiInterface().getUserDetails(intent.getStringExtra("user_id")).enqueue(object : Callback<com.bingalollc.com.users.Users>{
            override fun onResponse(
                call: Call<com.bingalollc.com.users.Users>,
                response: Response<com.bingalollc.com.users.Users>
            ) {
                hideLoading()
                if (response.body()?.status == 200){
                    users = response.body()?.data
                    setData()
                } else {
                    showToast(response.body()?.message?:"", this@SellerProfileDetails)
                }
            }

            override fun onFailure(call: Call<Users>, t: Throwable) {
                hideLoading()
            }

        })
    }

    private fun setData() {
        Glide.with(this)
            .load(users?.image)
            .placeholder(R.drawable.default_user)
            .error(R.drawable.default_user)
            .into(binding.userImage)
        Glide.with(this).load(users?.image)
            .placeholder(R.drawable.default_user)
            .error(R.drawable.default_user)
            .transform(BlurTransformation(40)).into(binding.bgUserImage)

        binding.userName.setText(users?.fullName)

        if (users?.rating != null && (users?.rating?:"0").equals(
                "0"
            ) )
            binding.starRatings.setStarRating(
                (users?.rating ?: "0").toString().toFloat(),
                this
            )
    }

    private fun fetchSellingProduct() {
        if (!isSellingDisplayed) {
            showLoading(this)
            isSellingDisplayed = true
            binding.sellingBottomView.visibility = View.VISIBLE
            binding.soldBottomView.visibility = View.GONE
            getproductFromServer("0");
        }
    }
    private fun fetchSoldProduct() {
        if (isSellingDisplayed) {
            showLoading(this)
            isSellingDisplayed = false
            binding.sellingBottomView.visibility = View.GONE
            binding.soldBottomView.visibility = View.VISIBLE
            getproductFromServer("1");
        }
    }

    override fun onItemClicked(postion: Int) {
        //Common.selectedProductDetails = prductData;
        Common.selectedProductDetails = productModeldata?.get(postion)
        startActivity(Intent(this, ProductDetails::class.java))

    }

    override fun provideBinding(inflater: LayoutInflater): ActivitySellerProfileDetailsBinding {
        return ActivitySellerProfileDetailsBinding.inflate(inflater)
    }

   /* override fun onClick(p0: View?) {
        when(p0?.id){
            R.id.sellingLayout->{
                fetchSellingProduct()
            }
            R.id.sellingText -> {
                fetchSellingProduct()
            }

            R.id.sellingCounter-> {
                fetchSellingProduct()
            }

            R.id.soldText -> {
                fetchSoldProduct()
            }
            R.id.soldCounter->{
                fetchSoldProduct()
            }
            R.id.soldLayout->{
                fetchSoldProduct()
            }
        }
    }*/
}
