package com.bingalollc.com.contact.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bingalollc.com.R

class AdapterContact(onClickListener: OnClickListener) :
    RecyclerView.Adapter<AdapterContact.ViewHolder>() {
    private lateinit var context: Context
    private var nameList: ArrayList<String> = ArrayList()
    private var numberList: ArrayList<String> = ArrayList()
    private var onClickListener: OnClickListener = onClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view: View =
            LayoutInflater.from(context).inflate(R.layout.layout_contact, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name.text = nameList.get(position)
        holder.contactNumber.text = numberList.get(position)

        holder.invite.setOnClickListener {
            onClickListener.onClick(numberList.get(position))
        }
    }

    override fun getItemCount(): Int {
        return nameList?.size!!
    }

    fun setUpdateData(nameList: ArrayList<String>, numberList: ArrayList<String>) {
        this.nameList = nameList
        this.numberList = numberList
        notifyDataSetChanged()

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView = itemView.findViewById(R.id.name)
        var contactNumber: TextView = itemView.findViewById(R.id.contactNumber)
        var invite: TextView = itemView.findViewById(R.id.invite)

    }

    interface OnClickListener {
        fun onClick(number: String)
    }

}
