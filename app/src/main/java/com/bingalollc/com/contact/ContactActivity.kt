package com.bingalollc.com.contact

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.ContactsContract
import android.text.TextUtils
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.widget.addTextChangedListener
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.contact.adapter.AdapterContact
import com.bingalollc.com.preference.PreferenceManager
import com.bingalollc.com.utils.afterTextChangedDebounce
import kotlinx.coroutines.*
import java.lang.Runnable
import kotlin.concurrent.thread


class ContactActivity : BaseActivity(), AdapterContact.OnClickListener {
    private var topLayer: LinearLayout? = null
    private var header: TextView? = null
    private var searchText: AppCompatEditText? = null
    private var back_icon: ImageView? = null
    private var rvContact: RecyclerView? = null
    private val PERMISSIONS_REQUEST_READ_CONTACTS = 100
    private lateinit var nameArray: ArrayList<String>
    private lateinit var numberArray: ArrayList<String>
    private lateinit var nameArraySearched: ArrayList<String>
    private lateinit var numberArraySearched: ArrayList<String>
    private lateinit var adapterContact: AdapterContact

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact)
        nameArray = ArrayList()
        nameArraySearched = ArrayList()
        numberArray = ArrayList()
        numberArraySearched = ArrayList()
        initViews()
        initRecyclerView()
        importSwipeBack()
        Handler(Looper.myLooper()!!).postDelayed(Runnable {
            showContacts()
        },100)

        searchText?.afterTextChangedDebounce {
            if (TextUtils.isEmpty(searchText?.text.toString()).not()) {
                nameArraySearched = ArrayList()
                numberArraySearched = ArrayList()
                thread {
                    nameArray?.forEachIndexed { index, nameString ->
                        if (nameString.contains(searchText?.text.toString(), ignoreCase = true)) {
                            nameArraySearched.add(nameString)
                            numberArraySearched.add(numberArray.get(index))
                        }
                    }
                    updateData(nameArraySearched, numberArraySearched)
                }
            } else {
                nameArraySearched = ArrayList()
                numberArraySearched = ArrayList()
                updateData(nameArray, numberArray)
            }
        }
    }

    private fun initRecyclerView() {
        rvContact?.layoutManager = LinearLayoutManager(this)
        adapterContact = AdapterContact(this)
        rvContact?.adapter = adapterContact

    }

    private fun showContacts() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                arrayOf(Manifest.permission.READ_CONTACTS),
                PERMISSIONS_REQUEST_READ_CONTACTS
            )
        } else {
            showLoading(this)
            val s = thread {
                getContactList()
            }

        }
    }

    @SuppressLint("Range")
    private fun getContactList() {
        val cr = contentResolver
        val cur: Cursor? = cr.query(
            ContactsContract.Contacts.CONTENT_URI,
            null, null, null, null
        )
        if ((if (cur != null) cur.getCount() else 0) > 0) {
            while (cur != null && cur.moveToNext()) {
                val id: String = cur.getString(
                    cur.getColumnIndex(ContactsContract.Contacts._ID)
                )
                var name = cur.getString(
                    cur.getColumnIndex(
                        ContactsContract.Contacts.DISPLAY_NAME
                    )
                )
                if (TextUtils.isEmpty(name)) name = ""
                if (cur.getInt(
                        cur.getColumnIndex(
                            ContactsContract.Contacts.HAS_PHONE_NUMBER
                        )
                    ) > 0
                ) {
                    val pCur: Cursor? = cr.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                        arrayOf(id),
                        null
                    )
                    while (pCur!!.moveToNext()) {
                        val phoneNo: String = pCur?.getColumnIndex(
                            ContactsContract.CommonDataKinds.Phone.NUMBER
                        )?.let {
                            pCur.getString(
                                it
                            )
                        }!!
                        nameArray.add(name)
                        numberArray.add(phoneNo)
                    }
                    pCur.close()
                }
            }
        }
        hideLoading()
        updateData(nameArray, numberArray)


        if (cur != null) {
            cur.close()
        }
    }

    private fun updateData(name: ArrayList<String>, number: ArrayList<String>) {
        runOnUiThread {
            adapterContact.setUpdateData(name, number)
        }
        Thread.sleep(100)
    }


    private fun initViews() {
        topLayer = findViewById(R.id.topLayer) as LinearLayout?
        back_icon = findViewById(R.id.back_icon) as ImageView?
        header = findViewById(R.id.header) as TextView?
        rvContact = findViewById(R.id.rvContact) as RecyclerView?
        searchText = findViewById(R.id.searchText) as AppCompatEditText?


        topLayer?.setBackgroundColor(resources.getColor(R.color.basecolor))
        header?.text = getString(R.string.contacts)
        back_icon?.setOnClickListener({ onBackPressed() })

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
//                showContacts();
                showLoading(this)
                thread {
                    getContactList()
                }
            }
        }

    }

    override fun onClick(number: String) {
        sendSms(number)
    }

    private fun sendSms(number: String) {
        val uri: Uri = Uri.parse("smsto:"+number)
        val it = Intent(Intent.ACTION_SENDTO, uri)
        it.putExtra("sms_body",
            "Hi please install Bingalo app https://play.google.com/store/apps/details?id=com.bingalollc.com")
        startActivity(it)
    }

}