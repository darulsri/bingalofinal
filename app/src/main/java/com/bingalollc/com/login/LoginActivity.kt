package com.bingalollc.com.login

import android.R.id
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.widget.*
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.core.content.ContextCompat
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.forgetpassword.ForgetPassword
import com.bingalollc.com.google_analytics.AnalyticsEvent
import com.bingalollc.com.google_analytics.GoogleAnalytics
import com.bingalollc.com.homeactivity.HomeActivity
import com.bingalollc.com.model.SocialLoginModel
import com.bingalollc.com.network.RestClient
import com.bingalollc.com.preference.Common
import com.bingalollc.com.signup.SignupActivity
import com.bingalollc.com.users.Users
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.ktx.Firebase
import com.twitter.sdk.android.core.*
import com.twitter.sdk.android.core.identity.TwitterAuthClient
import com.twitter.sdk.android.core.models.User
//import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.URL
import java.util.*


class LoginActivity : BaseActivity() {
    var tv_signup: LinearLayout? = null
    var et_email: TextInputEditText? = null
    var et_password: TextInputEditText? = null
    var tv_forgot: TextView? = null
    var bt_login: AppCompatButton? = null
    private var googleLogin: ImageView? = null
    private var fbLogin: ImageView? = null
    private var twitterLogin: ImageView? = null
    lateinit var mGoogleSignInClient: GoogleSignInClient
    val Req_Code: Int = 123
    var firebaseAuth = FirebaseAuth.getInstance()
    lateinit var callbackManager: CallbackManager
    private var client: TwitterAuthClient? = null
    private var cbRememberMe : AppCompatCheckBox?=null
    private var isPasswordVisible = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        tv_signup = findViewById(R.id.tv_signup) as LinearLayout?
        tv_forgot = findViewById(R.id.tv_forgot) as TextView?
        bt_login = findViewById(R.id.bt_login) as AppCompatButton?
        et_email = findViewById(R.id.et_email) as TextInputEditText?
        et_password = findViewById(R.id.et_password) as TextInputEditText?

        initSocialLogin()
        initViews()
        initClicks()
        if (preferenceManager?.isEnglish == 2) {
//            iv_header.setImageDrawable(resources.getDrawable(R.drawable.launch_logo_herbew))
        }
        tv_signup?.setOnClickListener {
            startActivity(Intent(this, SignupActivity::class.java))
        }
        tv_forgot?.setOnClickListener {
            startActivity(Intent(this, ForgetPassword::class.java))
        }

        bt_login?.setOnClickListener {
            if (validateInputs()) {
                showLoading(this)
                RestClient.getApiInterface()
                    .login(et_email?.text.toString(), et_password?.text.toString())
                    .enqueue(object : Callback<Users> {
                        override fun onFailure(call: Call<Users>, t: Throwable) {
                            hideLoading()
                        }

                        override fun onResponse(call: Call<Users>, response: Response<Users>) {
                            hideLoading()
                            if (response.body()?.status == 200) {
                                if (response?.body()?.data?.is_activated != 2) {
                                    preferenceManager?.userDetails = response?.body()?.data

                                    GoogleAnalytics.sentDataToGoogleAnalytics(response.body()?.data?.id.toString(),
                                    AnalyticsEvent.EventAction.LOGIN_TYPE.name, AnalyticsEvent.EventType.CLICK.name)

                                    startActivity(
                                        Intent(this@LoginActivity, HomeActivity::class.java)
                                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                    )
                                    finish()
                                } else {
                                    showDeactivateMsg()
                                }

                            } else {
                                showToast(response.body()?.message, this@LoginActivity)
                            }
                        }
                    })
            }
        }
    }

    private fun showDeactivateMsg() {
        showNormalDialogVerticalButton(this,
            getString(R.string.account_deactivated),
            getString(
                R.string.account_deactivated_desc
            ),
            getString(R.string.ok),
            "",
            BaseActivity.OnOkClicked {
                if (it) {

                }
            }, true)
    }

    private fun initClicks() {
        cbRememberMe?.setOnCheckedChangeListener(({ compoundButton: CompoundButton, b: Boolean ->
            if(b){
                preferenceManager?.setUserLoggedin(true)
            }else{
                preferenceManager?.setUserLoggedin(false)
            }

        }))
        cbRememberMe?.isChecked = true
        googleLogin?.setOnClickListener {
            googleLogin()
        }

        fbLogin?.setOnClickListener {
            LoginManager.getInstance()
                .logInWithReadPermissions(this, Arrays.asList("public_profile"))
            facebookLogin()

        }

        twitterLogin?.setOnClickListener {
            twitterLogin()
        }


//        showHidePassword.setOnClickListener {
//            if (isPasswordVisible) {
//                showHidePassword.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.show_password))
//                isPasswordVisible = false
//                et_password?.transformationMethod = PasswordTransformationMethod()
//            } else {
//                showHidePassword.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.hide_password))
//                isPasswordVisible = true
//                et_password?.transformationMethod = null
//            }
//            et_password?.setSelection(et_password?.text?.length?:0)
//
//        }


    }


    private fun twitterLogin() {
//        if (getTwitterSession() == null) {
        //if user is not authenticated start authenticating
        client?.cancelAuthorize()
        client!!.authorize(this, object : com.twitter.sdk.android.core.Callback<TwitterSession?>() {
            override fun success(result: Result<TwitterSession?>) {

                // Do something with result, which provides a TwitterSession for making API calls
                val twitterSession: TwitterSession? = result.data
                val twitterApiClient = TwitterCore.getInstance().apiClient
                val call: Call<User> =
                    twitterApiClient.accountService.verifyCredentials(true, false, true)
                call.enqueue(object : com.twitter.sdk.android.core.Callback<User>() {
                    override fun success(result: Result<User>?) {
                        val user = result!!.data
                        socialLogin(
                            user.email,
                            user.profileImageUrl,
                            user.name,
                            user.id.toString(),
                            "2",
                            "android"
                        )

                    }

                    override fun failure(exception: TwitterException?) {
                    }
                })
            }

            override fun failure(e: TwitterException?) {
                showToast("Failed to authenticate. Please try again.", this@LoginActivity)
            }
        })
        /* } else {
             //if user is already authenticated direct call fetch twitter email api
             Toast.makeText(this, "User already authenticated", Toast.LENGTH_SHORT).show()
         }*/
    }

    private fun facebookLogin() {
        var lastName = ""
        var email = ""
        LoginManager.getInstance().registerCallback(callbackManager, object :
            FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                val request = GraphRequest.newMeRequest(
                    loginResult!!.accessToken) { `object`, response ->
                    try {
                        Log.i("Response", response.toString())
                        val socail_id = response!!.jsonObject!!.getString("id")
                        val profilePicture =
                            URL("https://graph.facebook.com/$socail_id/picture?width=500&height=500")
                        if (response!!.jsonObject!!.has("email")) {
                            email = response.jsonObject!!.getString("email")
                        }
                        val firstName = response.jsonObject!!.getString("first_name")
                        if (response!!.jsonObject!!.getString("last_name") != null) {
                            lastName = response.jsonObject!!.getString("last_name")
                        } else {
                            lastName = ""
                        }
                        socialLogin(
                            email,
                            profilePicture.toString(),
                            firstName + " " + lastName,
                            socail_id,
                            "0",
                            "android"
                        )
                    } catch (e: java.lang.Exception) {
                        showToast(e.message, this@LoginActivity)
                    }
                }
                val parameters = Bundle()
                parameters.putString("fields", "id,email,first_name,last_name,gender")
                request.parameters = parameters
                request.executeAsync()
            }


            override fun onCancel() {
                showToast("Cancelled", this@LoginActivity)
            }

            override fun onError(exception: FacebookException) {
                showToast(exception.message, this@LoginActivity)
            }
        })
    }

    private fun googleLogin() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        firebaseAuth = FirebaseAuth.getInstance()


        val signInIntent: Intent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, Req_Code)


    }

    private fun initViews() {
        googleLogin = findViewById(R.id.googleLogin) as ImageView?
        twitterLogin = findViewById(R.id.twitterLogin) as ImageView?
        fbLogin = findViewById(R.id.fbLogin) as ImageView?
        cbRememberMe = findViewById(R.id.cbRememberMe) as AppCompatCheckBox?
        et_email?.setText(preferenceManager?.userDetails?.email)
    }

    private fun initSocialLogin() {
        callbackManager = CallbackManager.Factory.create() //facebook
        val config = TwitterConfig.Builder(this)
            .logger(DefaultLogger(Log.DEBUG)) //enable logging when app is in debug mode
            .twitterAuthConfig(
                TwitterAuthConfig(
                    resources.getString(R.string.twitter_api_key),
                    resources.getString(R.string.twitter_api_secret_key)
                )
            ) //pass the created app Consumer KEY and Secret also called API Key and Secret
            .debug(true) //enable debug mode
            .build()
        Twitter.initialize(config)
        client = TwitterAuthClient()

    }

    private fun validateInputs(): Boolean {
        if (TextUtils.isEmpty(et_email?.text.toString())) {
            et_email?.error = getString(R.string.email_required)
            et_email?.requestFocus()
            return false
        } else if (!et_email?.text.toString().contains("@")) {
            et_email?.error = getString(R.string.invalid_email)
            et_email?.requestFocus()
            return false
        } else if (TextUtils.isEmpty(et_password?.text.toString())) {
            et_password?.error = getString(R.string.password_required)
            et_password?.requestFocus()
            return false
        } else {
            return true
        }
    }

    private fun handleGoogleLoginResponse(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account: GoogleSignInAccount? = completedTask.getResult(ApiException::class.java)
            if (account != null) {
                val credential = GoogleAuthProvider.getCredential(account.idToken, null)
                socialLogin(
                    account.email,
                    account.photoUrl.toString(),
                    account.givenName,
                    account.id,
                    "1",
                    "android"
                )
            }
        } catch (e: ApiException) {
            e.printStackTrace()
        }
    }

    private fun socialLogin(
        email: String?,
        profilePic: String,
        fullName: String?,
        socialId: String?,
        loginType: String,
        deviceType: String
    ) {
        System.out.println(">>>>>>>>>>>>> "+Common.currentLat)
        showLoading(this)
        var userData = Users().Data()
        RestClient.getApiInterface().socialLogin(
            fullName,
            email,
            Common.currentLat,
            Common.currentLng,
            socialId,
            loginType,
            profilePic,
            "Android"
        )
            .enqueue(object : retrofit2.Callback<SocialLoginModel> {
                override fun onResponse(
                    call: Call<SocialLoginModel>,
                    response: Response<SocialLoginModel>
                ) {
                    hideLoading()
                    if (response.isSuccessful) {
                        if (response.body()?.getStatus() == 200) {
                            response?.body()?.data?.also {
                                userData.email = it.email
                                userData.id = it.id
                                userData.fullName = it.fullName
                                userData.image = it.image
                                userData.googleId = it.googleId
                                userData.language = it.language
                                userData.lat = it.lat
                                userData.lng = it.lng
                                userData.phone = it.phone
                                userData.token = it.token
                                userData.twitterId = it.pushToken
                                userData.facebookId = it.facebookId
                                userData.googleId = it.googleId

                            }
                            when (loginType) {
                                "0" -> {
                                    GoogleAnalytics.sentDataToGoogleAnalytics(loginType,
                                        AnalyticsEvent.LogInType.FACEBOOK.name, AnalyticsEvent.EventType.FACEBOOK_LOGIN.name)
                                }
                                "1" -> {
                                    GoogleAnalytics.sentDataToGoogleAnalytics(loginType,
                                        AnalyticsEvent.LogInType.GOOGLE.name, AnalyticsEvent.EventType.GOOGLE_LOGIN.name)
                                }
                                else -> {
                                    GoogleAnalytics.sentDataToGoogleAnalytics(loginType,
                                        AnalyticsEvent.LogInType.TWITTER.name, AnalyticsEvent.EventType.TWITTER_LOGIN.name)
                                }
                            }

                            if (userData?.is_activated != 2){
                                preferenceManager?.userDetails = userData
                                preferenceManager?.setUserLoggedin(true)
                                startActivity(Intent(this@LoginActivity, HomeActivity::class.java)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
                                finish()
                            } else {
                                showDeactivateMsg()
                            }

                        } else {
                            showToast(response.body()?.message, this@LoginActivity)
                        }
                    }
                }

                override fun onFailure(call: Call<SocialLoginModel>, t: Throwable) {
                    hideLoading()
                }

            })

    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Req_Code) { //Google Signin
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            if (task.isSuccessful) {
                handleGoogleLoginResponse(task)
            }
        } else if (requestCode == 140) {
            client?.onActivityResult(requestCode, resultCode, data)
        }

    }

}