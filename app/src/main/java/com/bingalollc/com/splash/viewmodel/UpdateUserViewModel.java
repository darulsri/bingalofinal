package com.bingalollc.com.splash.viewmodel;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModel;

import com.bingalollc.com.network.RestClient;
import com.bingalollc.com.preference.Common;
import com.bingalollc.com.preference.PreferenceManager;
import com.bingalollc.com.users.Users;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateUserViewModel extends ViewModel {
    FirebaseFirestore firebaseFirestore;
   public void updateUser(String user_id, PreferenceManager preferenceManager){
        RestClient.getApiInterface().updateUsersDetails(user_id).enqueue(new Callback<Users>() {
            @Override
            public void onResponse(Call<Users> call, Response<Users> response) {
                if (response.body().getStatus()==200){
                    preferenceManager.setUserDetails(response.body().getData());
                }
            }

            @Override
            public void onFailure(Call<Users> call, Throwable t) {

            }
        });
    }

    public void getBlockedList(String userId){
        firebaseFirestore = FirebaseFirestore.getInstance();
        CollectionReference chatroomsCollection = firebaseFirestore
                .collection("BlockedUsers");
        Query query = chatroomsCollection.whereEqualTo("blockedBy",userId);
        query.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException error) {
                Common.blockedUserId = new ArrayList<>();
                for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                    ArrayList<String> arrayList = (ArrayList<String>) documentSnapshot.getData().get("userIDs");
                    boolean otherUserId = arrayList.remove(userId);
                    if (otherUserId){
                        Common.blockedUserId.add(arrayList.get(0));
                    }

                }
            }
        });
    }
}
