package com.bingalollc.com.splash

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.LocationManager
import android.os.*
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProvider
import com.bingalollc.com.AppController
import com.bingalollc.com.BuildConfig
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.chat.ChatMessages
import com.bingalollc.com.google_analytics.AnalyticsEvent
import com.bingalollc.com.google_analytics.GoogleAnalytics
import com.bingalollc.com.homeactivity.HomeActivity
import com.bingalollc.com.homeactivity.location_view_model.LocationViewModel
import com.bingalollc.com.maintainance.MaintainanceActivity
import com.bingalollc.com.maintainance.model.MaintainanceResponse
import com.bingalollc.com.network.RestClient
import com.bingalollc.com.permissionpage.PermissionPage
import com.bingalollc.com.preference.Common
import com.bingalollc.com.preference.PreferenceManager
import com.bingalollc.com.splash.viewmodel.UpdateUserViewModel
import com.bingalollc.com.users.Users
import com.bingalollc.com.utils.AppLocationService
import com.facebook.FacebookSdk
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.messaging.FirebaseMessaging
//import kotlinx.android.synthetic.main.activity_splash.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


class SplashActivity : BaseActivity() {
    var updateUserViewModel: UpdateUserViewModel?=null
    private var locationViewModel: LocationViewModel? =null
    private lateinit var database: DatabaseReference
    val mDb: FirebaseFirestore? = null
    var isLocationEnabled = false
    var isNavigationCalled = false

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.SplashTheme);
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_splash)
        FacebookSdk.sdkInitialize(this)
        printKeyHash()
        if(preferenceManager?.isEnglish == 1) {
//            centerLogo.setImageDrawable(resources.getDrawable(R.drawable.launch_logo))
        }else{
//            centerLogo.setImageDrawable(resources.getDrawable(R.drawable.launch_logo_herbew))
        }

        GoogleAnalytics.sentDataToGoogleAnalytics("1234",
            AnalyticsEvent.EventAction.SPLASH_SCREEN.name, AnalyticsEvent.EventType.SPLASH.name)

//        showLoading(this)

        FirebaseMessaging.getInstance().subscribeToTopic("News")
        database = FirebaseDatabase.getInstance().getReference();
        updateUserViewModel = ViewModelProvider(this@SplashActivity).get(UpdateUserViewModel::class.java)
        locationViewModel = ViewModelProvider(this@SplashActivity).get(LocationViewModel::class.java)

        if (preferenceManager?.isUserLoggedIn == true) {
            updateUserViewModel?.updateUser(preferenceManager?.userDetails?.id,preferenceManager)
            updateUserViewModel?.getBlockedList(preferenceManager?.userDetails?.id)
            updateOnlineStatus()
        }


        if (preferenceManager?.isEnglish == 2) {
//            centerLogo.setImageDrawable(resources.getDrawable(R.drawable.launch_logo_herbew))
        }
        // showToast("SplashScreenLaunched", this)
        if (preferenceManager.currentVersion == 0) {
            preferenceManager.currentVersion = BuildConfig.VERSION_CODE
        } else if (preferenceManager.currentVersion < BuildConfig.VERSION_CODE) {
            preferenceManager.chooseLanguageAtStartUpDisplayed = false
        }
        if (intent.getBooleanExtra("notification", false) && intent.getStringExtra("user_id").toString().isNullOrEmpty().not() && preferenceManager.isUserLoggedIn) {
            getProfileDetails(intent.getStringExtra("user_id").toString()?:"")

        }else if (intent.getBooleanExtra("navigate_to_notification_screen", false) && preferenceManager.isUserLoggedIn){
            val intent = Intent(this, HomeActivity::class.java)
            intent.putExtra("navigate_to_notification_screen", true)
            startActivity(intent)
            finish()
        }else if (intent.getBooleanExtra("admin_notification", false) && preferenceManager.isUserLoggedIn){
            val intent = Intent(this, HomeActivity::class.java)
            intent.putExtra("admin_notification", true)
            startActivity(intent)
            finish()
        } else {
            if (!preferenceManager.chooseLanguageAtStartUpDisplayed) {
                // showToast("ShowDialog", this)

                showNormalDialog(
                    this,
                    getString(R.string.language),
                    getString(R.string.please_choose_language),
                    getString(R.string.english),
                    getString(R.string.hebrew),
                    BaseActivity.OnOkClicked {
                        //showToast("languageChoosed", this)
                        if (!it) {
                            chooseLanguage(true)
                            preferenceManager?.isEnglish = 1
//                            centerLogo.setImageDrawable(resources.getDrawable(R.drawable.launch_logo))
                        } else {
                            chooseLanguage(false)
                            preferenceManager?.isEnglish = 2
//                            centerLogo.setImageDrawable(resources.getDrawable(R.drawable.launch_logo_herbew))
                        }

                        preferenceManager.currentVersion = BuildConfig.VERSION_CODE
                        preferenceManager.chooseLanguageAtStartUpDisplayed = true
                        navigatetoNextUI()
                    })
            } else {
                System.out.println(">>>>>>>>>>>>idThree ")
                if (preferenceManager?.isEnglish == 1) {
                    chooseLanguage(true)
                } else {
                    chooseLanguage(false)
                }
                navigatetoNextUI()
            }
        }
        getPermission()

    }

    private fun printKeyHash() {
        try {
            val info =
                packageManager.getPackageInfo("com.bingalollc.com", PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.d("KeyHash:>>>>", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        } catch (e: PackageManager.NameNotFoundException) {
            Log.e("KeyHash:", e.toString())
        } catch (e: NoSuchAlgorithmException) {
            Log.e("KeyHash:", e.toString())
        }
    }

    private fun getProfileDetails(userId: String) {
        if (userId.isEmpty()) {return}
        showLoading(this)
        RestClient.getApiInterface().getUserDetails(userId).enqueue(object : Callback<Users> {
            override fun onResponse(
                call: Call<Users>,
                response: Response<Users>
            ) {
                hideLoading()
                if (response.body()?.status == 200){
                    val chatIntent = Intent(this@SplashActivity, ChatMessages::class.java)
                    chatIntent.putExtra("id", preferenceManager.userDetails.id)
                    chatIntent.putExtra("other_id", intent.getStringExtra("chat_id"))
                    chatIntent.putExtra("other_user_id", response.body()?.data?.id)
                    chatIntent.putExtra("prof_img", response.body()?.data?.image)
                    chatIntent.putExtra("fromDetails", false)
                    chatIntent.putExtra("friendname", response.body()?.data?.fullName)
                    chatIntent.putExtra("friendToken", response.body()?.data?.token)
                    startActivity(chatIntent)
                    finish()

                } else {
                    showToast(response.body()?.message?:"",this@SplashActivity)
                }
            }

            override fun onFailure(call: Call<Users>, t: Throwable) {
                hideLoading()
                showToast("Network Error", this@SplashActivity)
            }

        })
    }

    private fun getPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (this.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
            ) {
                //checkPermision()
            } else {
                if (checkGpsOn()) {
                    System.out.println(">>>>>>>>>>>>CHECCCCCC ")
                    isLocationEnabled = true
                    val appLocationService = AppLocationService(this)
                    var gpsLocation = appLocationService
                        .getLocation(LocationManager.GPS_PROVIDER)
                    if (gpsLocation == null) {
                        gpsLocation = appLocationService
                            .getLocation(LocationManager.NETWORK_PROVIDER)
                        if (gpsLocation == null) {
                            gpsLocation = appLocationService
                                .getLocation(LocationManager.PASSIVE_PROVIDER)
                        }
                    }
                    if (gpsLocation != null) {
                        Common.currentLat = gpsLocation.getLatitude().toString()
                        Common.currentLng = gpsLocation.getLongitude().toString()
                        if (preferenceManager?.isUserLoggedIn?: false) {
                            Common.productLocationLat = gpsLocation.getLatitude().toString()
                            Common.productLocationLng = gpsLocation.getLongitude().toString()
                            updateLocation()
                        }

                    }
                }
            }
        }
    }

    private fun updateLocation() {
        if (preferenceManager != null && preferenceManager.userDetails != null && preferenceManager.userDetails.id.isNullOrEmpty().not()) {
            locationViewModel?.updateLocation(
                preferenceManager!!, Common.currentLat, Common.currentLng, getAddress(
                    Common.productLocationLat.toDouble(),
                    Common.productLocationLng.toDouble(), this, true
                ) ?: ""
            )
        }
    }

    private fun navigatetoNextUI() {
        //showToast("Navigating", this)
//        if (preferenceManager?.isUserLoggedIn == true) {
//            updateUserViewModel?.updateUser(preferenceManager?.userDetails?.id,preferenceManager)
//            updateUserViewModel?.getBlockedList(preferenceManager?.userDetails?.id)
//            updateOnlineStatus()
//        }
        if (this.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED
        ) {
            checkPermision()
        }else {
            Handler().postDelayed({
                isNavigationCalled = true
                if (isLocationEnabled) {
                    redirectToHome()
                }

            }, 1000)
        }
    }

    private fun redirectToHome() {
        if (preferenceManager?.userDetails?.id.isNullOrEmpty().not()) {
            //Naviagte to Next Screen
            checkMaintainenceMode()
        } else {
            checkPermision()
        }
    }

    private fun updateOnlineStatus() {
        database.child("online").child(preferenceManager?.userDetails?.id?:"").setValue(System.currentTimeMillis())
        database.child("online").child(preferenceManager?.userDetails?.id?:"").onDisconnect().removeValue()

    }

    private fun checkPermision() {
        checkMaintainenceMode()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(isNavigationCalled && requestCode == 10001) {
            getPermission()
            if(isLocationEnabled) {
                redirectToHome()
            }
        }
    }

    private fun checkMaintainenceMode(){
        showLoading(this)
        RestClient.getApiInterface().fetchAppMaintainance().enqueue(object : Callback<MaintainanceResponse> {
            override fun onResponse(
                call: Call<MaintainanceResponse>,
                response: Response<MaintainanceResponse>
            ) {
                hideLoading()
                if (response.body()?.status == 200){
                    if(response.body()!!.data.isMaintaince == "1"){
                        val maintainaceIntent = Intent(this@SplashActivity, MaintainanceActivity::class.java)
                        maintainaceIntent.putExtra("maintainanceMode", response.body()!!.data)
                        startActivity(maintainaceIntent)
                        finish()
                    } else{
                        if (Build.VERSION.SDK_INT >= 23) {
                            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                                != PackageManager.PERMISSION_GRANTED
                            ) {
                                startActivity(Intent(this@SplashActivity, PermissionPage::class.java))
                                finish()
                            } else {
                                startActivity(Intent(this@SplashActivity, HomeActivity::class.java))
                                finish()
                            }
                        } else {
                            startActivity(Intent(this@SplashActivity, HomeActivity::class.java))
                            finish()
                        }
                    }

                } else {
                    showToast(response.body()?.message?:"",this@SplashActivity)
                }
            }

            override fun onFailure(call: Call<MaintainanceResponse>, t: Throwable) {
                hideLoading()
                showToast("Network Error", this@SplashActivity)
            }

        })
    }
}