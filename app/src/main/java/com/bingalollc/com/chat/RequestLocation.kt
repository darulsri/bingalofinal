package com.bingalollc.com.chat

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.chat.adapter.NearbyLocationAdapter
import com.bingalollc.com.chat.adapter.NearbySponsoredLocationAdapter
import com.bingalollc.com.chat.model.SponsoredDatum
import com.bingalollc.com.chat.model.SponsoredModel
import com.bingalollc.com.databinding.ActivityRequestLocationNewBinding
import com.bingalollc.com.network.RestClient
import com.bingalollc.com.preference.PreferenceManager
import com.bingalollc.com.utils.AppLocationService
import com.bingalollc.com.utils.nearbyLocation.PlacesResults
import com.google.android.gms.common.api.Status
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class RequestLocation : BaseActivity(), OnMapReadyCallback, NearbyLocationAdapter.OnOkClicked,
    NearbySponsoredLocationAdapter.OnOkSponsoredClicked {
    private lateinit var binding: ActivityRequestLocationNewBinding
    var views: View? = null
    private var isCurrentLocationClicked = false
    private var testDialog: AlertDialog? = null
    var mapFragment: SupportMapFragment? = null
    private var googleMap: GoogleMap? = null
    private var gpsLocation: Location? = null
    var appLocationService: AppLocationService? = null
    var nearbySponsoredLocationAdapter: NearbySponsoredLocationAdapter? = null
    var nearbyLocationAdapter: NearbyLocationAdapter? = null
    var currentMarker: Marker? = null
    var latti: Double? = null
    var longi: Double? = null
    var last_latti: Double? = null
    var last_longi: Double? = null
    val LOCATION_PERMISSION = 10001
    private var nameArray: ArrayList<String>? = null
    private var descArray: ArrayList<String>? = null
    private var distanceArray: ArrayList<String>? = null
    private var latArray: ArrayList<Double>? = null
    private var latArraySponsored: ArrayList<Double>? = null
    private var lngArray: ArrayList<Double>? = null
    private var lngArraySponsored: ArrayList<Double>? = null
    private var imageArray: ArrayList<String>? = null
    private var autocompleteFragment: AutocompleteSupportFragment? = null
    private val GOOGLEAPI = "AIzaSyCpm66tTgP2RZHIDXean_-YXoBuvSyZZqY"
    private val imageBaseUrl = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=1000&key=${GOOGLEAPI}&photoreference="

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_request_location_new)
        showFullScreenBaseColor(getColor(R.color.white))
        appLocationService = AppLocationService(this@RequestLocation)
        initHeader()
        initMap()
        initPlaces()
        initCLicks()
    }

    private fun initCLicks() {
        binding.locIcon.setOnClickListener {
            showConfirmation()

        }

        binding.addressTextLayout.setOnClickListener {
            nearbyLocationAdapter?.resetAllPos()
            nearbySponsoredLocationAdapter?.resetAllPos()
            if (!isCurrentLocationClicked) {
                selectCurrentLocation(true)
            } else {
                selectCurrentLocation(false)
            }

        }
    }

    private fun selectCurrentLocation(isSelected: Boolean) {
        if (isSelected) {
            isCurrentLocationClicked = true
            binding.addressTextLayout.background = getDrawable(R.drawable.curved_largefill_blue)
            binding.meetupLoca.setTextColor(getColor(R.color.white))
            binding.addressOne.setTextColor(getColor(R.color.white))
            binding.addressTwo.setTextColor(getColor(R.color.white))
            binding.locIcon.visibility = View.VISIBLE
            binding.locMarker.setImageDrawable(getDrawable(R.drawable.location_white_icon))

        } else {
            isCurrentLocationClicked = false
            binding.addressTextLayout.background = getDrawable(R.drawable.curved_large_outline_blue)
            binding.meetupLoca.setTextColor(getColor(R.color.blue_bg))
            binding.addressOne.setTextColor(getColor(R.color.blue_bg))
            binding.addressTwo.setTextColor(getColor(R.color.blue_bg))
            binding.locIcon.visibility = View.GONE
            binding.locMarker.setImageDrawable(getDrawable(R.drawable.location_share_icon))

        }

    }

    private fun sendLocationInMsg() {
        val intent = Intent();
        intent.putExtra("latitude", latti.toString())
        intent.putExtra("longitude", longi.toString())
        setResult(RESULT_OK, intent);
        finish();
    }

    private fun initPlaces() {
        if (!Places.isInitialized()) {
            Places.initialize(
                getApplicationContext(),
                getString(R.string.google_android_places_api_key)
            )
        }
        val placesClient =
            Places.createClient(this)
        autocompleteFragment = supportFragmentManager.findFragmentById(R.id.place_autocomplete_fragment) as? AutocompleteSupportFragment // Make casting of 'as' to nullable cast 'as?

        autocompleteFragment?.setPlaceFields(
            Arrays.asList(
                Place.Field.ID,
                Place.Field.NAME,
                Place.Field.ADDRESS,
                Place.Field.LAT_LNG
            )
        )

        autocompleteFragment?.setOnPlaceSelectedListener(object :
            PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {
                // TODO: Get info about the selected place.
                autocompleteFragment?.view?.findViewById<EditText>(R.id.places_autocomplete_search_input)?.setText(place.address)
                Log.i("fffff", "Place: " + place.name + ", " + place.id)
                binding.dummySearchtext.text = place.address
                binding.dummySearchtext.setTextColor(getColor(R.color.black))
                val cityName = place.name.toString()
                val latLng = place.latLng
                latti = latLng!!.latitude
                longi = latLng!!.longitude
                updateMapUi()
            }

            override fun onError(status: Status) {
                // TODO: Handle the error.
                Log.i("asas", "An error occurred: $status")
            }
        })

        autocompleteFragment?.view?.background = resources.getDrawable(R.drawable.curved_background_grey)
        autocompleteFragment?.setHint("Search all locations near you...")
        autocompleteFragment?.view?.findViewById<EditText>(R.id.places_autocomplete_search_input)?.textSize = 16.0f
        autocompleteFragment?.view?.findViewById<EditText>(R.id.places_autocomplete_search_input)?.setPadding(0,0,20,0)

        autocompleteFragment?.view?.findViewById<View>(R.id.place_autocomplete_clear_button)?.setOnClickListener {

        }

        val typeface: Typeface = ResourcesCompat.getFont(this, R.font.avenir_next_font)!!
        autocompleteFragment?.view?.findViewById<EditText>(R.id.places_autocomplete_search_input)?.setTypeface(typeface)
        autocompleteFragment?.view?.findViewById<EditText>(R.id.places_autocomplete_search_input)?.setTextColor(getColor(R.color.basecolor))
        autocompleteFragment?.view?.findViewById<EditText>(R.id.places_autocomplete_search_input)?.setCompoundDrawables(null,null,getDrawable(R.drawable.icon_search), null)

        binding.dummySearch.setOnClickListener {
            autocompleteFragment?.view?.findViewById<EditText>(R.id.places_autocomplete_search_input)?.performClick(); }
        binding.dummySearchtext.setOnClickListener {
            autocompleteFragment?.view?.findViewById<EditText>(R.id.places_autocomplete_search_input)?.performClick(); }

    }

    private fun initMap() {
        mapFragment = getSupportFragmentManager()
            .findFragmentById(R.id.mapView) as SupportMapFragment?
        checkGps()
    }

    private fun initHeader() {
        binding.appBar.header.setText(getString(R.string.request_a_meet_up_loc))
        binding.appBar.backIcon.setOnClickListener { finish() }
        binding.appBar.imgSatellite.setImageDrawable(getDrawable(R.drawable.icon_search))
        binding.appBar.imgSatellite.setOnClickListener {
            /*if (binding.appBar.searchLayout.isVisible) {
                binding.appBar.searchLayout.visibility = View.GONE
                binding.appBar.headerLayout.visibility = View.VISIBLE
                binding.appBar.imgSatellite.setImageDrawable(getDrawable(R.drawable.icon_search))
            }else{
                binding.appBar.searchLayout.visibility = View.VISIBLE
                binding.appBar.headerLayout.visibility = View.GONE
                binding.appBar.imgSatellite.setImageDrawable(getDrawable(R.drawable.btn_cross))
            }*/
        }

    }


    private fun checkGps() {

        // ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        val locationManager =
            this@RequestLocation.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            displayLocationSettingsRequest(this@RequestLocation)
        } else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            try {
                gpsLocation = appLocationService?.getLocation(LocationManager.GPS_PROVIDER)
                if (gpsLocation == null) {
                    gpsLocation = appLocationService?.getLocation(LocationManager.NETWORK_PROVIDER)
                    if (gpsLocation == null) {
                        gpsLocation = appLocationService?.getLocation(LocationManager.PASSIVE_PROVIDER)
                    }
                }
                if (gpsLocation != null){
                    latti = gpsLocation?.latitude?:0.0
                    longi = gpsLocation?.longitude?:0.0
                    mapFragment?.getMapAsync(this)
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


    override fun onMapReady(googleMaps: GoogleMap) {
        googleMap=googleMaps;
        updateMapUi()
    }

    private fun updateMapUi() {
        if (gpsLocation != null) {
            moveCamera(latti ?:0.0, longi ?:0.0)
            createMarker(latti ?:0.0, longi ?:0.0)
        }
        System.out.println(">>>>>>>>>>>>>>>>>lattisssss "+latti)
    }

    private fun moveCamera(lat: Double, lng:Double) {
        val builder = LatLngBounds.Builder();
        // builder.include(createMarker(lat, lng,name)?.getPosition());
        //  val bounds = builder.build();
        //val cu = CameraUpdateFactory.newLatLngBounds(LatLng(lat, lng), 5);
        val cu =
            CameraUpdateFactory.newLatLngZoom(LatLng(lat, lng), 10f)

        googleMap?.moveCamera(cu)
//        googleMap?.moveCamera(cu)
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        hideLoading()
        getAddressOfLatLng()
        getSponsoredLocation()
        getNearByDistance()
        googleMap?.isMyLocationEnabled=false
    }

    private fun getSponsoredLocation() {
        RestClient.getApiInterface().getSponsoredLocations(latti.toString(), longi.toString(), "20")
            .enqueue(object :
                Callback<SponsoredModel> {
                override fun onResponse(
                    call: Call<SponsoredModel>,
                    response: Response<SponsoredModel>
                ) {
                    if (response?.body()?.data?.size ?: 0 > 0) {
                        binding.sponsoredPlaces.visibility = View.VISIBLE
                        binding.rvSponsoredPlaces.visibility = View.VISIBLE
                        setRecyclerView(response.body()?.data)
                    } else {
                        binding.sponsoredPlaces.visibility = View.GONE
                        binding.rvSponsoredPlaces.visibility = View.GONE
                    }

                }

                override fun onFailure(call: Call<SponsoredModel>, t: Throwable) {

                }
            })
    }

    private fun setRecyclerView(data: ArrayList<SponsoredDatum?>?) {
        val nameData = ArrayList<String>()
        val descData = ArrayList<String>()
        val distanceData = ArrayList<String>()
        val imageData = ArrayList<String>()
        val website = ArrayList<String>()
        latArraySponsored = ArrayList<Double>()
            data?.forEach {
                nameData.add(it?.location_name?:"")
                descData.add(it?.location_address?:"")
                var dis = it?.distance?:"0"
                if (it?.distance?.substring(it.distance.toString().indexOf("."))?.length?:0 > 3) {
                    dis = Math.round(it?.distance?.toFloat()!!).toString()
                }
                latArraySponsored?.add(it?.latitude?.toDouble()!!)
                lngArraySponsored?.add(it?.longitude?.toDouble()!!)

                distanceData.add(dis+" Mi")
                imageData.add(it?.image?:"")
                website.add(it?.website?:"")
        }

        nearbySponsoredLocationAdapter = NearbySponsoredLocationAdapter(nameData,descData,distanceData,imageData,website,this)
        val layoutManager = LinearLayoutManager(this)
        binding.rvSponsoredPlaces.adapter = nearbySponsoredLocationAdapter
        binding.rvSponsoredPlaces.layoutManager = layoutManager
    }

    private fun getNearByDistance() {
        showLoading(this)
        val currentLocation: String =
            latti.toString() + "," + longi.toString()
        RestClient.getPlaceGoogleClient().getNearBy(
            currentLocation, 2000, "restaurant", "",
            GOOGLEAPI,"distance"
        ).enqueue(object : Callback<PlacesResults> {
            override fun onFailure(call: Call<PlacesResults>, t: Throwable) {

            }

            override fun onResponse(call: Call<PlacesResults>, response: Response<PlacesResults>) {
                if (response.body() != null && response.body()?.results != null){
                    nameArray = ArrayList()
                    descArray = ArrayList()
                    distanceArray = ArrayList()
                    imageArray = ArrayList()
                    lngArray = ArrayList()
                    latArray = ArrayList()
                    response.body()?.results?.forEachIndexed { index, result ->
                        nameArray?.add(result.name)
                        descArray?.add(result.vicinity)
                        if (result.photos != null && result.photos.size > 0)
                            imageArray?.add(imageBaseUrl+result.photos.get(0).photoReference)
                        else
                            imageArray?.add("")
                        val distance = distance(latti!!.toDouble(),longi!!.toDouble(), result.geometry.location.lat, result.geometry.location.lng)
                        distanceArray?.add((distance.toString()?:"0")+ " Mi")
                        lngArray?.add(result.geometry.location.lng)
                        latArray?.add(result.geometry.location.lat)
                    }
                    initRecyclerView()
                }

            }
        })
    }

    private fun distance(
        lat1: Double,
        lon1: Double,
        lat2: Double,
        lon2: Double
    ): Int {
        val theta = lon1 - lon2
        var dist = (Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + (Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta))))
        dist = Math.acos(dist)
        dist = rad2deg(dist)
        dist = dist * 60 * 1.1515
        return Math.round(dist).toInt()
    }


    private fun deg2rad(deg: Double): Double {
        return deg * Math.PI / 180.0
    }

    private fun rad2deg(rad: Double): Double {
        return rad * 180.0 / Math.PI
    }
    private fun initRecyclerView() {
        nearbyLocationAdapter = NearbyLocationAdapter(nameArray,descArray,distanceArray,imageArray,this)
        val layoutManager = LinearLayoutManager(this)
        binding.rvNearByPlaces.adapter = nearbyLocationAdapter
        binding.rvNearByPlaces.layoutManager = layoutManager
        hideLoading()
    }

    fun getAddressOfLatLng(): String? {
        val result = StringBuilder()
        var completeAddress = ""
        try {
            val geocoder = Geocoder(this, Locale.getDefault())
            val addresses =
                geocoder.getFromLocation(latti!!, longi!!, 1)
            if (addresses != null && addresses.size > 0) {
                val address = addresses[0]
                result.append(address.featureName).append("\n")
                result.append(address.locality).append("\n")
                result.append(address.countryName)
                val removeAddress = ", "+(address.locality?:"")+", "+(address.adminArea?:"")+" "+(address.postalCode?:"")+", "+(address.countryName?:"")
                binding.addressOne.setText(address.getAddressLine(0).replace(removeAddress,""))
                binding.addressTwo.setText((address.locality?:"") +", "+(address.adminArea?:"") )
            }
        } catch (e: IOException) {
            Log.e("tag", e.message!!)
        }
        return completeAddress
    }


    protected fun createMarker(
        latitude: Double,
        longitude: Double
    ): Marker? {
        val height = 80;
        val width = 40;
        val bitmapdraw = getResources().getDrawable(R.drawable.loc_pin_red) as BitmapDrawable;
        val b = bitmapdraw.getBitmap();
       // val smallMarker = Bitmap.createScaledBitmap(b, width, height, false)
        val latLng = LatLng(latitude, longitude)
        if (currentMarker != null)
            currentMarker?.remove()
        currentMarker= googleMap!!.addMarker(
            MarkerOptions()
                .position(LatLng(latitude, longitude))
                //   .anchor(1.5f, 1.5f)
                .title("")
                .draggable(true)
                .icon(BitmapDescriptorFactory.fromBitmap(b))
        )
        latti = latitude
        longi = longitude
        return currentMarker
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode==LOCATION_PERMISSION ){
           checkGps()
        }
    }

    override fun onOkClicked(pos: Int) {
       // showConfirmation( latArray?.get(pos), lngArray?.get(pos))
        latti = latArray?.get(pos)
        longi = lngArray?.get(pos)
        sendLocationInMsg()
    }

    override fun onPosClicked(pos: Int) {
        nearbySponsoredLocationAdapter?.resetAllPos()
        selectCurrentLocation(false)
    }



    override fun onOkSponsoredLocClicked(pos: Int) {
        latti = latArraySponsored?.get(pos)
        longi = lngArraySponsored?.get(pos)
        sendLocationInMsg()
    }

    override fun onSponsoredPosClicked(pos: Int) {
        nearbyLocationAdapter?.resetAllPos()
        selectCurrentLocation(false)
    }

    fun showConfirmation() {
        preferenceManager = PreferenceManager(this)
        val alertDialog = AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val add_menu_layout = inflater.inflate(R.layout.send_location_confirmation_dialog, null)
        val yesBtn = add_menu_layout.findViewById<TextView>(R.id.yesBtn)
        val noBtn = add_menu_layout.findViewById<TextView>(R.id.noBtn)

        alertDialog.setView(add_menu_layout)
        yesBtn.setOnClickListener {
            testDialog?.dismiss()
            sendLocationInMsg()
        }
        noBtn.setOnClickListener {
            testDialog?.dismiss()
            nearbyLocationAdapter?.resetSelectedPos()
            nearbySponsoredLocationAdapter?.resetSelectedPos()
        }
        alertDialog.setView(add_menu_layout)
        alertDialog.setCancelable(false)
        testDialog = alertDialog.create()
        testDialog?.show()
        testDialog?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }




}