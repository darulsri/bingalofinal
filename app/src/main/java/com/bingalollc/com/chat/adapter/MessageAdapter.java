package com.bingalollc.com.chat.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.Environment;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bingalollc.com.R;
import com.bingalollc.com.base.BaseActivity;
import com.bingalollc.com.contact.SellerProfileDetails;
import com.bingalollc.com.homeactivity.chatfragmet.CounterFragment;
import com.bingalollc.com.preference.PreferenceManager;
import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.Viewholder> {
    private ArrayList<String> frnd_msg;
    private ArrayList<String> own_msg;
    private ArrayList<String> frnd_image;
    private ArrayList<String> own_image;
    private ArrayList<String> frnd_pdf;
    private ArrayList<String> own_pdf;
    private ArrayList<String> own_content;
    private ArrayList<String> fnd_content;
    private ArrayList<String> lat;
    private ArrayList<String> lng;
    private ArrayList<String> content_type;
    private ArrayList<String> messageTime;
    private String prof_img,lastSeenTime;
    private Context mContext;
    private ItemClickedListener clickedListener;
    private PreferenceManager preferenceManager;
    private PdfRenderer mPdfRenderer;
    private final CounterFragment viewImageWithFrag;
    /*= new CounterFragment();*/

    public MessageAdapter(ItemClickedListener listener, String prof_img, ArrayList<String> frnd_msg, ArrayList<String> own_msg, ArrayList<String> frnd_image, ArrayList<String> own_image, ArrayList<String> fnd_content, ArrayList<String> own_content, ArrayList<String> lat, ArrayList<String> lng, ArrayList<String> content_type, ArrayList<String> frnd_pdf, ArrayList<String> own_pdf, ArrayList<String> messageTime, String lastSeenTime, CounterFragment viewImageWithFrag) {
        clickedListener = listener;
        this.frnd_msg = frnd_msg;
        this.own_msg = own_msg;
        this.frnd_image = frnd_image;
        this.own_image = own_image;
        this.fnd_content = fnd_content;
        this.own_content = own_content;
        this.messageTime = messageTime;
        this.lat = lat;
        this.lng = lng;
        this.content_type = content_type;
        this.prof_img = prof_img;
        this.frnd_pdf=frnd_pdf;
        this.own_pdf=own_pdf;
        this.lastSeenTime=lastSeenTime;
        this.viewImageWithFrag=viewImageWithFrag;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        preferenceManager = PreferenceManager.getInstance(mContext);
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_item, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        holder.textContentUser.setText(own_msg.get(position));
        if (frnd_msg.get(position).contains(mContext.getString(R.string.offer_sent_text))){
            holder.counterText.setVisibility(View.VISIBLE);
            SpannableString content = new SpannableString(frnd_msg.get(position));
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View textView) {
                }
                @Override
                public void updateDrawState(@NonNull TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setColor(mContext.getResources().getColor(R.color.basecolor));
                    //draw underline base on true/false
                    ds.setUnderlineText(false);
                }
            };
            content.setSpan(clickableSpan, frnd_msg.get(position).indexOf(":")+1, frnd_msg.get(position).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            holder.textContentFriend.setText(content);
            holder.counterText.setOnClickListener(view -> {
                showCounterscreen();
            });
        }else {
            holder.counterText.setVisibility(View.GONE);
            holder.textContentFriend.setText(frnd_msg.get(position));
        }

        try {
            holder.incomingMsgTime.setVisibility(View.GONE);
            Date date = new Date(Long.parseLong(messageTime.get(position))*1000L); // *1000 is to convert seconds to milliseconds
            Calendar cal = Calendar.getInstance();
            TimeZone tz = cal.getTimeZone();
            Log.d("Time zone: ", tz.getDisplayName());
            SimpleDateFormat sdftime = new SimpleDateFormat("HH:mm a"); // the format of your date
            sdftime.setTimeZone(tz);
            String date1= new SimpleDateFormat("yyyy-MM-dd").format(date);

            Date c = Calendar.getInstance().getTime();
            System.out.println("Current time => " + c);
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String date2 = df.format(c);
            if (!TextUtils.isEmpty(own_msg.get(position)) || !TextUtils.isEmpty(own_image.get(position)) || !TextUtils.isEmpty(own_content.get(position)) || !TextUtils.isEmpty(own_pdf.get(position))){
                holder.seenLayout.setVisibility(View.VISIBLE);
                holder.seenTiming.setText(sdftime.format(date));
                holder.seenTextView.setVisibility(View.VISIBLE);
                holder.seenTextView.setText(mContext.getString(R.string.delivered));
                holder.seenTextView.setTextColor(mContext.getResources().getColor(R.color.grey_800));
                Calendar calslastTime = Calendar.getInstance(Locale.ENGLISH);
                Calendar calsMsgTime= Calendar.getInstance(Locale.ENGLISH);

                calslastTime.setTimeInMillis(Long.parseLong(lastSeenTime) * 1000L);
                calsMsgTime.setTimeInMillis(Long.parseLong(messageTime.get(position)) * 1000L);
                String dateLastTime = DateFormat.format("dd-MM-yyyy hh:mm:ss", calslastTime).toString();
                String datecalsMsgTime = DateFormat.format("dd-MM-yyyy hh:mm:ss", calsMsgTime).toString();
                try {
                    SimpleDateFormat formats = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
                    Date dateLastTimeDuration = formats.parse(dateLastTime);
                    Date dateMSgTIme = formats.parse(datecalsMsgTime);
                    Boolean isLessThanFiveSecond = printDifference(dateMSgTIme,dateLastTimeDuration);
                    if (isLessThanFiveSecond) {
                        holder.seenTextView.setVisibility(View.VISIBLE);
                        holder.seenTextView.setText(mContext.getString(R.string.seen));
                        holder.seenTextView.setTextColor(mContext.getResources().getColor(R.color.basecolor));
                    }else {
                        holder.seenTextView.setVisibility(View.VISIBLE);
                        holder.seenTextView.setText(mContext.getString(R.string.delivered));
                        holder.seenTextView.setTextColor(mContext.getResources().getColor(R.color.grey_800));
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }else {
                holder.incomingMsgTime.setText(sdftime.format(date));
                holder.incomingMsgTime.setVisibility(View.VISIBLE);
                holder.seenLayout.setVisibility(View.GONE);
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        try {
            if (prof_img != null || !prof_img.equals("")) {
                Glide.with(mContext).load(prof_img).error(R.drawable.default_avata).placeholder(R.drawable.default_avata).into(holder.imageView3);
            } else {
                Glide.with(mContext).load(R.drawable.default_avata).into(holder.imageView3);
            }
        }catch (Exception e){
            Glide.with(mContext).load(R.drawable.default_avata).into(holder.imageView3);
        }



        if (frnd_msg.get(position).equals("")) {
            holder.imageView3.setVisibility(View.GONE);
            holder.textContentFriend.setVisibility(View.GONE);
        } else {
            holder.imageView3.setVisibility(View.VISIBLE);
            holder.textContentFriend.setVisibility(View.VISIBLE);
        }


        if (own_msg.get(position).equals("")) {
            holder.textContentUser.setText("");
            holder.textContentUser.setVisibility(View.INVISIBLE);
        } else {
            holder.textContentUser.setVisibility(View.VISIBLE);
        }


        if (!own_image.get(position).equals("")) {
            holder.outgoingImage.setVisibility(View.VISIBLE);
            if (own_image.get(position).equalsIgnoreCase("test")){
                holder.outgoingImage.setImageDrawable(mContext.getResources().getDrawable(R.drawable.chat_product_placeholder));
            }else {
                Glide.with(mContext).load(own_image.get(position)).placeholder(R.drawable.chat_product_placeholder).error(R.drawable.chat_product_placeholder).into(holder.outgoingImage);
            }
        } else {
            holder.outgoingImage.setVisibility(View.GONE);
        }

        if (!frnd_image.get(position).equals("")) {
            holder.incomingImage.setVisibility(View.VISIBLE);
            holder.imageView3.setVisibility(View.VISIBLE);
            Glide.with(mContext).load(frnd_image.get(position)).placeholder(R.drawable.chat_product_placeholder).error(R.drawable.chat_product_placeholder).into(holder.incomingImage);
        } else {
            holder.incomingImage.setVisibility(View.GONE);
            if (frnd_msg.get(position).equals("")) {
                holder.imageView3.setVisibility(View.GONE);
            }
        }

        if (content_type.get(position).equals("2")) {
            if (!fnd_content.get(position).equals("")) {
                holder.incomingImage.setImageDrawable(mContext.getResources().getDrawable(R.drawable.map_img));
                holder.incomingImage.setVisibility(View.VISIBLE);
                holder.imageView3.setVisibility(View.VISIBLE);
            } else {
                holder.incomingImage.setVisibility(View.GONE);
                holder.imageView3.setVisibility(View.GONE);
            }
            holder.outgoingImage.setClipToOutline(true);
            if (!own_content.get(position).equals("")) {
                holder.outgoingImage.setImageDrawable(mContext.getResources().getDrawable(R.drawable.map_img));
                holder.outgoingImage.setVisibility(View.VISIBLE);
            } else {
                holder.outgoingImage.setVisibility(View.GONE);
            }
        }
        holder.name_section_inc.setVisibility(View.GONE);
        holder.name_section_out.setVisibility(View.GONE);
        if (content_type.get(position).equals("3")){
            if (!frnd_pdf.get(position).equals("")) {
                holder.incomingImage.setVisibility(View.VISIBLE);
                holder.imageView3.setVisibility(View.VISIBLE);
                holder.name_section_inc.setVisibility(View.VISIBLE);
                holder.name_section_out.setVisibility(View.GONE);
                int index= frnd_pdf.get(position).indexOf("#");
                int chat_image_index= frnd_pdf.get(position).indexOf("chat_images/");
                String name = frnd_pdf.get(position).substring(chat_image_index,index).replace("chat_images/","");
                holder.nameOf_pdf_inc.setText(name);
                holder.incomingImage.setBackground(mContext.getResources().getDrawable(R.drawable.background_with_shadow));
                holder.incomingImage.setPadding(1,1,1,1);

                Glide.with(mContext).load(frnd_pdf.get(position).substring(index+1)).placeholder(R.drawable.chat_product_placeholder).error(R.drawable.chat_product_placeholder).into(holder.incomingImage);
            } else {
                holder.incomingImage.setVisibility(View.GONE);
                if (frnd_msg.get(position).equals("")) {
                    holder.imageView3.setVisibility(View.GONE);
                }
            }
            if (!own_pdf.get(position).equals("")) {
                holder.name_section_inc.setVisibility(View.GONE);
                holder.name_section_out.setVisibility(View.VISIBLE);
                int index= own_pdf.get(position).indexOf("#");
                int chat_image_index= own_pdf.get(position).indexOf("chat_images/");
                String name = own_pdf.get(position).substring(chat_image_index,index).replace("chat_images/","");
                holder.nameOf_pdf.setText(name);
                Glide.with(mContext).load(own_pdf.get(position).substring(index+1)).placeholder(R.drawable.chat_product_placeholder).error(R.drawable.chat_product_placeholder).into(holder.outgoingImage);
                holder.outgoingImage.setVisibility(View.VISIBLE);
                holder.outgoingImage.setBackground(mContext.getResources().getDrawable(R.drawable.background_with_shadow));
                holder.outgoingImage.setPadding(1,1,1,1);
            } else {
                holder.outgoingImage.setVisibility(View.GONE);
            }
        }

        holder.imageView3.setOnClickListener(view -> {
            clickedListener.onProfileClicked(position);
        });
    }

    private void showCounterscreen() {
        FragmentManager fragmentManager = ((FragmentActivity) mContext).getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_bottom, R.anim.slide_out_top, R.anim.slide_in_top, R.anim.slide_out_bottom);
        transaction.add(R.id.fragment_container, viewImageWithFrag, "counter_screen");
        transaction.commit();
    }

    public void updateSeenStatus(String timeStamp){
        this.lastSeenTime = timeStamp;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return frnd_msg.size();
    }


    public class Viewholder extends RecyclerView.ViewHolder {
        TextView textContentFriend, textContentUser,seenTextView,seenTiming,incomingMsgTime,counterText;
        RelativeLayout layoutOwn, layoutFrnd;
        ImageView incomingImage, outgoingImage;
        CircleImageView imageView3;
        LinearLayout name_section_inc,name_section_out,seenLayout;

        private TextView nameOf_pdf,nameOf_pdf_inc;

        public Viewholder(@NonNull View itemView) {
            super(itemView);
            textContentUser = itemView.findViewById(R.id.textContentUser);
            counterText = itemView.findViewById(R.id.counterText);
            incomingMsgTime = itemView.findViewById(R.id.incomingMsgTime);
            textContentFriend = itemView.findViewById(R.id.textContentFriend);
            layoutOwn = itemView.findViewById(R.id.layoutOwn);
            layoutFrnd = itemView.findViewById(R.id.layoutFrnd);
            outgoingImage = itemView.findViewById(R.id.outgoingImage);
            incomingImage = itemView.findViewById(R.id.incomingImage);
            imageView3 = itemView.findViewById(R.id.imageView3);
            nameOf_pdf = itemView.findViewById(R.id.nameOf_pdf);
            nameOf_pdf_inc = itemView.findViewById(R.id.nameOf_pdf_inc);
            name_section_inc = itemView.findViewById(R.id.name_section_inc);
            name_section_out = itemView.findViewById(R.id.name_section_out);
            seenTiming = itemView.findViewById(R.id.seenTiming);
            seenTextView = itemView.findViewById(R.id.seenTextView);
            seenLayout = itemView.findViewById(R.id.seenLayout);



            incomingImage.setClipToOutline(true);
            outgoingImage.setClipToOutline(true);
            outgoingImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (content_type.get(getAdapterPosition()).equals("2")) {
                        clickedListener.itemClicked(getAdapterPosition(),"out");

                    }else if (content_type.get(getAdapterPosition()).equals("3")){
                        /*Intent intent=new Intent(mContext, PDFViewActivity.class);
                        intent.putExtra("url",own_pdf.get(getAdapterPosition()));
                        mContext.startActivity(intent);*/
                        //openPdf(own_pdf.get(getAdapterPosition()));
                    } else {
                        BaseActivity.fullScreenImageDialog(mContext,own_image.get(getAdapterPosition()), 0);
                        // Toast.makeText(mContext, "IMages hai", Toast.LENGTH_SHORT).show();
                    }                }
            });


            incomingImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (content_type.get(getAdapterPosition()).equals("2")) {
                        clickedListener.itemClicked(getAdapterPosition(),"in");

                    } else if (content_type.get(getAdapterPosition()).equals("3")){
                      /* Intent intent=new Intent(mContext, PDFViewActivity.class);
                        intent.putExtra("url",frnd_pdf.get(getAdapterPosition()));
                        mContext.startActivity(intent);*/
                        //openPdf(frnd_pdf.get(getAdapterPosition()));
                    }else {
                        BaseActivity.fullScreenImageDialog(mContext,frnd_image.get(getAdapterPosition()), 0);
                        // Toast.makeText(mContext, "IMages hai", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
    public Boolean printDifference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();
        System.out.println(">>>>>>>>>>>>>>>>>>>>>ENDDATETIME "+endDate.getTime());
        System.out.println(">>>>>>>>>>>>>>>>>>>>>STARTE___DATE "+startDate.getTime());
        if (endDate.getTime() >=  startDate.getTime()){
            return true;
        }

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : "+ endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        System.out.printf(">>>>>>>>>>>>>TIme calculated "+
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);

        if (elapsedDays==0 && elapsedHours==0 && elapsedMinutes==0 && elapsedSeconds>0 && elapsedSeconds<6){
            return true;
        }else {
            return false;
        }
    }



    public final static String FOLDER = Environment.getExternalStorageDirectory() + "/PDF";
    private void saveImage(Bitmap bmp) {
        FileOutputStream out = null;
        try {
            File folder = new File(FOLDER);
            if(!folder.exists())
                folder.mkdirs();
            File file = new File(folder, "PDF.png");
            out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
        } catch (Exception e) {
            //todo with exception
        } finally {
            try {
                if (out != null)
                    out.close();
            } catch (Exception e) {
                //todo with exception
            }
        }
    }

    public Bitmap applyCrop(Bitmap bitmap, int leftCrop, int topCrop, int rightCrop, int bottomCrop) {
        int cropWidth = bitmap.getWidth() - rightCrop - leftCrop;
        int cropHeight = bitmap.getHeight() - bottomCrop - topCrop;
        return Bitmap.createBitmap(bitmap, leftCrop, topCrop, cropWidth, cropHeight);
    }
    public Bitmap screenShot(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(),
                view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        Bitmap bm= applyCrop(bitmap,20,30,20,20);
        return bm;
    }

    public interface ItemClickedListener {
        void itemClicked(int position, String out);
        void onProfileClicked(int position);
    }
}
