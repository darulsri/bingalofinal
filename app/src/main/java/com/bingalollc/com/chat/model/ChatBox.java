package com.bingalollc.com.chat.model;


import java.util.ArrayList;
import java.util.HashMap;

public class ChatBox {
    private String id;
    private String lastMessage;
    private Integer timestamp;
    private HashMap<String, Boolean> isRead;
    private ArrayList<String> userIDs;

    public ChatBox() {
    }

    public ChatBox(String id, String lastMessage, Integer timestamp, HashMap<String, Boolean> isRead, ArrayList<String> userIDs) {
        this.id = id;
        this.lastMessage = lastMessage;
        this.timestamp = timestamp;
        this.isRead = isRead;
        this.userIDs = userIDs;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public Integer getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
    }

    public HashMap<String, Boolean> getIsRead() {
        return isRead;
    }

    public void setIsRead(HashMap<String, Boolean> isRead) {
        this.isRead = isRead;
    }

    public ArrayList<String> getUserIDs() {
        return userIDs;
    }

    public void setUserIDs(ArrayList<String> userIDs) {
        this.userIDs = userIDs;
    }
}

