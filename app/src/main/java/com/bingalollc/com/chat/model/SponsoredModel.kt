package com.bingalollc.com.chat.model

import com.google.gson.annotations.*


data class SponsoredModel (

    @SerializedName("message")
     var message: String? = null,
    @SerializedName("status")
    var status: Int? = null,
    @SerializedName("data")
    var data: ArrayList<SponsoredDatum?>? = null
)




data class SponsoredDatum (
    @SerializedName("id")
    var id: String? = null,

    @SerializedName("location_name")
    var location_name: String? = null,

    @SerializedName("location_address")
    var location_address: String? = null,

    @SerializedName("website")
    var website: String? = null,

    @SerializedName("latitude")
    var latitude: String? = null,

    @SerializedName("longitude")
    var longitude: String? = null,

    @SerializedName("image")
    var image: String? = null,

    @SerializedName("created_at")
    var created_at: String? = null,

    @SerializedName("updated_at")
    var updated_at: String? = null,

    @SerializedName("distance")
    var distance: String? = null
)