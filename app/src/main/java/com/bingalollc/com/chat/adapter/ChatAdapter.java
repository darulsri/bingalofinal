package com.bingalollc.com.chat.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bingalollc.com.R;
import com.bingalollc.com.chat.model.ChatBox;
import com.bingalollc.com.preference.PreferenceManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.Viewholder> {
    private ArrayList<String> prod_img;
    private ArrayList<String> img;
    private ArrayList<String> name;
    private ArrayList<String> time;
    private ArrayList<String> lastMsg;
    private ArrayList<Boolean> onlineStatus;
    private ArrayList<String> isSold;
    private Context context;
    private onClickedListen onClickedListen;
    ArrayList<ChatBox> chatBoxArrayList;
    private static final int PENDING_REMOVAL_TIMEOUT = 100;
    HashMap<String, Runnable> pendingRunnables = new HashMap<>();
    private Handler handler = new Handler();
    private PreferenceManager preferenceManager;

    public ChatAdapter(ArrayList<Boolean> onlineStatus, ArrayList<String> prod_img, ArrayList<String> img, ArrayList<String> name, ArrayList<String> time, ArrayList<String> lastMsg, ArrayList<ChatBox> chatBoxArrayList,PreferenceManager preferenceManager, ChatAdapter.onClickedListen onClickedListen, ArrayList<String> isSold) {
        this.onlineStatus = onlineStatus;
        this.img = img;
        this.prod_img = prod_img;
        this.name = name;
        this.time = time;
        this.lastMsg = lastMsg;
        this.onClickedListen = onClickedListen;
        this.preferenceManager = preferenceManager;
        this.chatBoxArrayList=chatBoxArrayList;
        this.isSold=isSold;
    }

    public void updateData(ArrayList<Boolean> onlineStatus, ArrayList<String> prod_img, ArrayList<String> img, ArrayList<String> name, ArrayList<String> time, ArrayList<String> lastMsg, ArrayList<ChatBox> chatBoxArrayList,PreferenceManager preferenceManager, ChatAdapter.onClickedListen onClickedListen, ArrayList<String> isSold) {
        this.onlineStatus = onlineStatus;
        this.img = img;
        this.prod_img = prod_img;
        this.name = name;
        this.time = time;
        this.lastMsg = lastMsg;
        this.onClickedListen = onClickedListen;
        this.preferenceManager = preferenceManager;
        this.chatBoxArrayList=chatBoxArrayList;
        this.isSold=isSold;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context=parent.getContext();
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.rc_item_friend,parent,false);
        return new Viewholder(view);
    }

    public void updateProImages(ArrayList<String> prod_img){
        this.prod_img = prod_img;
        notifyDataSetChanged();
    }

    public void removeItem(int pos){
      notifyItemRemoved(pos);
    }
    public void refreshItem(int pos){
        notifyItemChanged(pos);
    }

    public void updateOnlineStatus(ArrayList<Boolean> onlineStatus, int pos){
        this.onlineStatus = onlineStatus;
        notifyItemChanged(pos);
    }

    public void updateProfileDetails(ArrayList<String> name,ArrayList<String> img){
        this.name = name;
        this.img = img;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        holder.txtName.setText(name.get(position));

        try {
            if (!img.get(position).equals("")){
                Log.d("IMGGGGG",img.get(position));
                Glide.with(context).load(img.get(position)).apply(RequestOptions.circleCropTransform().error(R.drawable.default_user).placeholder(R.drawable.default_user)).into(holder.icon_avata);
            }else {
                Glide.with(context).load(R.drawable.default_user).apply(RequestOptions.circleCropTransform()).into(holder.icon_avata);
            }
        }catch (Exception e){}

         try {
             Glide.with(context).load(prod_img.get(position)).apply(RequestOptions.circleCropTransform().error(R.drawable.product_placeholder).placeholder(R.drawable.product_placeholder)).into(holder.prod_img);
         }catch (Exception e){}

        try {
            Date date = new Date(Long.parseLong(time.get(position))*1000L); // *1000 is to convert seconds to milliseconds
            Calendar cal = Calendar.getInstance();
            TimeZone tz = cal.getTimeZone();
            Log.d("Time zone: ", tz.getDisplayName());
            // SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy "); // the format of your date
            // SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy "); // the format of your date
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy "); // the format of your date
            SimpleDateFormat sdftime = new SimpleDateFormat("HH:mm a"); // the format of your date
            sdf.setTimeZone(tz);
            sdftime.setTimeZone(tz);

           // long serverTimeStamp= Long.getLong(time.get(position));
            //whatever your server timestamp is, however you are getting it.
//You may have to use Long.parseLong(serverTimestampString) to convert it from a string

//3000(millliseconds in a second)*60(seconds in a minute)*5(number of minutes)=300000
            Date datess = new Date(Integer.parseInt(time.get(position)) * 1000);
             String date1= new SimpleDateFormat("yyyy-MM-dd").format(date);

            Date c = Calendar.getInstance().getTime();
            System.out.println("Current time => " + c);

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String date2 = df.format(c);

            if (date1.equals(date2)){
                holder.txtTime.setText(sdftime.format(date));
            } else {
                holder.txtTime.setText(sdf.format(date)+"");
            }



        }catch (Exception e){}


        try {
            holder.txtMessage.setText(lastMsg.get(position));
        }catch (Exception e){}
        try {
            if (chatBoxArrayList.get(position).getIsRead().get(preferenceManager.getUserDetails().getId()).equals(false)) {
                holder.txtMessage.setTypeface(holder.txtMessage.getTypeface(), Typeface.BOLD);
                holder.txtName.setTypeface(holder.txtMessage.getTypeface(), Typeface.BOLD);
                holder.txtTime.setTypeface(holder.txtMessage.getTypeface(), Typeface.BOLD);

                holder.txtMessage.setTextColor(context.getResources().getColor(R.color.basecolor));
            } else {
                holder.txtMessage.setTextColor(context.getResources().getColor(R.color.grey_800));
                holder.txtMessage.setTypeface(holder.txtMessage.getTypeface(), Typeface.NORMAL);
                holder.txtName.setTypeface(holder.txtMessage.getTypeface(), Typeface.NORMAL);
                holder.txtTime.setTypeface(holder.txtMessage.getTypeface(), Typeface.NORMAL);
            }
        }catch (Exception e){
            e.printStackTrace();
            onClickedListen.onErrorCalled();
        }

        try{
            if (lastMsg.get(position).equals("")){
                holder.completeLayout.setVisibility(View.GONE);
            }
        }catch (Exception e){
            holder.completeLayout.setVisibility(View.GONE);
        }

        if (onlineStatus!=null && onlineStatus.get(position) != null && onlineStatus.get(position)){
            holder.onlineBtn.setBackground(context.getResources().getDrawable(R.drawable.circle_fill_green));
        }else {
            holder.onlineBtn.setBackground(context.getResources().getDrawable(R.drawable.circle_fill_grey));
        }
        if (isSold.get(position).toString().equals("1")){
            holder.soldLabel.setVisibility(View.VISIBLE);
        } else {
            holder.soldLabel.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return name.size();
    }

    public void pendingRemoval(final int position) {

        final String data = name.get(position);
        onClickedListen.listPosition(name.indexOf(data));
       /* if (!name.contains(data)) {
           /// itemsPendingRemoval.add(data);
            // this will redraw row in "undo" state
            notifyItemChanged(position);
            //create, store and post a runnable to remove the data
            Runnable pendingRemovalRunnable = new Runnable() {
                @Override
                public void run() {
                    // remove(list.indexOf(data));lis

                }
            };

            handler.postDelayed(pendingRemovalRunnable, PENDING_REMOVAL_TIMEOUT);
            pendingRunnables.put(data, pendingRemovalRunnable);
        }*/
    }


    public class Viewholder extends RecyclerView.ViewHolder {
        ImageView icon_avata,prod_img,soldLabel;
        View onlineBtn;
        TextView txtName,txtTime,txtMessage;
        LinearLayout layoutComplete;
        RelativeLayout completeLayout;

        public Viewholder(@NonNull View itemView) {
            super(itemView);
            onlineBtn=itemView.findViewById(R.id.onlineBtn);
            soldLabel=itemView.findViewById(R.id.soldLabel);
            icon_avata=itemView.findViewById(R.id.icon_avata);
            prod_img=itemView.findViewById(R.id.prod_img);
            txtName=itemView.findViewById(R.id.txtName);
            txtTime=itemView.findViewById(R.id.txtTime);
            txtMessage=itemView.findViewById(R.id.txtMessage);
            layoutComplete=itemView.findViewById(R.id.layoutComplete);
            completeLayout=itemView.findViewById(R.id.completeLayout);
            layoutComplete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickedListen.onLayoutClick(getAdapterPosition());
                }
            });

        }
    }

    public interface onClickedListen{
        void onLayoutClick(int position);
        void listPosition(int position);
        void onErrorCalled();
    }

}
