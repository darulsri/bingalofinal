package com.bingalollc.com.chat.model;


import java.util.ArrayList;
import java.util.HashMap;

public class BlockListModel {
    private String blockedBy;
    private String id;
    private Integer timestamp;
    private ArrayList<String> userIDs;

    public BlockListModel() {
    }

    public BlockListModel(String id, String blockedBy, Integer timestamp, HashMap<String, Boolean> isRead, ArrayList<String> userIDs) {
        this.id = id;
        this.blockedBy = blockedBy;
        this.timestamp = timestamp;
        this.userIDs = userIDs;
    }

    public String getBlockedBy() {
        return blockedBy;
    }

    public void setBlockedBy(String blockedBy) {
        this.blockedBy = blockedBy;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
    }

    public ArrayList<String> getUserIDs() {
        return userIDs;
    }

    public void setUserIDs(ArrayList<String> userIDs) {
        this.userIDs = userIDs;
    }
}

