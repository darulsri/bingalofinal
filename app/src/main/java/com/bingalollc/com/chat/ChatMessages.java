package com.bingalollc.com.chat;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.pdf.PdfRenderer;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bingalollc.com.R;
import com.bingalollc.com.base.BaseActivity;
import com.bingalollc.com.chat.adapter.MessageAdapter;
import com.bingalollc.com.chat.model.Messages;
import com.bingalollc.com.contact.SellerProfileDetails;
import com.bingalollc.com.homeactivity.HomeActivity;
import com.bingalollc.com.homeactivity.chatfragmet.CounterFragment;
import com.bingalollc.com.homeactivity.model.ProductModel;
import com.bingalollc.com.homeactivity.profilefragment.ProfileFrag;
import com.bingalollc.com.model.ApiStatusModel;
import com.bingalollc.com.network.RestClient;
import com.bingalollc.com.preference.Common;
import com.bingalollc.com.preference.PreferenceManager;
import com.bingalollc.com.preference.StringsConstant;
import com.bingalollc.com.product_details.ProductDetails;
import com.bingalollc.com.utils.AppLocationService;
import com.bingalollc.com.utils.PickerUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.internal.LinkedTreeMap;
import com.kbeanie.multipicker.api.FilePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.FilePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenFile;
import androidx.activity.result.contract.ActivityResultContracts;
import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatMessages extends BaseActivity implements MessageAdapter.ItemClickedListener, FilePickerCallback, CounterFragment.OnCounterSendClick {
    private TextView headerTitle,base_header;
    private ImageView backBtn;
    private View viewLine;
    private FirebaseFirestore mDb;
    private String btnSendText="";
    private  Location gpsLocation;
    String currentPhotoPath;
    AlertDialog al;
    private String type="";
    private String offerPrice="";
    private ProgressDialog progressDialog;
    private RecyclerView recyclerChat;
    private static int REQUEST_CAMERA = 101;
    private static int PICK_IMAGE_REQUEST_GALLERY = 102;
    private static int PDF_FILE_REQUEST_GALLERY = 105;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 103;
    private static final int REQUEST_LOCATION = 104;
    private static final int OPEN_REQUEST_LOCATION = 107;
    private MultipartBody.Part fileToUpload;
    private MultipartBody.Part fileToUpload_random_image;
    AppLocationService appLocationService;
    // private ListMessageAdapter adapter;
    private String roomId;
    private ArrayList<CharSequence> idFriend;
    private TextView btnSend,dateText;
    private MaterialCardView dateTextLayout;
    private ImageView attachment_icon_asset,gallery_icon,camera_icon,loc_icon,pdf_icon;
    private EditText editWriteMessage;
    private LinearLayoutManager linearLayoutManager;
    public static HashMap<String, Bitmap> bitmapAvataFriend;
    public Bitmap bitmapAvataUser;
    private String id,other_id,other_user_id,other_user_pushtoken,prof_img,product_id;
    private ArrayList<String> frnd_msg=new ArrayList<>();
    private ArrayList<String> own_msg=new ArrayList<>();
    private ArrayList<String> frnd_img=new ArrayList<>();
    private ArrayList<String> own_img=new ArrayList<>();
    private ArrayList<String> frnd_pdf=new ArrayList<>();
    private ArrayList<String> own_pdf=new ArrayList<>();


    private ArrayList<String> idOfMsg=new ArrayList<>();
    private ArrayList<String> fnd_content=new ArrayList<>();
    private ArrayList<String> own_content=new ArrayList<>();
    private ArrayList<String> lat=new ArrayList<>();
    private ArrayList<String> lng=new ArrayList<>();
    private ArrayList<String> content_type=new ArrayList<>();
    private ArrayList<String> messageTime=new ArrayList<>();
    private String idnode;
    private RecyclerView recyclerView;
    private ShapeableImageView prodImg;
    private RelativeLayout meetup_loc;
    private TextView prodName;
    private TextView paySeller;
    private TextView prodPrice;
    private LinearLayout prodLayout;
    private  MessageAdapter messageAdapter;
    private boolean isFirstTime=true;
    private boolean isCameFromNotifcation=false;
    private PreferenceManager preferenceManager;
    private FilePicker filePicker;
    private AlertDialog alertDialogPermission;
    private DatabaseReference database;
    ListenerRegistration listenerRegistration;
    private String permissionsArrayLocation[] = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    private String permissionsCAMERA[] = {  Manifest.permission.CAMERA};
    private String permissionsGALLERY[] = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    private MutableLiveData<String> lastTime = new MutableLiveData<>();
    private ProductModel.Datum prductData = new ProductModel.Datum();
    private CounterFragment viewImageWithFrag;



    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_messages);
        mDb = FirebaseFirestore.getInstance();
        Common.isChatScreenOpened = true;
        database = FirebaseDatabase.getInstance().getReference();
        meetup_loc = (RelativeLayout) findViewById(R.id.meetup_loc);
        headerTitle = (TextView) findViewById(R.id.header);
        base_header = (TextView) findViewById(R.id.base_header);
        base_header.setVisibility(View.VISIBLE);
        LinearLayout topLayer = (LinearLayout) findViewById(R.id.topLayer);
        topLayer.setBackgroundColor(getResources().getColor(R.color.basecolor));
        backBtn = (ImageView) findViewById(R.id.back_icon);
        btnSend = (TextView) findViewById(R.id.btnSend);
        dateTextLayout = (MaterialCardView) findViewById(R.id.dateTextLayout);
        dateText = (TextView) findViewById(R.id.dateText);
        attachment_icon_asset = (ImageView) findViewById(R.id.attachment_icon_asset);
        gallery_icon = (ImageView) findViewById(R.id.gallery_icon);
        camera_icon = (ImageView) findViewById(R.id.camera_icon);
        pdf_icon = (ImageView) findViewById(R.id.pdf_icon);
        loc_icon = (ImageView) findViewById(R.id.loc_icon);
        editWriteMessage = (EditText) findViewById(R.id.editWriteMessage);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerChat);
        prodImg = (ShapeableImageView) findViewById(R.id.prodImg);
        prodName = (TextView) findViewById(R.id.prodName);
        paySeller = (TextView) findViewById(R.id.paySeller);
        prodPrice = (TextView) findViewById(R.id.prodPrice);
        prodLayout = (LinearLayout) findViewById(R.id.product_layout);
        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();
        preferenceManager = PreferenceManager.getInstance(this);
        appLocationService = new AppLocationService(ChatMessages.this);
        getLocation(false);
        headerTitle.setText(getIntent().getStringExtra("friendname"));
        headerTitle.setOnClickListener(view -> navigateToUserProfile());
        backBtn.setOnClickListener(view -> onBackPressed());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ChatMessages.this.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
            }
        }

        id = preferenceManager.getUserDetails().getId();
        other_id=getIntent().getStringExtra("other_id");
        other_user_id=getIntent().getStringExtra("other_user_id");
        other_user_pushtoken=getIntent().getStringExtra("friendToken");
        product_id= other_id.substring(other_id.lastIndexOf("-")+1);
        prof_img=getIntent().getStringExtra("prof_img");
        offerPrice = getIntent().getStringExtra("offerPrice");
        if (!TextUtils.isEmpty(offerPrice)){
            sendChat(getString(R.string.offer_sent_text)+" "+offerPrice);
        }

        if (preferenceManager.getTipsCountPerChat().containsKey(other_user_id+product_id)) {
            if (preferenceManager.getTipsCountPerChat().get(other_user_id+product_id)!=null &&
                    preferenceManager.getTipsCountPerChat().get(other_user_id+product_id) < 1) {
                showTipsDialog();
            }

        } else {
            showTipsDialog();
        }


        if (getIntent().getBooleanExtra("fromDetails",false)){
            setProdData();
        }else {
            fetchProduct();
        }
       // getOtherUserData();
        prodLayout.setOnClickListener(view -> {
            if (!TextUtils.isEmpty(product_id) && prductData != null && prductData.getId() != null){
                //Common.selectedProductDetails = prductData;
                startActivity(new Intent(ChatMessages.this, ProductDetails.class));
            }
        });

        checkOnlineStatus();

        if (getIntent().getStringExtra("notification")!=null){
            if (getIntent().getStringExtra("notification").toString().equals("true")){
                isCameFromNotifcation=true;
                if (Integer.parseInt(preferenceManager.getUserDetails().getId())>Integer.parseInt(getIntent().getStringExtra("other_id"))){
                    other_id=getIntent().getStringExtra("other_id")+"-"+preferenceManager.getUserDetails().getId();
                }
            }
        }
        attachment_icon_asset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attachment_icon_asset.setVisibility(View.GONE);
                gallery_icon.setVisibility(View.VISIBLE);
                camera_icon.setVisibility(View.VISIBLE);
                loc_icon.setVisibility(View.VISIBLE);

            }
        });

        paySeller.setOnClickListener(view -> {comingSoonDialog();});
         /*   paySeller.setOnClickListener(view ->


                showNormalDialog(ChatMessages.this,getString(R.string.are_you_with_seller),
                  getString(R.string.are_you_with_seller_desc), getString(R.string.yes_with_seller), getString(R.string.cancel), new OnOkClicked() {
                      @Override
                      public void onOkClicked(Boolean isPositiveBtnClick) {

                          ////-----------------------------------COMMENTED FOR NOW-----------------------------

                          *//*if (otherUserData != null && !TextUtils.isEmpty(otherUserData.getId())) {
                              NewPaymentFragment newPaymentFragment = new NewPaymentFragment();
                              FragmentManager manager = getSupportFragmentManager();
                              newPaymentFragment.setData(otherUserData);
                              FragmentTransaction transaction = manager.beginTransaction();
                              transaction.setCustomAnimations(R.anim.slide_in_bottom, R.anim.slide_out_top, R.anim.slide_in_top, R.anim.slide_out_bottom);
                              transaction.replace(R.id.fragment_container, newPaymentFragment, "newPaymentFragment");
                              transaction.addToBackStack("newPaymentFragment");
                              transaction.commit();
                          }else {
                              showToast("Please try again", ChatMessages.this);
                              getOtherUserData();
                          }*//*
                          ///-----------------------------------COMMENTED FOR NOW-----------------------------
                      }
                  }));*/
        gallery_icon.setOnClickListener(v -> {
            if (EasyPermissions.hasPermissions(ChatMessages.this, permissionsGALLERY)) {
                type="image";
                pickFileSingle("image");
            } else {
                EasyPermissions.requestPermissions(ChatMessages.this, getResources().getString(R.string.please_allow_permissions),
                        1003, permissionsGALLERY);
            }
            //   choosephoto();
        });
        camera_icon.setOnClickListener(v -> {
            if (EasyPermissions.hasPermissions(ChatMessages.this, permissionsCAMERA)) {
                openCamera();
            } else {
                EasyPermissions.requestPermissions(ChatMessages.this, getResources().getString(R.string.please_allow_permissions),
                        1002, permissionsCAMERA);
            }
        });

        pdf_icon.setOnClickListener(v -> {
            if (EasyPermissions.hasPermissions(ChatMessages.this, permissionsGALLERY)) {
                type="pdf";
                pickFileSingle("pdf");
            } else {
                EasyPermissions.requestPermissions(ChatMessages.this, getResources().getString(R.string.please_allow_permissions),
                        1003, permissionsGALLERY);
            }

        });

        loc_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (EasyPermissions.hasPermissions(ChatMessages.this, permissionsArrayLocation)) {
                    getLocation(true);
                } else {
                    EasyPermissions.requestPermissions(ChatMessages.this, getResources().getString(R.string.please_allow_permissions),
                            101, permissionsArrayLocation);
                }
            }
        });

        CollectionReference chatroomsCollection = mDb
                .collection("Conversations").document(other_id).collection("Messages");

        Query query = chatroomsCollection.orderBy("timestamp", Query.Direction.ASCENDING);
         listenerRegistration = query.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (queryDocumentSnapshots.getDocuments().size()==0){
                    progressDialog.dismiss();
                }

                for (int i = 0; i < queryDocumentSnapshots.getDocuments().size(); i++) {
                    if (isFirstTime) {

                        Messages messages = new Messages();
                        messages.setContentType(Integer.parseInt(queryDocumentSnapshots.getDocuments().get(i).getData().get("contentType").toString()));
                        messages.setTimestamp(Integer.parseInt(queryDocumentSnapshots.getDocuments().get(i).getData().get("timestamp").toString()));
                        content_type.add(messages.getContentType().toString());
                        if (messages.getTimestamp() != null){
                            messageTime.add(messages.getTimestamp().toString());
                        }else {
                            messageTime.add("");
                        }
                        if (messages.getContentType()==0){


                            if (queryDocumentSnapshots.getDocuments().get(i).getData().get("ownerID").equals(id)) {
                                own_msg.add(queryDocumentSnapshots.getDocuments().get(i).getData().get("message").toString());
                                frnd_msg.add("");


                            } else {
                                //if (queryDocumentSnapshots.getDocuments().get(i).getData().get("message") != null) {
                                frnd_msg.add(queryDocumentSnapshots.getDocuments().get(i).getData().get("message").toString());
                                own_msg.add("");
                            }
                            frnd_img.add("");
                            own_img.add("");
                            fnd_content.add("");
                            own_content.add("");
                            lat.add("");
                            lng.add("");
                            frnd_pdf.add("");
                            own_pdf.add("");



                        }else if (messages.getContentType()==1){

                            // if (queryDocumentSnapshots.getDocuments().get(i).getData().get("profilePicLink")!=null&& !queryDocumentSnapshots.getDocuments().get(i).getData().get("profilePicLink").toString().equals("")){
                            if (queryDocumentSnapshots.getDocuments().get(i).getData().get("ownerID").equals(id)) {
                                try {
                                    own_img.add(queryDocumentSnapshots.getDocuments().get(i).getData().get("profilePicLink").toString());
                                }catch (Exception e1){
                                    own_img.add("");
                                }

                                frnd_img.add("");


                            }else {
                                try {
                                    frnd_img.add(queryDocumentSnapshots.getDocuments().get(i).getData().get("profilePicLink").toString());
                                }catch (Exception e1){
                                    frnd_img.add("");
                                }

                                own_img.add("");

                            }
                            own_msg.add("");
                            frnd_msg.add("");
                            fnd_content.add("");
                            own_content.add("");
                            lat.add("");
                            lng.add("");
                            frnd_pdf.add("");
                            own_pdf.add("");


                        }else if (messages.getContentType()==2) {


                            // if (queryDocumentSnapshots.getDocuments().get(i).getData().get("content")!=null && !queryDocumentSnapshots.getDocuments().get(i).getData().get("content").toString().equals("")){
                            if (queryDocumentSnapshots.getDocuments().get(i).getData().get("ownerID").equals(id)) {
                                own_content.add(queryDocumentSnapshots.getDocuments().get(i).getData().get("content").toString());
                                fnd_content.add("");
                            } else {
                                fnd_content.add(queryDocumentSnapshots.getDocuments().get(i).getData().get("content").toString());
                                own_content.add("");
                            }
                            own_msg.add("");
                            frnd_msg.add("");
                            frnd_img.add("");
                            own_img.add("");
                            frnd_pdf.add("");
                            own_pdf.add("");

                            if (!queryDocumentSnapshots.getDocuments().get(i).getData().get("content").toString().equals("")){
                                String latlng= queryDocumentSnapshots.getDocuments().get(i).getData().get("content").toString();
                                String lats= latlng.substring(0,latlng.indexOf(":"));
                                String lngs= latlng.substring(latlng.indexOf(":"));
                                lat.add(lats);
                                lng.add(lngs);

                            }else {
                                lat.add("");
                                lng.add("");
                            }

                        }else if (messages.getContentType()==3){


                            if (queryDocumentSnapshots.getDocuments().get(i).getData().get("ownerID").equals(id)) {
                                try {
                                    own_pdf.add(queryDocumentSnapshots.getDocuments().get(i).getData().get("profilePicLink").toString());
                                }catch (Exception e1){
                                    own_pdf.add("");
                                }

                                frnd_pdf.add("");


                            }else {
                                try {
                                    frnd_pdf.add(queryDocumentSnapshots.getDocuments().get(i).getData().get("profilePicLink").toString());
                                }catch (Exception e1){
                                    frnd_pdf.add("");
                                }

                                own_pdf.add("");

                            }
                            own_msg.add("");
                            frnd_msg.add("");
                            frnd_img.add("");
                            own_img.add("");
                            fnd_content.add("");
                            own_content.add("");
                            lat.add("");
                            lng.add("");
                        }




                        idOfMsg.add(queryDocumentSnapshots.getDocuments().get(i).getData().get("id").toString());





                        if (i == queryDocumentSnapshots.getDocuments().size() - 1) {
                            initiRecyclerView(i);
                            isFirstTime=false;
                        }
                    }else {
                        if (!idOfMsg.contains(queryDocumentSnapshots.getDocuments().get(i).getData().get("id").toString())){
                            idOfMsg.add(queryDocumentSnapshots.getDocuments().get(i).getData().get("id").toString());


                            Messages messages = new Messages();
                            messages.setContentType(Integer.parseInt(queryDocumentSnapshots.getDocuments().get(i).getData().get("contentType").toString()));
                            messages.setTimestamp(Integer.parseInt(queryDocumentSnapshots.getDocuments().get(i).getData().get("timestamp").toString()));
                            content_type.add(messages.getContentType().toString());
                            messageTime.add(messages.getTimestamp().toString());
                            if (messages.getContentType()==0){


                                if (queryDocumentSnapshots.getDocuments().get(i).getData().get("ownerID").equals(id)) {
                                    own_msg.add(queryDocumentSnapshots.getDocuments().get(i).getData().get("message").toString());
                                    frnd_msg.add("");


                                } else {
                                    //if (queryDocumentSnapshots.getDocuments().get(i).getData().get("message") != null) {
                                    frnd_msg.add(queryDocumentSnapshots.getDocuments().get(i).getData().get("message").toString());
                                    own_msg.add("");
                                }
                                frnd_img.add("");
                                own_img.add("");
                                fnd_content.add("");
                                own_content.add("");
                                lat.add("");
                                lng.add("");
                                frnd_pdf.add("");
                                own_pdf.add("");



                            }else if (messages.getContentType()==1){

                                // if (queryDocumentSnapshots.getDocuments().get(i).getData().get("profilePicLink")!=null&& !queryDocumentSnapshots.getDocuments().get(i).getData().get("profilePicLink").toString().equals("")){
                                if (queryDocumentSnapshots.getDocuments().get(i).getData().get("ownerID").equals(id)) {
                                    try {
                                        own_img.add(queryDocumentSnapshots.getDocuments().get(i).getData().get("profilePicLink").toString());
                                    }catch (Exception e1){
                                        own_img.add("");
                                    }
                                    frnd_img.add("");


                                }else {
                                    try {
                                        frnd_img.add(queryDocumentSnapshots.getDocuments().get(i).getData().get("profilePicLink").toString());
                                    }catch (Exception f){
                                        frnd_img.add("");
                                    }
                                    own_img.add("");

                                }
                                own_msg.add("");
                                frnd_msg.add("");
                                fnd_content.add("");
                                own_content.add("");
                                lat.add("");
                                lng.add("");
                                frnd_pdf.add("");
                                own_pdf.add("");


                            }else if (messages.getContentType()==2) {


                                // if (queryDocumentSnapshots.getDocuments().get(i).getData().get("content")!=null && !queryDocumentSnapshots.getDocuments().get(i).getData().get("content").toString().equals("")){
                                if (queryDocumentSnapshots.getDocuments().get(i).getData().get("ownerID").equals(id)) {
                                    own_content.add(queryDocumentSnapshots.getDocuments().get(i).getData().get("content").toString());
                                    fnd_content.add("");
                                } else {
                                    fnd_content.add(queryDocumentSnapshots.getDocuments().get(i).getData().get("content").toString());
                                    own_content.add("");
                                }
                                own_msg.add("");
                                frnd_msg.add("");
                                frnd_img.add("");
                                own_img.add("");
                                frnd_pdf.add("");
                                own_pdf.add("");
                                if (!queryDocumentSnapshots.getDocuments().get(i).getData().get("content").toString().equals("")){
                                    String latlng= queryDocumentSnapshots.getDocuments().get(i).getData().get("content").toString();
                                    String lats= latlng.substring(0,latlng.indexOf(":")).replace(":","");
                                    String lngs= latlng.substring(latlng.indexOf(":")).replace(":","");
                                    lat.add(lats);
                                    lng.add(lngs);

                                }else {
                                    lat.add("");
                                    lng.add("");
                                }

                            }else if (messages.getContentType()==3){


                                if (queryDocumentSnapshots.getDocuments().get(i).getData().get("ownerID").equals(id)) {
                                    try {
                                        own_pdf.add(queryDocumentSnapshots.getDocuments().get(i).getData().get("profilePicLink").toString());
                                    }catch (Exception e1){
                                        own_pdf.add("");
                                    }

                                    frnd_pdf.add("");


                                }else {
                                    try {
                                        frnd_pdf.add(queryDocumentSnapshots.getDocuments().get(i).getData().get("profilePicLink").toString());
                                    }catch (Exception e1){
                                        frnd_pdf.add("");
                                    }

                                    own_pdf.add("");

                                }
                                own_msg.add("");
                                frnd_msg.add("");
                                frnd_img.add("");
                                own_img.add("");
                                fnd_content.add("");
                                own_content.add("");
                                lat.add("");
                                lng.add("");
                            }



                            if (i == queryDocumentSnapshots.getDocuments().size() - 1) {
                                messageAdapter.notifyItemInserted(i);
                                recyclerView.smoothScrollToPosition(i);
                            }
                        }
                        database.child(other_id).child(preferenceManager.getUserDetails().getId()).setValue(System.currentTimeMillis()/1000);
                    }
                }


            }
        });

        editWriteMessage.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {

            }
        });

        editWriteMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d("TAG", "onFocusChange: "+ "true");
                gallery_icon.setVisibility(View.GONE);
                camera_icon.setVisibility(View.GONE);
                pdf_icon.setVisibility(View.GONE);
                attachment_icon_asset.setVisibility(View.VISIBLE);
                loc_icon.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editWriteMessage.getText().length()>0){
                    btnSendText=editWriteMessage.getText().toString();
                    sendChat(btnSendText);
                }
            }
        });

        meetup_loc.setOnClickListener(view -> {
            meetup_loc.setVisibility(View.GONE);
            showNormalDialog(ChatMessages.this, getString(R.string.tips_for_chat), getString(R.string.tips_for_chat_description), getString(R.string.mark_guidelines), getString(R.string.got_it), new OnOkClicked() {
                @Override
                public void onOkClicked(Boolean isPositiveBtnClick) {
                    int count = 0;
                    HashMap<String, Integer> hashMap = preferenceManager.getTipsCountPerChat();
                    if (hashMap == null) {
                        hashMap = new HashMap<>();
                    }
                    if (hashMap.containsKey(other_user_id+product_id)) count = hashMap.get(other_user_id+product_id);
                    hashMap.put(other_user_id+product_id, ++count);
                    preferenceManager.setTipsCountPerChat(hashMap);
                    if (!isPositiveBtnClick) {
                        openUrl(StringsConstant.Market_Guidelines, ChatMessages.this, getString(R.string.marketGuideLines));
                    }

                }
            });
        });

        viewImageWithFrag = new CounterFragment(this);
        updateChatUser();
    }


    private void showTipsDialog() {
        meetup_loc.setVisibility(View.VISIBLE);
    }

    private void comingSoonDialog() {
        androidx.appcompat.app.AlertDialog testDialog=null;
        androidx.appcompat.app.AlertDialog.Builder alertDialog = new androidx.appcompat.app.AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View add_menu_layout = inflater.inflate(R.layout.coming_soon, null);
        TextView okBtn = add_menu_layout.findViewById(R.id.okBtn);
        alertDialog.setView(add_menu_layout);
        alertDialog.setView(add_menu_layout);
        alertDialog.setCancelable(false);
        testDialog = alertDialog.create();
       // testDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        androidx.appcompat.app.AlertDialog finalTestDialog = testDialog;
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finalTestDialog.dismiss();
            }
        });
        testDialog.show();
    }

   /* private void getOtherUserData() {
        RestClient.getApiInterface().getUsersList(other_user_id, preferenceManager.getUserDetails().getToken()).enqueue(new Callback<UsersArrayModel>() {
            @Override
            public void onResponse(Call<UsersArrayModel> call, Response<UsersArrayModel> response) {
                if (response.body()!=null && response.body().getData() != null && response.body().getData().size()>0){
                    otherUserData = response.body().getData().get(0);
                }
            }

            @Override
            public void onFailure(Call<UsersArrayModel> call, Throwable t) {

            }
        });
    }*/

    private void checkOnlineStatus() {
        database.child("online").child(other_user_id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    base_header.setText(getString(R.string.available));
                }else {
                    base_header.setText(getString(R.string.not_available));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void fetchProduct() {
        RestClient.getApiInterface().getProductDetails(product_id,preferenceManager.getUserDetails().getToken()).enqueue(new Callback<ProductModel>() {
            @Override
            public void onResponse(Call<ProductModel> call, Response<ProductModel> response) {
                if (response.body()!=null && response.body().getStatus()==200 && response.body().getData()!=null && response.body().getData().size()>0) {
                    try {
                        Common.selectedProductDetails = response.body().getData().get(0);
                        setVisiblityOfProductData();
                        if (response.body().getData() != null && response.body().getData().get(0).getImages() != null && response.body().getData().get(0).getImages().size() > 0) {
                            Glide.with(ChatMessages.this).load(response.body().getData().get(0).getImages().get(0).getImageUrl()).placeholder(R.drawable.product_placeholder).error(R.drawable.product_placeholder)
                                    // .apply(RequestOptions.bitmapTransform(new RoundedCorners(20)))
                                    .into(prodImg);
                            float radius = 20;
                        }
                        prductData = response.body().getData().get(0);
                        prodName.setText(response.body().getData().get(0).getProductTitle());
                        prodPrice.setText(Common.currencySymbol+ response.body().getData().get(0).getProductPrice());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ProductModel> call, Throwable t) {
                prodLayout.setVisibility(View.GONE);
            }
        });
    }

    private void setVisiblityOfProductData(){
        if (Common.selectedProductDetails != null){
            prodLayout.setVisibility(View.VISIBLE);
        }else  prodLayout.setVisibility(View.GONE);
        if (Common.selectedProductDetails.getUploadedByUserId().getId().equals(product_id)){
            paySeller.setVisibility(View.GONE);
        } else {
            paySeller.setVisibility(View.VISIBLE);
        }
    }

    private void updateRecyclerViewTime() {
        database.child(other_id).child(other_user_id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    try {
                        lastTime.setValue((BigDecimal.valueOf(Double.parseDouble(snapshot.getValue().toString())).intValue())+"");
                        messageAdapter.updateSeenStatus(lastTime.getValue());
                    }catch (NumberFormatException numberFormatException) {
                        numberFormatException.printStackTrace();
                    }catch (Exception e) {e.printStackTrace();}

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void sendChat(String btnSendText) {
        editWriteMessage.setText("");
        String android_id = getSaltString();

        Long tsLong = System.currentTimeMillis()/1000;
        String timestamp = tsLong.toString();
        Messages messages=new Messages();
        messages.setContentType(0);
        messages.setMessage(btnSendText);
        messages.setTimestamp(Integer.parseInt(timestamp));
        messages.setOwnerID(id);
        messages.setId(android_id);
        messages.setContent("");

        //sendNotification(btnSendText);
        DocumentReference newChatroomRefs = mDb
                .collection("Conversations").document(other_id).collection("Messages")
                .document(android_id);

        newChatroomRefs.set(messages).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    Log.d("TAG", "onComplete: ."+task.isSuccessful());
                    updateBuildChatRoom("no");
                    //  messageAdapter.notifyItemInserted();


                }
            }
        });
    }

    private void setProdData() {
        product_id = Common.selectedProductDetails.getId();
        prductData = Common.selectedProductDetails;
        setVisiblityOfProductData();
        if (Common.selectedProductDetails.getImages() !=null && Common.selectedProductDetails.getImages().size()>0) {
            Glide.with(this).load(Common.selectedProductDetails.getImages().get(0).getImageUrl()).placeholder(R.drawable.product_placeholder).error(R.drawable.product_placeholder)
                    .apply(RequestOptions.bitmapTransform(new RoundedCorners(20)))
                    .into(prodImg);
        }
        prodName.setText(Common.selectedProductDetails.getProductTitle());
        prodPrice.setText(Common.currencySymbol+Common.selectedProductDetails.getProductPrice());
    }

    private void pickFileSingle(String type) {
        filePicker = getFilePicker();
        if (type.equals("pdf")) {
            filePicker.setMimeType("application/pdf");
        }else {
            filePicker.setMimeType("image/*");
        }
        filePicker.pickFile();
    }
    private FilePicker getFilePicker() {
        filePicker = new FilePicker(this);
        filePicker.setFilePickerCallback(ChatMessages.this);
        filePicker.setCacheLocation(PickerUtils.getSavedCacheLocation(this));
        return filePicker;
    }



    private void getLocation(Boolean status) {
        checkGpsOn(status);
    }

    private void checkGpsOn(Boolean status) {

        // ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        LocationManager locationManager = (LocationManager) ChatMessages.this.getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            displayLocationSettingsRequest(ChatMessages.this);


        } else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            try {

                gpsLocation = appLocationService
                        .getLocation(LocationManager.GPS_PROVIDER);
                if (gpsLocation==null){
                    gpsLocation = appLocationService
                            .getLocation(LocationManager.NETWORK_PROVIDER);
                }
                if (gpsLocation==null){
                    gpsLocation = appLocationService
                            .getLocation(LocationManager.PASSIVE_PROVIDER);
                }

                if (gpsLocation!=null){
                    if (status) {
                        startActivityForResult(new Intent(this,RequestLocation.class),OPEN_REQUEST_LOCATION);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void sendLocation(String lat, String lon) {

        String android_id = getSaltString();

        Long tsLong = System.currentTimeMillis()/1000;
        String timestamp = tsLong.toString();
        Messages messages=new Messages();
        messages.setContentType(2);
        messages.setMessage("");
        messages.setTimestamp(Integer.parseInt(timestamp));
        messages.setOwnerID(id);
        messages.setId(android_id);
        messages.setContent(lat+":"+lon);

        DocumentReference newChatroomRefss = mDb
                .collection("Conversations").document(other_id).collection("Messages")
                .document(android_id);

        newChatroomRefss.set(messages).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    Log.d("TAG", "onComplete: ."+task.isSuccessful());
                    sendNotification("sent you a request for a Meet-Up Location. Tap to view.");
                    updateBuildChatRoom("img");
                    //  messageAdapter.notifyItemInserted();


                }
            }
        });

    }


    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i("TAG1", "All location settings are satisfied.");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i("TAG2", "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(ChatMessages.this, 1232);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i("TAG3", "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i("Tag4", "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }


    private void choosephoto() {
        //Intent intent = new Intent();
        /*Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST_GALLERY);*/
        // startActivityForResult(new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI), PICK_IMAGE_REQUEST_GALLERY);
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST_GALLERY);


    }

    private void openCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Uri outputFileUri = Uri.fromFile(BaseActivity.setCamerFilePath());
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        cameraIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        cameraIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        resultLauncher.launch(cameraIntent);
    }

    ActivityResultLauncher<Intent> resultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        File file1 = null;
                        try {
                            File files= new File(BaseActivity.getCameraFilePathName());
                            file1 = getCompressed(ChatMessages.this, files.getPath());
                            RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file1);
                            fileToUpload = MultipartBody.Part.createFormData("image", file1.getName(), mFile);
                            uploadToServer("");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });



    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void updateBuildChatRoom(String img) {

        String s="";
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .build();
        mDb.setFirestoreSettings(settings);
        /*if (Integer.parseInt(Common.userDataModel.getData().get(0).getId())<Integer.parseInt(other_id)){
            s=Common.userDataModel.getData().get(0).getId()+"-"+other_id;
        }else {
            s= other_id+"-"+Common.userDataModel.getData().get(0).getId();
        }*/
        s=other_id;

        Long tsLong = System.currentTimeMillis()/1000;
        String timestamp = tsLong.toString();

        HashMap<String,Boolean> isReadContent=new HashMap<>();
        isReadContent.put(id,false);
        isReadContent.put(preferenceManager.getUserDetails().getId(),true);

        ArrayList<String> as=new ArrayList<>();
        as.add(id);
        as.add(preferenceManager.getUserDetails().getId());
      /* // ChatBoxUpdate chatBox=new ChatBoxUpdate();
        chatBox.setTimestamp(Integer.parseInt(timestamp));
        chatBox.setLastMessage(btnSendText);
        chatBox.setIsRead(isReadContent);*/

        HashMap<String, Object> hs=new HashMap<>();
        //hs.put("timestamp",timestamp);
        if (img.equals("img")) {
            hs.put("lastMessage", "Attachment");
            sendNotification("Sent you an image");
        }else if (!btnSendText.isEmpty()){
            hs.put("lastMessage", btnSendText);
            sendNotification(btnSendText);
        } else if (!offerPrice.isEmpty()) {
            hs.put("lastMessage",  getString(R.string.offer_sent_text)+" "+offerPrice);
            sendNotification("asked you a question regarding your "+prductData.getProductTitle());
        } else {
            hs.put("lastMessage", "");
        }
        hs.put("isRead",isReadContent);

        DocumentReference newChatroomRefsss = mDb
                .collection("Conversations")
                .document(s);
        newChatroomRefsss.update(hs).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){

                }
            }
        });
        newChatroomRefsss.update("timestamp",Integer.parseInt(timestamp));

    }

    protected String getSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    private void initiRecyclerView(int position) {
        if (lastTime.getValue() == null) lastTime.setValue("");
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this);
        messageAdapter=new MessageAdapter(ChatMessages.this,prof_img,frnd_msg,own_msg,frnd_img,own_img,fnd_content,own_content,lat,lng,content_type,frnd_pdf,own_pdf,messageTime,lastTime.getValue(), viewImageWithFrag);
        recyclerView.setAdapter(messageAdapter);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.scrollToPosition(position);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setDateToDateString(linearLayoutManager.findFirstVisibleItemPosition());
            }
        },500);

        progressDialog.dismiss();
        if (TextUtils.isEmpty(lastTime.getValue()))
         updateRecyclerViewTime();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    int pos = linearLayoutManager.findFirstVisibleItemPosition();
                    setDateToDateString(pos);
                }
            }
        });


    }

    private void setDateToDateString(int pos) {
        try {
            Date date = new Date(Long.parseLong(messageTime.get(pos))*1000L); // *1000 is to convert seconds to milliseconds
            Calendar cal = Calendar.getInstance();
            TimeZone tz = cal.getTimeZone();
            Log.d("Time zone: ", tz.getDisplayName());
            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy "); // the format of your date
            SimpleDateFormat sdftime = new SimpleDateFormat("HH:mm a"); // the format of your date
            sdf.setTimeZone(tz);
            sdftime.setTimeZone(tz);
            String date2 = sdf.format(date);
            System.out.println("Current time => " + date2);
            dateText.setVisibility(View.VISIBLE);
            dateTextLayout.setVisibility(View.VISIBLE);
            dateText.setText(date2);
        }catch (Exception e){
            e.printStackTrace();
            dateText.setVisibility(View.GONE);
            dateTextLayout.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Picker.PICK_FILE && resultCode == RESULT_OK) {
            filePicker.submit(data);
        }else  if (requestCode == OPEN_REQUEST_LOCATION) {
            if(resultCode == RESULT_OK) {
                String latitude = data.getStringExtra("latitude");
                String longitude = data.getStringExtra("longitude");
                sendLocation(latitude,longitude);
            }
        }
    }



    private void uploadToServer(String type) {
        progressDialog.setMessage(getString(R.string.uploading));
        progressDialog.show();
        int content_type=1;
        int number_of_image=1;
        if (type.equals("")) {
            content_type=1;
            //sendNotification(getResources().getString(R.string.sent_you_an_image));
        }else if (type.equals("pdf")){
            content_type=3;
            number_of_image=2;
           // sendNotification(getResources().getString(R.string.sent_you_an_file));
        }
        final int finalContent_type = content_type;
        RequestBody number;
        number = RequestBody.create(MediaType.parse("plain/text"), number_of_image+"");
        RestClient.getApiInterface().uploadRandomImage(fileToUpload).enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Log.d("TAG", "onResponse: "+response.body());
                String imgurl= ((LinkedTreeMap) response.body()).get("data").toString();
                sendMsg(imgurl, finalContent_type);
                progressDialog.dismiss();

            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.d("TAG", "onFailure: "+t.getMessage());
                progressDialog.dismiss();

            }
        });
    }

    private void updateChatUser() {
        RestClient.getApiInterface().updateChatUser(preferenceManager.getUserDetails().getToken(), other_id, other_user_id, preferenceManager.getUserDetails().getId(), product_id).enqueue(new Callback<ApiStatusModel>() {
            @Override
            public void onResponse(Call<ApiStatusModel> call, Response<ApiStatusModel> response) {

            }

            @Override
            public void onFailure(Call<ApiStatusModel> call, Throwable t) {

            }
        });
    }


    private void sendMsg(String imgurl, int content_type) {
        String android_id = getSaltString();

        Long tsLong = System.currentTimeMillis()/1000;
        String timestamp = tsLong.toString();
        Messages messages=new Messages();
        messages.setContentType(content_type);
        messages.setMessage("");
        messages.setProfilePicLink(imgurl);
        messages.setTimestamp(Integer.parseInt(timestamp));
        messages.setOwnerID(id);
        messages.setId(android_id);
        messages.setContent("");


        DocumentReference newChatroomRef = mDb
                .collection("Conversations").document(other_id).collection("Messages")
                .document(android_id);

        newChatroomRef.set(messages).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    Log.d("TAG", "onComplete: ."+task.isSuccessful());
                    long time  = System.currentTimeMillis()/1000;
                    if (!TextUtils.isEmpty(lastTime.getValue()) && (tsLong - Long.parseLong(lastTime.getValue()) <6) && !TextUtils.isEmpty(other_id) && !TextUtils.isEmpty(other_user_id)){
                        database.child(other_id).child(other_user_id).setValue(time);
                    }
                    updateBuildChatRoom("img");
                    progressDialog.dismiss();

                    //  messageAdapter.notifyItemInserted();


                }
            }
        });
    }

    private void sendNotification(String body) {
        RestClient.getApiInterface().sendNotificationFromWeb(preferenceManager.getUserDetails().getFullName(),body,
                "chat", other_user_pushtoken, other_id, product_id, preferenceManager.getUserDetails().getId(), other_user_id).enqueue(new Callback<ApiStatusModel>() {
            @Override
            public void onResponse(Call<ApiStatusModel> call, Response<ApiStatusModel> response) {

            }

            @Override
            public void onFailure(Call<ApiStatusModel> call, Throwable t) {

            }
        });
    }

    @Override
    public void itemClicked(int position,String out) {
        Intent intent = new Intent(this, ChatMap.class);
        if (gpsLocation!=null) {
            if (out.equals("out")) {

                ArrayList<String> nearbyLat = new ArrayList<>();
                ArrayList<String> nearbyLng = new ArrayList<>();
                ArrayList<String> nearbyName = new ArrayList<>();
                nearbyLat.add(lat.get(position) + "");
                nearbyLng.add(lng.get(position) + "");
                nearbyName.add("");

                intent.putStringArrayListExtra("latdata", nearbyLat);
                intent.putStringArrayListExtra("lngdata", nearbyLng);
                intent.putStringArrayListExtra("name", nearbyName);
                intent.putExtra("frommap","yes");
                startActivity(intent);


            } else {
                ArrayList<String> nearbyLat = new ArrayList<>();
                ArrayList<String> nearbyLng = new ArrayList<>();
                ArrayList<String> nearbyName = new ArrayList<>();
                nearbyLat.add(lat.get(position) + "");
                nearbyLng.add(lng.get(position) + "");
                nearbyName.add("");

                intent.putStringArrayListExtra("latdata", nearbyLat);
                intent.putStringArrayListExtra("lngdata", nearbyLng);
                intent.putStringArrayListExtra("name", nearbyName);
                intent.putExtra("frommap","yes");
                startActivity(intent);
            }
        }else {
            Toast.makeText(ChatMessages.this, getResources().getString(R.string.your_location_not_enabled), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onProfileClicked(int position) {
        navigateToUserProfile();
    }

    private void navigateToUserProfile() {
        ProfileFrag profileFrag = new ProfileFrag();
        FragmentManager manager = getSupportFragmentManager();
        profileFrag.setUserDetails(other_user_id);
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_bottom, R.anim.slide_out_top, R.anim.slide_in_top, R.anim.slide_out_bottom);
        transaction.replace(R.id.fragment_container, profileFrag, "profileFrag");
        transaction.addToBackStack("profileFrag");
        transaction.commit();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        if (meetup_loc.getVisibility() == View.VISIBLE){
            meetup_loc.setVisibility(View.GONE);
        }else if (isCameFromNotifcation) {
            Intent intent = new Intent(ChatMessages.this, HomeActivity.class);
            startActivity(intent);
            finish();
        } else if (viewImageWithFrag.isVisible()){
            viewImageWithFrag.dismiss();
        }else {
            finish();
        }
    }

    @Override
    public void onFilesChosen(List<ChosenFile> list) {
        if (list.size()>0) {
            File file1 = new File(list.get(0).getOriginalPath());
//          uploadFile.setText(list.get(0).getOriginalPath());

                //
                try {
                    File file2 = new File(list.get(0).getOriginalPath());
                    File file3 = getCompressed(ChatMessages.this, file2.getPath());
                    RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file3);
                    fileToUpload = MultipartBody.Part.createFormData("image", file3.getName(), mFile);
                    uploadToServer("");
                    //  showDefaultImg();
                    //  removeDefaultImg();
                }catch (Exception e){
                    progressDialog.dismiss();
                    Toast.makeText(ChatMessages.this, getResources().getString(R.string.cant_upload_image), Toast.LENGTH_SHORT).show();
                    e.getMessage();
                }
        }

    }


    private  Bitmap pdfToBitmap(File pdfFile) {
        Bitmap bitmaps = null;

        try {
            PdfRenderer renderer = new PdfRenderer(ParcelFileDescriptor.open(pdfFile, ParcelFileDescriptor.MODE_READ_ONLY));
            Bitmap bitmap;
            final int pageCount = renderer.getPageCount();
            for (int i = 0; i < pageCount; i++) {
                PdfRenderer.Page page = renderer.openPage(i);

                int width = getResources().getDisplayMetrics().densityDpi / 72 * page.getWidth();
                int height = getResources().getDisplayMetrics().densityDpi / 72 * page.getHeight();
                bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                bitmap.eraseColor(Color.WHITE);

                page.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);

                bitmaps=bitmap;
                // close the page
                page.close();
                break;

            }

            // close the renderer
            renderer.close();
        } catch (Exception ex) {
            ex.printStackTrace();

        }

        return bitmaps;

    }

    @Override
    public void onError(String s) {

    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 101) {
            if (grantResults.length > 0) {
                if (EasyPermissions.hasPermissions(ChatMessages.this, permissionsArrayLocation)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        //getLatitudeLongitude();
                      //  sendNotification(getResources().getString(R.string.sent_you_his_location));
                        getLocation(true);
                    }
                } else {
                    showPermissionRequestDialog(getString(R.string.network_alert_lcoation),getString(R.string.do_u_wish_to_enable));
                }
            }
        }else if (requestCode==1002){
            if (EasyPermissions.hasPermissions(ChatMessages.this, permissionsCAMERA)) {
                openCamera();
            }else {
                showPermissionRequestDialog(getString(R.string.network_alert_camera),getString(R.string.do_u_wish_to_image));
            }


        }else if (requestCode==1003){
            if (EasyPermissions.hasPermissions(ChatMessages.this, permissionsGALLERY)) {
                if (type.equals("pdf")){
                    pickFileSingle("pdf");
                }else {
                    pickFileSingle("image");
                }
            }else {
                showPermissionRequestDialog(getString(R.string.network_alert_gallery),getString(R.string.do_u_wish_to_image));
            }


        }
    }

    public void showPermissionRequestDialog(String header_ones,String header_twos){

        final AlertDialog.Builder alertDialoga = new AlertDialog.Builder(ChatMessages.this);
        LayoutInflater inflater = ChatMessages.this.getLayoutInflater();
        View add_menu_layout = inflater.inflate(R.layout.item_text_alert, null);
        Button button_back=add_menu_layout.findViewById(R.id.button_back);
        Button button_allow=add_menu_layout.findViewById(R.id.button_allow);
        TextView headerOne=add_menu_layout.findViewById(R.id.headerOne);
        TextView headerTwo=add_menu_layout.findViewById(R.id.headerTwo);
        headerOne.setText(header_ones);
        headerTwo.setText(header_twos);
        alertDialoga.setView(add_menu_layout);
        alertDialoga.setCancelable(false);
        alertDialogPermission = alertDialoga.show();
        button_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogPermission.dismiss();
                // finish();
            }
        });
        button_allow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogPermission.dismiss();
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivityForResult(intent,2000);
            }
        });

    }

    @Override
    protected void onDestroy() {
        Common.isChatScreenOpened = false;
        if (listenerRegistration!=null)listenerRegistration.remove();
        super.onDestroy();
    }

    @Override
    public void sendCounterOffer(@NotNull String counterText) {
        offerPrice = Common.currencySymbol+counterText;
        sendChat(getString(R.string.counter_offer_sent_text)+" "+offerPrice);
    }
}