package com.bingalollc.com.chat.model;

public class Messages {
    private Integer contentType;
    private String id;
    private String ownerID;
    private String profilePicLink;
    private Integer timestamp;
    private String message;
    private String content;

    public Messages() {
    }

    public Messages(Integer contentType, String id, String ownerID, String profilePicLink, Integer timestamp, String message, String content) {
        this.contentType = contentType;
        this.id = id;
        this.ownerID = ownerID;
        this.profilePicLink = profilePicLink;
        this.timestamp = timestamp;
        this.message = message;
        this.content = content;;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getContentType() {
        return contentType;
    }

    public void setContentType(Integer contentType) {
        this.contentType = contentType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwnerID() {
        return ownerID;
    }

    public void setOwnerID(String ownerID) {
        this.ownerID = ownerID;
    }

    public String getProfilePicLink() {
        return profilePicLink;
    }

    public void setProfilePicLink(String profilePicLink) {
        this.profilePicLink = profilePicLink;
    }

    public Integer getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
    }
}
