package com.bingalollc.com.chat;


import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.VideoView;

import com.bingalollc.com.R;
import com.bumptech.glide.Glide;

public class FullScreenImageView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_image_view);
        String url = getIntent().getStringExtra("url");
        String videourl = getIntent().getStringExtra("videourl");
        int isCustomer = Integer.parseInt(getIntent().getStringExtra("cust"));
        ImageView fullImage = findViewById(R.id.fullImage);
        final VideoView videoView = findViewById(R.id.videoView);
        ImageView closeBtn = findViewById(R.id.closeBtn);

        if (url != null) {
            Glide.with(this).load(url).fitCenter().into(fullImage);
        } else if (videourl != null) {
            videoView.setVisibility(View.VISIBLE);
            Uri video = Uri.parse(videourl);
            videoView.setVideoURI(video);
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.setLooping(true);
                    videoView.start();
                }
            });
        }
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
}