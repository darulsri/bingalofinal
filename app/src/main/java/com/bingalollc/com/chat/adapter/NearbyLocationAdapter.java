package com.bingalollc.com.chat.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bingalollc.com.R;
import com.bingalollc.com.preference.PreferenceManager;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class NearbyLocationAdapter extends RecyclerView.Adapter<NearbyLocationAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<String> nameArray;
    private ArrayList<String> descArray;
    private ArrayList<String> distanceArray;
    private ArrayList<String> imageArray;
    private OnOkClicked onOkClicked;
    private int selectedLocation = -1;

    public NearbyLocationAdapter(ArrayList<String> nameArray, ArrayList<String> descArray, ArrayList<String> distanceArray, ArrayList<String> imageArray, OnOkClicked onOkClicked) {
        this.nameArray = nameArray;
        this.descArray = descArray;
        this.distanceArray = distanceArray;
        this.imageArray = imageArray;
        this.onOkClicked = onOkClicked;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.nearby_places_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (nameArray.get(position) != null)
            holder.titleText.setText(nameArray.get(position));
        if (descArray.get(position) != null)
            holder.descText.setText(descArray.get(position));
        if (distanceArray.get(position) != null)
            holder.distanceText.setText(distanceArray.get(position));
        Glide.with(mContext).load(imageArray.get(position)).placeholder(R.drawable.product_placeholder).
                error(R.drawable.product_placeholder).into(holder.imagLayout);
        if (selectedLocation == position) {
            holder.layoutComplete.setBackground(mContext.getDrawable(R.drawable.curved_background_blue));
            holder.titleText.setTextColor(mContext.getColor(R.color.white));
            holder.descText.setTextColor(mContext.getColor(R.color.white));
            holder.distanceText.setTextColor(mContext.getColor(R.color.white));
            holder.sendLocationbtn.setVisibility(View.VISIBLE);
        } else {
            holder.layoutComplete.setBackground(mContext.getDrawable(R.drawable.curved_background_white));
            holder.titleText.setTextColor(mContext.getColor(R.color.black));
            holder.descText.setTextColor(mContext.getColor(R.color.grey_500));
            holder.distanceText.setTextColor(mContext.getColor(R.color.blue_bg));
            holder.sendLocationbtn.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return nameArray.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView titleText, descText, distanceText;
        private CircleImageView imagLayout;
        private ImageView sendLocationbtn;
        private LinearLayout layoutComplete;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            titleText = itemView.findViewById(R.id.titleText);
            sendLocationbtn = itemView.findViewById(R.id.sendLocationbtn);
            descText = itemView.findViewById(R.id.descText);
            distanceText = itemView.findViewById(R.id.distanceText);
            imagLayout = itemView.findViewById(R.id.imagLayout);
            layoutComplete = itemView.findViewById(R.id.layoutComplete);
            layoutComplete.setOnClickListener(view -> {
                int oldPos = selectedLocation;
                selectedLocation = getAdapterPosition();
                onOkClicked.onPosClicked(selectedLocation);
                notifyItemChanged(selectedLocation);
                if (oldPos > -1)
                    notifyItemChanged(oldPos);
            });
            sendLocationbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onOkClicked.onOkClicked(getAdapterPosition());
                }
            });
        }
    }

    public void resetSelectedPos() {
        notifyItemChanged(selectedLocation);
        selectedLocation = -1;
    }

    public void resetAllPos() {
        selectedLocation = -1;
        notifyDataSetChanged();
    }

    public interface OnOkClicked{
        void onOkClicked(int pos);
        void onPosClicked(int pos);
    }
}
