package com.bingalollc.com.chat;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bingalollc.com.R;
import com.bingalollc.com.preference.PreferenceManager;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

public class ChatMap extends AppCompatActivity implements OnMapReadyCallback {
    private static final String TAG = "Map";
    private TextView headerTitle;
    private ImageView backBtn;
    private PreferenceManager preferenceManager;
    private GoogleMap googleMap;
    ArrayList<String> latdata;
    ArrayList<String> lngdata;
    ArrayList<String> mapName;
    private List<LatLng> route;
    Handler etaHandler = new Handler();
    Runnable runnable, etaRunnable;
    int delayToCalculateEta = 15 * 1000;
    private LatLng latlngDes;
    private Marker marker;
    private String ownlat, ownlng;
    LatLngBounds.Builder builder;
    SupportMapFragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_map);
        headerTitle = findViewById(R.id.header);
        //headerTitle2 = findViewById(R.id.headerTitle2);
        backBtn = findViewById(R.id.back_icon);
        headerTitle.setText(getResources().getString(R.string.map));
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        preferenceManager = PreferenceManager.getInstance(this);
        latdata = getIntent().getStringArrayListExtra("latdata");
        lngdata = getIntent().getStringArrayListExtra("lngdata");
        mapName = getIntent().getStringArrayListExtra("name");
       /* mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapView);*/
        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapView);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mapFragment.getMapAsync( ChatMap.this);
            }
        },1000);


    }


    protected Marker createMarker(double latitude, double longitude, String name) {
        LatLng latLng=new LatLng(latitude, longitude);
        return googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .anchor(1.5f, 1.5f)
                .title(name)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pin)));



    }


    @Override
    public void onMapReady(GoogleMap googleMaps) {
        googleMap=googleMaps;
        double lat,lng;
        String name;

        for(int i=0;i<latdata.size();i++) {
            String latti=latdata.get(i).replace(":","");
            String longi=lngdata.get(i).replace(":","");
            if(!latti.equals("") && !longi.equals("") && !latti.equals("null") && !longi.equals("null")) {
                lat = Double.parseDouble(latdata.get(i).replace(":",""));
                lng = Double.parseDouble(lngdata.get(i).replace(":",""));
                Log.d(TAG, "loadMap: " + lat + "   lng  " + lng);
                name = mapName.get(i);
                createMarker(lat, lng,name);
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(createMarker(lat, lng,name).getPosition());
                LatLngBounds bounds = builder.build();
                int padding = 0; // offset from edges of the map in pixels
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                googleMap.moveCamera(cu);
                googleMaps.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Float.parseFloat(latti), Float.parseFloat(longi)),12.0f));

            }
        }



    }
}
