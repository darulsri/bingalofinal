package com.bingalollc.com.maintainance.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Data implements Parcelable {

	@SerializedName("he_maintaince_detail")
	private String heMaintainceDetail;

	@SerializedName("he_maintaince_message")
	private String heMaintainceMessage;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("maintaince_detail")
	private String maintainceDetail;

	@SerializedName("maintaince_message")
	private String maintainceMessage;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private String id;

	@SerializedName("is_maintaince")
	private String isMaintaince;

	protected Data(Parcel in) {
		heMaintainceDetail = in.readString();
		heMaintainceMessage = in.readString();
		updatedAt = in.readString();
		maintainceDetail = in.readString();
		maintainceMessage = in.readString();
		createdAt = in.readString();
		id = in.readString();
		isMaintaince = in.readString();
	}

	public static final Creator<Data> CREATOR = new Creator<Data>() {
		@Override
		public Data createFromParcel(Parcel in) {
			return new Data(in);
		}

		@Override
		public Data[] newArray(int size) {
			return new Data[size];
		}
	};

	public String getHeMaintainceDetail(){
		return heMaintainceDetail;
	}

	public String getHeMaintainceMessage(){
		return heMaintainceMessage;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public String getMaintainceDetail(){
		return maintainceDetail;
	}

	public String getMaintainceMessage(){
		return maintainceMessage;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public String getId(){
		return id;
	}

	public String getIsMaintaince(){
		return isMaintaince;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeString(heMaintainceDetail);
		parcel.writeString(heMaintainceMessage);
		parcel.writeString(updatedAt);
		parcel.writeString(maintainceDetail);
		parcel.writeString(maintainceMessage);
		parcel.writeString(createdAt);
		parcel.writeString(id);
		parcel.writeString(isMaintaince);
	}
}