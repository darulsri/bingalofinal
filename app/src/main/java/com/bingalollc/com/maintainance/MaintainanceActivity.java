package com.bingalollc.com.maintainance;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bingalollc.com.R;
import com.bingalollc.com.base.BaseActivity;
import com.bingalollc.com.maintainance.model.Data;

public class MaintainanceActivity extends BaseActivity {

    Data data;
    TextView title_maintenance, desc_maintenance;
    ImageView logo_maintenance;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.maintainance_layout);

        data = getIntent().getParcelableExtra("maintainanceMode");

        title_maintenance = (TextView) findViewById(R.id.title_maintenance);
        desc_maintenance = (TextView) findViewById(R.id.desc_maintenance);
        logo_maintenance = (ImageView) findViewById(R.id.logo_maintenance);

        if(preferenceManager.getIsEnglish() == 1){
            logo_maintenance.setImageDrawable(getDrawable(R.drawable.login_bengalo_logo));
            title_maintenance.setText(data.getMaintainceMessage());
            desc_maintenance.setText(data.getMaintainceDetail());
        }else{
            logo_maintenance.setImageDrawable(getDrawable(R.drawable.launch_logo_herbew));
            title_maintenance.setText(data.getHeMaintainceMessage());
            desc_maintenance.setText(data.getHeMaintainceDetail());
        }

    }
}
