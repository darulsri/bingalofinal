package com.bingalollc.com.notification;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.bingalollc.com.product_details.RateUser;
import com.bingalollc.com.splash.SplashActivity;
import com.onesignal.OSNotificationOpenedResult;
import com.onesignal.OSNotificationReceivedEvent;
import com.onesignal.OneSignal;

import org.json.JSONException;

public class OneSignalNotificationReceiverTest implements OneSignal.OSRemoteNotificationReceivedHandler, OneSignal.OSNotificationOpenedHandler{

    @Override
    public void notificationOpened(OSNotificationOpenedResult osNotificationOpenedResult) {
        Log.d("notificationTest", "");
    }

    @Override
    public void remoteNotificationReceived(Context context, OSNotificationReceivedEvent osNotificationReceivedEvent) {
        Log.d("notificationTest", "");

    }
}
