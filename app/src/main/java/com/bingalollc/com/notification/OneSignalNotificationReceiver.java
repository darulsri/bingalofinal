package com.bingalollc.com.notification;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.bingalollc.com.AppController;
import com.bingalollc.com.homeactivity.HomeActivity;
import com.bingalollc.com.product_details.RateUser;
import com.bingalollc.com.splash.SplashActivity;
import com.onesignal.OSNotification;
import com.onesignal.OSNotificationOpenedResult;
import com.onesignal.OSNotificationReceivedEvent;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

public class OneSignalNotificationReceiver implements OneSignal.OSRemoteNotificationReceivedHandler, OneSignal.OSNotificationOpenedHandler {
    private Context context;

    public OneSignalNotificationReceiver(Context mContext) {
        context = AppController.getInstance();
    }

    @Override
    public void notificationOpened(OSNotificationOpenedResult result) {
        String click_action="";
        String user_id="";
        String image_url="";
        String friendname="";
        String product_id="";
        String sender_user_id="";
        String receiver_user_id="";
        try {
            if (result.getNotification().getAdditionalData().has("type"))
                click_action= result.getNotification().getAdditionalData().get("type").toString();
            if (result.getNotification().getAdditionalData().has("id"))
                user_id= result.getNotification().getAdditionalData().get("id").toString();
            if (result.getNotification().getAdditionalData().has("profile_pic"))
                image_url= result.getNotification().getAdditionalData().get("profile_pic").toString();
            if (result.getNotification().getAdditionalData().has("title"))
                friendname= result.getNotification().getAdditionalData().get("title").toString();
            if (result.getNotification().getAdditionalData().has("product_id"))
                product_id= result.getNotification().getAdditionalData().get("product_id").toString();
            if (result.getNotification().getAdditionalData().has("sender_user_id"))
                sender_user_id= result.getNotification().getAdditionalData().get("sender_user_id").toString();
            if (result.getNotification().getAdditionalData().has("receiver_user_id"))
                receiver_user_id= result.getNotification().getAdditionalData().get("receiver_user_id").toString();
            System.out.println(">>>>>>>>>>> product_id "+product_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (click_action.contains("chat")){
            Intent intent = new Intent(context, SplashActivity.class);
            intent.putExtra("chat_id",user_id);
            intent.putExtra("user_id",sender_user_id);
            intent.putExtra("notification",true);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);

        }else if (click_action.equals("2")){
            Intent intent = new Intent(context.getApplicationContext(), RateUser.class);
            Bundle bundle = new Bundle();
            bundle.putString("notifcation_from", sender_user_id);
            bundle.putString("click_action", click_action);
            bundle.putString("user_img", image_url);
            bundle.putString("user_name", friendname);
            bundle.putString("product_id", product_id);
            bundle.putBoolean("fromNotificationScreen", true);
            intent.putExtra("data", bundle);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);

        }else if (click_action.equals("3")){
            Intent intent = new Intent(context.getApplicationContext(), SplashActivity.class);
            intent.putExtra("navigate_to_notification_screen", true);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);

        }else if (click_action.equals("-1")){
            Intent intent = new Intent(context.getApplicationContext(), SplashActivity.class);
            intent.putExtra("admin_notification", true);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);

        }else {
            Intent intent = new Intent(context.getApplicationContext(), SplashActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }


    }

    @Override
    public void remoteNotificationReceived(Context context, OSNotificationReceivedEvent osNotificationReceivedEvent) {
        this.context = context;
        System.out.println(">>>>>>>>>>context "+context);
    }

   /* @Override
    public void notificationOpened(OSNotificationOpenResult result) {
        OSNotificationAction.ActionType actionType = result.action.type;
        JSONObject click_action = result.getNotification().getAdditionalData();
        String customKey="";
        try {
             customKey=click_action.getString("click_action");
        } catch (JSONException e) {
            e.printStackTrace();
        }



        Intent resultIntent = new Intent(customKey);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(
                        context,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        Drawable drawable= ContextCompat.getDrawable(context, R.drawable.back_icon);

        Bitmap bitmap = ((BitmapDrawable)drawable).getBitmap();

        //PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "WAZOO")
                .setContentTitle("This is title")
                .setContentText("This is body")
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                //.setLargeIcon(bitmap)
                .setSmallIcon(R.drawable.ic_back)
                .setPriority(NotificationManager.IMPORTANCE_HIGH)
                .setContentIntent(pendingIntent).setVisibility((NotificationCompat.VISIBILITY_PUBLIC));

        //   String title = remoteMessage.getData().get("title");
        // String body = remoteMessage.getData().get("body").replace("+"," ");

        NotificationManager noti = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        // noti.notify(0, builder.build());
    }*/

}
