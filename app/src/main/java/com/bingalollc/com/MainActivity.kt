package com.bingalollc.com

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.MyAppTheme);
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}