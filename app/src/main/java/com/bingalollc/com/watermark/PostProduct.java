//package com.bingalollc.com;
//
//import android.Manifest
//import android.annotation.SuppressLint
//import android.app.Activity
//import android.app.Dialog
//import android.content.ClipData
//import android.content.Context
//import android.content.Context.LOCATION_SERVICE
//import android.content.Intent
//import android.content.IntentSender.SendIntentException
//import android.content.pm.PackageManager
//import android.database.Cursor
//import android.graphics.Bitmap
//import android.graphics.BitmapFactory
//import android.graphics.Color
//import android.graphics.drawable.BitmapDrawable
//import android.graphics.drawable.ColorDrawable
//import android.location.Geocoder
//import android.location.Location
//import android.location.LocationListener
//import android.location.LocationManager
//import android.net.Uri
//import android.os.Build
//import android.os.Bundle
//import android.os.Handler
//import android.os.Looper
//import android.provider.MediaStore
//import android.text.Editable
//import android.text.TextUtils
//import android.text.TextWatcher
//import android.util.Log
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import android.widget.*
//import androidx.activity.result.contract.ActivityResultContracts
//import androidx.appcompat.app.AlertDialog
//import androidx.core.app.ActivityCompat
//import androidx.core.content.ContextCompat
//import androidx.databinding.DataBindingUtil
//import androidx.recyclerview.widget.GridLayoutManager
//import androidx.recyclerview.widget.LinearLayoutManager
//import androidx.recyclerview.widget.RecyclerView
//import com.bingalollc.com.R
//import com.bingalollc.com.base.BaseActivity
//import com.bingalollc.com.base.bottom_action_sheet.ActionSheet
//import com.bingalollc.com.databinding.FragmentPostproductBinding
//import com.bingalollc.com.databinding.PhotosRepositionDialogBinding
//import com.bingalollc.com.databinding.SelectMultiplePhotosDialogBinding
//import com.bingalollc.com.homeactivity.HomeActivity
//import com.bingalollc.com.homeactivity.fragment.MapFragment
//import com.bingalollc.com.homeactivity.model.CategoriesModel
//import com.bingalollc.com.homeactivity.model.PostProductDraftModel
//import com.bingalollc.com.homeactivity.postproduct.adapter.PostProductImagesAdapter
//import com.bingalollc.com.homeactivity.postproduct.adapter.PostProductNormalTextAdapter
//import com.bingalollc.com.homeactivity.postproduct.adapter.PostProductSpecificationTextAdapter
//import com.bingalollc.com.homeactivity.postproduct.adapter.ProductCategoryAdapter
//import com.bingalollc.com.homeactivity.postproduct.draft.DraftActivity
//import com.bingalollc.com.model.ApiStatusModel
//import com.bingalollc.com.network.MultipartParams
//import com.bingalollc.com.network.RestClient
//import com.bingalollc.com.preference.Common
//import com.bingalollc.com.preference.PreferenceManager
//import com.bingalollc.com.utils.AppLocationService
//import com.bingalollc.com.utils.Cache
//import com.bingalollc.com.utils.PickerUtils
//import com.bingalollc.com.utils.ViewBindingDialogFragment
//import com.bingalollc.com.utils.dialog.ProgressDialog
//import com.ernestoyaquello.dragdropswiperecyclerview.DragDropSwipeRecyclerView
//import com.ernestoyaquello.dragdropswiperecyclerview.listener.OnItemDragListener
//import com.google.android.gms.common.api.GoogleApiClient
//import com.google.android.gms.location.LocationRequest
//import com.google.android.gms.location.LocationServices
//import com.google.android.gms.location.LocationSettingsRequest
//import com.google.android.gms.location.LocationSettingsStatusCodes
//import com.google.android.gms.maps.CameraUpdateFactory
//import com.google.android.gms.maps.GoogleMap
//import com.google.android.gms.maps.OnMapReadyCallback
//import com.google.android.gms.maps.SupportMapFragment
//import com.google.android.gms.maps.model.*
//import com.kbeanie.multipicker.api.FilePicker
//import com.kbeanie.multipicker.api.callbacks.FilePickerCallback
//import com.kbeanie.multipicker.api.entity.ChosenFile
//import com.watermark.androidwm_light.WatermarkBuilder
//import com.watermark.androidwm_light.bean.WatermarkImage
//import com.watermark.androidwm_light.bean.WatermarkPosition
//import kotlinx.android.synthetic.main.fragment_postproduct.*
//import okhttp3.MediaType.Companion.toMediaTypeOrNull
//import okhttp3.MultipartBody
//import okhttp3.RequestBody
//import pub.devrel.easypermissions.EasyPermissions
//import retrofit2.Call
//import retrofit2.Callback
//import retrofit2.Response
//import java.io.ByteArrayOutputStream
//import java.io.File
//import java.io.IOException
//import java.text.SimpleDateFormat
//import java.util.*
//
//
//class PostProduct : ViewBindingDialogFragment<FragmentPostproductBinding>(), PostProductImagesAdapter.OnImageClicked,
//    FilePickerCallback,
//    PostProductSpecificationTextAdapter.OnClickedCalled,
//    PostProductNormalTextAdapter.OnClickedCalled, LocationListener, MapFragment.OnMapChoosed,
//    OnMapReadyCallback,
//    ActionSheet.onActionClick, ProductCategoryAdapter.OnClickedCalled {
//    private val REQUEST_READ_EXTERNAL_STORAGE_CHOOSE_IMG = 1000
//    private val REQUEST_CAMERA = 1001
//    private val PICK_IMAGE_REQUEST_GALLERY = 1002
//    private val PERMISSION_CAMERA_REQUEST_CODE = 1003
//    private val LOCATION_PERMISSION = 1000
//    private val currencySymbol = "₪ "
//    private var mArrayFile: java.util.ArrayList<File>? = ArrayList()
//    private var fileToUpload: MultipartBody.Part? = null
//    private var fileToUploadArray: ArrayList<MultipartBody.Part>? = null
//    private var fileTOSet: ArrayList<Uri>? = null
//    private var imagesStringUrl: ArrayList<String>? = null
//    private var postProductImagesAdapter: PostProductImagesAdapter? = null
//    private var postProductSpecificationTextAdapter: PostProductSpecificationTextAdapter? = null
//    private var postProductNormalTextAdapter: PostProductNormalTextAdapter? = null
//    private var postProductCategoryAdapter: ProductCategoryAdapter? = null
//
//    private var specificationString:ArrayList<String>? =null
//    private var headerTextCondition:ArrayList<String>? =null
//    private var headerTextConditionEng:ArrayList<String>? =null
//    private var headerTextTwoCondition:ArrayList<String>? =null
//    private var headerTextTwoConditionEng:ArrayList<String>? =null
//    private var headerTextCategory:ArrayList<String>? =null
//    private var headerTextCategoryEngish:ArrayList<String>? =null
//    private var headerTextCategoryImage:ArrayList<String>? =null
//    private var headerTextSubCategory:ArrayList<ArrayList<String>>? =null
//    private var headerTextSubCategoryEnglish:ArrayList<ArrayList<String>>? =null
//    private var rvImages: DragDropSwipeRecyclerView? = null
//    private var rvSpecification: RecyclerView? = null
//    private var addSpecification: ImageView? = null
//    private var contionText: TextView? = null
//    private var contionTextString: String = ""
//
//    private var product_title: EditText? = null
//    private var product_price: EditText? = null
//    private var product_desc: TextView? = null
//    private var cat_selected_pos = 0
//    private var cache: Cache? = null
//
//    private var categoryText: TextView? = null
//    private var parent_cat_text: String = ""
//    private var parent_sub_cat_text: String = ""
//    private var mCategoryHeader: Array<String>? = null
//    private var submitBtn: Button? = null
//    private var saveDraft: Button? = null
//    private var baseActivity: BaseActivity? = null
//    private var conditions: RelativeLayout? = null
//    private var categoryLayout: RelativeLayout? = null
//    private var testDialog: AlertDialog? = null
//    private var imageSelected: Int = 0
//    private var totalImages: Int = 8
//    private var isConditionClicked: Boolean = false
//    private var preferenceManager: PreferenceManager? = null
//    var locationManager: LocationManager? = null
//    private var googleMap: GoogleMap? = null
//    var et_state: String = ""
//    var et_city: String = ""
//    var et_address: String = ""
//    var et_zip: String = ""
//    var et_country: String = ""
//    private var topLayer: LinearLayout? = null
//    private var header: TextView? = null
//    private var back_icon: ImageView? = null
//    private var ChangeLocLayout: RelativeLayout? = null
//    private var addressText: TextView? = null
//    private lateinit var data: ArrayList<String>
//    var currentMarker: Marker? = null
//    private var isEditAble:Boolean = false
//    private var isFromDraft:Boolean = false
//    private var draftPosition:Int = 0
//    private var productId:String = ""
//    private var filePicker: FilePicker? = null
//    private val permissionsGALLERY = arrayOf(
//        Manifest.permission.WRITE_EXTERNAL_STORAGE,
//        Manifest.permission.READ_EXTERNAL_STORAGE
//    )
//
//    var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
//        if (result.resultCode == Activity.RESULT_OK) {
//            // There are no request codes
//            val files: File = File(BaseActivity.getCameraFilePathName())
//            val file = baseActivity?.getCompressed(requireContext(), files.path)
//            val mFile = RequestBody.create("image/*".toMediaTypeOrNull(), file!!)
//            fileToUpload = MultipartBody.Part.createFormData("image", file?.name, mFile)
//            mArrayFile?.add(file!!)
//            fileTOSet?.add(Uri.fromFile(file))
//            imagesStringUrl?.add("")
//            imageSelected++
//            postProductImagesAdapter?.updateImages(
//                fileTOSet,
//                imageSelected
//            )
//            fileToUploadArray!!.add(fileToUpload!!)
//        }
//    }
//
//    fun getRealPathFromURI(uri: Uri?): String? {
//        var path = ""
//        if (requireActivity().getContentResolver() != null) {
//            val cursor: Cursor? = requireActivity().getContentResolver().query(uri!!, null, null, null, null)
//            if (cursor != null) {
//                cursor.moveToFirst()
//                val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
//                path = cursor.getString(idx)
//                cursor.close()
//            }
//        }
//        return path
//    }
//    override fun onCreateView(
//        inflater: LayoutInflater,
//        container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View? {
//        if (view != null) {
//            val parent = view!!.parent as ViewGroup
//            parent?.removeView(view)
//        }
//  3
//        // if you want to use internal memory for storying images - default
//        return super.onCreateView(inflater, container, savedInstanceState)
//
//    }
//
//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//        initViews(view)
//
//        if(preferenceManager?.userDetails?.id.isNullOrEmpty().not()) {
//            addAllConditionValues()
//            initRecyclerViewImages()
//            initSpecification()
//            setData()
//            initClicks()
//        }
//
//    }
//
//
//    override fun onResume() {
//        super.onResume()
//
//        if(preferenceManager?.userDetails?.id.isNullOrEmpty().not()) {
//            if (EasyPermissions.hasPermissions(requireContext(), *permissionsGALLERY)) {
//            } else {
//                EasyPermissions.requestPermissions(
//                    requireActivity(), resources.getString(R.string.please_allow_permissions),
//                    1004, *permissionsGALLERY
//                )
//            }
//
//            if (preferenceManager?.postProductFromDraft?.size ?: 0 > 0 && !isFromDraft && !isEditAble) {
//                saveDraftLayout.visibility = View.VISIBLE
//                saveDraftText.text = getString(
//                    R.string.you_have_saved_draft,
//                    preferenceManager?.postProductFromDraft?.size.toString() ?: "0"
//                )
//            } else {
//                saveDraftLayout.visibility = View.GONE
//            }
//        }
//    }
//
//    private fun setData() {
//        if (isFromDraft){
//            saveDraft?.visibility = View.GONE
//            setDataFromDraft(draftPosition)
//        }else if (isEditAble){
//            saveDraft?.visibility = View.GONE
//            setAllFields()
//        }else {
//            getPermission()
//            setAddress()
//        }
//
//        btnCross.setOnClickListener({ saveDraftLayout.visibility = View.GONE })
//        saveDraftLayout.setOnClickListener {
//            val intent = Intent(requireContext(), DraftActivity::class.java)
//            startActivity(intent)
//        }
//    }
//
//    fun setDataFromDraft(position: Int){
//        product_title?.setText(preferenceManager?.postProductFromDraft?.get(position)?.title)
//        product_price?.setText(preferenceManager?.postProductFromDraft?.get(position)?.price)
//        product_desc?.setText(preferenceManager?.postProductFromDraft?.get(position)?.description)
//        specificationString = ArrayList()
//        specificationString = preferenceManager?.postProductFromDraft?.get(position)?.specification
//        initSpecification()
//        categoryText?.text = preferenceManager?.postProductFromDraft?.get(position)?.choose_a_cat
//        parent_cat_text = preferenceManager?.postProductFromDraft?.get(position)?.choose_a_parent_cat?:""
//        Common.productLocationLat = preferenceManager?.postProductFromDraft?.get(position)?.lat
//        Common.productLocationLng = preferenceManager?.postProductFromDraft?.get(position)?.lng
//        getPermission()
//        addressText?.text = getAddress(
//            Common.productLocationLat.toDouble(),
//            Common.productLocationLng.toDouble()
//        )
//        contionTextString = preferenceManager?.postProductFromDraft?.get(position)?.condition?:""
//        if ((preferenceManager?.postProductFromDraft?.get(position)?.condition?:"").contains("#")) {
//            contionText?.text = (preferenceManager?.postProductFromDraft?.get(position)?.condition?:"").substring(
//                0,
//                (preferenceManager?.postProductFromDraft?.get(position)?.condition
//                    ?: "").indexOf("#")
//            )
//        } else {
//            contionText?.text = preferenceManager?.postProductFromDraft?.get(position)?.condition?:""
//        }
//        if (preferenceManager?.postProductFromDraft?.get(position)?.images != null && preferenceManager?.postProductFromDraft?.get(
//                position
//            )?.images?.size?:0 > 0){
//
//            preferenceManager?.postProductFromDraft?.get(position)?.images!!.forEach {
//                val file = File(it.toString())
//                val mFile = RequestBody.create("image/*".toMediaTypeOrNull(), file)
//                fileToUpload = MultipartBody.Part.createFormData("image", file?.name, mFile)
//                mArrayFile?.add(file!!)
//                val contentUri = Uri.fromFile(file)
//                fileTOSet?.add(contentUri)
//                imagesStringUrl?.add("")
//                imageSelected++
//                fileToUploadArray!!.add(fileToUpload!!)
//            }
//            postProductImagesAdapter?.updateImages(
//                fileTOSet,
//                imageSelected
//            )
//        }
//    }
//
//    private fun getAddress(
//        lat: Double,
//        lng: Double
//    ): String? {
//        val result = StringBuilder()
//        var completeAddress = ""
//
//        try {
//            val geocoder = Geocoder(context, Locale.getDefault())
//            val addresses = geocoder.getFromLocation(lat, lng, 1)
//            if (addresses != null && addresses.size > 0) {
//                val address = addresses[0]
//                result.append(address.featureName).append("\n")
//                result.append(address.locality).append("\n")
//                result.append(address.countryName)
//                completeAddress = address.getAddressLine(0)
//                if (completeAddress.isEmpty() && address.adminArea.isNullOrEmpty().not()) {
//                    completeAddress = address.adminArea
//                }
//                et_address = completeAddress
//                if (address.adminArea.isNullOrEmpty().not())
//                    et_state = address.adminArea
//                else et_state = ""
//
//                if (address.subAdminArea.isNullOrEmpty().not())
//                 et_city = address.subAdminArea
//                else et_city = ""
//
//                if (address.countryName.isNullOrEmpty().not())
//                    et_country = address.countryName
//                else et_country = ""
//
//                if (address.postalCode.isNullOrEmpty().not())
//                et_zip = address.postalCode else et_zip =""
//            }
//        } catch (e: IOException) {
//            Log.e("tag", e.message!!)
//        }
//        return completeAddress
//    }
//
//    private fun setAllFields() {
//        header?.text = getString(R.string.edit_product)
//        back_icon?.visibility = View.VISIBLE
//        back_icon?.setOnClickListener { dismiss() }
//        submitBtn?.setText("UPDATE")
//        productId = Common.selectedProductDetails.id
//        product_title?.setText(Common.selectedProductDetails.productTitle)
//        product_price?.setText(
//            currencySymbol + Common.selectedProductDetails.productPrice.replace(
//                "?",
//                ""
//            )
//        )
//        product_desc?.setText(Common.selectedProductDetails.productDescription)
//        specificationString = ArrayList()
//        specificationString?.addAll(Common.selectedProductDetails.productSpecifications.split(","))
//        initSpecification()
//        if (Common.selectedProductDetails.productCondition.contains("#")) {
//            contionText?.text = Common.selectedProductDetails.productCondition.substring(
//                0, Common.selectedProductDetails.productCondition.indexOf(
//                    "#"
//                )
//            )
//        } else {
//            contionText?.text = Common.selectedProductDetails.productCondition
//        }
//
//        contionTextString = Common.selectedProductDetails.productCondition
//        categoryText?.text = Common.selectedProductDetails.productCategory
//        parent_cat_text = Common.selectedProductDetails.parentCategory
//        addressText?.text = getAddress(
//            Common.selectedProductDetails.productLat.toDouble(),
//            Common.selectedProductDetails.productLng.toDouble()
//        )
//        Common.productLocationLat = Common.selectedProductDetails.productLat
//        Common.productLocationLng = Common.selectedProductDetails.productLng
//        getPermission()
//        imagesStringUrl = ArrayList()
//        Common.selectedProductDetails.images.forEachIndexed { index, image ->
//            imagesStringUrl?.add(image.imageUrl)
//            fileTOSet?.add(Uri.parse(image.imageUrl))
//            imageSelected++
//        }
//        postProductImagesAdapter?.updateImages(fileTOSet, imageSelected)
//    }
//
//    fun setAllFieldsForEdit(isEditAble: Boolean){
//        this.isEditAble = isEditAble
//    }
//    fun setAllFieldsFromDraft(isFromDraft: Boolean, draftPosition: Int){
//        this.isFromDraft = isFromDraft
//        this.draftPosition = draftPosition
//    }
//
//    private fun initMap() {
//        /* mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.mapView);*/
//        val mapFragment = childFragmentManager.findFragmentById(R.id.mapView) as SupportMapFragment?
//
//      //  var mapFragment =   (activity!!.supportFragmentManager.findFragmentById(R.id.mapView) as SupportMapFragment?)
//        mapFragment?.getMapAsync(this)
//
//    /*    //var mapFragment = getFragmentManager()?.findFragmentById(R.id.mapView) as SupportMapFragment
//        Handler().postDelayed({ mapFragment?.getMapAsync(this) }, 1000)*/
//
//    }
//
//    private fun getPermission() {
//        if (Build.VERSION.SDK_INT >= 23) {
//            if (requireActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
//                    != PackageManager.PERMISSION_GRANTED
//            ) {
//                ActivityCompat.requestPermissions(
//                    requireContext() as Activity,
//                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
//                    LOCATION_PERMISSION
//                )
//            } else {
//                if (checkGpsOn()) {
//                    val appLocationService = AppLocationService(requireContext())
//                    var gpsLocation = appLocationService
//                            .getLocation(LocationManager.GPS_PROVIDER)
//                    if (gpsLocation == null) {
//                        gpsLocation = appLocationService
//                                .getLocation(LocationManager.NETWORK_PROVIDER)
//                        if (gpsLocation == null) {
//                            gpsLocation = appLocationService
//                                    .getLocation(LocationManager.PASSIVE_PROVIDER)
//                        }
//                    }
//                    if (gpsLocation != null) {
//                        if (!isFromDraft && !isEditAble) {
//                            Common.productLocationLat = gpsLocation.getLatitude().toString()
//                            Common.productLocationLng = gpsLocation.getLongitude().toString()
//                        }
//                        initMap()
//                    }
//                }
//            }
//        }
//    }
//        fun checkGpsOn(): Boolean {
//            if (!locationManager?.isProviderEnabled(LocationManager.GPS_PROVIDER)!!) {
//                displayLocationSettingsRequest(requireContext())
//                return false
//            } else {
//                return true
//            }
//        }
//
//    private fun displayLocationSettingsRequest(context: Context) {
//        val googleApiClient = GoogleApiClient.Builder(context)
//            .addApi(LocationServices.API).build()
//        googleApiClient.connect()
//        val locationRequest = LocationRequest.create()
//        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
//        locationRequest.interval = 10000
//        locationRequest.fastestInterval = 10000 / 2.toLong()
//        val builder =
//            LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
//        builder.setAlwaysShow(true)
//        val result =
//            LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build())
//        result.setResultCallback { result ->
//            val status = result.status
//            when (status.statusCode) {
//                LocationSettingsStatusCodes.SUCCESS -> Log.i(
//                    "TAG1",
//                    "All location settings are satisfied."
//                )
//                LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
//                    Log.i(
//                        "TAG2",
//                        "Location settings are not satisfied. Show the user a dialog to upgrade location settings "
//                    )
//                    try {
//                        // Show the dialog by calling startResolutionForResult(), and check the result
//                        // in onActivityResult().
//                        status.startResolutionForResult(
//                            requireActivity(),
//                            LOCATION_PERMISSION
//                        )
//                    } catch (e: SendIntentException) {
//                        Log.i("TAG3", "PendingIntent unable to execute request.")
//                    }
//                }
//                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> Log.i(
//                    "Tag4",
//                    "Location settings are inadequate, and cannot be fixed here. Dialog not created."
//                )
//            }
//        }
//    }
//
//
//
//        private fun addAllConditionValues() {
//            var mConditionHeaderArray = emptyArray<String>()
//            var mConditionHeaderTwoArray = emptyArray<String>()
//            mConditionHeaderArray = arrayOf("Brand New", "New", "Used", "Used", "Used")
//            mConditionHeaderTwoArray = arrayOf(
//                "Not Touched. In Original Packaging",
//                "Open Box (Packaging Is Off, Never Used)",
//                "Like-New (Excellent Condition. Signs Of Minor Use)",
//                "Very Good (Signs Of Significant Use. Functions Properly)",
//                "Acceptable (May Have Minor Scratches & Dents. Still Works Well)"
//            )
//            headerTextConditionEng?.addAll(mConditionHeaderArray)
//            headerTextTwoConditionEng?.addAll(mConditionHeaderTwoArray)
//
//            if (preferenceManager?.isEnglish == 2) {
//                mConditionHeaderArray = arrayOf("חדש לגמרי", "חָדָשׁ\n", "בשימוש", "בשימוש", "בשימוש")
//                mConditionHeaderTwoArray = arrayOf(
//                    "לא נגע. באריזה מקורית",
//                    "תיבה פתוחה (האריזה אינה פעילה, אף פעם לא בשימוש)",
//                    "כמו חדש (מצב מצוין. סימני שימוש קלים)",
//                    "טוב מאוד (סימני שימוש משמעותיים. פונקציות נכונות)",
//                    "מקובל (יכול להיות שיש שריטות ושקעים קלים. עדיין עובד טוב)"
//                )
//            }
//            headerTextCondition?.addAll(mConditionHeaderArray)
//            headerTextTwoCondition?.addAll(mConditionHeaderTwoArray)
//             mCategoryHeader = arrayOf(
//                 "Antiques:Other",
//                 "Appliances:Electronics",
//                 "Arts & Crafts:Sport and hobby",
//                 "Audio Equipment:Music & Movies",
//                 "Auto Parts:Transport",
//                 "Baby & Kids:Children's world",
//                 "Beauty & Health:Fashion and style",
//                 "Bicycles & Scooters:Transport",
//                 "Bicycles - Electric:Transport",
//                 "Boats:Transport",
//                 "Sefarim, Books & Magazines:Other",
//                 "Business Equipment:Other",
//                 "Campers & RVs:Other",
//                 "Cars & Trucks:Transport",
//                 "CDs & DVDS:Music & Movies",
//                 "Cell Phones:Electronics",
//                 "Women’s Clothing & Shoes:Fashion and style",
//                 "Men’s Clothing & Shoes:Fashion and style",
//                 "Tichel’s & Beanies:Other",
//                 "Collectibles:Other",
//                 "Computer Equipment:Electronics",
//                 "Computer Software:Other",
//                 "Electronics:Electronics",
//                 "Farming Furniture:Other",
//                 "Games & Toys:Games",
//                 "General:Games",
//                 "Home & Garden:Home and garden",
//                 "Jewelry & Accessories:Fashion and style",
//                 "Motorcycles:Transport",
//                 "Musical Instruments:Music & Movies",
//                 "Pet Supplies:Other",
//                 "Photography:Other",
//                 "Sports & Outdoors:Sport and hobby",
//                 "Tickets:Other",
//                 "Tools & Machinery:Other",
//                 "TVs:Electronics",
//                 "Video Equipment:Electronics",
//                 "Video Games:Games"
//             )
//          //  if(preferenceManager!!.isUserLoggedIn) {
//
//                getCategories()
//          //  }
//
//        }
//
//    private fun getCategories() {
//        RestClient.getApiInterface().categories.enqueue(object : Callback<CategoriesModel>{
//            override fun onFailure(call: Call<CategoriesModel>, t: Throwable) {
//
//            }
//
//            override fun onResponse(
//                call: Call<CategoriesModel>,
//                response: Response<CategoriesModel>
//            ) {
//                if (response.body() != null && response.body()?.status==200 && response.body()?.data != null){
//                    (response.body()?.data!!.forEachIndexed { index, datum ->
//                        if (datum.subCategories.isEmpty().not()) {
//                            headerTextCategoryImage?.add(datum?.categoryImage ?: "")
//                            headerTextCategoryEngish?.add(datum?.categoryName?:"")
//                            if (preferenceManager?.isEnglish == 1)
//                                headerTextCategory?.add(datum?.categoryName ?: "")
//                            else
//                                headerTextCategory?.add(datum?.herbewCategoryName ?: "")
//
//                            val subCat = ArrayList<String>()
//                            val subCat_heb = ArrayList<String>()
//
//                            datum?.subCategories?.forEach {
//                                subCat.add(it.name)
//                                subCat_heb.add(it.herbewName)
//                            }
//                            headerTextSubCategoryEnglish?.add(subCat)
//                            if (preferenceManager?.isEnglish == 1)
//                                headerTextSubCategory?.add(subCat)
//                            else
//                                headerTextSubCategory?.add(subCat_heb)
//                        }
//                    })
//                }
//
//            }
//        })
//    }
//
//    private fun initClicks() {
//            ChangeLocLayout?.setOnClickListener({
//                MapFragment.show(requireActivity(), this)
//            })
//            addSpecification?.setOnClickListener({
//                showNormalEditTextField(
//                    requireContext(), getString(R.string.product_specification), getString(
//                        R.string.add_a_specification
//                    ), getString(R.string.add), getString(R.string.cancel)
//                )
//            })
//
//            conditions?.setOnClickListener({
//                baseActivity?.hideKeyboard(activity)
//                showListDialog(true, getString(R.string.choose_product_condition))
//            })
//            categoryLayout?.setOnClickListener({
//                baseActivity?.hideKeyboard(activity)
//                showListDialog(false, getString(R.string.choose_product_category))
//            })
//
//            saveDraft?.setOnClickListener({
//                saveDataToDraft()
//            })
//            submitBtn?.setOnClickListener {
//                if (_allConditionValid()) {
//                    ProgressDialog.showProgressDialog(requireContext())
//                    val prod_specification =
//                        specificationString.toString().replace("[", "").replace(
//                            "]",
//                            ""
//                        )
//                    val builder = MultipartParams.Builder()
//                    if (isEditAble) {
//                        builder.add("product_id", productId)
//                    }
//                    builder.add("product_title", product_title?.text.toString())
//                    builder.add(
//                        "product_price", product_price?.text.toString().replace(
//                            currencySymbol,
//                            ""
//                        )
//                    )
//                    builder.add("product_specifications", prod_specification)
//                    builder.add("product_condition", contionTextString)
//                    builder.add("product_category", categoryText?.text)
//                    builder.add("parent_category", parent_cat_text)
//                    builder.add("product_description", product_desc?.text.toString())
//                    builder.add("product_location", et_address)
//                    builder.add("product_city", et_city)
//                    builder.add("product_state", et_state)
//                    builder.add("product_country", et_country)
//                    builder.add("lat", Common.productLocationLat)
//                    builder.add("lng", Common.productLocationLng)
//                    builder.add("uploaded_by_user_id", preferenceManager?.userDetails?.id)
//                    builder.add("product_is_sold", "0")
//
//                    val f = java.util.ArrayList<File>()
//                    for (i in 0 until mArrayFile?.size!!) {
//                        if (!TextUtils.isEmpty(mArrayFile?.get(i).toString()))
//                            builder.addFile("image[]", mArrayFile?.get(i))
//                        System.out.println(">>>>>>>>>>>>>>..post product params " +  mArrayFile?.get(i))
//                    }
//                    if (!isEditAble) {
//                        RestClient.getApiInterface().postproducts(builder.build().map)
//                            .enqueue(object :
//                                Callback<ApiStatusModel> {
//                                override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
//                                    ProgressDialog.dismissProgressDialog()
//                                }
//
//                                override fun onResponse(
//                                    call: Call<ApiStatusModel>,
//                                    response: Response<ApiStatusModel>
//                                ) {
//                                    ProgressDialog.dismissProgressDialog()
//
//                                    if (response.body()?.getStatus() == 200) {
//                                        baseActivity?.showToast(
//                                            getString(R.string.product_added),
//                                            requireContext()
//                                        )
//                                        refreshFragment()
//                                        if (!isFromDraft)
//                                            (activity as HomeActivity?)?.launchHomeFragment()
//                                        else {
//                                            val data = preferenceManager?.postProductFromDraft
//                                            data?.removeAt(draftPosition)
//                                            preferenceManager?.saveDraft(data)
//                                            dismiss()
//                                            val intent = Intent(
//                                                requireContext(),
//                                                HomeActivity::class.java
//                                            )
//                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
//                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                                            startActivity(intent)
//                                        }
//                                    } else {
//                                        baseActivity?.showToast(
//                                            "Something went wrong",
//                                            requireContext()
//                                        )
//                                    }
//
//
//                                }
//                            })
//                    } else {
//                        RestClient.getApiInterface().editproducts(builder.build().map).enqueue(
//                            object : Callback<Any> {
//                                override fun onFailure(call: Call<Any>, t: Throwable) {
//                                    ProgressDialog.dismissProgressDialog()
//                                }
//
//                                override fun onResponse(call: Call<Any>, response: Response<Any>) {
//                                    ProgressDialog.dismissProgressDialog()
//                                    baseActivity?.showToast("Updated", requireContext())
//                                    val intent = Intent(requireContext(), HomeActivity::class.java)
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                                    startActivity(intent)
//
//
//                                }
//                            })
//                    }
//                }
//
//            }
//
//
//        product_price?.addTextChangedListener(object : TextWatcher {
//                override fun afterTextChanged(p0: Editable?) {
//                    val pp = product_price?.text
//                    if (pp?.length == 1) {
//                        product_price?.setText(currencySymbol + pp)
//                        product_price?.setSelection(product_price!!.length())
//                    } else if (product_price?.text?.contains(currencySymbol) == true && product_price?.text?.replace(
//                            currencySymbol.toRegex(),
//                            ""
//                        )?.length == 0
//                    ) {
//                        product_price?.setText("")
//                    }
//
//                }
//
//                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
//
//                }
//
//                override fun onTextChanged(p0: CharSequence?, start: Int, before: Int, count: Int) {
//
//                }
//
//
//            })
//
//        }
//
//
//    private fun refreshFragment() {
//        mArrayFile?.clear()
//        fileTOSet?.clear()
//        imagesStringUrl?.clear()
//        imageSelected = 0
//        postProductImagesAdapter?.updateImages(
//            fileTOSet,
//            imageSelected
//        )
//        fileToUploadArray?.clear()
//        product_title?.text?.clear()
//        product_price?.text?.clear()
//        product_desc?.text = ""
//        contionText?.text = ""
//        categoryText?.text = ""
//        parent_cat_text = ""
//        parent_sub_cat_text= ""
//        cat_selected_pos = 0
//        draftPosition = 0
//        specificationString = ArrayList()
//        initSpecification()
//    }
//
//    private fun saveDataToDraft() {
//        val postProductDraftModel = PostProductDraftModel()
//        postProductDraftModel.title = product_title?.text.toString()
//        postProductDraftModel.price = product_price?.text.toString()
//        postProductDraftModel.specification = specificationString
//        postProductDraftModel.condition = contionTextString
//        postProductDraftModel.choose_a_cat = categoryText?.text.toString()
//        postProductDraftModel.description = product_desc?.text.toString()
//        postProductDraftModel.lat = Common.productLocationLat
//        postProductDraftModel.lng = Common.productLocationLng
//        postProductDraftModel.address = addressText?.text.toString()
//        postProductDraftModel.choose_a_parent_cat = parent_cat_text
//        val sdf = SimpleDateFormat("MMM dd, yyyy hh:mm a")
//        val date =  Date()
//        val date2: String = sdf.format(date)
//        postProductDraftModel.timeStamp = date2
//        val uriArray = ArrayList<String>()
//        mArrayFile?.forEach {
//            val imageName = System.currentTimeMillis().toString()
//            val filePath: String = it.getPath()
//            val bitmap = BitmapFactory.decodeFile(filePath)
//        /*    val cacheUri = cache?.saveToCacheAndGetUri(
//                bitmap,
//                imageName,
//                requireContext()
//            )
//            */
//            System.out.println(">>>>>>>>>>>>>>..mmmm "+it.absolutePath)
//            uriArray.add(it.getPath())
//        }
//        postProductDraftModel?.images = uriArray
//        val postProductSaved = preferenceManager?.postProductFromDraft?: ArrayList()
//        postProductSaved.add(postProductDraftModel)
//        preferenceManager?.saveDraft(postProductSaved)
//        baseActivity?.showToast("Saved to Draft", requireContext())
//        refreshFragment()
//        val intent = Intent(requireContext(), DraftActivity::class.java)
//        startActivity(intent)
//
//    }
//
//    private fun _allConditionValid(): Boolean {
//        if (product_title?.text.toString().length==0){
//            baseActivity?.showToast(getString(R.string.title_require), context);
//            return false
//        }else if (product_price?.text.toString().length==0){
//            baseActivity?.showToast(getString(R.string.title_price), context);
//            return false
//        }
//        else if (product_price?.text!!.substring(2).toInt() < 10){
//            baseActivity?.showToast(getString(R.string.min_price), context);
//            return false
//        }
////        else if (specificationString.toString().replace("[", "").replace("]", "").length==0){
////            baseActivity?.showToast(getString(R.string.specification_required), context);
////            return false
////        }
////        else if (contionText?.text.toString().length==0){
////            baseActivity?.showToast(getString(R.string.condition_required), context);
////            return false
////        }
//        else if (categoryText?.text.toString().length==0){
//            baseActivity?.showToast(getString(R.string.category_required), context);
//            return false
//        }else if (mArrayFile?.size==0 && imagesStringUrl?.size ==0){
//            baseActivity?.showToast(getString(R.string.add_image), context);
//            return  false
//        }else
//            return true
//
//    }
//
//    fun showListDialog(isCondition: Boolean, stringHeader: String) {
//            isConditionClicked = isCondition
//            val alertDialog = AlertDialog.Builder(context!!)
//            val inflater = layoutInflater
//            val add_menu_layout = inflater.inflate(R.layout.alert_dialog_header, null)
//            val rvListView: RecyclerView = add_menu_layout.findViewById(R.id.rvListView)
//            val rvCatView: RecyclerView = add_menu_layout.findViewById(R.id.rvCatView)
//            val crossBtn: RelativeLayout = add_menu_layout.findViewById(R.id.layout)
//            val tv_selectCategory: TextView = add_menu_layout.findViewById(R.id.tv_selectCategory)
//            tv_selectCategory.text=stringHeader
//            alertDialog.setView(add_menu_layout)
//            alertDialog.setCancelable(false)
//            initNameRecyclerView(rvListView,rvCatView, isCondition)
//            crossBtn?.setOnClickListener({ testDialog?.dismiss() })
//            testDialog = alertDialog.create()
//            testDialog!!.show()
//        }
//
//        private fun initNameRecyclerView(rvListView: RecyclerView,rvCatView: RecyclerView, condition: Boolean) {
//
//            if (condition) {
//                postProductNormalTextAdapter = PostProductNormalTextAdapter(
//                    headerTextCondition,
//                    headerTextTwoCondition,
//                    this
//                )
//                rvCatView.visibility = View.GONE
//            }
//            else {
//                if (headerTextSubCategory?.size?:0 > 0) {
//                    postProductNormalTextAdapter = PostProductNormalTextAdapter(
//                        headerTextSubCategory?.get(0),
//                        null,
//                        this
//                    )
//                    postProductCategoryAdapter =
//                        ProductCategoryAdapter(headerTextCategory, headerTextCategoryImage, this)
//                }
//                rvCatView.visibility = View.VISIBLE
//                val linearLayoutManagers = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
//                rvCatView.adapter = postProductCategoryAdapter;
//                rvCatView.layoutManager = linearLayoutManagers
//            }
//            val linearLayoutManager = LinearLayoutManager(requireContext())
//            rvListView.adapter = postProductNormalTextAdapter;
//            rvListView.layoutManager = linearLayoutManager
//
//
//        }
//
//        fun showNormalEditTextField(
//            context: Context?,
//            headerOne: String?,
//            headerTwo: String?,
//            button1: String?,
//            button2: String?
//        ) {
//
//            val alertDialog = AlertDialog.Builder(context!!)
//            val inflater = layoutInflater
//            val add_menu_layout = inflater.inflate(R.layout.edit_dialog_ui, null)
//            val header_one = add_menu_layout.findViewById<TextView>(R.id.header_one)
//            val header_two = add_menu_layout.findViewById<TextView>(R.id.header_two)
//            var edit_text = add_menu_layout.findViewById<EditText>(R.id.edit_text)
//            val cancelBtn = add_menu_layout.findViewById<TextView>(R.id.cancelBtn)
//            val yesBtn = add_menu_layout.findViewById<TextView>(R.id.yesBtn)
//            header_one.text = headerOne
//            header_two.text = headerTwo
//            cancelBtn.text = button2
//            yesBtn.text = button1
//            alertDialog.setView(add_menu_layout)
//            Handler(Looper.myLooper()!!).postDelayed({
//                if (edit_text != null) {
//                    edit_text.requestFocus()
//                    baseActivity?.showKeyBoardOfView(context, edit_text)
//                }
//
//            }, 200)
//            yesBtn.setOnClickListener {
//                Handler(Looper.myLooper()!!).postDelayed({
//                    edit_text.requestFocus()
//                    baseActivity?.hideKeyBoardOfView(requireActivity(), edit_text)
//                }, 200)
//                testDialog?.dismiss()
//                if (TextUtils.isEmpty(edit_text?.text?.toString()).not()) {
//                    specificationString?.add(edit_text?.text?.toString() ?: "")
//                    postProductSpecificationTextAdapter?.updateText(specificationString)
//                }
//
//
//            }
//            cancelBtn.setOnClickListener {
//                Handler(Looper.myLooper()!!).postDelayed({
//                    edit_text.requestFocus()
//                    baseActivity?.hideKeyBoardOfView(requireActivity(), edit_text)
//                }, 200)
//                testDialog?.dismiss()
//                baseActivity?.hideKeyboard(requireActivity())
//            }
//            alertDialog.setView(add_menu_layout)
//            alertDialog.setCancelable(false)
//            testDialog = alertDialog.create()
//            testDialog?.show()
//            testDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//
//
//        }
//
//    private fun setAddress() {
//        if (!TextUtils.isEmpty(Common.productLocationLat)) {
//            val adddress =getAddress(
//                Common.productLocationLat.toDouble(),
//                Common.productLocationLng.toDouble()
//            )
//            addressText?.text = adddress
//            createMarker(
//                Common.productLocationLat.toDouble(),
//                Common.productLocationLng.toDouble(),
//                adddress
//            )
//        }
//    }
//
//        private fun initSpecification() {
//            postProductSpecificationTextAdapter = PostProductSpecificationTextAdapter(
//                specificationString,
//                this
//            )
//            val linearLayoutManager = LinearLayoutManager(
//                requireContext(),
//                LinearLayoutManager.HORIZONTAL,
//                false
//            )
//            rvSpecification?.adapter = postProductSpecificationTextAdapter
//            rvSpecification?.layoutManager = linearLayoutManager
//        }
//
//        private fun initRecyclerViewImages() {
//            postProductImagesAdapter =
//                PostProductImagesAdapter(requireContext(), imageSelected, totalImages, fileTOSet, this)
//            val linearLayoutManager = GridLayoutManager(requireContext(), 4)
//
//            rvImages?.adapter = postProductImagesAdapter
//            rvImages?.layoutManager = linearLayoutManager
//            rvImages?.orientation = DragDropSwipeRecyclerView.ListOrientation.VERTICAL_LIST_WITH_UNCONSTRAINED_DRAGGING
//            rvImages?.dragListener = onItemDragListener
//
//        }
//
//    private val onItemDragListener = object : OnItemDragListener<Uri> {
//        override fun onItemDragged(previousPosition: Int, newPosition: Int, item: Uri) {
//            swipeItemInList(fileTOSet, previousPosition, newPosition)
//        }
//
//        override fun onItemDropped(initialPosition: Int, finalPosition: Int, item: Uri) {
//
//        }
//    }
//
//    @Synchronized
//    private fun swipeItemInList(list: ArrayList<Uri>?, initialPosition: Int, finalPosition: Int) {
//        val initialItem = list?.get(initialPosition)
//        val finalItem = list?.get(finalPosition)
//        finalItem?.let {
//            list.removeAt(initialPosition)
//            list.add(initialPosition, it)
//        }
//        initialItem?.let {
//            list.removeAt(finalPosition)
//            list.add(finalPosition, it)
//        }
//    }
//
//
//    private fun initViews(view: View) {
//            imageSelected = 0;
//            preferenceManager = PreferenceManager(requireContext())
//            cache = Cache()
//            rvImages = view.findViewById(R.id.rvImages)
//            rvSpecification = view.findViewById(R.id.rvSpecification)
//            addSpecification = view.findViewById(R.id.addSpecification)
//            conditions = view.findViewById(R.id.conditions)
//            categoryLayout = view.findViewById(R.id.categoryLayout)
//            contionText = view.findViewById(R.id.contionText)
//            product_title = view.findViewById(R.id.product_title)
//            product_price= view.findViewById(R.id.product_price)
//            product_desc= view.findViewById(R.id.product_desc)
//            categoryText = view.findViewById(R.id.categoryText)
//            submitBtn = view.findViewById(R.id.submitBtn)
//            saveDraft = view.findViewById(R.id.saveDraft)
//            ChangeLocLayout = view.findViewById(R.id.ChangeLocLayout)
//            addressText = view.findViewById(R.id.addressText)
//
//            fileToUploadArray = ArrayList()
//            fileTOSet = ArrayList()
//            imagesStringUrl = ArrayList()
//            mArrayFile = ArrayList()
//            specificationString = ArrayList()
//            headerTextCondition = ArrayList()
//            headerTextConditionEng = ArrayList()
//            headerTextTwoCondition = ArrayList()
//            headerTextTwoConditionEng = ArrayList()
//            headerTextCategory = ArrayList()
//            headerTextCategoryEngish = ArrayList()
//            headerTextCategoryImage = ArrayList()
//            headerTextSubCategory = ArrayList()
//            headerTextSubCategoryEnglish = ArrayList()
//            baseActivity = BaseActivity()
//            locationManager = requireActivity().getSystemService(LOCATION_SERVICE) as LocationManager
//
//            topLayer = view.findViewById(R.id.topLayer)
//            header = view.findViewById(R.id.header)
//            back_icon = view.findViewById(R.id.back_icon)
//            topLayer?.setBackgroundColor(requireContext().resources.getColor(R.color.basecolor))
//            header?.text = getString(R.string.post_product)
//            back_icon?.visibility = View.GONE
//            parent_cat_text = ""
//            isConditionClicked = false
//            productId = ""
//            contionTextString = ""
//            product_title?.setText("")
//            product_price?.setText("")
//            product_desc?.text = ""
//
//        }
//
//    override fun onImageSelectedClicked(postion: Int) {
////            showPickerDialog()
//        showBottomSheet()
//    }
//
//    override fun onImageDeleteClicked(position: Int) {
//        fileTOSet?.removeAt(position)
//        imageSelected--
//        postProductImagesAdapter?.updateImages(fileTOSet, imageSelected)
//    }
//
//
//    private fun deleteImageFromServer(position: Int) {
//        RestClient.getApiInterface().deleteimage(Common.selectedProductDetails.images.get(position).id).enqueue(
//            object : Callback<ApiStatusModel> {
//                override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
//
//                }
//
//                override fun onResponse(
//                    call: Call<ApiStatusModel>,
//                    response: Response<ApiStatusModel>
//                ) {
//                }
//            })
//
//    }
//
//    private fun showBottomSheet() {
//        data = ArrayList()
//        data.add(getString(R.string.take_photo))
//        data.add(getString(R.string.choose_from_library))
//        context?.let {
//            ActionSheet(it, data, this@PostProduct)
//                .setTitle(getString(R.string.upload_picture))
//                .setCancelTitle(getString(R.string.cancel))
//                .setColorTitle(resources.getColor(R.color.dim_gray))
//                .setColorBackground(resources.getColor(R.color.white)) //                        .hideTitle()
//                //                        .setFontData(R.font.meryana_script)
//                //                        .setFontCancelTitle(R.font.meryana_script)
//                //                        .setFontTitle(R.font.meryana_script)
//                //                        .setSizeTextCancel(30)
//                //                        .setSizeTextData(30)
//                //                        .setSizeTextTitle(30)
//                .setColorTitleCancel(resources.getColor(R.color.basecolor))
//                .setColorData(resources.getColor(R.color.basecolor))
//                .setColorSelected(resources.getColor(R.color.colorAccent))
//                .create()
//        }
//
//    }
//
//    override fun onBottomSheetClicked(data: ArrayList<String>, position: Int) {
//        if (data.get(position).equals(getString(R.string.take_photo))) {
//            if (checkPermissionCAMERA(requireContext())) {
//                openCamera()
//
//            }
//        } else if (data.get(position).equals(getString(R.string.choose_from_library))) {
//            if (checkPermissionREAD_EXTERNAL_STORAGE(
//                    requireContext(),
//                    REQUEST_READ_EXTERNAL_STORAGE_CHOOSE_IMG
//                )
//            ) {
//                choosephoto()
//            }
//        }
//    }
//
//
//
//    private fun showPickerDialog() {
//        val alertDialog = AlertDialog.Builder(requireContext())
//        val inflater: LayoutInflater = layoutInflater
//        val add_menu_layout: View = inflater.inflate(R.layout.layout_gallery_picker, null)
//        val tv_takephoto = add_menu_layout.findViewById<TextView>(R.id.tv_takephoto)
//        val tv_choose_gallery = add_menu_layout.findViewById<TextView>(R.id.tv_choose_gallery)
//        val tv_cancel = add_menu_layout.findViewById<TextView>(R.id.tv_cancel)
//
//        alertDialog.setView(add_menu_layout)
//        alertDialog.setCancelable(true)
//        val testDialog: AlertDialog = alertDialog.create()
//        tv_takephoto.setOnClickListener {
//            // showImagePickerDialog()
//            if (checkPermissionCAMERA(requireContext())) {
//                openCamera()
//            }
//            testDialog.dismiss()
//
//        }
//        tv_choose_gallery.setOnClickListener {
//            if (checkPermissionREAD_EXTERNAL_STORAGE(
//                    requireContext(),
//                    REQUEST_READ_EXTERNAL_STORAGE_CHOOSE_IMG
//                )
//            ) {
//                choosephoto()
//            }
//            testDialog.dismiss()
//        }
//
//        tv_cancel.setOnClickListener({ testDialog.dismiss() })
//        testDialog.show()
//
//    }
//
////        private fun openCamera() {
////            val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
////            val outputFileUri = Uri.fromFile(BaseActivity.setCamerFilePath())
////            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
////            cameraIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
////            cameraIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
////            resultLauncher.launch(cameraIntent)
////        }
//
//
//    private fun openCamera() {
//        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//        startActivityForResult(intent, REQUEST_CAMERA)
//    }
//        private fun choosephoto() {
//            showSelectMultiplePhotosDialog {
//                val intent = Intent()
//                intent.type = "image/*"
//                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
//                intent.action = Intent.ACTION_GET_CONTENT
//                startActivityForResult(
//                    Intent.createChooser(intent, "Select Picture"),
//                    PICK_IMAGE_REQUEST_GALLERY
//                )
//            }
///*
//
//            if (EasyPermissions.hasPermissions(requireContext(), *permissionsGALLERY)) {
//                pickImageMultiple("image")
//            } else {
//                EasyPermissions.requestPermissions(
//                    requireActivity(), resources.getString(R.string.please_allow_permissions),
//                    1003, *permissionsGALLERY
//                )
//            }*/
//        }
//    private fun pickImageMultiple(type: String) {
//        filePicker = getFilePicker()
//        filePicker?.allowMultiple()
//        filePicker?.setMimeType("image/*")
//        filePicker?.pickFile()
//    }
//    private fun getFilePicker(): FilePicker? {
//        filePicker = FilePicker(this)
//        filePicker!!.setFilePickerCallback(this)
//        filePicker!!.setCacheLocation(PickerUtils.getSavedCacheLocation(requireContext()))
//        return filePicker
//    }
//
//
//        private fun checkPermissionCAMERA(contexts: Context?): Boolean {
//            val currentAPIVersion = Build.VERSION.SDK_INT
//            return if (currentAPIVersion >= Build.VERSION_CODES.M) {
//                if (ContextCompat.checkSelfPermission(
//                        contexts!!,
//                        Manifest.permission.CAMERA
//                    ) != PackageManager.PERMISSION_GRANTED) {
//                    if (ActivityCompat.shouldShowRequestPermissionRationale(
//                            (requireContext() as Activity?)!!,
//                            Manifest.permission.CAMERA
//                        )) {
//                        //Toast.makeText(PostAdPage.this, "Do itititi", Toast.LENGTH_SHORT).show();
//                        showDialog(
//                            "Camera", requireContext(),
//                            Manifest.permission.CAMERA, PERMISSION_CAMERA_REQUEST_CODE
//                        )
//                    } else {
//                        requestPermissions(
//                            arrayOf(Manifest.permission.CAMERA),
//                            PERMISSION_CAMERA_REQUEST_CODE
//                        )
//                    }
//                    false
//                } else {
//                    true
//                }
//            } else {
//                true
//            }
//        }
//
//        fun checkPermissionREAD_EXTERNAL_STORAGE(
//            context: Context?, requestCode: Int
//        ): Boolean {
//            val currentAPIVersion = Build.VERSION.SDK_INT
//            return if (currentAPIVersion >= Build.VERSION_CODES.M) {
//                if (ContextCompat.checkSelfPermission(
//                        requireContext()!!,
//                        Manifest.permission.WRITE_EXTERNAL_STORAGE
//                    ) != PackageManager.PERMISSION_GRANTED) {
//                    if (ActivityCompat.shouldShowRequestPermissionRationale(
//                            requireActivity(),
//                            Manifest.permission.WRITE_EXTERNAL_STORAGE
//                        )) {
//                        //Toast.makeText(PostAdPage.this, "Do itititi", Toast.LENGTH_SHORT).show();
//                        showDialog(
//                            "External storage", context,
//                            Manifest.permission.WRITE_EXTERNAL_STORAGE, requestCode
//                        )
//                    } else {
//                        requestPermissions(
//                            arrayOf(
//                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
//                                Manifest.permission.READ_EXTERNAL_STORAGE
//                            ),
//                            requestCode
//                        )
//                    }
//                    false
//                } else {
//                    true
//                }
//            } else {
//                true
//            }
//        }
//
//        fun showDialog(
//            msg: String, context: Context?,
//            permission: String, statusCode: Int
//        ) {
//            val alertBuilder = android.app.AlertDialog.Builder(context)
//            alertBuilder.setCancelable(false)
//            alertBuilder.setTitle("Permission necessary")
//            alertBuilder.setMessage("$msg permission is necessary")
//            alertBuilder.setPositiveButton(
//                android.R.string.yes
//            ) { dialog, which ->
//                requestPermissions(
//                    arrayOf(permission),
//                    statusCode
//                )
//            }
//            val alert = alertBuilder.create()
//            alert.show()
//        }
//
//
//        override fun onRequestPermissionsResult(
//            requestCode: Int,
//            permissions: Array<out String>,
//            grantResults: IntArray
//        ) {
//            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
//            if (requestCode == REQUEST_READ_EXTERNAL_STORAGE_CHOOSE_IMG) {
//                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    choosephoto()
//                }
//            } else if (requestCode == PERMISSION_CAMERA_REQUEST_CODE) {
//                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    openCamera()
//                }
//            }
//        }
//
//
//
//        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//            super.onActivityResult(requestCode, resultCode, data)
//            if (requestCode == PICK_IMAGE_REQUEST_GALLERY && data != null) {
//                if (multiplePhotosDialog.isShowing) {
//                    multiplePhotosDialog.dismiss();
//                }
//                val clip: ClipData? = data.clipData
//                if (clip != null && clip.itemCount != null) {
//                    for (i in 0 until (clip?.getItemCount() ?: 0)) {
//                        val item: ClipData.Item = clip.getItemAt(i)
//                        val uri: Uri = item.getUri()
//                        val tempUri = addWaterMarkOnUri(uri)
//                        if (tempUri != null) {
//
//                            val file = File(getImagePathFromURI(tempUri))
//                            val mFile = RequestBody.create("image/*".toMediaTypeOrNull(), file)
//
//                            fileToUpload = MultipartBody.Part.createFormData("image",  file.name, mFile)
//                            mArrayFile?.add(file)
//                            fileTOSet?.add(tempUri!!)
//                            imageSelected++
//                            fileToUploadArray!!.add(fileToUpload!!)
//                        }
////                    val file = File(getPath(tempUri))
////                    val mFile = RequestBody.create(MediaType.parse("image/*"), file)
////                    fileToUpload = MultipartBody.Part.createFormData("image", file.name, mFile)
////                    fileTOSet?.add(tempUri!!)
////                    mArrayFile?.add(file)
////                    fileToUploadArray!!.add(fileToUpload!!)
////                    imageSelected++
//                    }
//                } else {
//                    val tempUri = addWaterMarkOnUri(data.data)
//                    if (tempUri != null) {
//                        val file = File(getImagePathFromURI(tempUri))
//
//                        val mFile = RequestBody.create("image/*".toMediaTypeOrNull(), file)
//                        fileToUpload = MultipartBody.Part.createFormData("image",  file.name, mFile)
//                        mArrayFile?.add(file)
//                        fileTOSet?.add(tempUri!!)
//                        imageSelected++
//                        fileToUploadArray!!.add(fileToUpload!!)
////                    val file = File(getPath(tempUri))
////                    val mFile = RequestBody.create(MediaType.parse("image/*"), file)
////                    fileToUpload = MultipartBody.Part.createFormData("image", file.name, mFile)
////                    fileTOSet?.add(tempUri!!)
////                    mArrayFile?.add(file)
////                    fileToUploadArray!!.add(fileToUpload!!)
////                    imageSelected++
//                    }
//
//                }
//
//                postProductImagesAdapter?.updateImages(fileTOSet, imageSelected)
//
//            } else if (requestCode == REQUEST_CAMERA && resultCode == Activity.RESULT_OK) {
//                if(data != null && data.extras != null) {
//                    try {
//                        val bundle = data.extras
//                        var bitmap = bundle!!["data"] as Bitmap?
//
//                      //  bitmap = addWaterMark(bitmap)
//
//                        val tempUri: Uri = getImageUri(requireContext()!!, bitmap!!)!!
//                        val file = File(getImagePathFromURI(tempUri))
//
//                        val mFile = RequestBody.create("image/*".toMediaTypeOrNull(), file)
//
//                        fileToUpload = MultipartBody.Part.createFormData("image",  file.name, mFile)
//                        mArrayFile?.add(file)
//                        fileTOSet?.add(tempUri!!)
//                        imageSelected++
//                        postProductImagesAdapter?.updateImages(fileTOSet, imageSelected)
//                        fileToUploadArray!!.add(fileToUpload!!)
//
//
//                    } catch (e: Exception) {
//                        e.message
//                    }
//                }
//            } else if (requestCode == LOCATION_PERMISSION && resultCode == -1) {
//                Log.d(">>>>>>>>>>>>>>>>> ", "HELLO")
//                getPermission()
//            }
//        }
//
////    private fun addWaterMarkOnUri(uri: Uri?): Uri? {
////        if (uri == null) return null
////        val backgroundBitmap =
////            MediaStore.Images.Media.getBitmap(requireActivity().getContentResolver(), uri)
////        val bitmap = addWaterMark(backgroundBitmap) ?: return null
////        return getImageUri(requireContext(), bitmap)
////    }
////
////    private fun addWaterMark(src: Bitmap?): Bitmap? {
////        if (src == null) return null
////        val watermarkImage = WatermarkImage(requireContext(), R.drawable.launch_logo)
////            .setPosition(WatermarkPosition(0.8, 0.0))
////            .setSize(0.2)
////            .setImageAlpha(255)
////
////        return WatermarkBuilder
////            .create(context, src)
////            .loadWatermarkImage(watermarkImage)
////            .getWatermark()
////            .getOutputImage();
////    }
//
//
//    private fun getImagePathFromURI(uri: Uri): String {
//        var path = ""
//        if (requireActivity().getContentResolver() != null) {
//            val cursor: Cursor? =
//                requireActivity().getContentResolver().query(uri, null, null, null, null)
//            if (cursor != null) {
//                cursor.moveToFirst()
//                val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
//                path = cursor.getString(idx)
//                cursor.close()
//            }
//        }
//        return path
//    }
//
//    fun getImageUri(inContext: Context, inImage: Bitmap): Uri? {
//        val bytes = ByteArrayOutputStream()
//        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
//        val currentTime = SimpleDateFormat("HHmmss", Locale.getDefault()).format(Date())
//
//        val path = MediaStore.Images.Media.insertImage(
//            inContext.contentResolver,
//            inImage,
//            "Title $currentTime",
//            null
//        )
//        return Uri.parse(path)
//    }
//
//    @SuppressLint("Range")
//    fun getPath(uri: Uri?): String? {
//        var cursor: Cursor =
//            requireActivity().getContentResolver().query(uri!!, null, null, null, null)!!
//        cursor.moveToFirst()
//        var document_id = cursor.getString(0)
//        document_id = document_id.substring(document_id.lastIndexOf(":") + 1)
//        cursor.close()
//        cursor = requireActivity().getContentResolver().query(
//            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
//            null, MediaStore.Images.Media._ID + " = ? ", arrayOf(document_id), null
//        )!!
//        cursor.moveToFirst()
//        val path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA))
//        cursor.close()
//        return path
//    }
//
//
//    override fun onDeleteClicked(position: Int) {
//            specificationString?.removeAt(position)
//            postProductSpecificationTextAdapter?.updateText(specificationString)
//        }
//
//   /*     override fun onItemClicked(textSelected: String, position: Int) {
//            testDialog?.dismiss()
//            if (isConditionClicked) {
//                contionTextString = textSelected
//                if (textSelected.contains("#")){
//                    contionText?.text = textSelected.subSequence(0, textSelected.indexOf("#"))
//                }else
//                    contionText?.text = textSelected
//
//            } else {
//                categoryText?.text = textSelected
//                if (headerTextCategoryEngish?.size?:0 > cat_selected_pos) {
//                    parent_cat_text = headerTextCategoryEngish?.get(cat_selected_pos) ?: ""
//                }
//                if (headerTextSubCategoryEnglish?.size?:0 > cat_selected_pos) {
//                    parent_sub_cat_text =
//                        headerTextSubCategoryEnglish?.get(cat_selected_pos)?.get(position)
//                            .toString()
//                }
//            }
//
//        }*/
//
//    private lateinit var multiplePhotosDialog: Dialog
//    private fun showSelectMultiplePhotosDialog(click: ()->Unit) {
//
//
//        multiplePhotosDialog= Dialog(requireContext(),R.style.DialogFullScreenTheme)
//        if(preferenceManager?.canShowSelectMultiplePhotosDialog ==true) {
//            val dialogBindig = DataBindingUtil.inflate<SelectMultiplePhotosDialogBinding>(
//                LayoutInflater.from(requireContext()),
//                R.layout.select_multiple_photos_dialog,
//                null,
//                false
//            )
//            multiplePhotosDialog.setContentView(dialogBindig.root)
//            multiplePhotosDialog.setCancelable(false)
//            multiplePhotosDialog.show()
//            dialogBindig.button2.setOnClickListener{
//                multiplePhotosDialog.dismiss()
//                preferenceManager?.canShowSelectMultiplePhotosDialog = false
//                click.invoke()
//            }
//        }else{
//            click.invoke()
//        }
//    }
//
//    private fun showRepositionPhotosDialog() {
//        if (preferenceManager?.getCanShowRepositionPhotosDialog() == true) {
//            val dialogBindig = DataBindingUtil.inflate<PhotosRepositionDialogBinding>(
//                LayoutInflater.from(requireContext()),
//                R.layout.photos_reposition_dialog,
//                null,
//                false
//            )
//            val dialog = Dialog(requireContext(), R.style.DialogFullScreenTheme)
//            dialog.setContentView(dialogBindig.root)
//            dialog.setCancelable(false)
//            dialog.show()
//            dialogBindig.button2.setOnClickListener {
//                preferenceManager?.setCanShowRepositionPhotosDialog(false)
//                dialog.dismiss()
//            }
//        }
//    }
//
//        override fun onItemClicked(textSelected: String?, position: Int, completeStringForServer: String) {
//            testDialog?.dismiss()
//        if (isConditionClicked) {
//            contionTextString = completeStringForServer
//            contionText?.text = textSelected
//        } else {
//            categoryText?.text = textSelected
//            if (textSelected!!.contains(":")) {
//                val index = textSelected.indexOf(":")
//                parent_cat_text = mCategoryHeader?.get(position)
//                    ?.substring(index, mCategoryHeader?.get(position)?.length!!)!!
//            }else {
//                parent_cat_text = mCategoryHeader?.get(position)!!
//            }
//        }
//    }
//
//        override fun onLocationChanged(location: Location) {
//            if (Common.productLocationLat.equals("")) {
//                Common.productLocationLat = location.getLatitude().toString()
//                Common.productLocationLng = location.getLongitude().toString()
//                initMap()
//            }
//        }
//
//        protected fun createMarker(
//            latitude: Double,
//            longitude: Double,
//            name: String?
//        ): Marker? {
//            try {
//                val height = 80;
//                val width = 40;
//                val bitmapdraw = getResources().getDrawable(R.drawable.map_pin) as BitmapDrawable;
//                val b = bitmapdraw.getBitmap();
//                val smallMarker = Bitmap.createScaledBitmap(b, width, height, false)
//                val latLng = LatLng(latitude, longitude)
//                currentMarker?.remove()
//                currentMarker= googleMap?.addMarker(
//                    MarkerOptions()
//                        .position(LatLng(latitude, longitude))
//                        //   .anchor(1.5f, 1.5f)
//                        .title(name)
//                        .draggable(true)
//                        .icon(BitmapDescriptorFactory.fromBitmap(smallMarker))
//                )
//
//
//                val builder = LatLngBounds.Builder()
//                builder.include(latLng)
//                val bounds = builder.build()
//                val padding = 0 // offset from edges of the map in pixels
//                val cu = CameraUpdateFactory.newLatLngBounds(bounds, padding)
//                googleMap?.moveCamera(cu)
//            }catch (e: Exception){
//
//            }finally {
//                return  currentMarker
//            }
//        }
//
//
//    override fun onMapChoosed() {
//        setAddress()
//    }
//
//    override fun provideBinding(inflater: LayoutInflater): FragmentPostproductBinding {
//        return FragmentPostproductBinding.inflate(inflater)
//    }
//
//    override fun onDestroyView() {
//        super.onDestroyView()
//    }
//
//    override fun onError(p0: String?) {
//
//    }
//
//    override fun onFilesChosen(list: MutableList<ChosenFile>?) {
//        if (list?.size ?: 0 > 0) {
//            for (i in 0 until list?.size!!) {
//                val file1 = File(list?.get(i)?.getOriginalPath())
//                val file = baseActivity?.getCompressed(requireContext(), file1.path)
//                val mFile = RequestBody.create("image/*".toMediaTypeOrNull(), file!!)
//                fileToUpload =
//                    MultipartBody.Part.createFormData("image", file?.name, mFile)
//                fileTOSet?.add(Uri.fromFile(file))
//                imagesStringUrl?.add("")
//                mArrayFile?.add(file!!)
//                fileToUploadArray!!.add(fileToUpload!!)
//                imageSelected++
//            }
//            postProductImagesAdapter?.updateImages(fileTOSet, imageSelected)
//        }
//    }
//
//    override fun onCategorySelected(text: String?, pos: Int) {
//        cat_selected_pos = pos
//        postProductNormalTextAdapter?.updateText(headerTextSubCategory?.get(pos))
//    }
//
//    override fun onMapReady(googleMaps: GoogleMap) {
//        googleMap = googleMaps
//        var lat: Double
//        var lng: Double
//        var name: String
//        val latti: String = Common.productLocationLat
//        val longi: String = Common.productLocationLng
//        if (latti != null && latti != "" && longi != null && longi != ""
//        ) {
//            lat = Common.productLocationLat.toDouble()
//            lng = Common.productLocationLng.toDouble()
//            Log.d(
//                "POSTPRODUCT",
//                "loadMap: $lat   lng  $lng"
//            )
//            name = "My Location"
//            createMarker(lat, lng, name)
//
//            googleMaps?.animateCamera(
//                CameraUpdateFactory.newLatLngZoom(
//                    LatLng(
//                        latti.toFloat().toDouble(),
//                        longi.toFloat().toDouble()
//                    ), 12.0f
//                )
//            )
//            try {
//                getAddress(lat, lng)
//            } catch (e: java.lang.Exception) {
//                e.printStackTrace()
//            }
//        }
//    }
//
//
//}