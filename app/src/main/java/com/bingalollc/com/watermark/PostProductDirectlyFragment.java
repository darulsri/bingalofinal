//package com.bingalollc.com;
//
//import android.content.Context
//import android.content.Intent
//import android.location.Geocoder
//import android.net.Uri
//import android.os.Bundle
//import android.text.Editable
//import android.text.TextUtils
//import android.text.TextWatcher
//import android.util.Log
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import android.view.inputmethod.InputMethodManager
//import android.widget.ImageView
//import android.widget.RelativeLayout
//import android.widget.TextView
//import androidx.appcompat.app.AlertDialog
//import androidx.fragment.app.DialogFragment
//import androidx.fragment.app.FragmentActivity
//import androidx.recyclerview.widget.LinearLayoutManager
//import androidx.recyclerview.widget.RecyclerView
//import com.bingalollc.com.R
//import com.bingalollc.com.base.BaseActivity
//import com.bingalollc.com.homeactivity.HomeActivity
//import com.bingalollc.com.homeactivity.model.CategoriesModel
//import com.bingalollc.com.homeactivity.postproduct.adapter.PostProductNormalTextAdapter
//import com.bingalollc.com.homeactivity.postproduct.adapter.ProductCategoryAdapter
//import com.bingalollc.com.model.ApiStatusModel
//import com.bingalollc.com.network.MultipartParams
//import com.bingalollc.com.network.RestClient
//import com.bingalollc.com.preference.Common
//import com.bingalollc.com.preference.PreferenceManager
//import com.bingalollc.com.utils.dialog.ProgressDialog
//import com.bumptech.glide.Glide
//import com.google.android.material.textfield.TextInputEditText
//import jp.wasabeef.glide.transformations.BlurTransformation
//import retrofit2.Call
//import retrofit2.Callback
//import retrofit2.Response
//import java.io.File
//import java.io.IOException
//import java.util.*
//import kotlin.collections.ArrayList
//
//
//class PostProductDirectlyFragment : DialogFragment(), PostProductNormalTextAdapter.OnClickedCalled,
//    ProductCategoryAdapter.OnClickedCalled {
//    private var setOnClickMethod: OnPostClickedCalled? = null
//    private var savedUri: Uri? = null
//    private var fullImageView: ImageView? = null
//    private var imageView: ImageView? = null
//    private var btn_cross: ImageView? = null
//    private var layout: RelativeLayout? = null
//    private var et_category: TextInputEditText? = null
//    private var product_title: TextInputEditText? = null
//    private var product_price: TextInputEditText? = null
//    private var headerTextCategory: ArrayList<String>? = null
//    private var headerTextCategoryEngish:ArrayList<String>? =null
//    private var headerTextCategoryImage: ArrayList<String>? = null
//    private var headerTextSubCategory:ArrayList<ArrayList<String>>? =null
//    private var headerTextSubCategoryEnglish:ArrayList<ArrayList<String>>? =null
//    private var baseActivity: BaseActivity? = null
//    private var testDialog: AlertDialog? = null
//    private var parent_cat_text: String = ""
//    private var parent_sub_cat_text: String = ""
//    private var doneBtn: TextView? = null
//    private var mArrayFile: java.util.ArrayList<File>? = ArrayList()
//    private var preferenceManager: PreferenceManager? = null
//    private var postProductNormalTextAdapter: PostProductNormalTextAdapter? = null
//    private var postProductCategoryAdapter: ProductCategoryAdapter? = null
//    private var isClicked=false
//    var et_city: String = ""
//    var et_state: String = ""
//    var et_address: String = ""
//    var et_zip: String = ""
//    var et_country: String = ""
//    private val currencySymbol = "₪ "
//    private var cat_selected_pos = 0
//
//
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setStyle(STYLE_NORMAL, R.style.FullScreenDialogStyle)
//
//    }
//
//    override fun onCreateView(
//        inflater: LayoutInflater, container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View? {
//        // Inflate the layout for this fragment
//        val view= inflater.inflate(R.layout.fragment_post_product_directly, container, false)
//        initViews(view)
//        try{
//            getAddress(Common.productLocationLat.toDouble(),Common.productLocationLng.toDouble())
//        }catch (e: Exception ){}
//        initValue()
//        return view
//    }
//
//    private fun getCategories() {
//        RestClient.getApiInterface().categories.enqueue(object : Callback<CategoriesModel>{
//            override fun onFailure(call: Call<CategoriesModel>, t: Throwable) {
//
//            }
//
//            override fun onResponse(
//                call: Call<CategoriesModel>,
//                response: Response<CategoriesModel>
//            ) {
//                if (response.body() != null && response.body()?.status==200 && response.body()?.data != null){
//                   // val headerTextCompleteSubCategory = ArrayList<String>()
//                    (response.body()?.data!!.forEachIndexed { index, datum ->
//                        if (datum.subCategories.isEmpty().not()) {
//                            headerTextCategoryImage?.add(datum?.categoryImage ?: "")
//                            headerTextCategoryEngish?.add(datum?.categoryName?:"")
//                            if (preferenceManager?.isEnglish == 1)
//                                headerTextCategory?.add(datum?.categoryName ?: "")
//                            else
//                                headerTextCategory?.add(datum?.herbewCategoryName ?: "")
//
//                            val subCat = ArrayList<String>()
//                            val subCat_heb = ArrayList<String>()
//
//                            datum?.subCategories?.forEach {
//                                subCat.add(it.name)
//                                subCat_heb.add(it.herbewName)
//                            }
//                            headerTextSubCategoryEnglish?.add(subCat)
//                            if (preferenceManager?.isEnglish == 1)
//                                headerTextSubCategory?.add(subCat)
//                            else
//                                headerTextSubCategory?.add(subCat_heb)
//                        }
//                    })
//
//                    //headerTextSubCategory?.set(0, headerTextCompleteSubCategory)
//                }
//
//            }
//        })
//    }
//
//
//    private fun initValue() {
//        getCategories()
//
//        Glide.with(requireContext()).load(savedUri).transform(BlurTransformation(40)).into(imageView!!)
//
//
//        product_price?.addTextChangedListener(object :TextWatcher{
//            override fun afterTextChanged(p0: Editable?) {
//                val pp = product_price?.text
//                if (pp?.length == 1) {
//                    product_price?.setText(currencySymbol + pp)
//                    product_price?.setSelection(product_price!!.length())
//                } else if (product_price?.text?.contains(currencySymbol) == true && product_price?.text?.replace(
//                        currencySymbol.toRegex(),
//                        ""
//                    )?.length == 0
//                ) {
//                    product_price?.setText("")
//                }
//            }
//
//            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
//
//            }
//
//            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
//
//            }
//        })
//
//    }
//
//    private fun initViews(view: View?) {
//        baseActivity= BaseActivity()
//        headerTextCategory= ArrayList()
//        headerTextCategoryEngish= ArrayList()
//        headerTextCategoryImage= ArrayList()
//        headerTextSubCategory= ArrayList()
//        headerTextSubCategoryEnglish= ArrayList()
//        preferenceManager = PreferenceManager(requireContext())
//        fullImageView = view?.findViewById(R.id.fullImageView)
//        imageView = view?.findViewById(R.id.imageView)
//        product_title = view?.findViewById(R.id.product_title)
//        product_price = view?.findViewById(R.id.product_price)
//        btn_cross = view?.findViewById(R.id.btn_cross)
//        et_category = view?.findViewById(R.id.et_category)
//        layout = view?.findViewById(R.id.layout)
//        doneBtn = view?.findViewById(R.id.doneBtn)
//        /*if (savedUri!=null){
//            Glide.with(requireContext()).load(savedUri).into(fullImageView!!)
//        }else{
//            fullImageView?.setBackgroundColor(requireContext().resources.getColor(R.color.black))
//        }*/
//        btn_cross?.setOnClickListener { dismiss() }
//        et_category?.setOnClickListener {
//            baseActivity?.hideKeyBoardOfView(activity,product_price)
//            showListDialog()
//        }
//
//        doneBtn?.setOnClickListener {
//            val imm: InputMethodManager =
//                activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//            imm.hideSoftInputFromWindow(getView()!!.windowToken, 0)
//            postProducts()
//        }
//    }
//
//    private fun postProducts() {
//        if (_allConditionValid() && !isClicked) {
//            isClicked=true
//            ProgressDialog.showProgressDialog(requireContext())
//           // val prod_specification = specificationString.toString().replace("[", "").replace("]", "")
//            val builder = MultipartParams.Builder()
//            builder.add("product_title", product_title?.text.toString())
//            builder.add("product_price", product_price?.text.toString().replace(currencySymbol, ""))
//            builder.add("product_specifications", "")
//            builder.add("product_condition", "")
//            builder.add("product_category", parent_sub_cat_text)
//            builder.add("parent_category", parent_cat_text)
//            builder.add("product_description", "")
//            builder.add("product_location", et_address)
//            builder.add("product_city", et_city)
//            builder.add("product_state", et_state)
//            builder.add("product_country", et_country)
//            builder.add("lat", Common.productLocationLat)
//            builder.add("lng", Common.productLocationLng)
//            builder.add("uploaded_by_user_id", preferenceManager?.userDetails?.id)
//            builder.add("product_is_sold", "0")
//
//            for (i in 0 until mArrayFile?.size!!) {
//                if (!TextUtils.isEmpty(mArrayFile?.get(i).toString()))
//                    builder.addFile("image[]", mArrayFile?.get(i))
//
//            }
//            RestClient.getApiInterface().postproducts(builder.build().map).enqueue(object :
//                Callback<ApiStatusModel> {
//                override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
//                    ProgressDialog.dismissProgressDialog()
//                }
//
//                override fun onResponse(call: Call<ApiStatusModel>, response: Response<ApiStatusModel>) {
//                    ProgressDialog.dismissProgressDialog()
//
//                    if (response.body()?.getStatus() == 200) {
//                        baseActivity?.showToast(getString(R.string.product_added), requireContext())
//                        val intent = Intent(requireContext(), HomeActivity::class.java)
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                        startActivity(intent)
//                        activity?.finish()
//                    } else {
//                        baseActivity?.showToast(
//                            "Something went wrong",
//                            requireContext()
//                        )
//                    }
//
//
//                }
//            })
//        }
//    }
//
//    private fun _allConditionValid(): Boolean {
//        if (product_title?.text.toString().length == 0) {
//            baseActivity?.showToast(getString(R.string.title_require), context);
//            return false
//        } else if (product_price?.text.toString().length == 0) {
//            baseActivity?.showToast(getString(R.string.title_price), context);
//            return false
//        }else if (product_price?.text!!.substring(2).toInt() < 10){
//            baseActivity?.showToast(getString(R.string.min_price), context);
//            return false
//        } else if (et_category?.text.toString().length == 0) {
//            baseActivity?.showToast(getString(R.string.category_required), context);
//            return false
//        } else if (mArrayFile?.size == 0) {
//            baseActivity?.showToast(getString(R.string.add_image), context);
//            return false
//        } else if (TextUtils.isEmpty(et_address)) {
//            getAddress(Common.productLocationLat.toDouble(), Common.productLocationLng.toDouble())
//            return true
//        } else
//            return true
//    }
//
//    fun showListDialog() {
//        val alertDialog = AlertDialog.Builder(requireContext())
//        val inflater = layoutInflater
//        val add_menu_layout = inflater.inflate(R.layout.alert_dialog_header, null)
//        val rvListView: RecyclerView = add_menu_layout.findViewById(R.id.rvListView)
//        val rvCatView: RecyclerView = add_menu_layout.findViewById(R.id.rvCatView)
//        val tv_selectCategory: TextView = add_menu_layout.findViewById(R.id.tv_selectCategory)
//        tv_selectCategory.text = getString(R.string.choose_product_category)
//        val crossBtn: ImageView = add_menu_layout.findViewById(R.id.crossBtn)
//        alertDialog.setView(add_menu_layout)
//        alertDialog.setCancelable(false)
//        initNameRecyclerView(rvListView, rvCatView)
//        crossBtn?.setOnClickListener({ testDialog?.dismiss() })
//        testDialog = alertDialog.create()
//        testDialog!!.show()
//    }
//
//    private fun initNameRecyclerView(rvListView: RecyclerView,rvCatView: RecyclerView, ) {
//
//        if (headerTextSubCategory?.size?:0 > 0) {
//            postProductNormalTextAdapter = PostProductNormalTextAdapter(
//                headerTextSubCategory?.get(0),
//                null,
//                this
//            )
//            postProductCategoryAdapter =
//                ProductCategoryAdapter(headerTextCategory, headerTextCategoryImage, this)
//        }
//        rvCatView.visibility = View.VISIBLE
//        val linearLayoutManagers = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
//        rvCatView.adapter = postProductCategoryAdapter;
//        rvCatView.layoutManager = linearLayoutManagers
//
//        val linearLayoutManager = LinearLayoutManager(requireContext())
//        rvListView.adapter = postProductNormalTextAdapter;
//        rvListView.layoutManager = linearLayoutManager
//    }
//
//
//    companion object {
//        @JvmStatic
//        fun show(context: FragmentActivity, savedUri: Uri,mArrayFile: java.util.ArrayList<File>, setOnClickMethod:OnPostClickedCalled) {
//            val fragment = PostProductDirectlyFragment()
//            fragment.setOnClickMethod = setOnClickMethod
//            fragment.savedUri = savedUri
//            fragment.mArrayFile = mArrayFile
//            val bundle = Bundle()
//            fragment.arguments = bundle
//            val transaction = context.supportFragmentManager.beginTransaction()
//            fragment.show(transaction, "dialog")
//        }
//    }
//
//    interface OnPostClickedCalled {
//        fun onSaveClicked(isPost: Boolean)
//    }
//
//    override fun onItemClicked(textSelected: String, position: Int, normalString: String) {
//        testDialog?.dismiss()
//        et_category?.setText(textSelected)
//        parent_cat_text = headerTextCategoryEngish?.get(cat_selected_pos)?:""
//        parent_sub_cat_text = headerTextSubCategoryEnglish?.get(cat_selected_pos)?.get(position).toString()
//    }
//
//    private fun getAddress(latitude: Double, longitude: Double): String? {
//        val result = StringBuilder()
//        var cityState =""
//
//        try {
//            val geocoder = Geocoder(requireContext(), Locale.getDefault())
//            val addresses = geocoder.getFromLocation(latitude, longitude, 1)
//            if (addresses.size > 0) {
//                val address = addresses[0]
//                result.append(address.featureName).append("\n")
//                result.append(address.locality).append("\n")
//                result.append(address.countryName)
//                // exactAddress = "Near " + address.getAddressLine(0);
//                if ( addresses[0].adminArea.isNullOrEmpty().not())
//                    et_state = addresses[0].adminArea
//                else et_state = ""
//
//                if (addresses[0].subAdminArea.isNullOrEmpty().not())
//                    et_city = addresses[0].subAdminArea
//                else et_city = ""
//
//                if (addresses[0].postalCode.isNullOrEmpty().not())
//                    et_zip =  addresses[0].postalCode
//                else
//                    et_zip = ""
//
//                if (address.countryName.isNullOrEmpty().not())
//                    et_country = address.countryName
//                else et_country = ""
//
//                et_address = result.toString()
//                if (address.locality.isNullOrEmpty().not() && address.adminArea.isNullOrEmpty().not())
//                    cityState = address.locality+", "+ address.adminArea
//                else cityState = ""
//
//            }
//        } catch (e: IOException) {
//            Log.e("tag", e.message!!)
//        }
//        return cityState
//    }
//
//    override fun onCategorySelected(text: String?, pos: Int) {
//        cat_selected_pos = pos
//        postProductNormalTextAdapter?.updateText(headerTextSubCategory?.get(pos))
//    }
//}