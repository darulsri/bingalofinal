package com.bingalollc.com.network;


import com.bingalo.com.homeactivity.model.ShopListingModel;
import com.bingalo.com.homeactivity.model.homebannerModel;
import com.bingalollc.com.cart.additem;
import com.bingalollc.com.cart.addressmodel;
import com.bingalollc.com.cart.storeDeliverymodel;
import com.bingalollc.com.chat.model.SponsoredModel;
import com.bingalollc.com.homeactivity.model.AdminMessageModel;
import com.bingalollc.com.homeactivity.model.AdsModel;
import com.bingalollc.com.homeactivity.model.CategoriesModel;
import com.bingalollc.com.homeactivity.model.ProductModel;
import com.bingalollc.com.homeactivity.model.ProfilePicResponseModel;
import com.bingalollc.com.homeactivity.model.TrendingSearches;
import com.bingalollc.com.homeactivity.notification.model.NotificationCountModel;
import com.bingalollc.com.homeactivity.notification.model.NotificationModel;
import com.bingalollc.com.homeactivity.profilefragment.model.ProductCountOfUser;
import com.bingalollc.com.maintainance.model.MaintainanceResponse;
import com.bingalollc.com.model.ApiStatusModel;
import com.bingalollc.com.model.BlockedUserModel;
import com.bingalollc.com.model.ClientIdModel;
import com.bingalollc.com.model.SignUpModel;
import com.bingalollc.com.model.SocialLoginModel;
import com.bingalollc.com.payment.model.BankAccountModel;
import com.bingalollc.com.payment.model.CardViewModel;
import com.bingalollc.com.stores.productmodel;
import com.bingalollc.com.stores.storecategorymodel;
import com.bingalollc.com.users.Users;
import com.bingalollc.com.users.UsersArrayModel;
import com.bingalollc.com.utils.nearbyLocation.PlacesResults;
import com.firebase.ui.auth.data.model.User;
import com.google.protobuf.Api;

import java.util.ArrayList;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

/**
 * <p>
 * The API interface for your application
 */
public interface ApiInterface {

    @GET("fetchMobileAds")
    Call<AdsModel> fetchMobileAds(@Query("ad_type") String ad_type);

    @GET("getCategories")
    Call<CategoriesModel> getCategories();

    @GET("getProductRankWiseAndroid")
    Call<TrendingSearches> getProductRankWise();

    @GET("getUserDetails")
    Call<Users> getUserDetails(@Query("user_id") String user_id);

    @GET("fetchAppMaintaince")
    Call<MaintainanceResponse> fetchAppMaintainance();

    @Multipart
    @POST("uploadRandomImage")
    Call<Object> uploadRandomImage(@Part MultipartBody.Part image);

    @GET("searchNearbyProducts")
    Call<ProductModel> searchNearbyProducts(@Query("lat") String lat,@Query("lng") String lng, @Query("max_distance") String max_distance,
                                            @Query("current_user_id") String current_user_id, @Query("limit") int limit, @Query("offset") int offset,
                                            @Query("current_date_time") String currentDate, @Query("language") Integer language);

    @GET("getUserDetails")
    Call<Users> updateUsersDetails(@Query("user_id") String user_id);

    @GET("getProductByUserIdAndroid")
    Call<ProductModel> getProductByUserId(@Query("user_id") String user_id,@Query("status") String status);

    @GET("getProductCountByUserId")
    Call<ProductCountOfUser> getProductCountByUserId(@Query("user_id") String userid);

    @GET("getProductListData")
    Call<ProductModel> getProductDetails(@Query("product_ids") String userid, @Query("token") String token);

    @GET("getAdminMessages")
    Call<AdminMessageModel> getAdminMessages();

    @GET("getNotifications")
    Call<NotificationModel> getNotifications(@Query("user_id") String userid);

    @GET("fetchNotificationBadge")
    Call<NotificationCountModel> fetchNotificationBadge(@Query("user_id") String userid, @Query("token") String token);

    @FormUrlEncoded
    @POST("prepareApplePay")
    Call<ClientIdModel> getStripeClientId(@Field("amount") String amount, @Field("currency") String currency);

    @GET("getUsersListData")
    Call<UsersArrayModel> getUsersList(@Query("user_ids") String user_ids, @Query("token") String token);

   // @GET("search_product")
    @GET("newSearchPoral")
    Call<ProductModel> search_product(@Query("name") String name,@Query("product_condition") String product_condition,@Query("lat") String lat,
                                      @Query("lng") String lng,
                                      @Query("current_lat") String currentlat,@Query("current_lng") String currentlng,
                                      @Query("sort_type") String sort_type, @Query("min_price") String min_price,
                                      @Query("max_price") String max_price,@Query("parent_category") String parent_category,
                                      @Query("max_distance") String max_distance, @Query("current_user_id") String current_user_id,
                                      @Query("limit") int limit, @Query("offset") int offset,
                                      @Query("current_date_time") String currentDate,  @Query("language") Integer language);

    @GET("search_product")
    Call<ProductModel> old_search_product(@Query("name") String name,@Query("product_condition") String product_condition,@Query("lat") String lat,
                                      @Query("lng") String lng,
                                      @Query("current_lat") String currentlat,@Query("current_lng") String currentlng,
                                      @Query("sort_type") String sort_type, @Query("min_price") String min_price,
                                      @Query("max_price") String max_price,@Query("parent_category") String parent_category,
                                      @Query("max_distance") String max_distance, @Query("current_user_id") String current_user_id,
                                      @Query("limit") int limit, @Query("offset") int offset,
                                      @Query("current_date_time") String currentDate,  @Query("language") Integer language);

    @Multipart
    @POST("postproductweb")
    Call<ApiStatusModel> postproducts(@PartMap() Map<String,RequestBody> mapFileAndName);

    @Multipart
    @POST("uploadProductImage")
    Call<ApiStatusModel> uploadImage(@PartMap() Map<String,RequestBody> mapFileAndName);

    @Multipart
    @POST("edit_product")
    Call<Object> editproducts(@PartMap() Map<String,RequestBody> mapFileAndName);

    @Multipart
    @POST("postproductweb")
    Call<Object> postproduct(@Part("product_title") RequestBody product_title, @Part("product_price") RequestBody product_price,
                                    @Part("product_specifications") RequestBody product_specifications, @Part("product_condition") RequestBody product_condition, @Part("product_category") RequestBody product_category,
                                    @Part("parent_category") RequestBody parent_category, @Part("product_description")RequestBody product_description, @Part("product_location") RequestBody product_location,
                             @Part("product_city")RequestBody product_city,
                             @Part("product_state")RequestBody product_state,
                                    @Part("product_country") RequestBody product_country,
                                    @Part("lat") RequestBody lat, @Part("lng") RequestBody lng, @Part("uploaded_by_user_id") RequestBody uploaded_by_user_id,
                             @Part("product_is_sold")RequestBody product_is_sold,
                                    @Part ArrayList<MultipartBody.Part> image);

    @FormUrlEncoded
    @POST("login")
    Call<Users> login(@Field("email") String email, @Field("password") String otp);

    @FormUrlEncoded
    @POST("payBrainTree")
    Call<ApiStatusModel> makePaymentApi(@Field("payment_method_nonce") String payment_method_nonce, @Field("amount") String amount,
                      @Field("device_data") String device_data);

    @FormUrlEncoded
    @POST("forgotPassword")
    Call<ApiStatusModel> forgotPassword(@Field("email") String email);

    @FormUrlEncoded
    @POST("updateLocation")
    Call<ApiStatusModel> updateLocation(@Field("user_id") String user_id, @Field("latitude") String latitude,
                                        @Field("longitude") String longitude,  @Field("location") String location);

    @FormUrlEncoded
    @POST("deleteproduct")
    Call<ApiStatusModel> deleteProduct(@Field("token") String token, @Field("product_id") String product_id);

    @FormUrlEncoded
    @POST("resetNotificationCount")
    Call<ApiStatusModel> resetNotificationCount(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("readNotification")
    Call<ApiStatusModel> readNotification(@Field("notification_id") String notification_id, @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("reportUserAndProduct")
    Call<ApiStatusModel> reportUser(@Field("post_id") String post_id, @Field("reported_user_id") String reported_user_id,
                                    @Field("reported_reason") String reported_reason, @Field("reported_desc") String reported_desc,
                                    @Field("reported_by") String reported_by);

    @Multipart
    @POST("signup")
    Call<SignUpModel> signUp(@PartMap() Map<String, RequestBody> signupData);

    @FormUrlEncoded
    @POST("social_login")
    Call<SocialLoginModel> socialLogin(@Field("full_name")String full_name,@Field("email")String email,
                                       @Field("lat")String lat,@Field("lng")String lng,
                                       @Field("social_id")String social_id,@Field("social_login_type")String social_login_type,
                                       @Field("photo_link")String photo_link,
                                       @Field("singup_from")String singup_from);

    @FormUrlEncoded
    @POST("addFavourite")
    Call<ApiStatusModel> addToFavourite(@Field("token")String token,@Field("product_id")String product_id,
                                        @Field("user_id")String user_id);

    @FormUrlEncoded
    @POST("updateChatUser")
    Call<ApiStatusModel> updateChatUser(@Field("token")String token,@Field("firebase_node")String firebase_node,
                                        @Field("user_id_one")String user_id_one,
                                        @Field("user_id_two")String user_id_two,
                                        @Field("product_id")String product_id);


    @FormUrlEncoded
    @POST("addUpdatedProductBump")
    Call<ApiStatusModel> addUpdatedProductBump(@Field("bump_date")String bump_date,@Field("bump_days")String bump_days,
                                        @Field("bump_user_id")String bump_user_id, @Field("bump_price")String bump_price,
                                               @Field("product_id")String product_id);

    @FormUrlEncoded
    @POST("sendNotificationFromWeb")
    Call<ApiStatusModel> sendNotificationFromWeb(@Field("title")String title,@Field("body")String body,
                                               @Field("type")String type, @Field("push_token")String push_token,
                                               @Field("id")String id, @Field("product_id") String product_id,
                                                 @Field("sender_user_id") String sender_user_id,
                                                 @Field("receiver_user_id") String receiver_user_id);

    @FormUrlEncoded
    @POST("updatePushToken")
    Call<ApiStatusModel> updateToken(@Field("token") String token, @Field("id") String user_id,@Field("push_token") String push_token);

    @FormUrlEncoded
    @POST("removefavourite")
    Call<ApiStatusModel> removeFromFavourite(@Field("token")String token,@Field("product_id")String product_id,
                                        @Field("user_id")String user_id);


    @GET("getBlockedUsers")
    Call<BlockedUserModel> getBlockedUsers(@Query("user_id")String user_id,@Query("token")String token);

    @GET("getSponsoredLocations")
    Call<SponsoredModel> getSponsoredLocations(@Query("lat")String lat, @Query("lng")String lng, @Query("max_distance")String max_distance);

    @FormUrlEncoded
    @POST("changeUserName")
    Call<SignUpModel> changeUsername(@Field("user_id") String userid, @Field("name") String name );

    @FormUrlEncoded
    @POST("change_password")
    Call<ApiStatusModel> changePassword(@Field("user_id") String userid, @Field("name") String name,
                                        @Field("token") String token, @Field("password") String password );

    @Multipart
    @POST("changeProfilePic")
    Call<ProfilePicResponseModel> changeProfilePic(@PartMap() Map<String, RequestBody> profileImage);

    @Multipart
    @POST("updateBankAccountOfStripe")
    Call<ApiStatusModel> updateBankAccountOfStripe(@PartMap() Map<String, RequestBody> data);

    @FormUrlEncoded
    @POST("submitReview")
    Call<ApiStatusModel> submitReview(@Field("token") String token, @Field("user_id") String user_id,
                                      @Field("review") String review,
                                      @Field("rating") String rating,
                                      @Field("review_to") String review_to);
    @FormUrlEncoded
    @POST("chageProductIsSoldStatus")
    Call<ApiStatusModel> chageIsSoldStatus(@Field("product_id") String product_id, @Field("status") String status);

    @FormUrlEncoded
    @POST("insert_build_trust")
    Call<ApiStatusModel> updateBuildTrust(@Field("token") String token, @Field("user_id") String user_id, @Field("facebook_id") String facebook_id,
                                          @Field("google_id") String google_id, @Field("twitter_id") String twitter_id);

    @FormUrlEncoded
    @POST("deleteimage")
    Call<ApiStatusModel> deleteimage(@Field("id") String product_image_id);

    @FormUrlEncoded
    @POST("sendOTP")
    Call<ApiStatusModel> sendOTP(@Field("email") String email, @Field("phone") String phone);

    @FormUrlEncoded
    @POST("verifyOtp")
    Call<ApiStatusModel> verifyOtp(@Field("user_id") String user_id , @Field("phone") String phone, @Field("email") String email, @Field("otp") String otp);

    @FormUrlEncoded
    @POST("changeEmail")
    Call<Users> changeEmail(@Field("user_id") String user_id, @Field("email") String email);

    @FormUrlEncoded
    @POST("deleteUserCard")
    Call<ApiStatusModel> deleteUserCard(@Field("card_id") String card_id);

    @FormUrlEncoded
    @POST("addUserCard")
    Call<ApiStatusModel> addUserCard(@Field("user_id") String user_id, @Field("card_holder_name") String card_holder_name, @Field("card_expiry_month") String card_expiry_month, @Field("card_expiry_year") String card_expiry_year,
                                    @Field("card_number") String card_number, @Field("card_cvv") String card_cvv, @Field("stripe_token") String stripe_token);

    @FormUrlEncoded
    @POST("update_verification")
    Call<ApiStatusModel> update_verification(@Field("user_id") String user_id, @Field("email_id") String email_id,
                                             @Field("verify_method") String verify_method);

    @GET("place/nearbysearch/json")
    Call<PlacesResults> getNearBy(
            @Query("location") String location,
            @Query("radius") int radius,
            @Query("type") String type,
            @Query("keyword") String keyword,
            @Query("key") String key,
            @Query("rank    By") String distance
    );

    ///CARD
    @GET("getUserCard")
    Call<CardViewModel> getUserCard(@Query("user_id") String user_ids, @Query("token") String token);

    ///CARD
    @GET("fetchUserBankDetails")
    Call<BankAccountModel> fetchUserBankDetails(@Query("user_id") String user_ids, @Query("token") String token);


    @FormUrlEncoded
    @POST("changeActivationStatus")
    Call<ApiStatusModel> deactivateAccount(@Field("user_id") String user_id,
                                             @Field("status") String status);

    @FormUrlEncoded
    @POST("submitNotificaton")
    Call<ApiStatusModel> submitNotificaton(@Field("notification_title") String notification_title,
                                           @Field("notification_description") String notification_description,
                                           @Field("notification_to") String notification_to,
                                           @Field("notification_by") String notification_by,
                                           @Field("notification_type") String notification_type,
                                           @Field("product_id") String product_id);


    @GET("fetchTopBanner")
    Call<homebannerModel>fetchTopBanner();

    @GET("fetchStores")
    Call<ShopListingModel>fetchStores();

    @GET("fetchStoreProducts")
    Call<productmodel>fetchStoreProducts(@Query("store_id") Integer store_id);

    @FormUrlEncoded
    @POST("addItemToCart")
    Call<additem>addItemToCart(@Field("token")String token , @Field("product_id") String product_id,
                               @Field("store_id") String store_id,@Field("user_id")String user_id);


    @GET("getStoreDeliveryMethods")
    Call<storeDeliverymodel>GetStoreDeliveryMethods(@Query("token")String token , @Query("store_id")Integer store_id);

    @GET("getUserCart")
    Call<additem>getUserCart(@Query("token")String token , @Query("user_id")String user_id);

    @GET("getStoreUSerAddress")
    Call<addressmodel>GetUserAddress(@Query("token")String token , @Query("user_id")String user_id);

    @FormUrlEncoded
    @POST("addStoreUserAddress")
    Call<ApiStatusModel>addStoreUserAddress(@Field("token") String token , @Field("user_id") String user_id,
                                            @Field("address_line1") String address_line1,@Field("address_line2") String address_line2,
                                            @Field("city") String City,@Field("state")String state);

    @FormUrlEncoded
    @POST("deleteItemFromCart")
    Call<additem>getdeleteItemFromCart(@Field("token") String token , @Field("product_id") String product_id,
                                              @Field("user_id")String user_id);

    @GET("fetchAllStoresByCategory")
   Call<storecategorymodel>getfetchAllStoresByCategory();

    Call<ApiStatusModel>getcheckPromocode();

    
}
