package com.bingalollc.com.forgetpassword

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.model.ApiStatusModel
import com.bingalollc.com.network.RestClient
import com.bingalollc.com.signup.SignupActivity
import com.google.android.material.textfield.TextInputEditText
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgetPassword : BaseActivity(), DialogInterface.OnClickListener {
    private var topLayer: LinearLayout? = null
    private var back_icon: ImageView? = null
    private var et_username: TextInputEditText? = null
    private var tv_signup: TextView? = null
    private var submitBtn: AppCompatButton? = null
    private lateinit var userName: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forget_password)
        initViews()
        initClicks()


    }

    private fun initClicks() {
        back_icon?.setOnClickListener { onBackPressed() }

        tv_signup?.setOnClickListener {
            startActivity(Intent(this, SignupActivity::class.java))
            finish()
        }
        submitBtn?.setOnClickListener {
            if (validateInputs()) {
                showLoading(this)
                RestClient.getApiInterface().forgotPassword(userName)
                    .enqueue(object : Callback<ApiStatusModel> {
                        override fun onResponse(
                            call: Call<ApiStatusModel>,
                            response: Response<ApiStatusModel>
                        ) {
                            hideLoading()
                            if (response.body()?.getStatus() == 200) {
                                showMessageOKCancel(getString(R.string.reset_link_sent_successfully), "",this@ForgetPassword, this@ForgetPassword,
                                    getString(R.string.ok),"", false)
                            }
                        }

                        override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
                            hideLoading()
                        }

                    })
            }
        }
    }

    private fun validateInputs(): Boolean {
        userName = et_username?.text.toString()
        if (TextUtils.isEmpty(userName)) {
            et_username?.error = getString(R.string.email_or_username_required)
            et_username?.requestFocus()
            return false
        } else {
            return true
        }
    }

    private fun initViews() {
        back_icon = findViewById(R.id.back_icon) as ImageView?
        et_username = findViewById(R.id.et_username) as TextInputEditText?
        tv_signup = findViewById(R.id.tv_signup) as TextView?
        submitBtn = findViewById(R.id.submitBtn) as AppCompatButton?
        topLayer = findViewById(R.id.topLayer) as LinearLayout?

        topLayer?.setBackgroundColor(resources.getColor(R.color.basecolor))

    }

    override fun onClick(p0: DialogInterface?, p1: Int) {
        onBackPressed()

    }
}