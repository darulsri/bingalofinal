package com.bingalollc.com.signup

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.databinding.ActivityOtpScreenBinding
import com.bingalollc.com.databinding.ActivityVerifyOtpBinding
import com.bingalollc.com.homeactivity.HomeActivity
import com.bingalollc.com.model.ApiStatusModel
import com.bingalollc.com.network.RestClient
import com.bingalollc.com.utils.ViewBindingDialogFragment
import okhttp3.Callback
import retrofit2.Call
import retrofit2.Response
import kotlin.math.log

class OtpScreen() : ViewBindingDialogFragment<ActivityOtpScreenBinding>() {

    private var phoneNumber = ""
    private var onClicked: OnClicked?= null
    private var baseActivity: BaseActivity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.DialogFullScreenTheme)
        isCancelable = false

    }

    public fun setPhoneNum(num: String) {
        phoneNumber = num
    }

    public fun setListner(onClicked: OnClicked) {
        this.onClicked = onClicked
    }

    override fun provideBinding(inflater: LayoutInflater): ActivityOtpScreenBinding {
        return ActivityOtpScreenBinding.inflate(inflater)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        baseActivity = BaseActivity()


        binding.otpheaderText.text = getString(R.string.enter_the_6_digit_code_we_sent_to, phoneNumber + " please enter that code here",)
        sendOtp()
        binding.submitBtn.setOnClickListener {
            if (binding.otpData.text!!.length == 6) {
                verifyOtp()
            }
        }

        binding.imgBack.setOnClickListener{
            this.dismiss()
        }

        binding.imgClose.setOnClickListener{
            this.dismiss()
        }

        binding.requestNew.setOnClickListener {
            this.dismiss()
            //  activity?.let { it1 -> (activity as HomeActivity?)?.showSignUpDialog(it1) }
            // sendOtp()
        }

    }

    private fun sendOtp() {
        baseActivity?.showLoading(requireContext())
        RestClient.getApiInterface().sendOTP("", phoneNumber).enqueue(object : retrofit2.Callback<ApiStatusModel>{
            override fun onResponse(
                call: Call<ApiStatusModel>,
                response: Response<ApiStatusModel>
            ) {
                baseActivity?.hideLoading()
                if (response.body()?.getStatus() == 200) {
                    //   baseActivity?.showToast(response.body()?.getMessage()?:"", requireContext())
                }else{
                    baseActivity?.showToast(response.body()?.getMessage()?:"", requireContext())
                }
            }

            override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {

            }

        })
    }


    private fun verifyOtp() {
        baseActivity?.showLoading(requireContext())
        RestClient.getApiInterface().verifyOtp("",phoneNumber, "",binding.otpData.text.toString()).enqueue(object : retrofit2.Callback<ApiStatusModel> {
            override fun onResponse(
                call: Call<ApiStatusModel>,
                response: Response<ApiStatusModel>
            ) {
                baseActivity?.hideLoading()
                if (response.body()?.getStatus() == 200) {
                    onClicked?.onOtpVerified(binding.otpData.text.toString())
                    binding.otpData.setText("")
                } else {
                    baseActivity?.showToast(response.body()?.getMessage()?:"", requireContext())
                }


            }

            override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
                baseActivity?.hideLoading()
            }

        })
    }


    public interface OnClicked{
        fun onOtpVerified(otp: String)
    }
}