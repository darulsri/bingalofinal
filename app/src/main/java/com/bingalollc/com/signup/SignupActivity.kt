package com.bingalollc.com.signup

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.text.TextUtils
import android.text.TextWatcher
import android.text.method.PasswordTransformationMethod
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.camerax.CameraConstant.mArrayFileConstant
import com.bingalollc.com.homeactivity.HomeActivity
import com.bingalollc.com.model.SignUpModel
import com.bingalollc.com.model.SocialLoginModel
import com.bingalollc.com.network.MultipartParams
import com.bingalollc.com.network.RestClient
import com.bingalollc.com.preference.Common
import com.bingalollc.com.users.Users
import com.bingalollc.com.utils.AppLocationService
import com.bingalollc.com.utils.PickerUtils
import com.bingalollc.com.utils.showDialogFragment
import com.bumptech.glide.Glide
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.hbb20.CountryCodePicker
import com.kbeanie.multipicker.api.FilePicker
import com.kbeanie.multipicker.api.Picker
import com.kbeanie.multipicker.api.callbacks.FilePickerCallback
import com.kbeanie.multipicker.api.entity.ChosenFile
import com.theartofdev.edmodo.cropper.CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE
import com.theartofdev.edmodo.cropper.CropImageView
import com.twitter.sdk.android.core.*
import com.twitter.sdk.android.core.identity.TwitterAuthClient
import com.twitter.sdk.android.core.models.User
import de.hdodenhof.circleimageview.CircleImageView
//import kotlinx.android.synthetic.main.activity_signup.*
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import pub.devrel.easypermissions.EasyPermissions
import retrofit2.Call
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.net.URL
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*


class SignupActivity : BaseActivity(), FilePickerCallback, OtpScreen.OnClicked {
    private var header: TextView? = null
    private var et_name: TextInputEditText? = null
    private var et_email: TextInputEditText? = null
    private var et_phone: TextInputEditText? = null
    private var et_password: TextInputEditText? = null
    private var et_confirm_password: TextInputEditText? = null
    private var submitBtn: Button? = null
    private var ccp: CountryCodePicker? = null
    private var back_icon: ImageView? = null
    private var profilePic: CircleImageView? = null
    private var checkbox: CheckBox? = null
    private lateinit var name: String
    private lateinit var email: String
    private lateinit var phone: String
    private lateinit var password: String
    private lateinit var confirmPassword: String
    private var alertDialog: AlertDialog.Builder? = null
    private val REQUEST_CAMERA = 101
    private val PICK_IMAGE_REQUEST_GALLERY = 102
    private val MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 2001
    private val PERMISSION_CAMERA_REQUEST_CODE = 2002
    private var fileToUpload: MultipartBody.Part? = null
    private var mArrayFile: java.util.ArrayList<File>? = ArrayList()
    private var googleLogin: ImageView? = null
    private var fbLogin: ImageView? = null
    private var twitterLogin: ImageView? = null
    lateinit var mGoogleSignInClient: GoogleSignInClient
    val Req_Code: Int = 123
    var firebaseAuth = FirebaseAuth.getInstance()
    lateinit var callbackManager: CallbackManager
    private var client: TwitterAuthClient? = null
    private var gpsLocation: Location? = null
    var appLocationService: AppLocationService? = null
    private var fullName = ""
    private var socialId = ""
    private var imageUrl = ""
    private var loginType = ""
    private var deviceType = ""
    private var isSocialLogin = false
    private var filePicker: FilePicker? = null
    private val permissionsGALLERY = arrayOf(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE
    )
    private var isPasswordVisible = false
    private var isConfirmPasswordVisible = false
    var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            // There are no request codes
            val files = File(getCameraFilePathName())
            val file = getCompressed(this, files.path)
            try {
                com.theartofdev.edmodo.cropper.CropImage.activity(Uri.fromFile(file))
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this@SignupActivity);

            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }
    }
    fun getRealPathFromURI(uri: Uri?): String? {
        var path = ""
        if (getContentResolver() != null) {
            val cursor: Cursor? = getContentResolver().query(uri!!, null, null, null, null)
            if (cursor != null) {
                cursor.moveToFirst()
                val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                path = cursor.getString(idx)
                cursor.close()
            }
        }
        return path
    }

    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        header = findViewById(R.id.header) as TextView?
        header?.setText(getString(R.string.signup))
        initSocialLogin()
        printKeyHash()
        initViews()
        initClicks()
        importSwipeBack()

//        fetchLatLng()
        if (EasyPermissions.hasPermissions(this, *permissionsArrayLocation)) {
            //  sendNotification(getResources().getString(R.string.sent_you_his_location));
            getLocation(true)
        } else {
            EasyPermissions.requestPermissions(
                this, resources.getString(R.string.please_allow_permissions),
                101, *permissionsArrayLocation
            )
        }
    }

    private fun getLocation(status: Boolean) {
        checkGpsOn(status)
    }


    private fun checkGpsOn(status: Boolean) {

        // ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        val locationManager =
            this.getSystemService(LOCATION_SERVICE) as LocationManager
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            displayLocationSettingsRequest(this)
        } else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            try {
                gpsLocation = appLocationService?.getLocation(LocationManager.GPS_PROVIDER)
                if (gpsLocation == null) {
                    gpsLocation = appLocationService?.getLocation(LocationManager.NETWORK_PROVIDER)
                }
                if (gpsLocation == null) {
                    gpsLocation = appLocationService?.getLocation(LocationManager.PASSIVE_PROVIDER)
                }
                if (gpsLocation != null) {
                    if (status) {
                        Common.productLocationLat = gpsLocation?.latitude.toString()
                        Common.productLocationLng = gpsLocation?.longitude.toString()
                    }
                }
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun initSocialLogin() {
        callbackManager = CallbackManager.Factory.create()
        val config = TwitterConfig.Builder(this)
            .logger(DefaultLogger(Log.DEBUG)) //enable logging when app is in debug mode
            .twitterAuthConfig(
                TwitterAuthConfig(
                    resources.getString(R.string.twitter_api_key),
                    resources.getString(R.string.twitter_api_secret_key)
                )
            ) //pass the created app Consumer KEY and Secret also called API Key and Secret
            .debug(true) //enable debug mode
            .build()
        Twitter.initialize(config)
        client = TwitterAuthClient()

    }

    private fun printKeyHash() {
        try {
            val info =
                packageManager.getPackageInfo("com.bingalollc.com", PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.d("KeyHash:>>>>", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        } catch (e: PackageManager.NameNotFoundException) {
            Log.e("KeyHash:", e.toString())
        } catch (e: NoSuchAlgorithmException) {
            Log.e("KeyHash:", e.toString())
        }
    }

    private fun initViews() {
        et_name = findViewById(R.id.et_name) as TextInputEditText?
        et_email = findViewById(R.id.et_email) as TextInputEditText?
        ccp = findViewById(R.id.ccp) as CountryCodePicker?
        et_phone = findViewById(R.id.et_phone) as TextInputEditText?
        et_password = findViewById(R.id.et_password) as TextInputEditText?
        et_confirm_password = findViewById(R.id.et_confirm_password) as TextInputEditText?
        submitBtn = findViewById(R.id.submitBtn) as Button?
        back_icon = findViewById(R.id.back_icon) as ImageView?
        checkbox = findViewById(R.id.checkbox) as CheckBox?
        profilePic = findViewById(R.id.profilePic) as CircleImageView?
        googleLogin = findViewById(R.id.googleLogin) as ImageView?
        twitterLogin = findViewById(R.id.twitterLogin) as ImageView?
        fbLogin = findViewById(R.id.fbLogin) as ImageView?

        appLocationService = AppLocationService(this@SignupActivity)
        ccp?.setCountryForNameCode("IL")
        ccp?.setCustomMasterCountries("il,gb,us,in")
        ccp?.setOnCountryChangeListener { checkValidNumber() }

        if (preferenceManager.isEnglish == 2) {
            profilePic?.setImageDrawable(getDrawable(R.drawable.btn_upload_photo_he))
        }
    }

    private fun initClicks() {
        back_icon?.setOnClickListener { onBackPressed() }

        submitBtn?.setOnClickListener {
            if (validateInputs()) {
                phone = ccp?.selectedCountryCode.toString() + et_phone?.text.toString()
                if (phone.contains("+").not()) {
                    phone = "+$phone"
                }
                optScreen()
            }
        }

        profilePic?.setOnClickListener({
            showImagePickerDialog()

        })

        googleLogin?.setOnClickListener({
            googleLogin()
        })

        fbLogin?.setOnClickListener({
            LoginManager.getInstance()
                .logInWithReadPermissions(this, Arrays.asList("public_profile"))
            facebookLogin()

        })

        twitterLogin?.setOnClickListener({
            twitterLogin()
        })

        et_phone?.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                checkValidNumber()
            }

        })


//        showHideSignupPassword.setOnClickListener {
//            if (isPasswordVisible) {
//                showHideSignupPassword.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.show_password))
//                isPasswordVisible = false
//                et_password?.transformationMethod = PasswordTransformationMethod()
//            } else {
//                showHideSignupPassword.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.hide_password))
//                isPasswordVisible = true
//                et_password?.transformationMethod = null
//            }
//            et_password?.setSelection(et_password?.text?.length?:0)
//
//        }

//        showHideConfirmPassword.setOnClickListener {
//            if (isConfirmPasswordVisible) {
//                showHideConfirmPassword.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.show_password))
//                isConfirmPasswordVisible = false
//                et_confirm_password?.transformationMethod = PasswordTransformationMethod()
//            } else {
//                showHideConfirmPassword.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.hide_password))
//                isConfirmPasswordVisible = true
//                et_confirm_password?.transformationMethod = null
//            }
//            et_confirm_password?.setSelection(et_confirm_password?.text?.length?:0)
//
//        }

    }

    private fun optScreen() {
        val frag = showDialogFragment<OtpScreen>()
        frag?.setPhoneNum(phone)
        frag?.setListner(this)
    }

    private fun checkValidNumber() {
        val countryCode = ccp?.selectedCountryCode.toString()
        var MAX = 10

        et_phone?.setFilters(arrayOf<InputFilter>(LengthFilter(MAX)))
        if (et_phone?.length()?:0 > MAX) {
            et_phone?.setText(et_phone?.text?.toString()?.substring(0,MAX))
            et_phone?.setSelection(et_phone?.text?.length?:0)
        }
    }

    private fun twitterLogin() {
//        if (getTwitterSession() == null) {
        //if user is not authenticated start authenticating
        client!!.authorize(this, object : Callback<TwitterSession?>() {
            override fun success(result: Result<TwitterSession?>) {

                // Do something with result, which provides a TwitterSession for making API calls
                val twitterSession: TwitterSession? = result.data
                val twitterApiClient = TwitterCore.getInstance().apiClient
                val call: Call<User> =
                    twitterApiClient.accountService.verifyCredentials(true, false, true)
                call.enqueue(object : Callback<User>() {
                    override fun success(result: Result<User>?) {
                        val user = result!!.data
                        setDataToUI(
                            user.email,
                            user.profileImageUrl,
                            user.name,
                            user.id.toString(),
                            "twitter",
                            "android"
                        )
                        email = user.email
                        imageUrl = user.profileImageUrl
                        fullName = user.name
                        socialId = user.id.toString()
                        loginType = "twitter"
                        deviceType = "android"
                        isSocialLogin = true
                        preferenceManager?.setUserLoggedin(true)

                    }

                    override fun failure(exception: TwitterException?) {
                    }
                })
            }

            override fun failure(e: TwitterException?) {
                showToast("Failed to authenticate. Please try again.", this@SignupActivity)
            }
        })
        /* } else {
             //if user is already authenticated direct call fetch twitter email api
             Toast.makeText(this, "User already authenticated", Toast.LENGTH_SHORT).show()
         }*/
    }

    private fun facebookLogin() {
        var lastName = ""
        LoginManager.getInstance().registerCallback(callbackManager, object :
            FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                val request = GraphRequest.newMeRequest(
                    loginResult!!.accessToken
                ) { `object`, response ->
                    try {
                        Log.i("Response", response.toString())
                        val socail_id = response!!.jsonObject!!.getString("id")
                        val profilePicture =
                            URL("https://graph.facebook.com/$socail_id/picture?width=500&height=500")
                        if (response!!.jsonObject!!.has("email")) {
                            email = response!!.jsonObject!!.getString("email")
                        }
                        val firstName = response!!.jsonObject!!.getString("first_name")
                        if (response!!.jsonObject!!.getString("last_name") != null) {
                            lastName = response.jsonObject!!.getString("last_name")
                        } else {
                            lastName = ""
                        }
                        setDataToUI(
                            email,
                            profilePicture.toString(),
                            firstName + " " + lastName,
                            socail_id,
                            "facebook",
                            "android"
                        )
                        email = email
                        imageUrl = profilePicture.toString()
                        fullName = firstName + " " + lastName
                        socialId = socail_id
                        loginType = "facebook"
                        deviceType = "android"
                        isSocialLogin = true
                        preferenceManager?.setUserLoggedin(true)
                    } catch (e: java.lang.Exception) {
                        showToast(e.message, this@SignupActivity)
                    }
                }
                val parameters = Bundle()
                parameters.putString("fields", "id,email,first_name,last_name,gender")
                request.parameters = parameters
                request.executeAsync()
            }

            override fun onCancel() {
            }

            override fun onError(exception: FacebookException) {
            }
        })
    }

    private fun googleLogin() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        firebaseAuth = FirebaseAuth.getInstance()


        val signInIntent: Intent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, Req_Code)


    }

    private fun setDataToUI(
        email: String?,
        imageUrl: String,
        fullName: String?,
        socialId: String?,
        loginType: String,
        deviceType: String
    ) {
        et_email?.setText(email)
        profilePic?.let { Glide.with(this).load(imageUrl).into(it) }
        et_name?.setText(fullName)

    }


    private fun socialLogin(
        email: String?,
        profilePic: String,
        fullName: String?,
        socialId: String?,
        loginType: String,
        deviceType: String
    ) {
        var userData = Users().Data()
        showLoading(this)
        RestClient.getApiInterface().socialLogin(
            fullName,
            email,
            Common.productLocationLat,
            Common.productLocationLng,
            socialId,
            loginType,
            profilePic,
            "Android"
        )
            .enqueue(object : retrofit2.Callback<SocialLoginModel> {
                override fun onResponse(
                    call: Call<SocialLoginModel>,
                    response: Response<SocialLoginModel>
                ) {
                    hideLoading()
                    if (response.isSuccessful) {
                        if (response.body()?.getStatus() == 200) {
                            response?.body()?.data?.also {
                                userData.email = it.email
                                userData.id = it.id
                                userData.fullName = it.fullName
                                userData.image = it.image
                                userData.googleId = it.googleId
                                userData.language = it.language
                                userData.lat = it.lat
                                userData.lng = it.lng
                                userData.phone = it.phone
                                userData.token = it.token
                                userData.twitterId = it.pushToken
                                userData.facebookId = it.facebookId
                                userData.googleId = it.googleId
                                preferenceManager?.userDetails = userData
                                startActivity(
                                    Intent(this@SignupActivity, HomeActivity::class.java)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                )
                            }


                        } else {
                            showToast(response.body()?.message, this@SignupActivity)
                        }
                    }
                }

                override fun onFailure(call: Call<SocialLoginModel>, t: Throwable) {
                    hideLoading()
                }

            })

    }

    private fun signUp() {
        var builder = MultipartParams.Builder();
        builder.add("full_name", name)
        builder.add("email", email)
        builder.add("phone", phone)
        builder.add("lat", Common.productLocationLat)
        builder.add("lng", Common.productLocationLng)
        builder.add("password", password)
        builder.add("singup_from", "Android")

        if (mArrayFile != null && mArrayFile?.size?:0 > 0) {
            builder.addFile("image", mArrayFile?.get(0))
        }else if (imageUrl.isNullOrEmpty().not()){
            builder.add("profile_pic_url", imageUrl)
        }
        val userData = Users().Data()

        showLoading(this)
        RestClient.getApiInterface().signUp(builder.build().map)
            .enqueue(object : retrofit2.Callback<SignUpModel> {
                override fun onResponse(call: Call<SignUpModel>, response: Response<SignUpModel>) {
                    hideLoading()
                    if (response.isSuccessful) {
                        mArrayFileConstant = ArrayList()
                        if (response.body()?.getStatus() != null && response.body()
                                ?.getStatus() == 200
                        ) {
                            showToast(response?.body()?.getMessage().toString(), applicationContext)
                            val signupData: SignUpModel.Data = response?.body()?.getData()!!
                            userData.email = signupData.email
                            userData.id = signupData.id
                            userData.fullName = signupData.fullName
                            userData.image = signupData.image
                            userData.googleId = signupData.googleId
                            userData.language = signupData.language
                            userData.lat = signupData.lat
                            userData.lng = signupData.lng
                            userData.phone = signupData.phone
                            userData.token = signupData.token
                            userData.twitterId = signupData.pushToken
                            userData.facebookId = signupData.facebookId
                            userData.googleId = signupData.googleId
                            preferenceManager?.userDetails = userData
                            startActivity(
                                Intent(this@SignupActivity, HomeActivity::class.java)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            )
                        }else{
                            showToast(response?.body()?.getMessage().toString(), applicationContext)
                        }
                    } else {
                        showToast(response?.body()?.getMessage().toString(), applicationContext)
                    }
                }

                override fun onFailure(call: Call<SignUpModel>, t: Throwable) {
                    mArrayFileConstant = ArrayList()
                    hideLoading()
                    showToast(getString(R.string.something_went_wring), applicationContext)

                }

            })
    }

    private fun showImagePickerDialog() {
        alertDialog = AlertDialog.Builder(this)
        val inflater: LayoutInflater = getLayoutInflater()
        val add_menu_layout: View = inflater.inflate(R.layout.image_mode_upload, null)
        val submitMode = add_menu_layout.findViewById<Button>(R.id.submitMode)
        alertDialog!!.setView(add_menu_layout)
        alertDialog!!.setCancelable(true)
        val testDialog: AlertDialog = alertDialog!!.create()
        val tv_camera = add_menu_layout.findViewById<TextView>(R.id.tv_camera)
        val tv_gallery = add_menu_layout.findViewById<TextView>(R.id.tv_gallery)
        tv_camera.setOnClickListener {
            if (checkPermissionForCamera(this)) {
                openCamera()
            } else {
                requestPermissionForCamera(this)
            }
            testDialog.dismiss()
        }
        tv_gallery.setOnClickListener {
            if (checkPermissionREAD_EXTERNAL_STORAGE(this)) {
                choosephoto()
            }
            testDialog.dismiss()
        }
        testDialog.show()

    }

    private fun choosephoto() {
        /* val intent = Intent()
         intent.type = "image/*"
         intent.action = Intent.ACTION_GET_CONTENT
         startActivityForResult(
             Intent.createChooser(intent, "Select Picture"),
             PICK_IMAGE_REQUEST_GALLERY
         )*/

         */
        if (EasyPermissions.hasPermissions(this, *permissionsGALLERY)) {
            pickImageMultiple("image")
        } else {
            EasyPermissions.requestPermissions(
                this, resources.getString(R.string.please_allow_permissions),
                1003, *permissionsGALLERY
            )
        }
    }

    private fun pickImageMultiple(type: String) {
        filePicker = getFilePicker()
        filePicker?.allowMultiple()
        filePicker?.setMimeType("image/*")
        filePicker?.pickFile()
    }
    private fun getFilePicker(): FilePicker? {
        filePicker = FilePicker(this)
        filePicker!!.setFilePickerCallback(this)
        filePicker!!.setCacheLocation(PickerUtils.getSavedCacheLocation(this))
        return filePicker
    }


    private fun openCamera() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val outputFileUri = Uri.fromFile(setCamerFilePath())
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri)
        cameraIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        cameraIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        resultLauncher.launch(cameraIntent)
    }

    fun checkPermissionREAD_EXTERNAL_STORAGE(
        context: Context?
    ): Boolean {
        val currentAPIVersion = Build.VERSION.SDK_INT
        return if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this!!,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        (context as Activity?)!!,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )
                ) {
                    showDialog(
                        "External storage",
                        context,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE
                    )
                } else {
                    requestPermissions(
                        arrayOf(
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        ),
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE
                    )
                }
                false
            } else {
                true
            }
        } else {
            true
        }
    }

    fun showDialog(
        msg: String, context: Context?,
        permission: String, statusCode: Int
    ) {
        val alertBuilder = android.app.AlertDialog.Builder(context)
        alertBuilder.setCancelable(false)
        alertBuilder.setTitle("Permission necessary")
        alertBuilder.setMessage("$msg permission is necessary")
        alertBuilder.setPositiveButton(
            android.R.string.yes
        ) { dialog, which ->
            requestPermissions(
                arrayOf(permission),
                statusCode
            )
        }
        val alert = alertBuilder.create()
        alert.show()
    }


    private fun validateInputs(): Boolean {
        name = et_name?.text.toString()
        email = et_email?.text.toString()
        phone = et_phone?.text.toString()
        password = et_password?.text.toString()
        confirmPassword = et_confirm_password?.text.toString()

        if (TextUtils.isEmpty(name)) {
            et_name?.error = getString(R.string.name_required)
            et_name?.requestFocus()
            return false
        } else if (TextUtils.isEmpty(email)) {
            et_email?.error = getString(R.string.email_required)
            et_email?.requestFocus()
            return false
        } else if (!email.contains("@")) {
            et_email?.error = getString(R.string.invalid_email)
            et_email?.requestFocus()
            return false
        } else if (TextUtils.isEmpty(phone)) {
            et_phone?.error = getString(R.string.phone_required)
            et_phone?.requestFocus()
            return false
        } else if (TextUtils.isEmpty(password)) {
            et_password?.error = getString(R.string.password_required)
            et_password?.requestFocus()
            return false
        } else if (TextUtils.isEmpty(confirmPassword)) {
            et_confirm_password?.error = getString(R.string.confirm_password_required)
            et_confirm_password?.requestFocus()
            return false
        } else if (!password.equals(confirmPassword)) {
            et_confirm_password?.error = getString(R.string.password_not_matched)
            et_confirm_password?.requestFocus()
            return false
        } else if (!checkbox?.isChecked!!) {
            checkbox?.error = getString(R.string.terms_of_conditions_required)
            checkbox?.requestFocus()
            return false
        } else {
            return true
        }
    }

    fun getImageUri(inContext: Context, inImage: Bitmap): Uri? {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path =
            MediaStore.Images.Media.insertImage(inContext.contentResolver, inImage, "Title", null)
        return Uri.parse(path)
    }

    private fun getImagePathFromURI(uri: Uri): String {
        var path = ""
        if (getContentResolver() != null) {
            val cursor: Cursor? = getContentResolver().query(uri, null, null, null, null)
            if (cursor != null) {
                cursor.moveToFirst()
                val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                path = cursor.getString(idx)
                cursor.close()
            }
        }
        return path

    }

    @SuppressLint("WrongConstant")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager.onActivityResult(requestCode, resultCode, data)


        if (requestCode == Picker.PICK_FILE && resultCode == BaseActivity.RESULT_OK) {
            filePicker!!.submit(data)
        }else if (requestCode == REQUEST_CAMERA && data != null && data.extras != null) {
            try {
                val bundle = data.extras
                val bitmap = bundle!!["data"] as Bitmap?
                val tempUri: Uri = getImageUri(this!!, bitmap!!)!!
                com.theartofdev.edmodo.cropper.CropImage.activity(tempUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this);

            } catch (e: Exception) {
                e.message
            }
        } else if (requestCode == PICK_IMAGE_REQUEST_GALLERY && data != null) {
            val tempUri = data.data
            com.theartofdev.edmodo.cropper.CropImage.activity(tempUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);

            //  val tempUri = data.data
        }else if (requestCode ==  CROP_IMAGE_ACTIVITY_REQUEST_CODE && data != null) {
            val result: com.theartofdev.edmodo.cropper.CropImage.ActivityResult = com.theartofdev.edmodo.cropper.CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK) {
                val resultUri: Uri = result.getUri()
                val bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri)
                val file =File( resultUri.path)


                //    val file = File(getPath(resultUri))
                if (file!=null)
                    mArrayFile?.add(file!!)

                val mFile = RequestBody.create("image/*".toMediaTypeOrNull(), file)
                fileToUpload = MultipartBody.Part.createFormData("image", file?.name, mFile)
                try {
                    profilePic?.setImageBitmap(bitmap)
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            } else if (resultCode === com.theartofdev.edmodo.cropper.CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error: java.lang.Exception = result.getError()
            }
        }

        if (requestCode == Req_Code) { //Google Signin
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            if (task.isSuccessful) {
                handleGoogleLoginResponse(task)
            }
        }
    }

    private fun handleGoogleLoginResponse(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account: GoogleSignInAccount? = completedTask.getResult(ApiException::class.java)
            if (account != null) {
                val credential = GoogleAuthProvider.getCredential(account.idToken, null)
                setDataToUI(
                    account.email,
                    account.photoUrl.toString(),
                    account.givenName,
                    account.id,
                    "google",
                    "android"
                )
                email = account.email!!
                imageUrl = account.photoUrl.toString()
                fullName = account.givenName!!
                socialId = account.id!!
                loginType = "google"
                deviceType = "android"
                isSocialLogin = true
                preferenceManager?.setUserLoggedin(true)
            }
        } catch (e: ApiException) {
        }
    }


    @SuppressLint("Range")
    fun getPath(uri: Uri?): String? {
        var cursor: Cursor = getContentResolver().query(uri!!, null, null, null, null)!!
        cursor.moveToFirst()
        var document_id = cursor.getString(0)
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1)
        cursor.close()
        cursor = getContentResolver().query(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            null, MediaStore.Images.Media._ID + " = ? ", arrayOf(document_id), null
        )!!
        cursor.moveToFirst()
        val path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA))
        cursor.close()
        return path
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(
            requestCode,
            permissions,
            grantResults,
            this
        )

        if (requestCode == PERMISSION_CAMERA_REQUEST_CODE) {
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openCamera()
            }
        } else if (requestCode == MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE) {
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                choosephoto()

            }
        }else if (requestCode == REQUEST_LOCATION) {
            if (grantResults.size > 0) {
                if (EasyPermissions.hasPermissions(this@SignupActivity, *permissionsArrayLocation)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        //getLatitudeLongitude();
                        //  sendNotification(getResources().getString(R.string.sent_you_his_location));
                        getLocation(true)
                    }
                } else {
                    showPermissionRequestDialog(
                        getString(R.string.network_alert_lcoation),
                        getString(R.string.do_u_wish_to_enable)
                    )
                }
            }
        }

    }

    override fun onStart() {
        super.onStart()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onError(p0: String?) {

    }

    override fun onFilesChosen(list: MutableList<ChosenFile>?) {
        if (list?.size ?: 0 > 0) {
            for (i in 0 until list?.size!!) {
                val file1 = File(list?.get(i)?.getOriginalPath())
                val file = getCompressed(this, file1.path)
                val tempUri = Uri.fromFile(file)
                com.theartofdev.edmodo.cropper.CropImage.activity(tempUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this);
                break
            }
        }
    }

    override fun onOtpVerified(otp: String) {
        signUp()
    }

}