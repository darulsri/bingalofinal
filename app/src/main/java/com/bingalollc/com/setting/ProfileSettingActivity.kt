package com.bingalollc.com.setting

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.*
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.homeactivity.HomeActivity
import com.bingalollc.com.model.ApiStatusModel
import com.bingalollc.com.model.SignUpModel
import com.bingalollc.com.network.RestClient
import com.bingalollc.com.preference.Common
import com.bingalollc.com.setting.adapter.AdapterBlockedUser
import com.bingalollc.com.splash.viewmodel.UpdateUserViewModel
import com.bingalollc.com.users.Users
import com.bingalollc.com.users.UsersArrayModel
import com.bingalollc.com.utils.showDialogFragment
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
//import kotlinx.android.synthetic.main.activity_profile_setting.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProfileSettingActivity : BaseActivity(), AdapterBlockedUser.OnButtonClicked,
    DialogInterface.OnClickListener {
    private var otpSend = false
    private var clickedPos = -1
    private var mDb: FirebaseFirestore? = null
    private var emailEntered = ""
    private var topLayer: LinearLayout? = null
    private var header: TextView? = null
    private var changeEmailDesc: TextView? = null
    private var noBlockeduser: TextView? = null
    private var back_icon: ImageView? = null
    private var englishTick: ImageView? = null
    private var hebrewTick: ImageView? = null
    private var layoutName: LinearLayout? = null
    private var layoutEmail: LinearLayout? = null
    private var layoutLanguage: LinearLayout? = null
    private var verifyOtpFragment: VerifyOtpFragment? = null
    private var layoutChangePassword: LinearLayout? = null
    private var layoutBlockedUsers: LinearLayout? = null
    private var hebrewLanguage: RelativeLayout? = null
    private var englishLanguage: RelativeLayout? = null
    private var etOldPassword: EditText? = null
    private var etNewPassword: EditText? = null
    private var etConfirmPassword: EditText? = null
    private var etFirstName: EditText? = null
    private var etLastName: EditText? = null
    private var save: TextView? = null
    private var rvBlockedUsers: RecyclerView? = null
    private var headerName = ""
    private lateinit var adapterBlockedUser: AdapterBlockedUser
    private lateinit var password: String
    private lateinit var confirmPassword: String
    private var blockedUserList: ArrayList<String>? = null
    var updateUserViewModel: UpdateUserViewModel?=null
//    private var onSettingChange: OnSettingChange? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_setting)
        updateUserViewModel = ViewModelProvider(this).get(UpdateUserViewModel::class.java)
        initViews()
        setUIVisibility()
        initClicks()
        importSwipeBack()

    }

    private fun initClicks() {
        hebrewLanguage?.setOnClickListener({
            //hebrewLanguage?.setBackgroundColor(getColor(R.color.lightgrey))
          //  englishLanguage?.setBackgroundColor(getColor(R.color.white))
            selecteLangUi(false)
        })
        englishLanguage?.setOnClickListener({
           // hebrewLanguage?.setBackgroundColor(getColor(R.color.white))
           // englishLanguage?.setBackgroundColor(getColor(R.color.lightgrey))
            selecteLangUi(true)
        })

        save?.setOnClickListener {
//            if (otpSend){
//                if (enterEmail.text.isNullOrEmpty().not() && enterEmail.text.length == 6){
//                   // verifyOtp()
//                } else {
//                    showToast(getString(R.string.enter_valid_otp), this)
//                }
//            } else if (headerName.equals(getString(R.string.enter_name))) {
//                updateUserName()
//
//            } else if (headerName.equals(getString(R.string.change_email))) {
//                if (enterEmail.text.toString().isEmpty().not() && enterEmail.text.contains("@")) {
//                    if (!enterEmail.text.toString().trim().equals(preferenceManager?.userDetails?.email?:""))
//                        sendOtpToVerify()
//                    else{
//                        showToast(getString(R.string.same_email_alert), this)
//                    }
//                } else {
//                    showToast(getString(R.string.enter_valid_email), this)
//                }
//
//            } else if (headerName.equals(getString(R.string.select_language))) {
//                selectedLanguage()
//
//            } else if (headerName.equals(getString(R.string.change_password))) {
//                if (validatePassword()) {
//                    changePassword()
//                }
//            }

        }

    }

    private fun verifyOtp(otpString: String) {
        showLoading(this)
        RestClient.getApiInterface().verifyOtp("","",emailEntered, otpString).enqueue(
            object : Callback<ApiStatusModel> {
                override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
                    hideLoading()
                }

                override fun onResponse(
                    call: Call<ApiStatusModel>,
                    response: Response<ApiStatusModel>
                ) {
                    hideLoading()
                    if (response.body()?.getStatus() == 200) {
                        update_verification()
                    } else {
                        showToast(response.body()?.getMessage() ?: "", this@ProfileSettingActivity)
                    }

                }
            })
    }

    private fun update_verification() {
        showLoading(this)
        RestClient.getApiInterface().changeEmail(preferenceManager?.userDetails?.id, emailEntered).enqueue(
            object : Callback<Users> {
                override fun onResponse(
                    call: Call<Users>,
                    response: Response<Users>
                ) {
                    hideLoading()
                    if (response.body()?.getStatus() == 200) {
                        preferenceManager?.userDetails?.email = emailEntered
                        showNormalDialogVerticalButton(
                            this@ProfileSettingActivity,
                            getString(R.string.email_changed),
                            getString(
                                R.string.email_changed_desc, emailEntered
                            ),
                            "",
                            getString(R.string.ok),
                            {
                                if (it) {
                                    preferenceManager!!.userDetails = response.body()?.getData()
                                    if (verifyOtpFragment != null && verifyOtpFragment?.isVisible?:false){
                                        verifyOtpFragment?.dismiss()
                                    }
                                    finish();
                                }
                            }, false
                        )
                    } else {
                        showToast(response.body()?.getMessage() ?: "", this@ProfileSettingActivity)
                    }
                }

                override fun onFailure(call: Call<Users>, t: Throwable) {
                    hideLoading()
                }

            })
    }

//    private fun sendOtpToVerify() {
//        showLoading(this)
//        RestClient.getApiInterface().sendOTP(enterEmail.text.toString().trim(), null).enqueue(object :
//            Callback<ApiStatusModel> {
//            override fun onResponse(
//                call: Call<ApiStatusModel>,
//                response: Response<ApiStatusModel>
//            ) {
//                hideLoading()
//                if (response.body()?.getStatus() == 200) {
//                    if (verifyOtpFragment == null || verifyOtpFragment?.isVisible?.not()?:false) {
////                        emailEntered = enterEmail.text.toString().trim()
//                        verifyOtpFragment = showDialogFragment<VerifyOtpFragment>()
////                        verifyOtpFragment?.setEmail(
////                            enterEmail.text.toString().trim(),
////                            preferenceManager?.userDetails?.email ?: ""
////                        )
//                        verifyOtpFragment?.listenerCatcher(object :
//                            VerifyOtpFragment.OnButtonClick {
//                            override fun onResendClick() {
//                                sendOtpToVerify()
//                            }
//
//                            override fun onVerifyClick(otpString: String) {
//                                verifyOtp(otpString)
//                            }
//                        })
//                    } else {
//                        showToast(getString(R.string.otp_resent), this@ProfileSettingActivity)
//                    }
//                }
//            }
//
//            override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
//                hideLoading()
//            }
//
//        })
//    }

    private fun validatePassword(): Boolean {
        password = etNewPassword?.text.toString()
        confirmPassword = etConfirmPassword?.text.toString()
        if (TextUtils.isEmpty(password)) {
            etNewPassword?.error = getString(R.string.password_required)
            etNewPassword?.requestFocus()
            return false
        } else if (TextUtils.isEmpty(confirmPassword)) {
            etConfirmPassword?.error = getString(R.string.confirm_password_required)
            etConfirmPassword?.requestFocus()
            return false
        } else if (!password.equals(confirmPassword)) {
            etConfirmPassword?.error = getString(R.string.password_not_matched)
            etConfirmPassword?.requestFocus()
            return false
        } else {
            return true
        }
    }

    private fun changePassword() {
        showLoading(this)
        RestClient.getApiInterface().changePassword(
            preferenceManager?.userDetails?.id,
            preferenceManager?.userDetails?.fullName,
            preferenceManager?.userDetails?.token,
            confirmPassword
        )
            .enqueue(object : Callback<ApiStatusModel> {
                override fun onResponse(
                    call: Call<ApiStatusModel>,
                    response: Response<ApiStatusModel>
                ) {
                    hideLoading()
                    if (response.isSuccessful) {
                        if (response.body()?.getStatus() == 200) {
                            showToast(response.body()?.getMessage(), this@ProfileSettingActivity)
                            finish()
                        }
                    }
                }

                override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
                    hideLoading()
                }

            })

    }

    private fun updateUserName() {
        var name = etFirstName?.text.toString().trim()
        if (TextUtils.isEmpty(name)) {
            etFirstName?.error = getString(R.string.name_required)
            etFirstName?.requestFocus()
            return
        }
        name = name + " " + etLastName?.text.toString().trim()
        showLoading(this)
        RestClient.getApiInterface().changeUsername(
            preferenceManager?.userDetails?.id,
            name
        )
            .enqueue(object : Callback<SignUpModel> {
                override fun onResponse(call: Call<SignUpModel>, response: Response<SignUpModel>) {
                    hideLoading()
                    if (response.isSuccessful) {
                        if (response.body() != null && response.body()?.getStatus() == 200) {
//                            onSettingChange?.newName(name, getString(R.string.name))
                            showToast(
                                response.body()?.getMessage().toString(),
                                this@ProfileSettingActivity
                            )
                            updateUserViewModel?.updateUser(preferenceManager?.userDetails?.id,preferenceManager)
                        }
                    }
                }

                override fun onFailure(call: Call<SignUpModel>, t: Throwable) {
                    hideLoading()
                }

            })

    }

    private fun setUIVisibility() {
        if (intent != null)
            headerName = intent.getStringExtra("header")!!
            header?.text = headerName
        if (headerName.equals(getString(R.string.enter_name))) {
            etFirstName?.setText(preferenceManager?.userDetails?.fullName?.trim())
            layoutName?.visibility = View.VISIBLE
            layoutEmail?.visibility = View.GONE
            layoutLanguage?.visibility = View.GONE
            layoutChangePassword?.visibility = View.GONE
            layoutBlockedUsers?.visibility = View.GONE
        } else if (headerName.equals(getString(R.string.change_email))) {
            layoutEmail?.visibility = View.VISIBLE
            layoutName?.visibility = View.GONE
            layoutLanguage?.visibility = View.GONE
            layoutChangePassword?.visibility = View.GONE
            layoutBlockedUsers?.visibility = View.GONE
            val changeEmailStringDesc = getString(
                R.string.change_email_desc,
                preferenceManager?.userDetails?.email
            )
            val startIndex = changeEmailStringDesc.indexOf(
                preferenceManager?.userDetails?.email ?: ""
            )
            val endIndex = changeEmailStringDesc.indexOf(" ", startIndex = startIndex)
            setSpannableString(
                this, changeEmailStringDesc, changeEmailDesc, startIndex+1,
                endIndex,
                getResources().getColor(R.color.basecolor)
            )
        } else if (headerName.equals(getString(R.string.select_language))) {
            layoutLanguage?.visibility = View.VISIBLE
            layoutEmail?.visibility = View.GONE
            layoutName?.visibility = View.GONE
            layoutChangePassword?.visibility = View.GONE
            layoutBlockedUsers?.visibility = View.GONE
            updateLanguageUI()
        } else if (headerName.equals(getString(R.string.change_password))) {
            layoutChangePassword?.visibility = View.VISIBLE
            layoutLanguage?.visibility = View.GONE
            layoutEmail?.visibility = View.GONE
            layoutName?.visibility = View.GONE
            layoutBlockedUsers?.visibility = View.GONE
        } else if (headerName.equals(getString(R.string.blocked_users))) {
            layoutBlockedUsers?.visibility = View.VISIBLE
            layoutChangePassword?.visibility = View.GONE
            layoutLanguage?.visibility = View.GONE
            layoutEmail?.visibility = View.GONE
            layoutName?.visibility = View.GONE
            save?.visibility = View.GONE
            getBlockedList(preferenceManager.userDetails.id)
        }
    }

    private fun updateLanguageUI() {
        if (preferenceManager?.isEnglish == 1) {
            englishTick?.visibility  = View.VISIBLE
            hebrewTick?.visibility  = View.GONE
        } else {
            hebrewTick?.visibility  = View.VISIBLE
            englishTick?.visibility  = View.GONE
        }
    }

    private fun selecteLangUi(isEnglish: Boolean) {
        if (isEnglish) {
            englishTick?.visibility  = View.VISIBLE
            hebrewTick?.visibility  = View.GONE
        } else {
            hebrewTick?.visibility  = View.VISIBLE
            englishTick?.visibility  = View.GONE
        }
    }

    private fun selectedLanguage() {
        if (englishTick?.visibility == View.VISIBLE) {
            chooseLanguage(true)
            preferenceManager?.isEnglish = 1
        } else {
            chooseLanguage(false)
            preferenceManager?.isEnglish = 2
        }
        val intent = Intent(this, HomeActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    fun getBlockedList(userId: String?) {
        val chatroomsCollection: CollectionReference = mDb?.collection("BlockedUsers")!!
        val query = chatroomsCollection.whereEqualTo("blockedBy", userId)
        query.addSnapshotListener { queryDocumentSnapshots, error ->
            Common.blockedUserId = ArrayList()
            blockedUserList = ArrayList()
            for (documentSnapshot in queryDocumentSnapshots!!) {
                val arrayList = documentSnapshot.data["userIDs"] as ArrayList<String?>?
                val otherUserId = arrayList!!.remove(userId)
                if (otherUserId) {
                    blockedUserList?.add(documentSnapshot.id)
                    Common.blockedUserId.add(arrayList[0])
                }
            }
            if ((blockedUserList?.size ?: 0) > 0) {
                getBlockUserData()
            } else {
                noBlockeduser?.visibility = View.VISIBLE
                rvBlockedUsers?.visibility = View.GONE

            }
        }
    }

    private fun getBlockUserData() {
        showLoading(this)
        val userList: String = Common.blockedUserId.toString().replace("[", "").replace("]", "")
        RestClient.getApiInterface().getUsersList(userList, preferenceManager.userDetails.token)
            .enqueue(object : Callback<UsersArrayModel?> {
                override fun onResponse(
                    call: Call<UsersArrayModel?>,
                    response: Response<UsersArrayModel?>
                ) {
                    if (response.body() != null && response.body()!!.data != null) {
                        if (response.isSuccessful && response.body()?.getStatus() == 200) {
                            setAdapter(response.body()?.getData())
                        }
                    }
                    hideLoading()

                }

                override fun onFailure(call: Call<UsersArrayModel?>, t: Throwable) {
                    hideLoading()
                }
            })
    }


    private fun setAdapter(data: java.util.ArrayList<UsersArrayModel.Data>?) {
        adapterBlockedUser = AdapterBlockedUser(data, this)
        rvBlockedUsers?.layoutManager = LinearLayoutManager(this)
        rvBlockedUsers?.adapter = adapterBlockedUser

    }

    private fun initViews() {
        topLayer = findViewById(R.id.topLayer) as LinearLayout?
        back_icon = findViewById(R.id.back_icon) as ImageView?
        header = findViewById(R.id.header) as TextView?
        changeEmailDesc = findViewById(R.id.changeEmailDesc) as TextView?
        noBlockeduser = findViewById(R.id.noBlockeduser) as TextView?
        layoutName = findViewById(R.id.layoutName) as LinearLayout?
        layoutEmail = findViewById(R.id.layoutEmail) as LinearLayout?
        layoutLanguage = findViewById(R.id.layoutLanguage) as LinearLayout?
        hebrewLanguage = findViewById(R.id.hebrewLanguage) as RelativeLayout?
        englishLanguage = findViewById(R.id.englishLanguage) as RelativeLayout?
        etOldPassword = findViewById(R.id.etOldPassword) as EditText?
        etNewPassword = findViewById(R.id.etNewPassword) as EditText?
        etConfirmPassword = findViewById(R.id.etConfirmPassword) as EditText?
        layoutChangePassword = findViewById(R.id.layoutChangePassword) as LinearLayout?
        layoutBlockedUsers = findViewById(R.id.layoutBlockedUsers) as LinearLayout?
        rvBlockedUsers = findViewById(R.id.rvBlockedUsers) as RecyclerView?
        etFirstName = findViewById(R.id.etFirstName) as EditText?
        etLastName = findViewById(R.id.etLastName) as EditText?
        save = findViewById(R.id.save) as TextView?
        hebrewTick = findViewById(R.id.hebrewTick) as ImageView?
        englishTick = findViewById(R.id.englishTick) as ImageView?
        mDb = FirebaseFirestore.getInstance()
        topLayer?.setBackgroundColor(resources.getColor(R.color.basecolor))
        back_icon?.setOnClickListener({ onBackPressed() })

    }

    override fun onUnblocked(position: Int) {
        clickedPos = position
        showMessageOKCancel(
            getString(R.string.are_you_sure_want_to_block),
            "",
            this,
            this,
            getString(R.string.block),
            getString(R.string.cancel),
            false
        )


    }

    override fun onClick(p0: DialogInterface?, p1: Int) {
        val newChatroomRef = mDb?.collection("BlockedUsers")?.document(blockedUserList?.get(clickedPos)
            .toString())
        newChatroomRef?.delete()?.addOnCompleteListener {

        }
        clickedPos = -1
    }

}