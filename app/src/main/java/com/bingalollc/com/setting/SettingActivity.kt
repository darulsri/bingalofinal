package com.bingalollc.com.setting

import android.content.Intent
import android.location.Geocoder
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.widget.SwitchCompat
import androidx.databinding.DataBindingUtil
import com.bingalollc.com.AppController
import com.bingalollc.com.BuildConfig
import com.bingalollc.com.R
import com.bingalollc.com.SplashActivityTwo
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.contact.ContactActivity
import com.bingalollc.com.databinding.ActivitySettingBinding
import com.bingalollc.com.homeactivity.fragment.MapFragment
import com.bingalollc.com.login.LoginActivity
import com.bingalollc.com.model.ApiStatusModel
import com.bingalollc.com.network.RestClient
import com.bingalollc.com.payment.AddPayMenthod
import com.bingalollc.com.payment.BankListActivity
import com.bingalollc.com.preference.Common
import com.bingalollc.com.preference.PreferenceManager
import com.bingalollc.com.preference.StringsConstant
import com.bingalollc.com.splash.SplashActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*

class SettingActivity : BaseActivity(), MapFragment.OnMapChoosed {
    private val PROFILE_SETTING_PAGE = 10001;
    private lateinit var binding: ActivitySettingBinding
    private var topLayer: LinearLayout? = null
    private var header: TextView? = null
    private var back_icon: ImageView? = null
    private var logout_btn: TextView? = null
    private var connectToFb : RelativeLayout?=null
    private var layoutContact : RelativeLayout?=null
    private var switch_mapView : SwitchCompat?=null
    private var layoutName : RelativeLayout?=null
    private var layoutEmail : RelativeLayout?=null
    private var layoutLocation : RelativeLayout?=null
    private var layoutLanguage : RelativeLayout?=null
    private var layoutBlockedUsers : RelativeLayout?=null
    private var layoutChangePassword : RelativeLayout?=null
    private var payMethod : RelativeLayout?=null
    private var bankMethod : RelativeLayout?=null
    private var tvName : TextView?= null
    private var changeLoc : TextView?= null
    private var version : TextView?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_setting)
        /*   SwipeBack.attach(this, Position.LEFT)
               .setContentView(R.layout.activity_setting)
               .setSwipeBackView(R.layout.swipeback_default);*/

        initViews()
        initClicks()
        setAddress()
        setUserName()
        importSwipeBack()
    }

    private fun setUserName() {
        tvName?.text = preferenceManager?.userDetails?.fullName
        binding.tvEmail?.text = preferenceManager?.userDetails?.email?:""
        if (preferenceManager?.isEnglish == 1) {
            binding.languageSelected.text = getString(R.string.english)
        } else {
            binding.languageSelected.text = getString(R.string.hebrew)
        }


    }

    private fun initClicks() {
        switch_mapView?.isChecked =  preferenceManager?.notification?:true
        switch_mapView?.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
                preferenceManager?.notification = p1
            }

        })
        logout_btn?.setOnClickListener {
            showNormalDialog(
                this,
                getString(R.string.logout_questionmark),
                getString(R.string.logout_sure),
                getString(R.string.cancel),
                getString(R.string.logout)
            ) {
                if (it)
                    logout()
            }

        }

        layoutContact?.setOnClickListener({
            startActivity(Intent(this,ContactActivity::class.java))
        })

        layoutName?.setOnClickListener({
            startActivity(Intent(this@SettingActivity, ProfileSettingActivity::class.java)
                .putExtra("header",getString(R.string.enter_name)))
        })

        layoutEmail?.setOnClickListener({
            startActivityForResult(Intent(this@SettingActivity, ProfileSettingActivity::class.java)
                .putExtra("header",getString(R.string.change_email)), PROFILE_SETTING_PAGE)
        })

        layoutLanguage?.setOnClickListener({
            startActivity(Intent(this@SettingActivity, ProfileSettingActivity::class.java)
                .putExtra("header",getString(R.string.select_language)))
        })

        layoutLocation?.setOnClickListener({
            MapFragment.show(this, this)
        })

        layoutBlockedUsers?.setOnClickListener({
            startActivity(Intent(this@SettingActivity, ProfileSettingActivity::class.java)
                .putExtra("header",getString(R.string.blocked_users)))
        })

        layoutChangePassword?.setOnClickListener({
            startActivity(Intent(this@SettingActivity, ProfileSettingActivity::class.java)
                .putExtra("header",getString(R.string.change_password)))
        })

        binding.prohibittedItem.setOnClickListener {
            openUrl(StringsConstant.Prohibited_Items, this, getString(R.string.prohibitted_item));
        }
        binding.safetyTips.setOnClickListener {
            openUrl(StringsConstant.Safety_Tips, this, getString(R.string.safetyTips));
        }
        binding.marketGuideLines.setOnClickListener {
            openUrl(StringsConstant.Market_Guidelines, this, getString(R.string.marketGuideLines));
        }
        binding.birchatHaMazon.setOnClickListener {
            openUrl(StringsConstant.BIRCHAT_HAMAZON, this, getString(R.string.birchathaMazon));
        }
        binding.aboutBingalo.setOnClickListener {
            openUrl(StringsConstant.About_Bingalo, this, getString(R.string.aboutBingalo));
        }
        binding.advertiseOnBingalo.setOnClickListener {
            openEmail(StringsConstant.ADVERTISE_SUBJECT, StringsConstant.SUPPORT_EMAIL_ID, this)
        }
        binding.contactUs.setOnClickListener {
            openEmail(StringsConstant.CONTACT_SUBJECT, StringsConstant.CONTACT_US_EMAIL_ID,this)
        }
        binding.help.setOnClickListener {
            openEmail("", StringsConstant.SUPPORT_EMAIL_ID, this)
            // openUrl(StringsConstant.HELP, this, getString(R.string.help));
        }
        binding.faq.setOnClickListener {
            openUrl(StringsConstant.FAQ, this, getString(R.string.faq));
        }
        binding.termsAndCondition.setOnClickListener {
            openUrl(StringsConstant.Terms_of_conditions, this, getString(R.string.terms_of_conditions));
        }
        binding.privacyPolicy.setOnClickListener {
            openUrl(StringsConstant.Privacy_Policy, this, getString(R.string.privacyPolicy));
        }
        binding.deactivateBtn.setOnClickListener {
            showNormalDialogVerticalButton(this,
                getString(R.string.deativate_your_account),
                getString(
                    R.string.deativate_your_account_desc
                ),
                getString(R.string.cancel),
                getString(R.string.deactivate),
                BaseActivity.OnOkClicked {
                    if (it) {
                        deactivateAccount()
                    }
                }, true)
        }

    }

    private fun deactivateAccount() {
        showLoading(this)
        RestClient.getApiInterface().deactivateAccount(preferenceManager.userDetails.id, "2").enqueue(object : Callback<ApiStatusModel>{
            override fun onResponse(
                call: Call<ApiStatusModel>,
                response: Response<ApiStatusModel>
            ) {
                hideLoading()
                if (response.body()?.getStatus() == 200) {
                    logout()
                }
            }

            override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
                hideLoading()
            }
        })
    }

    private fun logout() {
        preferenceManager?.setUserLoggedin(false)
        preferenceManager?.userDetails = null
        AppController.getInstance().haveCalled = true
        preferenceManager.chooseLanguageAtStartUpDisplayed = false
        startActivity(Intent(this@SettingActivity, SplashActivityTwo::class.java)
            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
        finish()
    }

    private fun setAddress() {
        if(preferenceManager.userDetails!=null) {
            if (!TextUtils.isEmpty(preferenceManager.userDetails.lat)) {
                val adddress = getBasicAddress(preferenceManager.userDetails.lat.toDouble(), preferenceManager.userDetails.lng.toDouble(),
                    this
                )
                changeLoc?.text = adddress
            }
        }
    }

    private fun initViews() {
        payMethod = findViewById(R.id.payMethod) as RelativeLayout?
        bankMethod = findViewById(R.id.bankMethod) as RelativeLayout?
        topLayer = findViewById(R.id.topLayer) as LinearLayout?
        back_icon = findViewById(R.id.back_icon) as ImageView?
        header = findViewById(R.id.header) as TextView?
        logout_btn = findViewById(R.id.logout_btn) as TextView?;
        connectToFb = findViewById(R.id.connectToFb) as RelativeLayout?;
        layoutContact = findViewById(R.id.layoutContact) as RelativeLayout?;
        switch_mapView = findViewById(R.id.switch_mapView) as SwitchCompat?;
        layoutName = findViewById(R.id.layoutName) as RelativeLayout?;
        layoutEmail = findViewById(R.id.layoutEmail) as RelativeLayout?;
        layoutLocation = findViewById(R.id.layoutLocation) as RelativeLayout?;
        layoutLanguage = findViewById(R.id.layoutLanguage) as RelativeLayout?;
        layoutBlockedUsers = findViewById(R.id.layoutBlockedUsers) as RelativeLayout?;
        layoutChangePassword = findViewById(R.id.layoutChangePassword) as RelativeLayout?;
        tvName = findViewById(R.id.tvName) as TextView?;
        changeLoc = findViewById(R.id.changeLoc) as TextView?;
        version = findViewById(R.id.version) as TextView?;

        version?.text = "v ${BuildConfig.VERSION_NAME}.${BuildConfig.VERSION_CODE}"
        topLayer?.setBackgroundColor(resources.getColor(R.color.basecolor))
        header?.text = getString(R.string.settings)
        back_icon?.setOnClickListener({ onBackPressed() })
        payMethod?.setOnClickListener { startActivity(Intent(this, AddPayMenthod::class.java)) }
        bankMethod?.setOnClickListener { startActivity(Intent(this, BankListActivity::class.java)) }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        /*overridePendingTransition(R.anim.swipeback_stack_to_front,
            R.anim.swipeback_stack_right_out);*/
    }





    override fun onMapChoosed() {

        val usersData = preferenceManager.userDetails
        usersData.lat = Common.currentLat
        usersData.lng = Common.currentLng
        preferenceManager.userDetails = usersData
        setAddress()
    }

    override fun onStart() {
        super.onStart()
        setUserName()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PROFILE_SETTING_PAGE) {
            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>> "+ preferenceManager?.userDetails?.email)
            binding.tvEmail.text = preferenceManager?.userDetails?.email?:""
        }
    }
}