package com.bingalollc.com.setting

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.bingalollc.com.R
import com.bingalollc.com.databinding.FragmentVerifyOtpBinding
import com.bingalollc.com.homeactivity.homefragment.search.SearchFragment
import com.bingalollc.com.utils.ViewBindingDialogFragment


class VerifyOtpFragment : ViewBindingDialogFragment<FragmentVerifyOtpBinding>() {
    private var newEmail: String? = null
    private var oldEmail: String? = null
    private var onButtonClick: OnButtonClick? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle)
    }

    override fun provideBinding(inflater: LayoutInflater): FragmentVerifyOtpBinding {
        return FragmentVerifyOtpBinding.inflate(inflater)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (newEmail.isNullOrEmpty().not()) {
            binding.emailID.setText(newEmail)
        }

        binding.resendOtp.setOnClickListener {
           onButtonClick?.onResendClick()
        }

        binding.verify.setOnClickListener {
            val otp  = binding.otpLayout.getOtpEntered(requireContext())
            if (otp.isEmpty().not()){
                onButtonClick?.onVerifyClick(otp)
            }
        }
    }

    fun setEmail(newEmail: String, oldEmail: String) {
        this.newEmail = newEmail
        this.oldEmail = oldEmail
    }
    fun listenerCatcher(onButtonClick: OnButtonClick){
        this.onButtonClick = onButtonClick
    }

    public interface OnButtonClick{
        fun onResendClick()
        fun onVerifyClick(otpString : String)
    }

}