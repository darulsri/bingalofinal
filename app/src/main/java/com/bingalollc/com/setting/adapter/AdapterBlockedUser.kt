package com.bingalollc.com.setting.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bingalollc.com.R
import com.bingalollc.com.model.BlockedUserModel
import com.bingalollc.com.users.UsersArrayModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import java.util.ArrayList

class AdapterBlockedUser(var data: ArrayList<UsersArrayModel.Data>?, var onButtonClicked: OnButtonClicked) : RecyclerView.Adapter<AdapterBlockedUser.ViewHolder>() {
    private lateinit var context: Context


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        context = parent.context
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.layout_blocked_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: AdapterBlockedUser.ViewHolder, position: Int) {
        holder.tvName.text = data?.get(position)?.fullName
        Glide.with(context)
            .load(data?.get(position)?.image)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .skipMemoryCache(true)
            .placeholder(R.drawable.default_user)
            .error(R.drawable.default_user)
            .into(holder.profileImg)
        holder.unblockBtn.setOnClickListener {
            onButtonClicked.onUnblocked(position)
        }


    }

    override fun getItemCount(): Int {
        return data?.size!!
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)  {
        var tvName : TextView = itemView.findViewById(R.id.tvName)
        var profileImg : ImageView = itemView.findViewById(R.id.profileImg)
        var unblockBtn : TextView = itemView.findViewById(R.id.unblockBtn)

    }

    public interface OnButtonClicked {
        public fun onUnblocked(position: Int)
    }
}
