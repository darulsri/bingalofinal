package com.bingalollc.com.utils

import android.content.Context
import android.util.AttributeSet
import android.widget.ImageView
import android.widget.LinearLayout
import com.bingalollc.com.R
//import kotlinx.android.synthetic.main.star_image_view.view.*

class StartRating: LinearLayout {

    @JvmOverloads
    constructor(
        context: Context,
        attrs: AttributeSet? = null
    ) : super(context, attrs) {
        inflate(context, R.layout.star_image_view, this)}


    fun setStarRating(number: Float, context: Context) {
        if (number < 1) {
//            starOne.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_half_filled))
//            starTwo.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_unfilled))
//            starThree.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_unfilled))
//            starFour.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_unfilled))
//            starFive.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_unfilled))
        } else if (number == 1f) {
//            starOne.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_filled))
//            starTwo.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_unfilled))
//            starThree.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_unfilled))
//            starFour.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_unfilled))
//            starFive.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_unfilled))
        } else if (number < 2) {
//            starOne.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_filled))
//            starTwo.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_half_filled))
//            starThree.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_unfilled))
//            starFour.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_unfilled))
//            starFive.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_unfilled))
        } else if (number == 2f) {
//            starOne.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_filled))
//            starTwo.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_filled))
//            starThree.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_unfilled))
//            starFour.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_unfilled))
//            starFive.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_unfilled))
        } else if (number < 3) {
//            starOne.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_filled))
//            starTwo.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_filled))
//            starThree.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_half_filled))
//            starFour.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_unfilled))
//            starFive.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_unfilled))
        } else if (number == 3f) {
//            starOne.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_filled))
//            starTwo.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_filled))
//            starThree.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_filled))
//            starFour.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_unfilled))
//            starFive.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_unfilled))
        } else if (number < 4) {
//            starOne.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_filled))
//            starTwo.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_filled))
//            starThree.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_filled))
//            starFour.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_half_filled))
//            starFive.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_unfilled))
        } else if (number == 4f) {
//            starOne.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_filled))
//            starTwo.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_filled))
//            starThree.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_filled))
//            starFour.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_filled))
//            starFive.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_unfilled))
        } else if (number < 5) {
//            starOne.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_filled))
//            starTwo.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_filled))
//            starThree.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_filled))
//            starFour.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_filled))
//            starFive.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_half_filled))
        } else if (number >= 5) {
//            starOne.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_filled))
//            starTwo.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_filled))
//            starThree.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_filled))
//            starFour.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_filled))
//            starFive.setImageDrawable(context.resources.getDrawable(R.drawable.rate_star_filled))
        }
    }
}