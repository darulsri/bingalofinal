package com.bingalollc.com.utils;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.Keep;

/**
 * Created by kbibek on 2/26/16.
 */
@Keep
public class AppPrefrences {

  private final static String PREFS_FILE = "app_prefs";
  private SharedPreferences prefs;

  private final static String KEY_CACHE_LOCATION = "key_cache_location";

  public AppPrefrences(Context context) {
    prefs = context.getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE);
  }

  public void setCacheLocation(int cacheLocation) {
    prefs.edit().putInt(KEY_CACHE_LOCATION, cacheLocation).commit();
  }

  public int getCacheLocation() {
    return prefs.getInt(KEY_CACHE_LOCATION, 0);
  }
}