package com.bingalollc.com.utils

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.util.Log
import android.view.KeyEvent
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.bingalollc.com.R
//import kotlinx.android.synthetic.main.otp_screen_layout.view.*

class OtpLayout: LinearLayout {

    private val mutableLiveData = MutableLiveData<Boolean>()

    fun getMutableLiveData(): MutableLiveData<Boolean> {
        return mutableLiveData
    }

    @JvmOverloads
    constructor(
        context: Context,
        attrs: AttributeSet? = null
    ) : super(context, attrs) {
        inflate(context, R.layout.otp_screen_layout, this)
        initOtpBox()
    }

    private fun initOtpBox() {
//        moveFrontAndBack(1, otpOne, otpTwo, otpThree, otpFour, otpFive, otpSix);
        otpSixTextChangeListner()
    }

    fun getOtpEntered(context: Context): String{
//        if (isOtpValid())
//        {
////            val otp = otpOne.text.toString()+ otpTwo.text.toString() +
////                    otpThree.text.toString()+ otpFour.text.toString() +
////                    otpFive.text.toString()+ otpSix.text.toString()
//            return ""
//
//        } else {
//            Toast.makeText(context, "Invalid OTP", Toast.LENGTH_SHORT).show()
//            return ""
//        }
        return TODO("Provide the return value")
    }

    fun otpSixTextChangeListner() {
//        otpSix.addTextChangedListener(object : TextWatcher {
//            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
//            }
//
//            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
//
//            }
//
//            override fun afterTextChanged(p0: Editable?) {
//                mutableLiveData.value = true
//            }
//
//        })
    }


//    fun isOtpValid(): Boolean {
//        if (otpOne.length()>0 && otpTwo.length()>0 && otpThree.length()>0 &&
//            otpFour.length()>0 && otpFive.length()>0 && otpSix.length()>0)
//        {
//            return true
//
//        }
//        return false
//    }


    fun moveFrontAndBack(mSize: Int, vararg editText: EditText?) {
        for (pos in 0 until editText.size - 1) {
           moveToNextEt(editText[pos]!!, editText[pos + 1]!!, mSize)
        }
        for (pos in 1 until editText.size) {
            moveToPreviousEt(editText[pos]!!, editText[pos - 1]!!)
        }
    }

    fun moveToPreviousEt(mCurrentEt: EditText, mPrevEt: EditText) {
        mCurrentEt.setOnKeyListener { view, keyCode, keyEvent ->
            Log.e("onKey pressed", "==$keyCode")
            Log.e("text", "==" + mCurrentEt.text.toString())
            if (keyCode == KeyEvent.KEYCODE_DEL && keyEvent.action == KeyEvent.ACTION_DOWN && mCurrentEt.text.toString()
                    .isEmpty()
            ) {
                mPrevEt.requestFocus()
            }
            false
        }
    }
    fun moveToNextEt(mCurrentEt: EditText, mNextEt: EditText, mSize: Int) {
        mCurrentEt.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (mCurrentEt.text.toString().length == mSize) {
                    mNextEt.requestFocus()
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        })
    }

}