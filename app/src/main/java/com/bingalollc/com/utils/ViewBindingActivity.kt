package com.bingalollc.com.utils


import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import androidx.viewbinding.ViewBinding
import com.bingalollc.com.base.BaseActivity

abstract class ViewBindingActivity<Binding : ViewBinding> : BaseActivity() {

    private var _binding: Binding? = null
    protected val binding get() = _binding!!
    abstract fun provideBinding(inflater: LayoutInflater): Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = provideBinding(LayoutInflater.from(this))
        setContentView(binding.root)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }


    override fun onResume() {
        super.onResume()
    }

    override fun applyOverrideConfiguration(overrideConfiguration: Configuration) {
        super.applyOverrideConfiguration(overrideConfiguration)
    }
}