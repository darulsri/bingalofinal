package com.bingalollc.com.utils

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.bingalollc.com.R
import com.bingalollc.com.preference.StringsConstant
import java.text.SimpleDateFormat
import java.util.*


@Throws(IllegalAccessException::class, InstantiationException::class)
inline fun <reified T> Fragment.showDialogFragment(
    fromActivity: Boolean = true,
    extras: Bundle.() -> Unit = {}
): T? {
    val instance = newFragmentInstance<T>(extras)
    (instance as DialogFragment).show(
        if (fromActivity) {
            requireActivity().supportFragmentManager
        } else {
            childFragmentManager
        },
        T::class.java.simpleName
    )
    return instance
}

@Throws(IllegalAccessException::class, InstantiationException::class)
inline fun <reified T> newFragmentInstance(extras: Bundle.() -> Unit = {}): T? {

    return (T::class.java.newInstance() as Fragment).apply {
        arguments = Bundle().apply { extras() }
    } as T

}


@Throws(IllegalAccessException::class, InstantiationException::class)
inline fun <reified T : DialogFragment> AppCompatActivity.showDialogFragment(extras: Bundle.() -> Unit = {}): T? {

    return newFragmentInstance<T>(extras)?.apply {
        show(supportFragmentManager, T::class.java.simpleName)
    }
}

@Throws(IllegalAccessException::class, InstantiationException::class)
inline fun <reified T : DialogFragment> AppCompatActivity.showDialogFragmentWithBottomAnimation(extras: Bundle.() -> Unit = {}): T? {

    val fragmentTransaction: FragmentTransaction = supportFragmentManager.beginTransaction()
    fragmentTransaction.setCustomAnimations(
        R.anim.slide_in_bottom,
        R.anim.slide_out_top,
        R.anim.slide_in_top,
        R.anim.slide_out_bottom
    )
    val previous = supportFragmentManager.findFragmentByTag(
        T::class.java.getName()
    )
    if (previous != null) {
        fragmentTransaction.remove(previous)
    }
    fragmentTransaction.addToBackStack(null)

    return newFragmentInstance<T>(extras)?.apply {
        show(fragmentTransaction, T::class.java.simpleName)
    }

}

fun convertStringToDate(date: String): Date {
    var format = SimpleDateFormat(StringsConstant.CREATED_DATE, Locale.ENGLISH)
    format.timeZone = TimeZone.getTimeZone("IST+05:30")
    val newDate = format.parse(date)
    return newDate?: Date()
}

