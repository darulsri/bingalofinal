package com.bingalollc.com.crop_images

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import com.bingalollc.com.R
import com.bingalollc.com.databinding.FragmentCropImageBinding
import com.bingalollc.com.utils.ViewBindingDialogFragment
import com.bumptech.glide.Glide
import jp.wasabeef.glide.transformations.BlurTransformation
import java.io.IOException


class CCropImageActivityropImage : ViewBindingDialogFragment<FragmentCropImageBinding>(){
    private var imagePath: Uri? = null
    private val PIC_CROP= 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       // setStyle(STYLE_NORMAL, R.style.FullScreenDialogStyle)


    }

    override fun provideBinding(inflater: LayoutInflater): FragmentCropImageBinding {
        return FragmentCropImageBinding.inflate(inflater)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        readBundle()
        with(binding) {
            try {

                val bitmap = MediaStore.Images.Media.getBitmap(requireContext().getContentResolver(), imagePath)
                Glide.with(requireContext()).load(imagePath)
                    .transform(BlurTransformation(40)).into(bgImg)
                imagePreview.setImageUriAsync(imagePath)
            } catch (e: IOException) {
                e.printStackTrace()
            }

            saveBtn?.setOnClickListener {
                imagePreview?.getCroppedImageAsync();

            }
        }

    }

    private fun readBundle() {
        arguments?.let {
            imagePath = Uri.parse(it.getString("imageUri", "")) }
    }


}