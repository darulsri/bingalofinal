package com.bingalollc.com.crop_images

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.widget.Button
import android.widget.ImageView
import com.bingalollc.com.R
import com.bumptech.glide.Glide
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import jp.wasabeef.glide.transformations.BlurTransformation
import java.io.IOException

class CropTheImageActivity : AppCompatActivity() {
    private var imagePath:Uri?=null
    private var imagePreview:CropImageView?=null
    private var bgImg:ImageView?=null
    private var saveBtn:Button?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_crop_the_image)
        try {
            imagePreview = findViewById(R.id.imagePreview)
            imagePath  = Uri.parse(intent.getStringExtra("imageUri"))
            bgImg = findViewById(R.id.bgImg)
            saveBtn = findViewById(R.id.saveBtn)
            imagePreview?.isShowCropOverlay = true
            val bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imagePath)
            Glide.with(this).load(imagePath)
                .transform(BlurTransformation(40)).into(bgImg!!)
            imagePreview?.guidelines = CropImageView.Guidelines.ON
            imagePreview?.showContextMenu()
            imagePreview?.setImageUriAsync(imagePath)
            imagePreview?.guidelines = CropImageView.Guidelines.ON
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }
}