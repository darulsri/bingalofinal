package com.bingalollc.com.bottom_sheet

import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import android.text.InputFilter
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.widget.AppCompatButton
import androidx.core.text.isDigitsOnly
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.model.ApiStatusModel
import com.bingalollc.com.network.RestClient
import com.bingalollc.com.preference.PreferenceManager
import com.bingalollc.com.users.Users
import com.bingalollc.com.utils.OtpLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.textfield.TextInputEditText
import com.hbb20.CountryCodePicker
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class AddPhoneNumberPopUpDialog : BottomSheetDialogFragment() {

    private var countryPicker: CountryCodePicker? = null
    private var phoneNumber: TextInputEditText? = null
    private var closeDialog: ImageView?= null
    private var submitBtn: AppCompatButton? = null
    private var clickBtn: LinearLayout? = null
    private var phone_layout: LinearLayout? = null
    private var otpLayout: OtpLayout? = null
    private var resendOtp: TextView? = null
    private var header_text_two: TextView? = null
    private var disc: TextView? = null
    private var verify: TextView? = null
    private var baseActivity: BaseActivity? = null
    private var verifyingNumberOrEmail = ""
    private var isEmailVerifying = false
    private var preferenceManager: PreferenceManager? = null
    private var headerIcon: ImageView? = null
    private var mob = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.BottomSheetDialog);

    }
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.setOnShowListener { dialogInterface ->
            val bottomSheetDialog = dialogInterface as BottomSheetDialog
            setupFullHeight(bottomSheetDialog)
        }

        return dialog
    }

    private fun setupFullHeight(bottomSheetDialog: BottomSheetDialog) {
        val bottomSheet =
            bottomSheetDialog.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet)
        bottomSheet?.let { it ->
            val behaviour = BottomSheetBehavior.from(it)
            behaviour.skipCollapsed = true
            setupFullHeight(it)
            behaviour.state = BottomSheetBehavior.STATE_EXPANDED
        }
    }

    private fun setupFullHeight(bottomSheet: View) {
        val layoutParams = bottomSheet.layoutParams
        val height = (resources.displayMetrics.heightPixels * 0.90).toInt()

        layoutParams.height = height
        bottomSheet.layoutParams = layoutParams
    }

    private fun getWindowHeight(): Int {
        // Calculate window height for fullscreen use
        val displayMetrics = DisplayMetrics()
        (context as Activity?)!!.windowManager.defaultDisplay.getMetrics(displayMetrics)
        return displayMetrics.heightPixels
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.add_phone_number_popup, container, false)
        closeDialog = view.findViewById(R.id.popUpClose)
        submitBtn = view.findViewById(R.id.submitBtn)
        countryPicker = view.findViewById(R.id.ccp)
        phoneNumber = view.findViewById(R.id.et_phone)
        otpLayout = view.findViewById(R.id.otpLayout)
        resendOtp = view.findViewById(R.id.resendOtp)
        verify = view.findViewById(R.id.verify)
        header_text_two = view.findViewById(R.id.headerPhone)
        clickBtn = view.findViewById(R.id.clickBtn)
        phone_layout = view.findViewById(R.id.phone_layout)
        headerIcon = view.findViewById(R.id.headerIcon)
        disc = view.findViewById(R.id.disc)
        baseActivity = BaseActivity()
        preferenceManager = PreferenceManager(requireContext())


        // val behavior = BottomSheetBehavior.from(view)
//        val layoutParams = view.layoutParams
//        layoutParams.height = getBottomSheetDialogDefaultHeight(80)
//        view.layoutParams = layoutParams
        //behavior.state = BottomSheetBehavior.STATE_EXPANDED

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        closeDialog!!.setOnClickListener {
            dismiss()
        }

        countryPicker?.setCountryForNameCode("IL")
        countryPicker?.setCustomMasterCountries("il,gb,us,in")
        countryPicker?.setOnCountryChangeListener { checkValidNumber() }


        submitBtn?.setOnClickListener {
            if (phoneNumber?.text?.length!! > 0) {
                mob =  phoneNumber!!.text.toString()
                sendOtpToEmailPhone("+" + countryPicker?.selectedCountryCode + phoneNumber!!.text.toString(), 1)


            } else {
                phoneNumber?.error = getString(R.string.enter_phone_number)
            }
        }

        verify?.setOnClickListener {
            if (otpLayout!!.getOtpEntered(requireContext()).length > 5) {
                verifYOtp(otpLayout!!.getOtpEntered(requireContext()))
            }

        }

        otpLayout?.getMutableLiveData()?.observe(this) {
            if (it) {
//                if (otpLayout?.isOtpValid() == true) {
//                    //Enable Button
//                    verify?.background = AppCompatResources.getDrawable(requireContext(),R.drawable.curve6dp_basecolor)
//                    verify?.isEnabled = true
//
//                } else {
//                    //Disable Button
//                    verify?.background = AppCompatResources.getDrawable(requireContext(),R.drawable.curve6dp_grey)
//                    verify?.isEnabled = false
//                }
            }

        }

        resendOtp?.setOnClickListener { sendOtpToEmailPhone(phoneNumber!!.text.toString(),1) }

    }


    private fun checkValidNumber() {
        val countryCode = countryPicker?.selectedCountryCode.toString()
        var MAX = 12
        if (countryCode == "972") {
            MAX = 9
        } else if (countryCode == "91") {
            MAX = 10
        }
        phoneNumber?.setFilters(arrayOf<InputFilter>(android.text.InputFilter.LengthFilter(MAX)))
        if (phoneNumber?.length() ?: 0 > MAX) {
            phoneNumber?.setText(phoneNumber?.text?.toString()?.substring(0, MAX))
            phoneNumber?.setSelection(phoneNumber?.text?.length ?: 0)
        }
    }



    private fun sendOtpToEmailPhone(text: String, otpSendType: Int) {
        //0-Email 1-Phone
        baseActivity!!.showLoading(requireContext())
        verifyingNumberOrEmail = text
        var emailID = text
        var phoneNum = text
        if (otpSendType == 1) {
            emailID = ""
        } else {
            phoneNum = ""
        }

        RestClient.getApiInterface().sendOTP(emailID, phoneNum).enqueue(object :
            Callback<ApiStatusModel> {
            override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
                baseActivity!!.hideLoading()
            }

            override fun onResponse(
                call: Call<ApiStatusModel>,
                response: Response<ApiStatusModel>
            ) {
                baseActivity!!.hideLoading()
                if (response.body()?.getStatus() == 200) {
                    setUiForOtp(otpSendType)
                }
            }
        })
    }

    private fun setUiForOtp(otpSendType: Int) {
        //0-Email 1-Phone
        if (otpSendType == 0) {
            isEmailVerifying = true
        }
        otpLayout?.requestFocus()
        otpLayout?.visibility = View.VISIBLE
        clickBtn?.visibility = View.VISIBLE
        phone_layout?.visibility = View.GONE
        submitBtn?.visibility = View.GONE
        headerIcon?.setImageDrawable(requireContext().getDrawable(R.drawable.icon_otp))
        disc?.text = getString(R.string.we_have_sent_otp)
        header_text_two?.visibility = View.VISIBLE
        header_text_two?.text = "+"+countryPicker?.selectedCountryCode.toString()+"0"+phoneNumber?.text.toString()
    }


    private fun verifYOtp(otp: String) {
        baseActivity!!.showLoading(requireContext())
        var email = ""
        var phone = ""
        if (verifyingNumberOrEmail.isDigitsOnly()) {
            phone = verifyingNumberOrEmail
        } else {
            email = verifyingNumberOrEmail
        }

        baseActivity?.showLoading(requireContext())
        RestClient.getApiInterface().verifyOtp(preferenceManager?.userDetails?.id, verifyingNumberOrEmail, "", otp)
            .enqueue(object : retrofit2.Callback<ApiStatusModel> {
                override fun onResponse(
                    call: Call<ApiStatusModel>,
                    response: Response<ApiStatusModel>
                ) {
                    baseActivity?.hideLoading()
                    if (response.body()?.getStatus() == 200) {
                        val userDetails = preferenceManager?.userDetails
                        userDetails?.phone = mob
                        preferenceManager?.userDetails = userDetails
                        update_verificationForEmailPhone("phone")
                    } else {
                        baseActivity?.showToast(
                            response.body()?.getMessage() ?: "",
                            requireContext()
                        )
                    }


                }

                override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
                    baseActivity?.hideLoading()
                    baseActivity?.showToast(t.message.toString() ?: "", requireContext())

                }

            })

    }

    private fun update_verificationForEmailPhone(verify_type: String) {
        RestClient.getApiInterface().update_verification(
            preferenceManager?.userDetails?.id,
            verifyingNumberOrEmail,
            verify_type
        ).enqueue(object : Callback<ApiStatusModel> {
            override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
                baseActivity!!.hideLoading()
            }

            override fun onResponse(
                call: Call<ApiStatusModel>,
                response: Response<ApiStatusModel>
            ) {
                baseActivity!!.hideLoading()
                val v = Users.Verification()
                v.id = ""
                v.userId = preferenceManager?.userDetails?.id
                v.verifiedBy = verify_type
                v.createdAt = ""
                v.otp = ""
                v.verifiedStatus = ""
                preferenceManager?.userDetails?.verification?.add(v)

                dismiss()

                if (response.body()?.getStatus() == 200) {
                    // testDialog?.dismiss()
                } else {
                    baseActivity?.showToast(response.body()?.getMessage(), requireContext())
                }

            }
        })

    }
}