package com.bingalollc.com.webview

import android.os.Bundle
import android.util.Log
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.widget.ImageView
import android.widget.TextView
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity

class OpenWebView : BaseActivity() {
    private var back_icon: ImageView?= null
    private var header: TextView?= null
    private var webview: WebView?= null
    private var pdf = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_open_web_view)
        back_icon = findViewById(R.id.back_icon) as ImageView?
        header = findViewById(R.id.header) as TextView?
        webview = findViewById(R.id.webview) as WebView?
        back_icon?.setOnClickListener { finish() }
        header?.setText(intent.getStringExtra("headers"))
        showLoading(this)

        if (intent.getStringExtra("pdfurl").isNullOrEmpty().not()) {
            pdf = intent.getStringExtra("pdfurl").toString()
            webview?.getSettings()?.setJavaScriptEnabled(true);
            webview?.loadUrl("https://drive.google.com/viewerng/viewer?embedded=true&url=$pdf")
        } else {
            webview?.loadUrl(intent.getStringExtra("url").toString())
        }
        webview?.isScrollbarFadingEnabled = true
        webview?.setWebChromeClient(object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, progress: Int) {
                Log.e("progress", "" + progress)
                if (progress == 100) { //...page is fully loaded.
                    hideLoading()
                }
            }
        })
    }
}