package com.bingalollc.com.payment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bingalollc.com.R
import com.bingalollc.com.databinding.FragmentNewPaymentBinding
import com.bingalollc.com.users.UsersArrayModel
import com.bingalollc.com.utils.ViewBindingDialogFragment
import com.bumptech.glide.Glide


class NewPaymentFragment : ViewBindingDialogFragment<FragmentNewPaymentBinding>() {
    private var userData=  UsersArrayModel.Data()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    fun setData(userData: UsersArrayModel.Data){
        this.userData = userData
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding){
            crossBtn.setOnClickListener { dismiss() }

            Glide.with(requireContext()).load(userData.image).placeholder(R.drawable.default_user_pic).error(R.drawable.default_user_pic).into(sellerProfileImg)
            sellerProfileName.setText(userData.fullName)

            selectPayMethod.setOnClickListener { startActivity(Intent(requireContext(),AddPayMenthod::class.java)) }
        }
    }

    override fun provideBinding(inflater: LayoutInflater): FragmentNewPaymentBinding {
        return FragmentNewPaymentBinding.inflate(inflater)
    }

}