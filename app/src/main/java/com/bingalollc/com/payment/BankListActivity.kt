package com.bingalollc.com.payment

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.databinding.ActivityBankListBinding
import com.bingalollc.com.network.RestClient
import com.bingalollc.com.payment.model.BankAccountModel
import com.bingalollc.com.preference.PreferenceManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BankListActivity : BaseActivity() {
    private lateinit var binding: ActivityBankListBinding
    private var preferences: PreferenceManager? = null
    private val ADDBANKCALLED = 10000;


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        preferences = PreferenceManager(this)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_bank_list)
        initHeader()
        searchUserBankDet()
        binding.addBankBtn.setOnClickListener { startActivityForResult(Intent(this, AddBankActivity::class.java), ADDBANKCALLED)  }
    }

    private fun searchUserBankDet() {
        showLoading(this)
        RestClient.getApiInterface().fetchUserBankDetails(preferences?.userDetails?.id, preferences?.userDetails?.token).enqueue(object : Callback<BankAccountModel>{
            override fun onFailure(call: Call<BankAccountModel>, t: Throwable) {
                setDataToUi(false, null)
                hideLoading()
            }

            override fun onResponse(
                call: Call<BankAccountModel>,
                response: Response<BankAccountModel>
            ) {
                if (response.body()?.status == 200){
                    setDataToUi(true, response.body())
                }else{
                    setDataToUi(false, null)
                }
                hideLoading()
            }
        })
    }

    private fun setDataToUi(
        isBankAdded: Boolean,
        response: BankAccountModel?
    ) {
        if (isBankAdded) {
            binding.bankAccountLayout.visibility = View.VISIBLE
            binding.addBankBtn.visibility = View.GONE
            binding.noBankFound.visibility = View.GONE
            binding.topLayer.header.text = getString(R.string.bank_details)
            binding.accountNo.text = getString(R.string.acc_no_tag, response?.data?.accountNumber)
            binding.accoutnHolderName.text = getString(R.string.acc_holder_tag, response?.data?.holderName?.toUpperCase())
            binding.rountingNo.text = getString(R.string.routing_no_tag, response?.data?.routingNumber)
        }else{
            binding.topLayer.header.text = getString(R.string.add_bank_details)
            binding.bankAccountLayout.visibility = View.GONE
            binding.addBankBtn.visibility = View.VISIBLE
            binding.noBankFound.visibility = View.VISIBLE
        }

    }

    private fun initHeader() {
        binding.topLayer.header.text = getString(R.string.bank_details)
        binding.topLayer.backIcon.setOnClickListener { finish() }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ADDBANKCALLED){
            searchUserBankDet()
        }
    }
}