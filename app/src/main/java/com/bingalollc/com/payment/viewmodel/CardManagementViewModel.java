package com.bingalollc.com.payment.viewmodel;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.bingalollc.com.network.RestClient;
import com.bingalollc.com.payment.model.CardViewModel;
import com.bingalollc.com.preference.PreferenceManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CardManagementViewModel extends ViewModel {
    public MutableLiveData<CardViewModel> getCardViewModel = new MutableLiveData<>();

    public void getCardDetails(Context context, PreferenceManager preferenceManager){
        RestClient.getApiInterface().getUserCard(preferenceManager.getUserDetails().getId(), preferenceManager.getUserDetails().getToken()).enqueue(new Callback<CardViewModel>() {
            @Override
            public void onResponse(Call<CardViewModel> call, Response<CardViewModel> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == 200) {
                        Log.d("TAG", "onResponse: "+response.body());
                        getCardViewModel.setValue(response.body());
                    }
                }
            }

            @Override
            public void onFailure(Call<CardViewModel> call, Throwable t) {

            }
        });
    }
}
