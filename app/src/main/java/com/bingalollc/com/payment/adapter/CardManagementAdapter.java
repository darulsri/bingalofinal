package com.bingalollc.com.payment.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bingalollc.com.R;
import com.bingalollc.com.preference.PreferenceManager;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public abstract class CardManagementAdapter extends RecyclerView.Adapter<CardManagementAdapter.Viewholder> {
    private ArrayList<String> user_card_type;
    private ArrayList<String> card_number;
    private ArrayList<String> expiry_date;
    private ArrayList<String> card_name;
    private ArrayList<String> card_id;
    private Context context;
    private PreferenceManager preferenceManager;

    public CardManagementAdapter(ArrayList<String> user_card_type, ArrayList<String> card_number, ArrayList<String> expiry_date, ArrayList<String> card_name,ArrayList<String> card_id,PreferenceManager preferenceManager) {
        this.user_card_type = user_card_type;
        this.card_number = card_number;
        this.expiry_date = expiry_date;
        this.card_name = card_name;
        this.card_id = card_id;
        this.preferenceManager = preferenceManager;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context=parent.getContext();
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.card_items,parent,false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        //holder.card_name.setText(card_name.get(position));
        if (expiry_date.get(position)!=null && !expiry_date.get(position).equalsIgnoreCase("")) {
            holder.expiry_date.setText("EXP " + expiry_date.get(position));
        }
        holder.card_number.setText(card_number.get(position));
        switch (user_card_type.get(position)){
            case "MC":
                Glide.with(context).load(R.drawable.icon_mastercard).into(holder.card_img);
            case "":
        }
        if (user_card_type.get(position).startsWith("4")){
            Glide.with(context).load(R.drawable.icon_visa).into(holder.card_img);
        }else   if (user_card_type.get(position).startsWith("5")||user_card_type.get(position).startsWith("2")){
            Glide.with(context).load(R.drawable.icon_mastercard).into(holder.card_img);
        }else   if (user_card_type.get(position).startsWith("3")){
            Glide.with(context).load(R.drawable.icon_american_ex).into(holder.card_img);
        }else   if (user_card_type.get(position).toLowerCase().equals("amex")){
            Glide.with(context).load(R.drawable.amexs_card).into(holder.card_img);
        }else {
            Glide.with(context).load(R.drawable.card_icon).into(holder.card_img);
        }
        if (preferenceManager.getSelectedCard()!=null && preferenceManager.getSelectedCard().getId()!=null&&preferenceManager.getSelectedCard().getId().equalsIgnoreCase(card_id.get(position))){
            holder.selected_card.setVisibility(View.VISIBLE);
        }else {
            holder.selected_card.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return card_number.size();
    }

    public void pendingRemoval(final int position) {
        final String data = card_number.get(position);
        onDeleteClicked(card_number.indexOf(data));
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        private ImageView card_img,selected_card;
        private RelativeLayout card_layout;
        private TextView card_number,expiry_date;
        public Viewholder(@NonNull View itemView) {
            super(itemView);
            card_img = itemView.findViewById(R.id.card_img);
            card_number = itemView.findViewById(R.id.card_number);
            expiry_date = itemView.findViewById(R.id.expiry_date);
            card_layout = itemView.findViewById(R.id.card_layout);
            selected_card = itemView.findViewById(R.id.selected_card);
            card_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onViewClicked(getAdapterPosition());


                }
            });
        }
    }

    public abstract void onDeleteClicked(int position);
    public abstract void onViewClicked(int position);
}

