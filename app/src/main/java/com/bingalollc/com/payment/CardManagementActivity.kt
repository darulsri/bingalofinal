package com.bingalollc.com.payment

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.chat.SwipeUtil
import com.bingalollc.com.model.ApiStatusModel
import com.bingalollc.com.network.RestClient
import com.bingalollc.com.payment.adapter.CardManagementAdapter
import com.bingalollc.com.payment.model.CardViewModel
import com.bingalollc.com.payment.viewmodel.CardManagementViewModel
import com.bingalollc.com.preference.Common
import com.bingalollc.com.preference.PreferenceManager
import com.google.gson.internal.LinkedTreeMap
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class CardManagementActivity : BaseActivity() {
    private var side_logo: ImageView? = null
    private var no_card_text: TextView? = null
    private var addLayout: LinearLayout? = null
    private var cardManagementViewModel: CardManagementViewModel? = null
    private var cardManagementAdapter: CardManagementAdapter? = null
    private var alertDialog: AlertDialog? = null
    private var cardArrayList: ArrayList<CardViewModel.Datum> =
        ArrayList<CardViewModel.Datum>()

    // private ArrayList<String> card_number;
    private var expiry_date: ArrayList<String>? = null
    private var card_name: ArrayList<String>? = null
    private var user_card_type: ArrayList<String>? = null
    private var user_card_cc4: ArrayList<String>? = null
    private var user_card_id: ArrayList<String>? = null

    //  private ArrayList<String> card_number_search;
    private var expiry_date_search: ArrayList<String>? = null
    private var card_name_search: ArrayList<String>? = null
    private var user_card_type_search: ArrayList<String>? = null
    private var user_card_cc4_search: ArrayList<String>? = null
    private var user_card_id_search: ArrayList<String>? = null
    private var rvCardRecyclerView: RecyclerView? = null
    private var back_btn: ImageView? = null
    private var progressDialog: ProgressDialog? = null
    private var search_field: EditText? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card_management)
        progressDialog = ProgressDialog(this)
        progressDialog!!.setCancelable(false)
        progressDialog!!.setMessage("Loading...")
        progressDialog!!.show()
        side_logo = findViewById(R.id.side_logo) as ImageView?
        no_card_text = findViewById(R.id.no_card_text) as TextView?
        addLayout = findViewById(R.id.addLayout) as LinearLayout?
        search_field = findViewById(R.id.search_field) as EditText?
        rvCardRecyclerView = findViewById(R.id.rvCardRecyclerView) as RecyclerView?
        back_btn = findViewById(R.id.back_btn) as ImageView?
        addLayout?.setVisibility(View.VISIBLE)
        back_btn?.setOnClickListener(View.OnClickListener { finish() })
        addLayout?.setOnClickListener(View.OnClickListener {
            startActivityForResult(
                Intent(
                    this@CardManagementActivity,
                    AddCardActivity::class.java
                ), CARD_ADD_CLICK
            )
        })
        loadDataFromServer()
        search_field?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                charSequence: CharSequence,
                i: Int,
                i1: Int,
                i2: Int
            ) {
            }

            override fun onTextChanged(
                charSequence: CharSequence,
                i: Int,
                i1: Int,
                i2: Int
            ) {
            }

            override fun afterTextChanged(editable: Editable) {
                if (search_field?.getText().toString().length == 0) {
                    card_name = card_name_search
                    expiry_date = expiry_date_search
                    user_card_type = user_card_type_search
                    user_card_cc4 = user_card_cc4_search
                    user_card_id = user_card_id_search
                    if (card_name!!.size == 0) {
                        no_card_text?.setVisibility(View.VISIBLE)
                    } else {
                        no_card_text?.setVisibility(View.GONE)
                    }
                    initRecyclerView()
                } else {
                    if (card_name_search!!.size != 0) {
                        expiry_date = ArrayList()
                        card_name = ArrayList()
                        user_card_type = ArrayList()
                        user_card_cc4 = ArrayList()
                        user_card_id = ArrayList()
                        for (i in card_name_search!!.indices) {
                            if (card_name_search!![i].toLowerCase()
                                    .contains(search_field?.getText().toString().toLowerCase()) ||
                                user_card_cc4_search!![i].toLowerCase()
                                    .contains(search_field?.getText().toString().toLowerCase())
                            ) {
                                card_name!!.add(card_name_search!![i])
                                expiry_date!!.add(expiry_date_search!![i])
                                user_card_type!!.add(user_card_type_search!![i])
                                user_card_cc4!!.add(user_card_cc4_search!![i])
                                user_card_id!!.add(user_card_id_search!![i])
                            }
                        }
                        if (card_name!!.size == 0) {
                            no_card_text?.setVisibility(View.VISIBLE)
                        } else {
                            no_card_text?.setVisibility(View.GONE)
                        }
                        initRecyclerView()
                    }
                }
            }
        })
    }

    private fun loadDataFromServer() {
        cardManagementViewModel = ViewModelProviders.of(this@CardManagementActivity).get(
            CardManagementViewModel::class.java
        )
        cardManagementViewModel?.getCardDetails(this@CardManagementActivity, preferenceManager)
        cardManagementViewModel?.getCardViewModel?.observe(
            this@CardManagementActivity,
            Observer<CardViewModel> { cardViewModels ->
                cardArrayList = ArrayList()
                expiry_date = ArrayList()
                card_name = ArrayList()
                user_card_type = ArrayList()
                user_card_cc4 = ArrayList()
                user_card_id = ArrayList()
                expiry_date_search = ArrayList()
                card_name_search = ArrayList()
                user_card_type_search = ArrayList()
                user_card_cc4_search = ArrayList()
                user_card_id_search = ArrayList()
                if (cardViewModels.data != null) {
                    cardArrayList.addAll(cardViewModels.data)
                }
                for (i in cardArrayList.indices) {
                    card_name!!.add(cardArrayList[i].cardHolderName)
                    card_name_search!!.add(cardArrayList[i].cardHolderName)
                    expiry_date!!.add(cardArrayList[i].cardExpiryMonth+" / "+cardArrayList[i].cardExpiryYear)
                    expiry_date_search!!.add(cardArrayList[i].cardExpiryMonth+" / "+cardArrayList[i].cardExpiryYear)
                    user_card_type!!.add(cardArrayList[i].cardType)
                    user_card_type_search!!.add(cardArrayList[i].cardType)
                    user_card_cc4!!.add(cardArrayList[i].cardCvv)
                    user_card_cc4_search!!.add(cardArrayList[i].cardCvv)
                    user_card_id!!.add(cardArrayList[i].id)
                    user_card_id_search!!.add(cardArrayList[i].id)
                }
                if (card_name!!.size == 0) {
                    no_card_text!!.visibility = View.VISIBLE
                } else {
                    no_card_text!!.visibility = View.GONE
                }
                initRecyclerView()
                progressDialog!!.dismiss()
            })
    }

    private fun initRecyclerView() {
        val linearLayoutManager = LinearLayoutManager(this)
        cardManagementAdapter = object : CardManagementAdapter(
            user_card_type,
            user_card_cc4,
            expiry_date,
            card_name,
            user_card_id,
            preferenceManager
        ) {
            override fun onDeleteClicked(position: Int) {
                showDialogOfDelete(position)
            }

            override fun onViewClicked(position: Int) {
                if (intent.getStringExtra("from_cart") != null && intent.getStringExtra("from_cart")
                        .equals("true", ignoreCase = true)
                ) {
                    Common.selected_card = cardArrayList[position]
                    finish()
                } else {
                    cardManagementAdapter?.notifyDataSetChanged()
                }
                preferenceManager?.setSelectedCard(cardArrayList[position])
            }
        }
        rvCardRecyclerView!!.isNestedScrollingEnabled = false
        rvCardRecyclerView!!.adapter = cardManagementAdapter
        rvCardRecyclerView!!.layoutManager = linearLayoutManager
        progressDialog!!.dismiss()
        setSwipeForRecyclerView()
    }

    private fun showDialogOfDelete(position: Int) {
        val alertDialoga =
            AlertDialog.Builder(this@CardManagementActivity)
        val inflater = this@CardManagementActivity.layoutInflater
        val add_menu_layout: View = inflater.inflate(R.layout.logout_dialog, null)
        val rvState =
            add_menu_layout.findViewById<Button>(R.id.logoutBtn)
        rvState.text = "DELETE"
        val textMsg = add_menu_layout.findViewById<TextView>(R.id.textMsg)
        textMsg.text = resources.getString(R.string.sure_delete_card)
        val cancelBtn =
            add_menu_layout.findViewById<Button>(R.id.cancelBtn)
        rvState.setOnClickListener {
            deleteCard(user_card_id!![position])
            alertDialog!!.dismiss()
            if (search_field!!.text.toString().length == 0) {
                val deletingpos = user_card_id_search!!.indexOf(user_card_id!![position])
                user_card_id_search!!.remove(user_card_id!![position])
                card_name_search!!.removeAt(deletingpos)
                expiry_date_search!!.removeAt(deletingpos)
                user_card_type_search!!.removeAt(deletingpos)
                user_card_cc4_search!!.removeAt(deletingpos)
                user_card_id!!.removeAt(position)
                card_name!!.removeAt(position)
                expiry_date!!.removeAt(position)
                user_card_type!!.removeAt(position)
                user_card_cc4!!.removeAt(position)
                cardManagementAdapter?.notifyDataSetChanged()
            } else {
                progressDialog!!.show()
                search_field!!.setText("")
                loadDataFromServer()
            }
        }
        cancelBtn.setOnClickListener {
            cardManagementAdapter?.notifyItemChanged(position)
            alertDialog!!.dismiss()
        }
        alertDialoga.setView(add_menu_layout)
        alertDialog = alertDialoga.show()
    }

    private fun deleteCard(card_id: String) {
        RestClient.getApiInterface().deleteUserCard(
            card_id
        ).enqueue(object : Callback<ApiStatusModel?> {
            override fun onResponse(
                call: Call<ApiStatusModel?>,
                response: Response<ApiStatusModel?>
            ) {
                progressDialog!!.dismiss()
                if (response.body()?.getStatus() ==200){
                    showToast("Your card has been deleted successfully",this@CardManagementActivity)
                }else{
                    showToast(response?.body()?.getMessage()?:"",this@CardManagementActivity)
                }
            }

            override fun onFailure(
                call: Call<ApiStatusModel?>,
                t: Throwable
            ) {
                progressDialog!!.dismiss()
            }
        })
    }

    private fun setSwipeForRecyclerView() {
        val swipeHelper: SwipeUtil = object : SwipeUtil(0, ItemTouchHelper.LEFT, this) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val swipedPosition = viewHolder.adapterPosition
                cardManagementAdapter = rvCardRecyclerView!!.adapter as CardManagementAdapter?
                cardManagementAdapter?.pendingRemoval(swipedPosition)
            }

            override fun getSwipeDirs(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder
            ): Int {
                val position = viewHolder.adapterPosition
                cardManagementAdapter = rvCardRecyclerView!!.adapter as CardManagementAdapter?
                return super.getSwipeDirs(recyclerView, viewHolder)
            }

        }
        val mItemTouchHelper = ItemTouchHelper(swipeHelper)
        mItemTouchHelper.attachToRecyclerView(rvCardRecyclerView)
        //set swipe label
        swipeHelper.setLeftSwipeLable("Deleting")
        //set swipe background-Color
        swipeHelper.setLeftcolorCode(ContextCompat.getColor(this, R.color.swipe_color))
    }

    companion object {
        private const val CARD_ADD_CLICK = 1000
    }
}