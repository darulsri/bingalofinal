package com.bingalollc.com.payment

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.bingalollc.com.R
import com.bingalollc.com.databinding.ActivityAddPayMenthodBinding
import com.bingalollc.com.preference.PreferenceManager

class AddPayMenthod : AppCompatActivity() {
    private lateinit var binding: ActivityAddPayMenthodBinding
    private var preferenceManager: PreferenceManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        preferenceManager = PreferenceManager(this)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_pay_menthod)
        initAppBar()
    }

    private fun initAppBar() {
        binding.appBar.header.setText(getString(R.string.add_payment_method))
        binding.appBar.backIcon.setOnClickListener { finish() }
        var cardTypeText = ""
        if (!preferenceManager?.selectedCard?.cardNumber.isNullOrEmpty()) {
            if (preferenceManager?.selectedCard?.cardType.isNullOrEmpty().not()) {

                if (preferenceManager?.selectedCard?.cardType.equals("4")) {

                }else if (preferenceManager?.selectedCard?.cardType.equals("5") && preferenceManager?.selectedCard?.cardType.equals("2")) {

                }
                if (preferenceManager?.selectedCard?.cardType!!?.startsWith("4") == true) {
                    cardTypeText = getString(R.string.visa)
                } else if (preferenceManager?.selectedCard?.cardType!!.startsWith("5") || preferenceManager?.selectedCard?.cardType!!.startsWith("2")) {
                    cardTypeText = getString(R.string.master_card)
                } else if (preferenceManager?.selectedCard?.cardType!!.startsWith("3")) {
                    cardTypeText = getString(R.string.amex)
                }
            }
            binding.creditCardText.setText(cardTypeText+ " ending "+preferenceManager?.selectedCard?.cardLast4)
        }
        binding.creditLayout.setOnClickListener {
            startActivity(Intent(this, CardManagementActivity::class.java))
        }
        binding.creditCardArrow.setOnClickListener {
            checkCardPaytype()
        }
        binding.payPalLayout.setOnClickListener {
            setCurrentPaymentMethod(1)
        }
        binding.venoLayout.setOnClickListener {
            setCurrentPaymentMethod(2)
        }
        binding.gpay.setOnClickListener {
            setCurrentPaymentMethod(3)
        }
    }

    override fun onStart() {
        super.onStart()
        setCurrentPaymentMethod(preferenceManager?.defaultPaymentType?:-1)
    }

    private fun setCurrentPaymentMethod(paymentMethod: Int) {
        //0-Credit/Debit   1-Paypal  2-Veno
        if (paymentMethod == 0) {
            binding.payPalCurrent.visibility = View.GONE
            binding.creditCardCurrent.visibility = View.VISIBLE
            binding.venoCurrent.visibility = View.GONE
            binding.gpayCurrent.visibility = View.GONE

            binding.creditCardArrow.visibility = View.GONE
            binding.venoArraow.visibility = View.VISIBLE
            binding.payPalArrow.visibility = View.VISIBLE
            binding.gpayArrow.visibility = View.VISIBLE

        } else if (paymentMethod == 1) {
            binding.payPalCurrent.visibility = View.VISIBLE
            binding.creditCardCurrent.visibility = View.GONE
            binding.venoCurrent.visibility = View.GONE
            binding.gpayCurrent.visibility = View.GONE

            binding.creditCardArrow.visibility = View.VISIBLE
            binding.venoArraow.visibility = View.VISIBLE
            binding.payPalArrow.visibility = View.GONE
            binding.gpayArrow.visibility = View.VISIBLE
        } else if (paymentMethod == 2) {
            binding.payPalCurrent.visibility = View.GONE
            binding.creditCardCurrent.visibility = View.GONE
            binding.venoCurrent.visibility = View.VISIBLE
            binding.gpayCurrent.visibility = View.GONE

            binding.creditCardArrow.visibility = View.VISIBLE
            binding.venoArraow.visibility = View.GONE
            binding.payPalArrow.visibility = View.VISIBLE
            binding.gpayArrow.visibility = View.VISIBLE
        } else if (paymentMethod == 3) {
            binding.payPalCurrent.visibility = View.GONE
            binding.creditCardCurrent.visibility = View.GONE
            binding.gpayCurrent.visibility = View.VISIBLE
            binding.venoCurrent.visibility = View.GONE

            binding.creditCardArrow.visibility = View.VISIBLE
            binding.venoArraow.visibility = View.VISIBLE
            binding.payPalArrow.visibility = View.VISIBLE
            binding.gpayArrow.visibility = View.GONE
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1000){
            checkCardPaytype()
        }
    }

    private fun checkCardPaytype() {
        if (preferenceManager?.selectedCard?.cardType.isNullOrEmpty().not()){
            setCurrentPaymentMethod(0)
        }else
            startActivity(Intent(this, CardManagementActivity::class.java))
    }
}