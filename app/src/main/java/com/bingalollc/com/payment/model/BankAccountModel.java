package com.bingalollc.com.payment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BankAccountModel {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("bank_name")
        @Expose
        private String bankName;
        @SerializedName("bank_location")
        @Expose
        private String bankLocation;
        @SerializedName("account_number")
        @Expose
        private String accountNumber;
        @SerializedName("holder_name")
        @Expose
        private String holderName;
        @SerializedName("payment_email")
        @Expose
        private String paymentEmail;
        @SerializedName("routing_number")
        @Expose
        private String routingNumber;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("id_number")
        @Expose
        private String idNumber;
        @SerializedName("dob")
        @Expose
        private String dob;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("stret_address")
        @Expose
        private String stretAddress;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("post_code")
        @Expose
        private String postCode;
        @SerializedName("address_rear_doc_id")
        @Expose
        private String addressRearDocId;
        @SerializedName("address_front_doc_id")
        @Expose
        private String addressFrontDocId;
        @SerializedName("doc_front_id")
        @Expose
        private String docFrontId;
        @SerializedName("doc_rear_id")
        @Expose
        private String docRearId;
        @SerializedName("stripe_account_id")
        @Expose
        private String stripeAccountId;
        @SerializedName("stripe_bank_id")
        @Expose
        private String stripeBankId;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("modified_at")
        @Expose
        private String modifiedAt;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getBankName() {
            return bankName;
        }

        public void setBankName(String bankName) {
            this.bankName = bankName;
        }

        public String getBankLocation() {
            return bankLocation;
        }

        public void setBankLocation(String bankLocation) {
            this.bankLocation = bankLocation;
        }

        public String getAccountNumber() {
            return accountNumber;
        }

        public void setAccountNumber(String accountNumber) {
            this.accountNumber = accountNumber;
        }

        public String getHolderName() {
            return holderName;
        }

        public void setHolderName(String holderName) {
            this.holderName = holderName;
        }

        public String getPaymentEmail() {
            return paymentEmail;
        }

        public void setPaymentEmail(String paymentEmail) {
            this.paymentEmail = paymentEmail;
        }

        public String getRoutingNumber() {
            return routingNumber;
        }

        public void setRoutingNumber(String routingNumber) {
            this.routingNumber = routingNumber;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getIdNumber() {
            return idNumber;
        }

        public void setIdNumber(String idNumber) {
            this.idNumber = idNumber;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getStretAddress() {
            return stretAddress;
        }

        public void setStretAddress(String stretAddress) {
            this.stretAddress = stretAddress;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getPostCode() {
            return postCode;
        }

        public void setPostCode(String postCode) {
            this.postCode = postCode;
        }

        public String getAddressRearDocId() {
            return addressRearDocId;
        }

        public void setAddressRearDocId(String addressRearDocId) {
            this.addressRearDocId = addressRearDocId;
        }

        public String getAddressFrontDocId() {
            return addressFrontDocId;
        }

        public void setAddressFrontDocId(String addressFrontDocId) {
            this.addressFrontDocId = addressFrontDocId;
        }

        public String getDocFrontId() {
            return docFrontId;
        }

        public void setDocFrontId(String docFrontId) {
            this.docFrontId = docFrontId;
        }

        public String getDocRearId() {
            return docRearId;
        }

        public void setDocRearId(String docRearId) {
            this.docRearId = docRearId;
        }

        public String getStripeAccountId() {
            return stripeAccountId;
        }

        public void setStripeAccountId(String stripeAccountId) {
            this.stripeAccountId = stripeAccountId;
        }

        public String getStripeBankId() {
            return stripeBankId;
        }

        public void setStripeBankId(String stripeBankId) {
            this.stripeBankId = stripeBankId;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getModifiedAt() {
            return modifiedAt;
        }

        public void setModifiedAt(String modifiedAt) {
            this.modifiedAt = modifiedAt;
        }

    }

}