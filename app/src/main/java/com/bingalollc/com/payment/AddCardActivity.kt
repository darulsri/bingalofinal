package com.bingalollc.com.payment


import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.view.MotionEvent
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.SimpleOnItemTouchListener
import androidx.viewpager.widget.ViewPager
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.model.ApiStatusModel
import com.bingalollc.com.network.RestClient
import com.bingalollc.com.payment.adapter.CardInputAdapter
import com.bingalollc.com.payment.model.CardViewModel
import com.bingalollc.com.preference.Common
import com.bingalollc.com.preference.PreferenceManager
import com.google.gson.internal.LinkedTreeMap
import com.bingalollc.com.utils.creditcardview.CreditCardView
import org.w3c.dom.Text
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class AddCardActivity : BaseActivity() {
    private var card: CreditCardView? = null
    private var rvNameDescription: RecyclerView? = null
    private val hintText = ArrayList<String>()
    private val type = ArrayList<String>()
    private var card_num = ""
    private var card_name = ""
    private var card_exp = ""
    private var card_cvv = ""
    private var progressDialog: ProgressDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_card)
        hintText.add(getString(R.string.enter_card_no))
        hintText.add(getString(R.string.enter_card_name))
        hintText.add(getString(R.string.enter_card_exp))
        hintText.add(getString(R.string.enter_card_cvv))
        type.add("0")
        type.add("0")
        type.add("0")
        type.add("0")
        val back_btn =
            findViewById(R.id.back_icon)
        val header =
            findViewById(R.id.header) as TextView
        header.text = getString(R.string.add_card)
        progressDialog = ProgressDialog(this)
        progressDialog!!.setMessage(getString(R.string.adding_card))
        progressDialog!!.setCancelable(false)
        back_btn.setOnClickListener { finish() }
        rvNameDescription = findViewById(R.id.rvNameDescription) as RecyclerView?

        //  arrow_entry = findViewById(R.id.arrow_entry);
        card = findViewById(R.id.card) as CreditCardView?
        card?.setNumberFormat("%d4 %d4 %d4 %d4 %d4 %d4 %d4 %d4")
        initRecyclerView()
    }

    private fun addCardToServer() {
        progressDialog!!.show()
        RestClient.getApiInterface().addUserCard(
            preferenceManager?.userDetails?.id,
            card_name,
            card_exp.substring(0, 2),
            card_exp.substring(3),
            card_num.substring(0, 4),
            card_cvv, preferenceManager?.userDetails?.token?:""
        ).enqueue(object : Callback<ApiStatusModel?> {
            override fun onResponse(
                call: Call<ApiStatusModel?>,
                response: Response<ApiStatusModel?>
            ) {
                progressDialog!!.dismiss()
                if (response.body()?.getStatus() ==200){
                    val al =
                        AlertDialog.Builder(this@AddCardActivity)
                    al.setMessage(getString(R.string.card_added_successfully))
                    al.setCancelable(false)
                    al.setPositiveButton(
                        getString(R.string.ok)
                    ) { dialogInterface, i ->
                        val intent = Intent(
                            this@AddCardActivity,
                            CardManagementActivity::class.java
                        )
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                        finish()
                    }
                    al.show()
                }else{
                    showToast(response?.body()?.getMessage()?:"",this@AddCardActivity)
                }
            }

            override fun onFailure(
                call: Call<ApiStatusModel?>,
                t: Throwable
            ) {
                progressDialog!!.dismiss()
            }
        })
    }
    private fun initRecyclerView() {
        val linearLayoutManager =
            LinearLayoutManager(this@AddCardActivity, RecyclerView.HORIZONTAL, false)
        val cardInputAdapter: CardInputAdapter = object : CardInputAdapter(hintText, card) {
            override fun onDialogfilled(position: Int, text: String) {
                if (position == 0) {
                    if (text.length == 1) {
                        checkAndSetLogo(text)
                    }
                    if (text.length == 16 || text.startsWith("3") && text.length == 15) {
                        card_num = text.replace(" ", "")
                        Handler().postDelayed({
                            rvNameDescription!!.smoothScrollToPosition(1)
                            Common.cardPosition = 1
                            notifyDataSetChanged()
                        }, 500)
                    } else {
                        card_num = ""
                    }
                } else if (position == 2) {
                    card_exp = if (text.length == 5) {
                        text
                    } else {
                        ""
                    }
                }
            }

            override fun onDialogClicked(position: Int, text: String) {
                if (position == 2) {
//                    card!!.flip()
                }
                if (position == 1) {
                    card_name = text
                }
                if (position == 3) {
                    if (text.length == 3) {
                        card_cvv = text
                        addCardToServer()
                    } else {
                        card_cvv = ""
                    }
                } else {
                    Handler().postDelayed({
                        rvNameDescription!!.smoothScrollToPosition(position + 1)
                        Common.cardPosition = position + 1
                        // notifyDataSetChanged();
                    }, 500)
                }
            }

            override fun onBackClicked(position: Int) {
                if (position == 3) {
                    if (card!!.isFlipped) {
//                        card!!.flip()
                    }
                }
                Handler().postDelayed({
                    rvNameDescription!!.smoothScrollToPosition(position - 1)
                    Common.cardPosition = position - 1
                    // notifyDataSetChanged();
                }, 500)
            }
        }
        rvNameDescription!!.adapter = cardInputAdapter
        rvNameDescription!!.layoutManager = linearLayoutManager
        rvNameDescription!!.addOnItemTouchListener(object : SimpleOnItemTouchListener() {
            override fun onInterceptTouchEvent(
                rv: RecyclerView,
                e: MotionEvent
            ): Boolean {
                // Stop only scrolling.
                return rv.scrollState == RecyclerView.SCROLL_STATE_DRAGGING
            }
        })
    }

    private fun checkAndSetLogo(user_card_type: String) {
        if (user_card_type.startsWith("4")) {
            card!!.setImageLogo(resources.getDrawable(R.drawable.card_visa))
        } else if (user_card_type.startsWith("5") || user_card_type.startsWith("2")) {
            card!!.setImageLogo(resources.getDrawable(R.drawable.card_mastercard))
        } else if (user_card_type.startsWith("3")) {
            card!!.setImageLogo(resources.getDrawable(R.drawable.card_amex_dark))
        } else if (user_card_type.toLowerCase() == "amex") {
            card!!.setImageLogo(resources.getDrawable(R.drawable.card_amex_dark))
        } else {
            card!!.setImageLogo(resources.getDrawable(R.drawable.card_unknown))
        }
    }

    private fun setMaxLength(editText: EditText, length: Int) {
        val filterArray = arrayOfNulls<InputFilter>(1)
        filterArray[0] = LengthFilter(length)
        editText.filters = filterArray
    }

}