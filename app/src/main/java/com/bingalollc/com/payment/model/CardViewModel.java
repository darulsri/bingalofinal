package com.bingalollc.com.payment.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CardViewModel {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("card_holder_name")
        @Expose
        private String cardHolderName;
        @SerializedName("card_expiry_month")
        @Expose
        private String cardExpiryMonth;
        @SerializedName("card_expiry_year")
        @Expose
        private String cardExpiryYear;
        @SerializedName("card_number")
        @Expose
        private String cardNumber;
        @SerializedName("card_cvv")
        @Expose
        private String cardCvv;
        @SerializedName("stripe_token")
        @Expose
        private String stripeToken;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("modified")
        @Expose
        private String modified;
        @SerializedName("card_last4")
        @Expose
        private String cardLast4;
        @SerializedName("card_type")
        @Expose
        private String cardType;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getCardHolderName() {
            return cardHolderName;
        }

        public void setCardHolderName(String cardHolderName) {
            this.cardHolderName = cardHolderName;
        }

        public String getCardExpiryMonth() {
            return cardExpiryMonth;
        }

        public void setCardExpiryMonth(String cardExpiryMonth) {
            this.cardExpiryMonth = cardExpiryMonth;
        }

        public String getCardExpiryYear() {
            return cardExpiryYear;
        }

        public void setCardExpiryYear(String cardExpiryYear) {
            this.cardExpiryYear = cardExpiryYear;
        }

        public String getCardNumber() {
            return cardNumber;
        }

        public void setCardNumber(String cardNumber) {
            this.cardNumber = cardNumber;
        }

        public String getCardCvv() {
            return cardCvv;
        }

        public void setCardCvv(String cardCvv) {
            this.cardCvv = cardCvv;
        }

        public String getStripeToken() {
            return stripeToken;
        }

        public void setStripeToken(String stripeToken) {
            this.stripeToken = stripeToken;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getModified() {
            return modified;
        }

        public void setModified(String modified) {
            this.modified = modified;
        }

        public String getCardLast4() {
            return cardLast4;
        }

        public void setCardLast4(String cardLast4) {
            this.cardLast4 = cardLast4;
        }

        public String getCardType() {
            return cardType;
        }

        public void setCardType(String cardType) {
            this.cardType = cardType;
        }

    }

}
