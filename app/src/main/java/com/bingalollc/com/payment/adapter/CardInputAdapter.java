package com.bingalollc.com.payment.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bingalollc.com.R;
import com.google.android.material.textfield.TextInputLayout;
import com.bingalollc.com.utils.creditcardview.CreditCardView;
import com.maxpilotto.creditcardview.models.CardInput;

import java.util.ArrayList;

public abstract class CardInputAdapter extends RecyclerView.Adapter<CardInputAdapter.Viewholder> {
    private ArrayList<String> hintText;
    private CreditCardView card;
    private CreditCardFormatTextWatcher creditCardFormatTextWatcher;
    private Context context;

    public CardInputAdapter(ArrayList<String> hintText, CreditCardView card) {
        this.hintText = hintText;
        this.card = card;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context=parent.getContext();
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.card_inout_layout_item,parent,false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        holder.cardtextField.setHint(hintText.get(position));
       if (position==0){
           creditCardFormatTextWatcher=new CreditCardFormatTextWatcher(holder.cardtextField);
           card.pairInput(CardInput.NUMBER, holder.cardtextField);
           holder.cardtextField.setInputType(InputType.TYPE_CLASS_NUMBER);
           setMaxLength(holder.cardtextField,16);
           holder.cardtextField.addTextChangedListener(creditCardFormatTextWatcher);
       }else if (position==1){
           setMaxLength(holder.cardtextField,30);
           card.pairInput(CardInput.HOLDER, holder.cardtextField);
           holder.cardtextField.setInputType(InputType.TYPE_TEXT_VARIATION_FILTER);
       }else if (position==2){
           setMaxLength(holder.cardtextField,5);
           card.pairInput(CardInput.EXPIRY, holder.cardtextField);
           holder.cardtextField.setInputType(InputType.TYPE_CLASS_NUMBER);
       }else if (position==3){
           setMaxLength(holder.cardtextField,3);
           card.pairInput(CardInput.CVV, holder.cardtextField);
           holder.cardtextField.setInputType(InputType.TYPE_CLASS_NUMBER);
       }
     /* if (CommonData.cardPosition==position){

           InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
           holder.cardtextField.requestFocus();
           if (imm != null){
               imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
           }
       }*/

    }

    private void setMaxLength(EditText editText, int length){
        InputFilter[] filterArray = new InputFilter[1];
        filterArray[0] = new InputFilter.LengthFilter(length);
        editText.setFilters(filterArray);
    }


    @Override
    public int getItemCount() {
        return hintText.size();
    }


    public class Viewholder extends RecyclerView.ViewHolder {
        private EditText cardtextField;
        private ImageView click_arrow,click_arrow_back;
        public Viewholder(@NonNull View itemView) {
            super(itemView);
            cardtextField=itemView.findViewById(R.id.cardtextField);
            click_arrow=itemView.findViewById(R.id.click_arrow);
            click_arrow_back=itemView.findViewById(R.id.click_arrow_back);
            click_arrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null){
                        imm.hideSoftInputFromWindow(cardtextField.getWindowToken(), 0);
                    }
                    onDialogClicked(getAdapterPosition(), cardtextField.getText().toString());
                }
            });

            click_arrow_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackClicked(getAdapterPosition());
                }
            });

            cardtextField.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    cardtextField.setSelection(cardtextField.getText().length());
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {

                    if (getAdapterPosition()==0){
                        click_arrow_back.setVisibility(View.GONE);
                        String sa = cardtextField.getText().toString();
                        if (sa.length()==16 || (sa.startsWith("3") && sa.length()==15)){
                            click_arrow.setVisibility(View.VISIBLE);
                            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                            if (imm != null){
                                imm.hideSoftInputFromWindow(cardtextField.getWindowToken(), 0);
                            }
                        }else { click_arrow.setVisibility(View.GONE);}

                    }else if (getAdapterPosition()==1){
                        click_arrow_back.setVisibility(View.VISIBLE);
                        String sa = cardtextField.getText().toString();
                        if (sa.length()>0){
                            click_arrow.setVisibility(View.VISIBLE);
                        }else {
                            click_arrow.setVisibility(View.GONE);
                        }

                    }else if (getAdapterPosition()==2){
                        click_arrow_back.setVisibility(View.VISIBLE);
                        String sa = cardtextField.getText().toString();
                        if (sa.length()>4){
                            click_arrow.setVisibility(View.VISIBLE);
                        }else {
                            click_arrow.setVisibility(View.GONE);
                        }
                        if (cardtextField.length()==2){
                            if (!cardtextField.getText().toString().contains("/")) {
                                cardtextField.setText(cardtextField.getText().toString() + "/");
                                cardtextField.setSelection(cardtextField.getText().length());
                            }
                        }
                    }else if (getAdapterPosition()==3){
                        click_arrow_back.setVisibility(View.VISIBLE);
                        String sa = cardtextField.getText().toString();
                        if (sa.length()==3){
                            click_arrow.setVisibility(View.VISIBLE);
                        }else {
                            click_arrow.setVisibility(View.GONE);
                        }
                    }
                        onDialogfilled(getAdapterPosition(),  cardtextField.getText().toString());

                }
            });
        }
    }


    public abstract void onDialogfilled(int position, String text);
    public abstract void onDialogClicked(int position, String text);
    public abstract void onBackClicked(int position);
}
