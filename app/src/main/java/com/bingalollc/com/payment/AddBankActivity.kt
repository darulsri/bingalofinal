package com.bingalollc.com.payment

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.format.DateFormat
import android.text.format.Time
import android.widget.DatePicker
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.camerax.CameraConstant
import com.bingalollc.com.databinding.ActivityAddBankBinding
import com.bingalollc.com.model.ApiStatusModel
import com.bingalollc.com.network.MultipartParams
import com.bingalollc.com.network.RestClient
import com.bingalollc.com.preference.PreferenceManager
import id.zelory.compressor.Compressor.compress
import io.grpc.Compressor
//import kotlinx.android.synthetic.main.activity_add_bank.*
//import kotlinx.android.synthetic.main.activity_bank_list.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.util.*
import kotlin.collections.ArrayList


class AddBankActivity : BaseActivity(), DialogInterface.OnClickListener {
    private val REQUEST_READ_EXTERNAL_STORAGE_CHOOSE_IMG = 1000
    private val PICK_IMAGE_REQUEST_GALLERY = 1002
    private lateinit var binding: ActivityAddBankBinding
    private var mArrayFile: java.util.ArrayList<File>? = ArrayList()
    private var file1: File? = null
    private var file2: File? = null
    private var file3: File? = null
    private var file4: File? = null
    private var preferences: PreferenceManager? = null
    private var datePicker: DatePicker? = null
    private var calendar: Calendar? = null
    private var year = 0
    private  var month:Int = 0
    private  var day:Int = 0
    private var imageClicked = -1;
    private var validDateOfBirth = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_bank)
        preferences = PreferenceManager(this)
        calendar = Calendar.getInstance();
        year = calendar!!.get(Calendar.YEAR);
        month = calendar!!.get(Calendar.MONTH);
        day = calendar!!.get(Calendar.DAY_OF_MONTH);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_bank)
        initData()
        initHeader()
    }

    private fun initData() {
        mArrayFile = ArrayList()
    }

    private fun initHeader() {
        binding.topLayer.header.text = getString(R.string.add_bank_details)
        binding.topLayer.backIcon.setOnClickListener { finish() }
        binding.etDateOfBirth.setOnClickListener { openDobPicker() }
        binding.addBankBottom.addressFront.setOnClickListener { uploadPhoto(1) }
        binding.addBankBottom.addressBack.setOnClickListener { uploadPhoto(2) }
        binding.addBankBottom.verificationFront.setOnClickListener { uploadPhoto(3) }
        binding.addBankBottom.verificationBack.setOnClickListener { uploadPhoto(4) }
        binding.saveButton.setOnClickListener { verifyDoc() }
    }

    private fun verifyDoc() {
        if (_allFieldsValid()){
            showLoading(this)
            val builder = MultipartParams.Builder();
            updateBuilder(builder)
            RestClient.getApiInterface().updateBankAccountOfStripe(builder.build().map).enqueue(object : Callback<ApiStatusModel>{
                override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
                    showToast(getString(R.string.something_went_wring), this@AddBankActivity)
                    hideLoading()
                }

                override fun onResponse(
                    call: Call<ApiStatusModel>,
                    response: Response<ApiStatusModel>
                ) {
                    hideLoading()
                    showMessageOKCancel(getString(R.string.bank_added_successfully), "",this@AddBankActivity, this@AddBankActivity,
                    getString(R.string.ok),"", false)

                }
            })
        }
    }

    private  fun updateBuilder(builder: MultipartParams.Builder) {
        builder.add("user_id", preferences?.userDetails?.id)
        builder.addFile("image[]", file1)
        builder.addFile("image[]", file2)
        builder.addFile("image[]", file3)
        builder.addFile("image[]", file4)
        /*GlobalScope.launch {
            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>.1111111>> "+file1?.length())
            file1 = compress(this@AddBankActivity, file1!!)
            file2 = compress(this@AddBankActivity, file1!!)
            file3 = compress(this@AddBankActivity, file1!!)
            file4 = compress(this@AddBankActivity, file1!!)
        }*/
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>.22222>  "+file1?.length())
//        builder.add("account_holder_name", et_accountName.text)
//        builder.add("sort_code", et_routing_number.text)
//        builder.add("account_number", et_accnt_number.text)
//        builder.add("email", et_accnt_email_num.text)
        if (preferences?.userDetails?.fullName.toString()?.contains(" ") == true) {
            builder.add("first_name", preferences?.userDetails?.fullName?.substring(0, preferences?.userDetails?.fullName.toString().indexOf(" ")))
            builder.add("last_name", preferences?.userDetails?.fullName?.substring(preferences?.userDetails?.fullName.toString().lastIndexOf(" "))?.trim())

        }else {
            builder.add("first_name", preferences?.userDetails?.fullName)
            builder.add("last_name", "")
        }
        builder.add("city", binding.addBankBottom.etCity.text)
        builder.add("line1", binding.addBankBottom.etAddressLineOne.text)
        builder.add("line2", "")
        builder.add("postalcode", binding.addBankBottom.etPostal.text)
        builder.add("state", binding.addBankBottom.etState.text)
        builder.add("ssn_last", binding.etSsnNumber.text)
        builder.add("dob", validDateOfBirth)
        builder.add("phone_number", binding.etPhone.text)
    }

    private fun _allFieldsValid(): Boolean {
        if (binding.etAccountName.text?.isEmpty()!!) {
            showToast(getString(R.string.please_enter_acc_name),this)
            return false
        }

        if (binding.etAccntNumber.text?.isEmpty()!!) {
            showToast(getString(R.string.please_enter_acc_no),this)
            return false
        }

        if (binding.etRoutingNumber.text?.isEmpty()!!) {
            showToast(getString(R.string.please_enter_routing_no),this)
            return false
        }

        if (binding.etAccntEmailNum.text?.isEmpty()!!) {
            showToast(getString(R.string.please_enter_payment_email),this)
            return false
        }

        if (binding.etPhone.text?.isEmpty()!!) {
            showToast(getString(R.string.please_enter_phone_number),this)
            return false
        }

        if (binding.etDateOfBirth.text?.isEmpty()!!) {
            showToast(getString(R.string.please_enter_dob),this)
            return false
        }

        if (binding.addBankBottom.etAddressLineOne.text?.isEmpty()!!) {
            showToast(getString(R.string.please_enter_address_one),this)
            return false
        }

        if (binding.addBankBottom.etCity.text?.isEmpty()!!) {
            showToast(getString(R.string.please_enter_city),this)
            return false
        }

        if (binding.addBankBottom.etState.text?.isEmpty()!!) {
            showToast(getString(R.string.please_enter_state),this)
            return false
        }

        if (binding.addBankBottom.etPostal.text?.isEmpty()!!) {
            showToast(getString(R.string.please_enter_postal),this)
            return false
        }

        if (file1 == null) {
            showToast(getString(R.string.please_selected_front_address),this)
            return false
        }
        if (file2 == null) {
            showToast(getString(R.string.please_selected_rear_address),this)
            return false
        }
        if (file3 == null) {
            showToast(getString(R.string.please_selected_front_id),this)
            return false
        }
        if (file4 == null) {
                showToast(getString(R.string.please_selected_rear_id),this)
            return false
        }
        return true;
    }

    private fun uploadPhoto(i: Int) {
        imageClicked = i;
        if (checkPermissionREAD_EXTERNAL_STORAGE(
                this,
                REQUEST_READ_EXTERNAL_STORAGE_CHOOSE_IMG)) {
            choosephoto()
        }
    }


    private fun choosephoto() {
        val intent = Intent()
        intent.type = "image/*"
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_picture)), PICK_IMAGE_REQUEST_GALLERY)
    }
    private fun checkPermissionREAD_EXTERNAL_STORAGE(
        context: Context?, requestCode: Int): Boolean {
        val currentAPIVersion = Build.VERSION.SDK_INT
        return if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this!!,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        (context as Activity?)!!,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    //Toast.makeText(PostAdPage.this, "Do itititi", Toast.LENGTH_SHORT).show();
                    showDialog(getString(R.string.external_storage), context,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE, requestCode)
                } else {
                    requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE),
                        requestCode)
                }
                false
            } else {
                true
            }
        } else {
            true
        }
    }

    fun showDialog(msg: String, context: Context?,
                   permission: String, statusCode: Int) {
        val alertBuilder = android.app.AlertDialog.Builder(context)
        alertBuilder.setCancelable(false)
        alertBuilder.setTitle(getString(R.string.permission_necessary))
        alertBuilder.setMessage("$msg permission is necessary")
        alertBuilder.setPositiveButton(android.R.string.yes
        ) { dialog, which ->
            requestPermissions(arrayOf(permission),
                statusCode)
        }
        val alert = alertBuilder.create()
        alert.show()
    }

    private fun openDobPicker() {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(this, OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            val chosenDate = Time()
            chosenDate.set(dayOfMonth, monthOfYear, year)
            val dtDob: Long = chosenDate.toMillis(true)
            val strDate = DateFormat.format("MMM dd, yyyy", dtDob)
            val strDateUploads = DateFormat.format("dd-MM-yyyy", dtDob)
            validDateOfBirth = strDateUploads.toString()
            binding.etDateOfBirth.setText(strDate)
        }, year, month, day)

        dpd.datePicker.maxDate = Date().time
        dpd.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_REQUEST_GALLERY && data != null) {
            CameraConstant.mArrayFileConstant?.clear()

            val tempUri = data.data
            if (tempUri != null) {
                val file = File(getPath(tempUri))
                if (imageClicked ==1){
                    file1 = file
                    binding.addBankBottom.addressFront.setImageURI(tempUri)
                }else if (imageClicked ==2){
                    file2 = file
                    binding.addBankBottom.addressBack.setImageURI(tempUri)
                }else if (imageClicked ==3){
                    file3 = file
                    binding.addBankBottom.verificationFront.setImageURI(tempUri)
                }else if (imageClicked ==4){
                    file4 = file
                    binding.addBankBottom.verificationBack.setImageURI(tempUri)
                }
            }
        }
    }

    @SuppressLint("Range")
    fun getPath(uri: Uri?): String? {
        var cursor: Cursor = this.getContentResolver().query(uri!!, null, null, null, null)!!
        cursor.moveToFirst()
        var document_id = cursor.getString(0)
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1)
        cursor.close()
        cursor = this.getContentResolver().query(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            null, MediaStore.Images.Media._ID + " = ? ", arrayOf(document_id), null)!!
        cursor.moveToFirst()
        val path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA))
        cursor.close()
        return path
    }

    override fun onClick(p0: DialogInterface?, p1: Int) {
        //ON ALertDialog Listener
        onBackPressed()
    }


}