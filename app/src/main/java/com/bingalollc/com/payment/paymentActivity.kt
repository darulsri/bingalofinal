package com.bingalollc.com.payment

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.Gravity
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.airbnb.lottie.parser.FloatParser
import com.bingalollc.com.R
import com.bingalollc.com.cart.Product_1
import com.bingalollc.com.cart.additem
import com.bingalollc.com.cart.usercartAdapter
import com.bingalollc.com.network.RestClient
import com.bingalollc.com.preference.PreferenceManager
import com.paypal.android.sdk.payments.PayPalConfiguration
import com.paypal.android.sdk.payments.PayPalPayment
import com.paypal.android.sdk.payments.PayPalService
import com.paypal.android.sdk.payments.PaymentActivity
import com.paypal.android.sdk.payments.PaymentConfirmation
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.math.BigDecimal

class paymentActivity : AppCompatActivity() {

    lateinit var Add: TextView
    lateinit var confirm: Button
     var Tipone :TextView?= null
     var Tiptwo :TextView?= null
     var Tipthree :TextView?= null
     var Tipfour :TextView?= null
     var Tipfive :TextView?= null
    var orderTotal :TextView?= null
    var Tipamount:TextView?= null
    var grandtotal:TextView? = null
    var Tipsamount :TextView?=null
    lateinit var L_Tipone :LinearLayout
    lateinit var L_Tiptwo :LinearLayout
    lateinit var L_Tipthree :LinearLayout
    lateinit var L_Tipfour :LinearLayout
    lateinit var L_Tipfive :LinearLayout
    var total : Int?=null

    var preferenceManager: PreferenceManager? =null
    var ordercart :ArrayList<Product_1> ?= null


    @SuppressLint("SetTextI18n", "MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)

        Add = findViewById(R.id.ADDpromo)
        confirm = findViewById(R.id.Confirmbutton)
        Tipone = findViewById(R.id.tip_1)
        Tiptwo = findViewById(R.id.tip_2)
        Tipthree = findViewById(R.id.tip_3)
        Tipfour = findViewById(R.id.tip_4)
        Tipfive = findViewById(R.id.tip_5)
        orderTotal = findViewById(R.id.ordertotalamount)
        Tipamount = findViewById(R.id.Tips)
        grandtotal = findViewById(R.id.total_amount)
        L_Tipone = findViewById(R.id.L_Tip1)
        L_Tiptwo = findViewById(R.id.L_Tip2)
        L_Tipthree = findViewById(R.id.L_Tip3)
        L_Tipfour = findViewById(R.id.L_Tip4)
        L_Tipfive = findViewById(R.id.L_Tip5)
        Tipamount = findViewById(R.id.Tipsamount)

        getUserCart()

        Tipone?.text = "5"
        Tiptwo?.text = "10"
        Tipthree?.text = "15"
        Tipfour?.text = "20"
        Tipfive?.text = "Others"

        var addressid = intent.getIntExtra("address_id", -1)
        var deliveryid = intent.getIntExtra("delivery_id",-1)
        total = intent.getIntExtra("total",-1)

        Log.e("data_paymentActivity", "$addressid,$deliveryid,$total")

        orderTotal?.text = total.toString()
        grandtotal?.text =total.toString()


        Add.setOnClickListener {
            Bottompromo()

        }

        confirm.setOnClickListener {
            getPayment()
        }

        L_Tipone.setOnClickListener {

            L_Tipone.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.background_blue))
            L_Tiptwo.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.backround))
            L_Tipthree.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.backround))
            L_Tipfour.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.backround))
            L_Tipfive.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.backround))
            var Total_num = orderTotal?.text.toString().toInt()
            var tot = Total_num?.plus(5)
            grandtotal?.text = tot.toString()
            Tipamount?.text = "5"

        }

        L_Tiptwo.setOnClickListener {

            L_Tipone.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.backround))
            L_Tiptwo.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.background_blue))
            L_Tipthree.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.backround))
            L_Tipfour.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.backround))
            L_Tipfive.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.backround))
            var Total_num = total
            var tot = Total_num?.plus(10)
            grandtotal?.text = tot.toString()
            Tipamount?.text = "10"
        }

        L_Tipthree.setOnClickListener {

            L_Tipone.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.backround))
            L_Tiptwo.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.backround))
            L_Tipthree.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.background_blue))
            L_Tipfour.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.backround))
            L_Tipfive.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.backround))
            var Total_num = total
            var tot = Total_num?.plus(15)
            grandtotal?.text = tot.toString()
            Tipamount?.text = "15"
        }

        L_Tipfour.setOnClickListener {

            L_Tipone.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.backround))
            L_Tiptwo.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.backround))
            L_Tipthree.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.backround))
            L_Tipfour.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.background_blue))
            L_Tipfive.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.backround))

            var Total_num = total
            var tot = Total_num?.plus(20)
            grandtotal?.text = tot.toString()
            Tipamount?.text = "20"
        }

        L_Tipfive.setOnClickListener {

            L_Tipone.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.backround))
            L_Tiptwo.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.backround))
            L_Tipthree.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.backround))
            L_Tipfour.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.backround))
            L_Tipfive.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.background_blue))
            popup()

        }

    }

    private fun getPayment(){

        var amount = grandtotal?.text

        val payment = PayPalPayment(BigDecimal(amount.toString()),"USD","Shopping",PayPalPayment.PAYMENT_INTENT_SALE)

        var intent = Intent(this,PaymentActivity::class.java)

        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config)
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT,payment)
        startActivityForResult(intent, REQUEST_CODE_PAYMENT)

    }

    private fun Bottompromo(){

        var dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.payment_popup)
        dialog.show()

        dialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window?.setGravity(Gravity.CENTER_HORIZONTAL)
    }


    private fun getUserCart(){

        RestClient.getApiInterface().getUserCart(preferenceManager?.userDetails?.token,
            preferenceManager?.userDetails?.id).enqueue(object : Callback<additem> {
            override fun onResponse(call: Call<additem>, response: Response<additem>) {
                if (response.isSuccessful)
                {
                    val ordersummary =response.body()
                    if (ordersummary!=null){
                        val Product = ordersummary?.data
                        ordercart = Product?.products as ArrayList<Product_1>?

                    }

                }
            }

            override fun onFailure(call: Call<additem>, t: Throwable) {
                Log.e("Succesful", t.message.toString())
            }

        })
    }


    private fun popup(){
        var dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.tippopup)
        dialog.show()
        var enterTip : EditText ?= null


        enterTip = dialog.findViewById(R.id.enterTip)
        val cancel : Button = dialog.findViewById(R.id.cancel)
        val done : Button = dialog.findViewById(R.id.donepop)


        cancel.setOnClickListener {

            dialog.dismiss()
        }

        done.setOnClickListener {

            var Tip = enterTip?.text
            Log.e("Custom Tip amount",Tip.toString())


            var Total_num = total
            var tot = Total_num?.plus(Integer.parseInt(Tip.toString()))
            grandtotal?.text = tot.toString()
            Tipamount?.text = Tip

            dialog.dismiss()

        }


//        dialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window?.setGravity(Gravity.CENTER)
    }


    companion object {
        private val TAG = "paymentExample"

        private val CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_NO_NETWORK

        private val Client_id = "AZ9FAhj7XjoU2zdYdwLIOO1KfoLyWoR9QjEqh9oAtLIxiezXUQ_BF5qhUWbbOzgfNiJrig9Zrar37E3b"

        private val REQUEST_CODE_PAYMENT = 1
        private val REQUEST_CODE_FUTURE_PAYMENT = 2
        private val REQUEST_CODE_PROFILE_SHARING = 3

        private val config = PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(Client_id)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_PAYMENT){
            if (requestCode == Activity.RESULT_OK){
                val confirm : PaymentConfirmation? = data?.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION)
                if (confirm != null){
                    try {
                        val Paymentdetails = confirm.toJSONObject().getString("id")
                        Log.e("confirm",confirm.toJSONObject().toString()+Paymentdetails)
                        Log.e("confirm payment",confirm.payment.toJSONObject().toString())

                    }catch (e: JSONException){
                        Log.e("PaymentActivity","an extremely unlikely failure occurred:",e)
                    }
                }
            }

        }
    }

}