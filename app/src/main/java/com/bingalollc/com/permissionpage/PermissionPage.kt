package com.bingalollc.com.permissionpage

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.core.app.ActivityCompat
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.homeactivity.HomeActivity
import com.bingalollc.com.preference.Common
import com.bingalollc.com.utils.AppLocationService
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions

object TrackingUtility {

    fun hasLocationPermissions(context: Context) =
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.S) {
            EasyPermissions.hasPermissions(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION,
            )
        } else {
            EasyPermissions.hasPermissions(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION,
            )
        }
}

class PermissionPage : AppCompatActivity() , EasyPermissions.PermissionCallbacks{
    private val LOCATION_PERMISSION = 1000
    private val STORAGE_PERMISSION = 1001
    private val CAMERA_PERMISSION = 1002
    private var yesBtn: AppCompatButton? = null
    private var button_ok: ImageView? = null
    private var textheader: TextView? = null
    private  var textheader_two:TextView? = null
    private var crossBtn: ImageView? = null
    private var relativeLayout: RelativeLayout? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_permission_page)
        yesBtn = findViewById(R.id.yesBtn)
        button_ok = findViewById(R.id.button_ok)
        textheader = findViewById(R.id.textheader)
        relativeLayout = findViewById(R.id.relativeLayout)
        textheader_two = findViewById(R.id.textheader_two)
        crossBtn = findViewById(R.id.crossBtn)


        yesBtn?.setOnClickListener(View.OnClickListener { askPErmision() })
        button_ok?.setOnClickListener(View.OnClickListener { askPErmision() })
        crossBtn?.setOnClickListener(View.OnClickListener {
           startActivity(Intent(this@PermissionPage, HomeActivity::class.java))
            finish()
        })
    }

    private fun getPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (this.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    BaseActivity.LOCATION_PERMISSION
                )
            } else {
                val appLocationService = AppLocationService(this)
                var gpsLocation = appLocationService
                    .getLocation(LocationManager.GPS_PROVIDER)
                if (gpsLocation == null) {
                    gpsLocation = appLocationService
                        .getLocation(LocationManager.NETWORK_PROVIDER)
                    if (gpsLocation == null) {
                        gpsLocation = appLocationService
                            .getLocation(LocationManager.PASSIVE_PROVIDER)
                    }
                }
                if (gpsLocation != null) {
                    Common.productLocationLat = gpsLocation.getLatitude().toString()
                    Common.currentLat = gpsLocation.getLatitude().toString()
                    Common.productLocationLng = gpsLocation.getLongitude().toString()
                    Common.currentLng = gpsLocation.getLongitude().toString()
                }
            }
        }
    }


    private fun askPErmision() {
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this@PermissionPage,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION
            )
        }else{
            getPermission()
            startActivity(Intent(this@PermissionPage, HomeActivity::class.java))
            finish()
        }
    }


//    override fun onRequestPermissionsResult(
//        requestCode: Int,
//        permissions: Array<String?>,
//        grantResults: IntArray
//    ) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
//        if (requestCode ==LOCATION_PERMISSION) {
//            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                startActivity(Intent(this@PermissionPage, HomeActivity::class.java))
//                finish()
//            }else{
//                if (ActivityCompat.shouldShowRequestPermissionRationale(this@PermissionPage,
//                        Manifest.permission.ACCESS_FINE_LOCATION)) {
//                    askPErmision()
//                } else {
//                    startActivity(Intent(this@PermissionPage, HomeActivity::class.java))
//                    finish()
//                }
//            }
//        }
//    }
    private fun requestPermissions() {
        if (TrackingUtility.hasLocationPermissions(this)) {
            return
        }
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.S) {
            EasyPermissions.requestPermissions(
                this,
                "You need to accept location permissions to use this app",
                LOCATION_PERMISSION,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            )
        } else {
            EasyPermissions.requestPermissions(
                this,
                "You need to accept location permissions to use this app",
                LOCATION_PERMISSION,
                android.Manifest.permission.ACCESS_FINE_LOCATION,
            )
        }
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        startActivity(Intent(this@PermissionPage, HomeActivity::class.java))
        finish()
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            AppSettingsDialog.Builder(this).build().show()
        } else {
            startActivity(Intent(this@PermissionPage, HomeActivity::class.java))
            finish()
            requestPermissions()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }
}