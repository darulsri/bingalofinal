package com.bingalollc.com.preference;


import androidx.annotation.Keep;
import androidx.lifecycle.MutableLiveData;

import com.bingalollc.com.homeactivity.model.ProductModel;
import com.bingalollc.com.payment.model.CardViewModel;

import java.util.ArrayList;

@Keep
public class StringsConstant {
    public static final String CREATED_DATE = "yyyy-MM-dd hh:mm:ss";


    public static final String BIRCHAT_HAMAZON = "https://page.bingalo.com";
    public static final String Teffilat_HaDedech = "http://ijyaweb.com/bingalo_api/teffilat.pdf";
    public static final String Prohibited_Items = "https://page.bingalo.com/prohibited-items/";
    public static final String Safety_Tips = " https://page.bingalo.com/prohibited-items/";
    public static final String Market_Guidelines = "https://page.bingalo.com/prohibited-items/";
    public static final String About_Bingalo = "https://page.bingalo.com/about-us/";
    public static final String HELP = "Support@Bingalo.com";
    public static final String FAQ = "https://page.bingalo.com/faq-1/";
    public static final String Terms_of_conditions = "https://page.bingalo.com/terms-of-service/";
    public static final String Privacy_Policy = "https://page.bingalo.com/privacy-policy/";
    public static final String EMAIL_ID = "info@m.bingalo.com";
    public static final String CONTACT_US_EMAIL_ID = "hello@bingalo.com";
    public static final String SUPPORT_EMAIL_ID = "support@bingalo.com";
    public static final String ADVERTISE_SUBJECT = "Ads@Bingalo.com";
    public static final String CONTACT_SUBJECT = "https://page.bingalo.com/contact-us/";

}
