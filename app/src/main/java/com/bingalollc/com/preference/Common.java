package com.bingalollc.com.preference;


import android.content.pm.ActivityInfo;

import androidx.annotation.Keep;
import androidx.lifecycle.MutableLiveData;

import com.bingalollc.com.homeactivity.ViewPager.ImagesFragment;
import com.bingalollc.com.homeactivity.model.ProductModel;
import com.bingalollc.com.payment.model.CardViewModel;
import com.bingalollc.com.product_details.ProductDetails;

import java.util.ArrayList;
import java.util.HashMap;

@Keep
public class Common {
    //public static final String BASE_URL = "https://ijyaweb.com/bingalo_api/api/";
    public static final String BASE_URL = "https://api.bingalo.com/api/";
    //public static final String PRODUCT_BASE_URL = "http://bingalo.ijyaweb.com/product/";
    public static final String PRODUCT_BASE_URL = "https://api.bingalo.com/api/product/";
    public static final String SHARE_BASE_URL = "https://bingalo.com/product/";
    public static final String ONESIGNAL_APP_ID = "0e260056-a2e7-4f4d-a294-f2bd9f77cccf";
    private static Common common;
    public static String productLocationLat = "";
    public static String productLocationLng = "";
    public static String currentLat = "";
    public static String currentLng = "";
    public static String currencySymbol = "₪ ";
    public static String commonDistance = "50";
    public static int commonLimit = 20;
    public static CardViewModel.Datum selected_card=null;
    public static int cardPosition=0;
    public static boolean isProductRefreshNeeded = false;
    public static ProductModel.Datum selectedProductDetails = new ProductModel.Datum();
    public final MutableLiveData<String> onClickedViewPager = new MutableLiveData<>();
    public static ArrayList<String> blockedUserId = new ArrayList<>();
    public static ArrayList<String> categoryNameEng = new ArrayList<>();
    public static ArrayList<String> categoryNameHeb = new ArrayList<>();
    public static int currentTab = 0;
    public static int swipePositionInChat = -3;
    public static boolean isChatScreenOpened = false;
    public final MutableLiveData<ArrayList<ProductModel.Datum>> allProductOfUser = new MutableLiveData<>();
    public final MutableLiveData<Boolean> refreshLatLong = new MutableLiveData<>();
    public static ArrayList<ProductModel.Datum> selectedProductModeldata = new ArrayList<>();

    public interface PermissionConstant {
        int PERMISSION_REQUEST_CODE_CAMERA_MODE = 207;
    }

    public static Common getInstance() {
        if (common == null) {
            common = new Common();
        }
        return common;
    }
}
