package com.bingalollc.com.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

import com.bingalollc.com.BuildConfig;
import com.bingalollc.com.homeactivity.model.PostProductDraftModel;
import com.bingalollc.com.payment.model.CardViewModel;
import com.bingalollc.com.users.Users;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

public class PreferenceManager {
    Context context;
    private static PreferenceManager instance = null;
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;
    public PreferenceManager(Context context) {
        this.context =context;
    }

    public static PreferenceManager getInstance(Context context) {
        if (instance == null) {
            instance = new PreferenceManager(context);
            sharedPreferences = context.getSharedPreferences("info", Context.MODE_PRIVATE);
            editor = sharedPreferences.edit();
        }
        return instance;
    }

    public void cleanAllData(){
        sharedPreferences  = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }


    public void setIsEnglish(int isEnglish){
        //1- English
        //2- Herbrew
        sharedPreferences = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putInt("isEnglish",isEnglish).apply();
    }

    public Integer getIsEnglish(){
        sharedPreferences = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        return sharedPreferences.getInt("isEnglish",0);
    }

    public void setChooseLanguageAtStartUpDisplayed(boolean b){
        sharedPreferences = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("language_start",b).apply();
    }
    public boolean getChooseLanguageAtStartUpDisplayed(){
        sharedPreferences = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("language_start",false);
    }

    public void setCanShowRepositionPhotosDialog(boolean b){
        sharedPreferences = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("reposition_dialog",b).apply();
    }
    public boolean getCanShowRepositionPhotosDialog(){
        sharedPreferences = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("reposition_dialog",true);
    }


    public void setNotificationCount(boolean b){
        sharedPreferences = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("notification_count",b).apply();
    }
    public boolean getNotificationCount(){
        sharedPreferences = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("notification_count",true);
    }


    public void setCanShowSelectMultiplePhotosDialog(boolean b){
        sharedPreferences = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("multiple_dialog_image",b).apply();
    }
    public boolean getCanShowSelectMultiplePhotosDialog(){
        sharedPreferences = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("multiple_dialog_image",true);
    }

    public void setCurrentVersion(int  b){
        sharedPreferences = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("current_version",b).apply();
    }
    public Integer  getCurrentVersion(){
        sharedPreferences = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        return sharedPreferences.getInt("current_version",0);
    }



    public void setUserLoggedin(boolean b){
        System.out.println(">>>>>>>>>.LGON "+b);
        sharedPreferences = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("login",b).apply();
    }
    public boolean isUserLoggedIn(){
        sharedPreferences = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("login",false);
    }

    public void setUserLoggedOut(boolean b){
        sharedPreferences = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("isLogout",b).apply();
    }
    public boolean getUserLoggedOut(){
        sharedPreferences = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("isLogout",false);
    }

    public void setCameraTipsViewed(boolean b){
        sharedPreferences = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("cameratips",b).apply();
    }
    public boolean isCameraTipsViewed(){
        sharedPreferences = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("cameratips",false);
    }

    public void setUserDetails(Users.Data userDetails){
        sharedPreferences  = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(userDetails);
        editor.putString("users",json).apply();
    }
    public Users.Data getUserDetails(){
        sharedPreferences = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("users","");
        Users.Data address=  gson.fromJson(json, Users.Data.class);
        return address;
    }

    public void setDeletedNotifications(ArrayList<String> deletedNotification){
        sharedPreferences  = context.getSharedPreferences("deletedNotification", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(deletedNotification);
        editor.putString("deletedNotification",json).apply();
    }
    public ArrayList<String> getDeletedNotifications(){
        sharedPreferences = context.getSharedPreferences("deletedNotification", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("deletedNotification","");
        Type type = new TypeToken<ArrayList<String>>() {}.getType();
        return gson.fromJson(json, type);
    }

    public void setNotification(boolean b){
        sharedPreferences = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("notifications_enabled",b).apply();
    }
    public boolean getNotification(){
        sharedPreferences = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("notifications_enabled",true);
    }



    public void setRecentSearches(ArrayList<String> recentSearch){
        sharedPreferences  = context.getSharedPreferences("recentSearch", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(recentSearch);
        editor.putString("recentSearch",json).apply();
    }
    public ArrayList<String> getRecentSearches(){
        sharedPreferences = context.getSharedPreferences("recentSearch", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("recentSearch","");
        Type type = new TypeToken<ArrayList<String>>() {}.getType();
        return gson.fromJson(json, type);
    }

    public void setAdminMessgae(HashMap<String, Integer> jsonMap) {
        String jsonString = new Gson().toJson(jsonMap);
        sharedPreferences = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(BuildConfig.VERSION_NAME.toString(), jsonString);
        editor.apply();
    }
    public HashMap<String, Integer> getAdminMessage(){
        sharedPreferences = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        String defValue = new Gson().toJson(new HashMap<String, Integer>());
        String json=sharedPreferences.getString(BuildConfig.VERSION_NAME.toString(),defValue);
        TypeToken<HashMap<String,Integer>> token = new TypeToken<HashMap<String,Integer>>() {};
        HashMap<String,Integer> retrievedMap=new Gson().fromJson(json,token.getType());
        return retrievedMap;
    }

    public void setTipsCountPerChat(HashMap<String, Integer> jsonMap) {
        String jsonString = new Gson().toJson(jsonMap);
        sharedPreferences = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("tips_count", jsonString);
        editor.apply();
    }
    public HashMap<String, Integer> getTipsCountPerChat(){
        sharedPreferences = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        String defValue = new Gson().toJson(new HashMap<String, Integer>());
        String json=sharedPreferences.getString("tips_count",defValue);
        TypeToken<HashMap<String,Integer>> token = new TypeToken<HashMap<String,Integer>>() {};
        HashMap<String,Integer> retrievedMap=new Gson().fromJson(json,token.getType());
        return retrievedMap;
    }

    public void setSelectedCard(CardViewModel.Datum selectedCard){
        sharedPreferences  = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(selectedCard);
        editor.putString("selectedCard",json).apply();
    }
    public CardViewModel.Datum getSelectedCard(){
        sharedPreferences = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("selectedCard","");
        CardViewModel.Datum card=  gson.fromJson(json, CardViewModel.Datum.class);
        return card;
    }

    public void setDefaultPaymentType(int isEnglish){
        //0- Credit/Debit Card
        //1- Paypal
        //2- Veno
        sharedPreferences = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putInt("paymentType",isEnglish).apply();
    }

    public Integer getDefaultPaymentType(){
        sharedPreferences = context.getSharedPreferences("bingalo", Context.MODE_PRIVATE);
        return sharedPreferences.getInt("paymentType",-1);
    }

    public void saveDraft(ArrayList<PostProductDraftModel> postProductDraftModels){
        sharedPreferences  = context.getSharedPreferences("recentSearch", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(postProductDraftModels);
        editor.putString("postProductDraftModels",json).apply();
    }
    public ArrayList<PostProductDraftModel> getPostProductFromDraft(){
        sharedPreferences = context.getSharedPreferences("recentSearch", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("postProductDraftModels","");
        Type type = new TypeToken<ArrayList<PostProductDraftModel>>() {}.getType();
        return gson.fromJson(json, type);
    }

}
