package com.bingalollc.com.camerax

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.OrientationEventListener
import android.view.Surface
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.camerax.CameraConstant.*
import com.bingalollc.com.homeactivity.postproduct.PostProductDirectlyFragment
import com.bingalollc.com.preference.Common
import com.bingalollc.com.preference.PreferenceManager
import com.bingalollc.com.utils.Cache
import com.bingalollc.com.utils.PickerUtils
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.TransformationUtils.rotateImage
import com.kbeanie.multipicker.api.FilePicker
import com.kbeanie.multipicker.api.Picker
import com.kbeanie.multipicker.api.callbacks.FilePickerCallback
import com.kbeanie.multipicker.api.entity.ChosenFile
//import kotlinx.android.synthetic.main.fragment_crop_image.*
import okhttp3.MediaType
import okhttp3.RequestBody
import pub.devrel.easypermissions.EasyPermissions
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import kotlin.collections.ArrayList
import androidx.annotation.NonNull
import com.bingalollc.com.homeactivity.tips.TipsFrag
import com.bingalollc.com.utils.showDialogFragment
//import kotlinx.android.synthetic.main.activity_camera_post_product.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull


class CameraPostProduct : BaseActivity(), CameraXDisplayImage.SetOnClickMethod, FilePickerCallback,
    PostProductDirectlyFragment.OnPostClickedCalled, TipsFrag.OnDismissTipsDialog {
    private val REQUEST_READ_EXTERNAL_STORAGE_CHOOSE_IMG = 1000
    private var filePicker: FilePicker? = null
    private val PICK_IMAGE_REQUEST_GALLERY = 1002
    private val LOCATION_PERMISSION = 1000
    private var imageCapture: ImageCapture? = null
    private var curreBunCamer: Camera? = null
    private lateinit var outputDirectory: File
    private lateinit var cameraExecutor: ExecutorService
    private lateinit var camera_capture_button: ImageView
    private lateinit var upload_pic: ImageView
    private lateinit var flash_on_off: ImageView
    private lateinit var cam_rotate: ImageView
    private lateinit var crossBtn: ImageView
    private lateinit var layoutCamera: RelativeLayout
    private lateinit var viewFinder: PreviewView
    private var mArrayFile: java.util.ArrayList<File>? = ArrayList()
    private var cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA
    private var screen = ""
    private var currenCamer: Camera? = null
    private val permissionsGALLERY = arrayOf(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera_post_product)
      //  setRequestedOrientation(CameraLastOrientation);
//        onImageCaptured = this as OnImageCaptured


        initViews()
        importSwipeBack()
        if (preferenceManager?.isCameraTipsViewed == true) {
            preferenceManager?.isCameraTipsViewed = false
//            blurImg.visibility = View.VISIBLE
            val tipsFrag = showDialogFragment<TipsFrag>()
            tipsFrag?.setTipsDialogDismiss(this)
        } else {
          //Show Tips

        }


        if (intent != null) {
            screen = intent.getStringExtra("screen")!!
        }
        // Set up the listener for take photo button
        camera_capture_button.setOnClickListener {
            takePhoto()
        }
        camera_capture_button.isEnabled = false
        mArrayFile = ArrayList()
        mArrayFileConstant = ArrayList<File>()
        outputDirectory = getOutputDirectory()

        if(preferenceManager?.userDetails?.id.isNullOrEmpty().not()) {
            requestPermissionForCamera(this)
            if (checkPermissionForCamera(this)) {
                startCamera()
            } else {
                requestPermissionForCamera(this)
            }
        }




        upload_pic?.setOnClickListener {
            if (checkPermissionREAD_EXTERNAL_STORAGE(
                    this,
                    REQUEST_READ_EXTERNAL_STORAGE_CHOOSE_IMG
                )
            ) {
                choosephoto()
            }
        }
        flash_on_off?.setOnClickListener {
            try {
                if (currenCamer != null) {
                    if (currenCamer?.getCameraInfo()?.hasFlashUnit() ?: false) {
                        if (currenCamer?.cameraInfo?.torchState?.value == TorchState.OFF) {
                            currenCamer?.getCameraControl()?.enableTorch(true);
                            flash_on_off?.setImageDrawable(resources.getDrawable(R.drawable.ic_flash_off))
                        } else {
                            currenCamer?.getCameraControl()?.enableTorch(false);
                            flash_on_off?.setImageDrawable(resources.getDrawable(R.drawable.ic_flash_on))
                        }
                    }
                }
            } catch (e: Exception) {
            }
        }
        cam_rotate.setOnClickListener {
            if (cameraSelector == CameraSelector.DEFAULT_FRONT_CAMERA) {
                cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA
            } else {
                cameraSelector = CameraSelector.DEFAULT_FRONT_CAMERA
            }
            startCamera()
        }
        crossBtn?.setOnClickListener { onBackPressed() }
    }


    private fun initViews() {
        camera_capture_button = findViewById(R.id.camera_capture_button) as ImageView
        viewFinder = findViewById(R.id.viewFinder) as PreviewView
        upload_pic = findViewById(R.id.upload_pic) as ImageView
        flash_on_off = findViewById(R.id.flash_on_off) as ImageView
        cam_rotate = findViewById(R.id.cam_rotate) as ImageView
        crossBtn = findViewById(R.id.crossBtn) as ImageView
        layoutCamera = findViewById(R.id.layoutCamera) as RelativeLayout
    }

    private fun choosephoto() {
        if (EasyPermissions.hasPermissions(this@CameraPostProduct, *permissionsGALLERY)) {
            pickFileSingle("image")
        } else {
            EasyPermissions.requestPermissions(
                this@CameraPostProduct, resources.getString(R.string.please_allow_permissions),
                1003, *permissionsGALLERY
            )
        }
    }

    private fun pickFileSingle(type: String) {
        filePicker = getFilePicker()
        filePicker?.setMimeType("image/*")
        filePicker?.pickFile()
    }

    private fun getFilePicker(): FilePicker? {
        filePicker = FilePicker(this)
        filePicker!!.setFilePickerCallback(this@CameraPostProduct)
        filePicker!!.setCacheLocation(PickerUtils.getSavedCacheLocation(this))
        return filePicker
    }

    @SuppressLint("UnsafeOptInUsageError")
    private fun startCamera() {
        cameraExecutor = Executors.newSingleThreadExecutor()
        val cameraProviderFuture = ProcessCameraProvider.getInstance(this)
        cameraProviderFuture.addListener(Runnable {
            // Used to bind the lifecycle of cameras to the lifecycle owner
            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()

            // Preview
            val preview = Preview.Builder()
                .build()
                .also {
                    it.setSurfaceProvider(viewFinder.surfaceProvider)
                }
            preview.targetRotation = Surface.ROTATION_0
            //  preview.targetRotation = Surface.ROTATION_180
            //  preview.setSurfaceProvider { viewFinder.surfaceProvider }
            imageCapture = ImageCapture.Builder()
                .build()

            val orientationEventListener = object : OrientationEventListener(this as Context) {
                override fun onOrientationChanged(orientation: Int) {
                    // Monitors orientation values to determine the target rotation value
                    val rotation: Int = when (orientation) {
                        in 45..134 -> Surface.ROTATION_0
                        in 135..224 -> Surface.ROTATION_0
                        in 225..314 -> Surface.ROTATION_0
                        else -> Surface.ROTATION_0
                    }

                    imageCapture?.targetRotation = rotation
                    preview.targetRotation = Surface.ROTATION_0
                }
            }

            orientationEventListener.enable()

            // Select back camera as a default
//            val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA

            try {
                // Unbind use cases before rebinding
                cameraProvider.unbindAll()

                // Bind use cases to camera
                currenCamer = cameraProvider.bindToLifecycle(
                    this, cameraSelector, preview, imageCapture
                )
                camera_capture_button.isEnabled = true

            } catch (exc: Exception) {
                Log.e("TAG", "Use case binding failed", exc)
            }
        }, ContextCompat.getMainExecutor(this))
        currenCamer?.getCameraControl()?.enableTorch(false);
        flash_on_off?.setImageDrawable(resources.getDrawable(R.drawable.ic_flash_on))
    }

    private fun takePhoto() {
        // Get a stable reference of the modifiable image capture use case
        val imageCapture = imageCapture ?: return
        // Create time-stamped output file to hold the image
        val photoFile = File(
            outputDirectory,
            SimpleDateFormat(
                "dd-mm-yyyy-hh-mm-ss", Locale.US
            ).format(System.currentTimeMillis()) + ".jpg"
        )

        // Create output options object which contains file + metadata
        val outputOptions = ImageCapture.OutputFileOptions.Builder(photoFile).build()

        // Set up image capture listener, which is triggered after photo has
        // been taken
        imageCapture.takePicture(
            outputOptions,
            ContextCompat.getMainExecutor(this),
            object : ImageCapture.OnImageSavedCallback, CameraXDisplayImage.SetOnClickMethod,
                PostProductDirectlyFragment.OnPostClickedCalled {
                override fun onError(exc: ImageCaptureException) {
                    Log.e("TAG", "Photo capture failed: ${exc.message}", exc)
                }

                override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                  //  val bitmap = rotateImageIfRequired(BitmapFactory.decodeFile(photoFile.path), Uri.fromFile(photoFile));
                 //   val newphotoFile = compressBitmap(this@CameraPostProduct, bitmap)
                    val newphotoFile = getCompressed(this@CameraPostProduct, photoFile.path)
                    val savedUri = Uri.fromFile(newphotoFile)

                  //  rotateImageIfRequired(BitmapFactory.decodeFile(newphotoFile.path), savedUri)
                    uriOfClickedImage = savedUri
                    mArrayFile = ArrayList()
                    mArrayFileConstant = ArrayList<File>()
                    mArrayFile?.add(newphotoFile)
                    mArrayFileConstant?.add(newphotoFile)
                    launchDisplayImage()
                    // CameraXDisplayImage.show(this@CameraPostProduct,savedUri,this)
                    cameraExecutor.shutdown()
                    val msg = "Photo capture succeeded: $savedUri"
                    //Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
                    Log.d("TAG", msg)
                }

                override fun onOPostBtnClicked(isPost: Boolean) {
                    if (!isPost) {
                        startCamera()
                    } else {
                        showLoading(this@CameraPostProduct)
                        PostProductDirectlyFragment.show(
                            this@CameraPostProduct,
                            uriOfClickedImage!!,
                            mArrayFile!!,
                            this
                        )
                        hideKeyboard(this@CameraPostProduct)
                    }
                }

                override fun onFinishCalled() {
                    onBackPressed()
                }

                override fun onSaveClicked(isPost: Boolean) {

                }
            })
    }
    @Throws(IOException::class)
    private fun rotateImageIfRequired(img: Bitmap, selectedImage: Uri): Bitmap? {
        val ei = ExifInterface(selectedImage.path ?: "")
        val orientation: Int =
            ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
        return when (cameraSelector) {
            CameraSelector.DEFAULT_FRONT_CAMERA -> rotateImage(img, 270)
            CameraSelector.DEFAULT_BACK_CAMERA -> rotateImage(img, 90)
            else -> img
        }
    }

    private fun saveImage(img: Bitmap): File {
        val f3 = File(getCacheDir().path+"imagess")
        if (!f3.exists()) f3.mkdirs()
        var outStream: OutputStream? = null
        val file: File = File(
            getCacheDir().path + "/imagess/" + "seconds" + ".png"
        )
        try {
            outStream = FileOutputStream(file)
            rotateImage(img, 90).compress(Bitmap.CompressFormat.PNG, 85, outStream)
            outStream.close()
            Toast.makeText(applicationContext, "Saved", Toast.LENGTH_LONG).show()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return file
    }
    private fun getOutputDirectory(): File {
        val mediaDir = externalMediaDirs.firstOrNull()?.let {
            File(it, resources.getString(R.string.app_name)).apply { mkdirs() }
        }
        return if (mediaDir != null && mediaDir.exists())
            mediaDir else filesDir
    }

    override fun onDestroy() {
        super.onDestroy()

        cameraExecutor.shutdown()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            Common.PermissionConstant.PERMISSION_REQUEST_CODE_CAMERA_MODE -> if (grantResults.isNotEmpty()) {
                val cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED
                if (cameraAccepted) {
                    try {
                        startCamera()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                            showMessageOKCancel(getString(R.string.grant_camera_permission),
                                DialogInterface.OnClickListener { _: DialogInterface?, _: Int ->
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermissions(
                                            arrayOf(Manifest.permission.CAMERA),
                                            Common.PermissionConstant.PERMISSION_REQUEST_CODE_CAMERA_MODE
                                        )
                                    }
                                })
                            return
                        }
                    }
                }
            }
            REQUEST_READ_EXTERNAL_STORAGE_CHOOSE_IMG -> if (grantResults.isNotEmpty()) {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    choosephoto()
                }
            }
            else -> Log.e("TAG", "Default mode")
        }
    }

    private fun showMessageOKCancel(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(this)
            .setMessage(message)
            .setPositiveButton(getString(R.string.ok), okListener)
            .setNegativeButton(getString(R.string.cancel), null)
            .create()
            .show()
    }

    fun checkPermissionREAD_EXTERNAL_STORAGE(
        context: Context?, requestCode: Int
    ): Boolean {
        val currentAPIVersion = Build.VERSION.SDK_INT
        return if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this!!,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        (context as Activity?)!!,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )
                ) {
                    //Toast.makeText(PostAdPage.this, "Do itititi", Toast.LENGTH_SHORT).show();
                    showDialog(
                        getString(R.string.external_storage), context,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE, requestCode
                    )
                } else {
                    requestPermissions(
                        arrayOf(
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        ),
                        requestCode
                    )
                }
                false
            } else {
                true
            }
        } else {
            true
        }
    }

    fun showDialog(
        msg: String, context: Context?,
        permission: String, statusCode: Int
    ) {
        val alertBuilder = android.app.AlertDialog.Builder(context)
        alertBuilder.setCancelable(false)
        alertBuilder.setTitle("Permission necessary")
        alertBuilder.setMessage("$msg permission is necessary")
        alertBuilder.setPositiveButton(
            android.R.string.yes
        ) { dialog, which ->
            requestPermissions(
                arrayOf(permission),
                statusCode
            )
        }
        val alert = alertBuilder.create()
        alert.show()
    }


    private fun launchDisplayImage() {
        /* setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        CameraLastOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT*/
        if (screen != null && screen.equals("signup")) {
            onBackPressed()
        } else {
            CameraXDisplayImage.show(this@CameraPostProduct, uriOfClickedImage!!, this)
        }

    }

    @SuppressLint("Range")
    fun getPath(uri: Uri?): String? {
        var cursor: Cursor = this.getContentResolver().query(uri!!, null, null, null, null)!!
        cursor.moveToFirst()
        var document_id = cursor.getString(0)
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1)
        cursor.close()
        cursor = this.getContentResolver().query(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            null, MediaStore.Images.Media._ID + " = ? ", arrayOf(document_id), null
        )!!
        cursor.moveToFirst()
        val path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA))
        cursor.close()
        return path
    }

    fun getRealPathFromURI(uri: Uri?): String? {
        var path = ""
        if (getContentResolver() != null) {
            val cursor: Cursor? = getContentResolver().query(uri!!, null, null, null, null)
            if (cursor != null) {
                cursor.moveToFirst()
                val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                path = cursor.getString(idx)
                cursor.close()
            }
        }
        return path
    }


    override fun onOPostBtnClicked(isPost: Boolean) {
        if (!isPost) {
            startCamera()
        } else {
            PostProductDirectlyFragment.show(
                this@CameraPostProduct,
                uriOfClickedImage!!,
                mArrayFile!!,
                this
            )
        }
    }

    override fun onFinishCalled() {
        onBackPressed()
    }

    override fun onSaveClicked(isPost: Boolean) {

    }


    override fun onBackPressed() {
        super.onBackPressed()
        uriOfClickedImage = null
        //CameraLastOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
    }

    override fun onError(p0: String?) {

    }

    override fun onFilesChosen(list: MutableList<ChosenFile>?) {
        if (list?.size ?: 0 > 0) {
            mArrayFile?.clear()
            mArrayFileConstant?.clear()
            try {
                val file1 = File(list?.get(0)?.getOriginalPath())
                val file = getCompressed(this, file1.path)
                val mFile = RequestBody.create("image/*".toMediaTypeOrNull(), file)
                uriOfClickedImage = Uri.fromFile(file)
                mArrayFile?.add(file)
                mArrayFileConstant?.add(file)
                launchDisplayImage()
                cameraExecutor.shutdown()

            } catch (e: java.lang.Exception) {
                e.message
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Picker.PICK_FILE && resultCode == RESULT_OK) {
            filePicker!!.submit(data)
        }
    }

    override fun onTipsDialogDismissed() {
//        blurImg.visibility = View.GONE
    }
}