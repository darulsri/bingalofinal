package com.bingalollc.com.camerax

import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import com.bingalollc.com.R
import com.bingalollc.com.camerax.CameraConstant.uriOfClickedImage
import com.bingalollc.com.homeactivity.fragment.FilterFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.signature.ObjectKey
import java.lang.Exception


class CameraXDisplayImage : DialogFragment() {
    private var imageView:ImageView?=null
    private var crossBtn:LinearLayout?=null
    private var retakeText:TextView?=null
    private var postImage:TextView?=null
    private var loadingText:TextView?=null
    private var setOnClickMethod: SetOnClickMethod? = null
    private var imageUrl = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.FullScreenDialogStyle)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view=inflater.inflate(R.layout.fragment_camera_x_display_image, container, false)
        initView(view)
        initClicks()
        initValue()
        return view
    }

    private fun initValue() {

        if(imageUrl!=null && !imageUrl.equals("")){
            postImage?.visibility = View.GONE
            retakeText?.visibility = View.GONE
            Glide.with(requireContext()).load(imageUrl) .diskCacheStrategy(DiskCacheStrategy.NONE)
                .listener(object : RequestListener<Drawable?> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any,
                        target: Target<Drawable?>,
                        isFirstResource: Boolean
                    ): Boolean {
                        loadingText?.text = getString(R.string.no_image)
                        return true
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any,
                        target: Target<Drawable?>,
                        dataSource: DataSource,
                        isFirstResource: Boolean
                    ): Boolean {
                        return false
                    }
                })
                .skipMemoryCache(true).into(imageView!!)
        }else{
            postImage?.visibility = View.VISIBLE
            retakeText?.visibility = View.VISIBLE
            loadingText?.visibility = View.GONE
            Glide.with(requireContext()).load(uriOfClickedImage).diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true).into(imageView!!)
        }
    }

    private fun initClicks() {
        postImage?.setOnClickListener {
            setOnClickMethod?.onOPostBtnClicked(true)
        }
        retakeText?.setOnClickListener {
            setOnClickMethod?.onOPostBtnClicked(false)
            dismiss() }
        crossBtn?.setOnClickListener {
            setOnClickMethod?.onFinishCalled()
            dismiss()
        }
    }

    private fun initView(view: View) {
        imageView = view.findViewById(R.id.imageView)
        crossBtn = view.findViewById(R.id.crossBtn)
        retakeText = view.findViewById(R.id.retakeText)
        postImage = view.findViewById(R.id.postImage)
        loadingText = view.findViewById(R.id.loadingText)
    }

    override fun onStart() {
        super.onStart()

        val decorView = getDialog()?.getWindow()?.getDecorView();

        decorView?.animate()?.setStartDelay(300)?.setDuration(300)?.start() //.translationY((-100.0).toFloat())?
    }

    companion object {
        @JvmStatic
        fun show(context: FragmentActivity,savedUri:Uri, setOnClickMethod: SetOnClickMethod) {
            val fragment = CameraXDisplayImage()
            fragment.setOnClickMethod = setOnClickMethod
            val bundle = Bundle()
            fragment.arguments = bundle
            val transaction = context.supportFragmentManager.beginTransaction()
            fragment.show(transaction, "dialog")
        }
        fun fullImageDisplay(context: FragmentActivity, imageUrl : String){
            val fragment = CameraXDisplayImage()
            fragment.imageUrl = imageUrl
            val bundle = Bundle()
            fragment.arguments = bundle
            val transaction = context.supportFragmentManager.beginTransaction()
            fragment.show(transaction, "dialog")
        }
    }


    interface SetOnClickMethod {
        fun onOPostBtnClicked(isPost: Boolean)
        fun onFinishCalled()
    }
}