package com.bingalollc.com.cart

data class addressmodel(
    var message :String,
    var status :String,
    var data :List<address>
)
data class address(
    var id: Int,
    var address_line1 :String,
    var address_line2:String,
    var city :String,
    var state:String,
    var created_at :String,
    var updated_at:String,
    var user_id : String

)
