package com.bingalollc.com.cart

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bingalollc.com.R

class useraddressadapter (var context: Context,var getAddress :List<address>):RecyclerView.Adapter<useraddressadapter.MyViewHolder>(){

    var onItemClick : ((address) -> Unit)? = null



    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.addresslist,parent,false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
         var setAddress = getAddress[position]

        holder.Address_1.text = setAddress.address_line1
        holder.Address_2.text = setAddress.address_line2
        holder.city.text =setAddress.city+","+setAddress.state

        holder.itemView.setOnClickListener {
            onItemClick?.invoke(setAddress)

        }
    }

    override fun getItemCount(): Int {
        return getAddress.size
    }
    fun updateData(getaddress: List<address>?) {
        getAddress = getaddress!!
        notifyDataSetChanged()

    }

    class MyViewHolder(view: View):RecyclerView.ViewHolder (view){
        val Address_1 :TextView = view.findViewById(R.id.address_line1)
        val Address_2 :TextView = view.findViewById(R.id.address_line2)
        val city : TextView = view.findViewById(R.id.city)

    }

//    interface onItemClickLis{
//        fun onClick(position: Int, product: Product)
//    }


}