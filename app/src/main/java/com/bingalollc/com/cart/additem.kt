package com.bingalollc.com.cart


data class additem(
    val message: String,
    val status: Int,
    val data: data,

    )

data class data(
    val products: List<Product_1>,
    val cart_total : Float,
    val cart_quantity : Int,
    )

data class Product_1(
    val id: String,
    val store_id: String,
    val product_name: String,
    val product_price: String,
    val product_detail: String,
    val product_thumb_image: String,
    val created_at: String,
    val updated_at: String,
    val position: String,
    val quantity:Int,
    val cart_price :String,
)