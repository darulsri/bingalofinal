package com.bingalollc.com.cart

data class storeDeliverymodel(
    var message :String,
    var status :String,
    var data :List<data_1>
)
data class data_1(
    var id: Int,
    var store_id :String,
    var type:String,
    var delievery_eta :String,
    var price:Int,
    var created :String,
    var modified:String,
    var description : String

)
