package com.bingalollc.com.cart

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bingalollc.com.R
import com.bumptech.glide.Glide
import com.chauthai.swipereveallayout.SwipeRevealLayout
import com.chauthai.swipereveallayout.ViewBinderHelper

class usercartAdapter(
    var context: Context,
    var userpro:MutableList<Product_1>
):RecyclerView.Adapter<usercartAdapter.MyViewHolder> (){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.orderproduct,parent,false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return userpro.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
//        var orderproduct = usercart[position]
        var orderpro = userpro[position]
        holder.productTitle.text = orderpro.product_name
        holder.productQTY.text = orderpro.quantity.toString()
        Glide.with(context).load(orderpro.product_thumb_image).placeholder(R.drawable.download).
        error(R.drawable.download).into(holder.productimage)


    }


    fun updateData(newDelivery: MutableList<Product_1>?) {
        userpro = (newDelivery)!!
        notifyDataSetChanged()
    }

    class MyViewHolder(view :View) :RecyclerView.ViewHolder(view){
        val productTitle: TextView = view.findViewById(R.id.productTitle)
        val productQTY: TextView = view.findViewById(R.id.qty)
        val productimage:ImageView = view.findViewById(R.id.product_Icon)
//        val layoutdelete:LinearLayout = view.findViewById(R.id.layoutdelete)
//        val swiplayout : SwipeRevealLayout = view.findViewById(R.id.swipelayout)

    }


    fun getItem(position: Int): Product_1 {
        return userpro[position]
    }

    fun removeItem(position: Int){
        userpro.removeAt(position)
        notifyItemRemoved(position)
    }


}