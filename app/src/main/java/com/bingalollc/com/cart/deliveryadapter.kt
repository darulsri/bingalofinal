package com.bingalollc.com.cart

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bingalollc.com.R

class deliveryadapter(
    var context: Context,
    var deliverynew:List<data_1>
):
    RecyclerView.Adapter<deliveryadapter.MyViewHolder>() {

    var onItemClick : ((data_1) -> Unit)? = null
    var delivery_1 :Int =  -1


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
      val view = LayoutInflater.from(parent.context).inflate(R.layout.deliverymethod,parent,false)
   return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
    return deliverynew.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var delivery = deliverynew[position]


        holder.deliveryTitle.text = delivery.description
        holder.deliverydescr.text ="Delivery ETA "+delivery.delievery_eta
        holder.rate.text = delivery.price.toString()

        holder.backgroun.setOnClickListener {
            onItemClick?.invoke(delivery)

            delivery_1 = position
            notifyDataSetChanged()


        }

        if (delivery_1 == position){
//            holder.backgroun.setBackgroundColor(Color.BLUE)
            holder.deliveryselect.setBackgroundResource(R.drawable.color_background)

        }else{
//            holder.backgroun.setBackgroundColor(Color.WHITE)
            holder.deliveryselect.setBackgroundResource(R.drawable.backround)

        }

    }

    fun updateData(newDelivery: List<data_1>?) {
        deliverynew = newDelivery!!
        notifyDataSetChanged()
    }

    class MyViewHolder(view: View) :RecyclerView.ViewHolder(view) {
        val deliveryTitle: TextView = view.findViewById(R.id.deliverTitle)
        val deliverydescr: TextView = view.findViewById(R.id.deliverhour)
        val rate: TextView = view.findViewById(R.id.price)
        val backgroun:RelativeLayout = view.findViewById(R.id.deliveryback)
        var deliveryselect : LinearLayout = view.findViewById(R.id.deliveryselect)



    }
    }


