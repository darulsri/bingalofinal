package com.bingalollc.com.cart

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.Gravity
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bingalollc.com.R
import com.bingalollc.com.model.ApiStatusModel
import com.bingalollc.com.network.RestClient
import com.bingalollc.com.payment.paymentActivity
import com.bingalollc.com.preference.PreferenceManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class confirmproduct : AppCompatActivity() {

    var ordersummary : RecyclerView?=null
    var usercartAdapter :usercartAdapter?=null
    var ordernew : MutableList<Product_1>?=null
    var linearLayoutManager: LinearLayoutManager?=null
    var preferenceManager: PreferenceManager? =null
    var description : TextView?=null
    var delivery_rate : TextView?=null
    var Total : TextView?=null
    lateinit var addres: LinearLayout
    var useraddressadapter :useraddressadapter?=null
    var getaddress : List<address>?=null
    var tap : TextView?=null
    lateinit var Next : Button
    var Product_Cart = ArrayList<Product_1>()
    var id : Int? = null
    var deliveryId :Int ? = null
    var Total_Sum :Int ?= null


    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirmproduct)
        ordersummary = findViewById(R.id.ordersummary)
        description = findViewById(R.id.delivermodel)
        delivery_rate = findViewById(R.id.price_rate)
        Total = findViewById(R.id.Total_sum)
        addres = findViewById(R.id.address)
        tap = findViewById(R.id.tap)
        Next = findViewById(R.id.Next)


        preferenceManager = PreferenceManager(this)



//        var total = rate+9
//        Total?.text = total.toString()
        ordersummary?.layoutManager = LinearLayoutManager(this)
        ordernew = ArrayList()
        usercartAdapter = this.let { usercartAdapter(it,ordernew as ArrayList<Product_1>) }
        ordersummary?.adapter =usercartAdapter


        getUserCart()

        val itemTouchHelper = ItemTouchHelper(itemTouchCallback)
        itemTouchHelper.attachToRecyclerView(ordersummary)

//        var deliveryId = intent.getStringExtra("id")
//        var Total_Sum = Total?.text.toString()


        addres.setOnClickListener {
            bottomsheet()
        }

        Next.setOnClickListener {

            setOnCLick()

        }

    }

    private val itemTouchCallback = object :ItemTouchHelper.SimpleCallback
        (0,ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT){
        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ): Boolean {
            return false
        }

        @SuppressLint("SuspiciousIndentation")
        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            val position = viewHolder.adapterPosition
            val item = usercartAdapter?.getItem(position)
                getdeleteItemFromCart(item!!)
            usercartAdapter?.removeItem(position)
            Log.e("Product id", item.id)

        }


    }



    private fun getdeleteItemFromCart(item : Product_1) {
        val P_Id: String? = item?.id
        Log.e("P_Id", P_Id.toString())
        RestClient.getApiInterface().getdeleteItemFromCart(preferenceManager?.userDetails?.token,item?.id,preferenceManager?.userDetails?.id)
            .enqueue(object : Callback<additem>{
            override fun onResponse(call: Call<additem>, response: Response<additem>) {
                if (response.isSuccessful)
                {
                    val ordersummary =response.body()
                    if (ordersummary!=null){
                        val Product = ordersummary?.data
//                         Product_Cart = Product?.products as ArrayList<Product_1>
//                        usercartAdapter?.updateData(ordernew as MutableList<Product_1>?)
//
//                        var cartTotal = Product?.cart_total
//
//                        var desc = intent.getStringExtra("desc")
//                        var rate = intent.getIntExtra("rate",-1)
//
//                        description?.text = desc
//                        delivery_rate?.text = rate.toString()
//
//                        var total = rate +9 + cartTotal!!
//                        Total?.text = total.toString()
//
//                        Log.e("cartTotal",cartTotal.toString())

                    }
                    Log.e("Successful_deleted_cart_item", ordersummary?.message.toString())


                }            }

            override fun onFailure(call: Call<additem>, t: Throwable) {
                Log.e("Successful", t.message.toString())
            }

        })

    }

    private fun getUserCart(){
        RestClient.getApiInterface().getUserCart(preferenceManager?.userDetails?.token,
            preferenceManager?.userDetails?.id).enqueue(object : Callback<additem> {
            override fun onResponse(call: Call<additem>, response: Response<additem>) {
                if (response.isSuccessful)
                {
                    val ordersummary =response.body()
                    if (ordersummary!=null){
                        val Product = ordersummary?.data
                        val ordernew = Product?.products
                        usercartAdapter?.updateData(ordernew as MutableList<Product_1>?)
//                        Product_Cart = Product?.products as ArrayList<Product_1>


                        var cartTotal = Product?.cart_total

                        deliveryId = intent.getIntExtra("id",-1)
                        var desc = intent.getStringExtra("desc")
                        var rate = intent.getIntExtra("rate",-1)

                        description?.text = desc
                        delivery_rate?.text = rate.toString()

                        var total = rate +9 + cartTotal!!
                        Total?.text = total.toString()
                        Total_Sum = total.toInt()

                        Log.e("cartTotal",cartTotal.toString())

                    }
//                    Log.e("Succesful", ordersummary?.message.toString())


                }
            }

            override fun onFailure(call: Call<additem>, t: Throwable) {
                Log.e("Succesful", t.message.toString())
            }

        })
    }

    private fun AddAddress(address_line1: Editable, address_line2: Editable, city: Editable, state: Editable){
        RestClient.getApiInterface().addStoreUserAddress(preferenceManager?.userDetails?.token,preferenceManager?.userDetails?.id,
            address_line1.toString(),
            address_line2.toString(),
            city.toString(),
            state.toString()
        ).enqueue(object : Callback<ApiStatusModel> {
            override fun onResponse(
                call: Call<ApiStatusModel>,
                response: Response<ApiStatusModel>
            ) {
                if (response.isSuccessful){
                    Log.e("Successfull",response.body()?.getMessage().toString())
                }
            }

            override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
                Log.e("Error",t.message.toString())
            }

        })

    }

    private fun Addresslist(){

        RestClient.getApiInterface().GetUserAddress(preferenceManager?.userDetails?.token,
            preferenceManager?.userDetails?.id).enqueue(object : Callback<addressmodel> {
            override fun onResponse(call: Call<addressmodel>, response: Response<addressmodel>) {

                if (response.isSuccessful){
                    var addressmo = response.body()
                    if (addressmo != null){
                        val getaddress = addressmo?.data
                        useraddressadapter?.updateData(getaddress)

                    }
                }

            }

            override fun onFailure(call: Call<addressmodel>, t: Throwable) {
                TODO("Not yet implemented")
            }

        })

    }

    @SuppressLint("SetTextI18n")
    private fun bottomsheet(){

        var dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.selectaddress)
        dialog.show()

        var addresspop:RecyclerView = dialog.findViewById(R.id.addresslist)
        var Addaddress: TextView = dialog.findViewById(R.id.add_address)
        var linearLayoutManager:LinearLayoutManager
        var getaddress : List<address>

        Addresslist()

        Addaddress.setOnClickListener {
            bottomsheetAddAddress()

        }

        addresspop?.layoutManager = LinearLayoutManager(this)
        getaddress=ArrayList()
        useraddressadapter = this.let { useraddressadapter(it,getaddress as ArrayList<address>) }
        addresspop?.adapter = useraddressadapter

        useraddressadapter?.onItemClick={

            id = it.id

            if(it.id != null) {
                tap?.text =
                    it.address_line1 + System.getProperty("line.separator") + it.address_line2 + System.getProperty(
                        "line.separator"
                    ) +
                            it.city + "," + it.state
            }
            dialog.dismiss()
        }





        dialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window?.setGravity(Gravity.BOTTOM)
    }

    private fun setOnCLick(){
        var intent = Intent(this , paymentActivity::class.java)
        intent.putExtra("address_id",id)
        intent.putExtra("delivery_id",deliveryId)
        intent.putExtra("total",Total_Sum)
        startActivity(intent)
        Log.e("data",id.toString()+","+deliveryId+","+Total_Sum)
    }


    private fun bottomsheetAddAddress(){

        var dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.addaddresspopup)
        dialog.show()
//        var Address_line1 : EditText
//        var Address_line2 :EditText
//        var city1:EditText
//        var state1:EditText
        var Done : Button

        val Address_line1=dialog.findViewById<EditText>(R.id.address_1)
        val Address_line2 = dialog.findViewById<EditText>(R.id.address_2)
        val city1 = dialog.findViewById<EditText>(R.id.city)
        val state1  = dialog.findViewById<EditText>(R.id.state)
        Done  = dialog.findViewById(R.id.done)


        val address_line1 = Address_line1.text.toString()
        val address_line2 = Address_line2.text.toString()
        val city = city1.text.toString()
        val state = state1.text.toString()
        Done.setOnClickListener {
            AddAddress(Address_line1.text,Address_line2.text,city1.text,state1.text)
            Toast.makeText(this, "data"+Address_line1.text, Toast.LENGTH_SHORT).show()

        }

        dialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window?.setGravity(Gravity.BOTTOM)
    }




}