package com.bingalollc.com

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.homeactivity.HomeActivity
//import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivityTwo : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)


        showNormalDialog(
            this,
            getString(R.string.language),
            getString(R.string.please_choose_language),
            getString(R.string.english),
            getString(R.string.hebrew),
            BaseActivity.OnOkClicked {
                //showToast("languageChoosed", this)
                if (!it) {
                    chooseLanguage(true)
                    preferenceManager?.isEnglish = 1
                  //  centerLogo.setImageDrawable(resources.getDrawable(R.drawable.launch_logo))
                } else {
                    chooseLanguage(false)
                    preferenceManager?.isEnglish = 2
                 //   centerLogo.setImageDrawable(resources.getDrawable(R.drawable.launch_logo_herbew))
                }

                preferenceManager.currentVersion = BuildConfig.VERSION_CODE
                preferenceManager.chooseLanguageAtStartUpDisplayed = true
                startActivity(Intent(this@SplashActivityTwo, HomeActivity::class.java))
                finish()
            })
    }
}