package com.bingalollc.com.google_analytics

class AnalyticsEvent {

    enum class EventAction(val action: String) {
        LOGIN_TYPE("LOGIN"),
        PRODUCT_CLICK("PRODUCT_CLICK"),
        SPLASH_SCREEN("SPLASH_SCREEN_OPENED")
    }

    enum class LogInType(val logInType : Int){
        TWITTER(2),
        GOOGLE(1),
        FACEBOOK(0)
    }

    enum class EventType(val type: String) {
        CLICK("CLICK"),
        SCROLL("SCROLL"),
        SPLASH("SPLASH"),
        GOOGLE_LOGIN("GOOGLE_LOGIN"),
        FACEBOOK_LOGIN("FACEBOOK_LOGIN"),
        TWITTER_LOGIN("TWITTER_LOGIN")
    }

}