package com.bingalollc.com.google_analytics

import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase

class GoogleAnalytics {

    companion object {

        private var firebaseAnalytics = Firebase.analytics

        fun sentDataToGoogleAnalytics(id : String, name : String, content_type: String){
            val bundle = Bundle()
            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, id)
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, name)
            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, content_type)
            firebaseAnalytics.logEvent(
                FirebaseAnalytics.Event.SELECT_CONTENT,
                bundle
            )
        }
    }




}