package com.bingalollc.com

interface UserClickCallbacks {
    fun onUserClick(position: Int)
}