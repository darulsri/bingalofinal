package com.bingalollc.com;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.util.Log;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.bingalollc.com.notification.OneSignalNotificationReceiver;
import com.bingalollc.com.preference.Common;
import com.bingalollc.com.preference.PreferenceManager;
import com.facebook.FacebookSdk;
import com.facebook.LoggingBehavior;
import com.onesignal.OSNotificationReceivedEvent;
import com.onesignal.OneSignal;

//import dagger.hilt.android.HiltAndroidApp;
//import com.squareup.leakcanary.LeakCanary;

/**
 * Created by bitmarvels3 on 14/6/16.
 */
//@HiltAndroidApp
public class AppController extends MultiDexApplication implements OneSignal.OSRemoteNotificationReceivedHandler {

    //public static final String TAG = AppController.class.getSimpleName()
    public static final String TAG = "BINGALO";
    public static final String ONESIGNAL_APP_ID = "0e260056-a2e7-4f4d-a294-f2bd9f77cccf";
    public Context mContext;
    public  boolean haveCalled = true;


    private static AppController mInstance;
  //  public static SharedPreferences sharedPreferences;
    Thread thread;
    public  PreferenceManager preferenceManager;



    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        mContext = base;

        MultiDex.install(this);
        preferenceManager = PreferenceManager.getInstance(base);
        mInstance = this;
//        thread = new Thread(runnable);
//        thread.start();
        startTimer();

    }


    //Declare timer
    CountDownTimer cTimer = null;

    //start timer function
    void startTimer() {
        cTimer = new CountDownTimer(1000, 1000) {
            public void onTick(long millisUntilFinished) {

            }
            public void onFinish() {
                Log.d("hello", "hello after 10 sec");
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());


                OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);
                OneSignal.initWithContext(AppController.this);
                OneSignal.setAppId(Common.ONESIGNAL_APP_ID);
                OneSignal.promptForPushNotifications();
                OneSignal.setNotificationWillShowInForegroundHandler(osNotificationReceivedEvent -> System.out.println(">>>>>>>>>>>>>>>>>>FOREGROUNDDDDD"));
   /*     OneSignal.setNotificationReceivedHandler(new OneSignalNotificationReceiver(mInstance))
                .init();*/

                OneSignal.setNotificationOpenedHandler(new OneSignalNotificationReceiver(mContext));

                cancelTimer();
            }
        };
        cTimer.start();
    }


    //cancel timer
    void cancelTimer() {
        if(cTimer!=null)
            cTimer.cancel();
    }

    Runnable runnable = new Runnable(){
        public void run() {
            //some code here
       //     sharedPreferences = getSharedPreferences("contact", MODE_PRIVATE);


 /*       CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts /RobotoSlab_Bold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());*/
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());


            OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);
            OneSignal.initWithContext(AppController.this);
            OneSignal.setAppId(Common.ONESIGNAL_APP_ID);

            OneSignal.promptForPushNotifications();
            OneSignal.setNotificationWillShowInForegroundHandler(osNotificationReceivedEvent -> System.out.println(">>>>>>>>>>>>>>>>>>FOREGROUNDDDDD"));
   /*     OneSignal.setNotificationReceivedHandler(new OneSignalNotificationReceiver(mInstance))
                .init();*/

            OneSignal.setNotificationOpenedHandler(new OneSignalNotificationReceiver(mContext));
            //OneSignal.setAppId(ONESIGNAL_APP_ID);
        }
    };

    public static synchronized AppController getInstance() {
        return mInstance;
    }


    @Override
    public void remoteNotificationReceived(Context context, OSNotificationReceivedEvent osNotificationReceivedEvent) {
        mContext = context;
    }
}