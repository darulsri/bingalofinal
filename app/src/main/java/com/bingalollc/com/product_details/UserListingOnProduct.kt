package com.bingalollc.com.product_details

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.homeactivity.HomeActivity
import com.bingalollc.com.model.ApiStatusModel
import com.bingalollc.com.network.RestClient
import com.bingalollc.com.preference.Common
import com.bingalollc.com.preference.PreferenceManager
import com.bingalollc.com.product_details.adapter.UserListingAdapter
import com.bingalollc.com.users.UsersArrayModel
import com.google.firebase.firestore.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class UserListingOnProduct : AppCompatActivity(), UserListingAdapter.OnNameClicked {
    private var usersData = java.util.ArrayList<UsersArrayModel.Data>()
    private var header: TextView? = null
    private var viewBase: View? = null
    private var back_icon: ImageView? = null
    private var selectBtn: TextView? = null
    private var mDb: FirebaseFirestore? = null
    private var preferenceManager:PreferenceManager? =null
    private var baseActivity: BaseActivity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_listing_on_product)
        baseActivity = BaseActivity()
        initViews()
        getDataFromFirebase()
    }

    private fun getDataFromFirebase() {
        var allusersList = ArrayList<String>()
        val chatroomsCollection = mDb?.collection("Conversations")
        val query = chatroomsCollection?.whereArrayContains("userIDs", preferenceManager?.getUserDetails()?.getId()!!)
        query?.addSnapshotListener(object :EventListener<QuerySnapshot>{
            override fun onEvent(queryDocumentSnapshots: QuerySnapshot?, e: FirebaseFirestoreException?) {
                queryDocumentSnapshots?.forEachIndexed { index, documentSnapshot ->
                    val ids = documentSnapshot.getData().get("id").toString().split("-")
                    if (ids.size>=3){
                        if (ids.get(2).equals(Common.selectedProductDetails.id)){
                            if (ids.get(0).equals(preferenceManager?.userDetails?.id)){
                                allusersList.add(ids.get(1))
                            }else{
                                allusersList.add(ids.get(0))
                            }

                        }
                    }

                }
                if (allusersList.size>0) {
                    viewBase?.visibility = View.VISIBLE
                    searchUsersFromApi(allusersList)
                }
                else {
                    viewBase?.visibility = View.GONE
                }

            }
        })
    }

    private fun navigateToHome() {
        val intent = Intent(this@UserListingOnProduct, HomeActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    private fun searchUsersFromApi(allusersList: java.util.ArrayList<String>) {
        val userList = allusersList.toString().replace("[","").replace("]","")
        RestClient.getApiInterface().getUsersList(userList,preferenceManager?.userDetails?.token).enqueue(object : Callback<UsersArrayModel>{
            override fun onFailure(call: Call<UsersArrayModel>, t: Throwable) {

            }

            override fun onResponse(
                call: Call<UsersArrayModel>,
                response: Response<UsersArrayModel>
            ) {
                if (response.body()!=null && response.body()?.data!=null) {
                    usersData = response.body()?.data!!
                    initRecyclerView()
                }
            }

        })
    }

    private fun initRecyclerView() {
        val rvUserLists = findViewById<RecyclerView>(R.id.rvUserLists)
        val userListingAdapter = UserListingAdapter(usersData, this)
        val linearLayoutManager = LinearLayoutManager(this)
        rvUserLists.adapter = userListingAdapter
        rvUserLists.layoutManager = linearLayoutManager
    }

    private fun initViews() {
        usersData = ArrayList()
        preferenceManager = PreferenceManager(this)
        selectBtn = findViewById(R.id.selectBtn)
        viewBase = findViewById(R.id.viewBase)
        back_icon = findViewById(R.id.back_icon) as ImageView?
        header = findViewById(R.id.header) as TextView?
        header?.setText(getString(R.string.select_user))
        back_icon?.setOnClickListener { finish() }
        mDb = FirebaseFirestore.getInstance()
        val settings = FirebaseFirestoreSettings.Builder()
            .build()
        mDb!!.firestoreSettings = settings

        selectBtn?.setOnClickListener {
            markProductSold(-1, false)
        }
    }

    override fun onClicked(position: Int) {
        markProductSold(position, true)

    }

    private fun markProductSold(position: Int, navigateToRatingPage: Boolean) {
        baseActivity?.showLoading(this)
        RestClient.getApiInterface().chageIsSoldStatus(
            Common.selectedProductDetails.id,
            "1"
        ).enqueue(object : Callback<ApiStatusModel> {
            override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
                baseActivity?.hideLoading()
                baseActivity?.showToast("Please try again", this@UserListingOnProduct)
            }

            override fun onResponse(
                call: Call<ApiStatusModel>,
                response: Response<ApiStatusModel>
            ) {
                Common.isProductRefreshNeeded = true
                baseActivity?.hideLoading()
                baseActivity?.showToast(getString(R.string.product_marked_sold), this@UserListingOnProduct)
                if (navigateToRatingPage) {
                    val soldToUser = usersData.get(position)
                    val intent = Intent(this@UserListingOnProduct, RateUser::class.java)
                    val bundle = Bundle()
                    bundle.putString("user_id", soldToUser.id)
                    bundle.putString("user_push_token", soldToUser.pushToken)
                    bundle.putString("user_img", soldToUser.image)
                    bundle.putString("user_name", soldToUser.fullName)
                    bundle.putString("product_id", Common.selectedProductDetails.id)
                    intent.putExtra("data", bundle)
                    startActivity(intent)
                } else {
                    navigateToHome()
                }



            }
        })
    }
}