package com.bingalollc.com.product_details

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RatingBar
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.homeactivity.HomeActivity
import com.bingalollc.com.homeactivity.model.ProductModel
import com.bingalollc.com.model.ApiStatusModel
import com.bingalollc.com.network.RestClient
import com.bingalollc.com.preference.Common
import com.bingalollc.com.users.Users
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RateUser : BaseActivity() {
    private var header: TextView? = null
    private var nameSellString: TextView? = null
    private var notMyProduct: TextView? = null
    private var back_icon: ImageView? = null
    private var submitRatingBtn: AppCompatButton? = null
    private var ratingBar: RatingBar? = null
    private var imageUsers: ImageView? = null
    private var productImage: ImageView? = null
    private var productDetailUI: LinearLayout? = null
    private var reviewTo = ""
    private var user_push_token = ""
    private var product_id = ""
    private var notification_type = "2"

    //Send product_id and user_id only.
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rate_user)
        initViews()
        getValue()
        importSwipeBack()
        submitRatingBtn?.setOnClickListener {
            if (ratingBar?.rating!!>0){
                showLoading(this)
                uploadRatingToServer()
            }else
                showToast(getString(R.string.please_rate),this)
        }
    }

    private fun uploadRatingToServer() {
        val ratings = ratingBar?.rating?.toInt().toString()
        val review = "left ${ratings} star review. Tap to View"
        RestClient.getApiInterface().submitReview(preferenceManager?.userDetails?.token,preferenceManager?.userDetails?.id,
            review,ratingBar?.rating?.toInt().toString(),reviewTo).enqueue(object :Callback<ApiStatusModel>{
            override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
                hideLoading()
                showToast("Something went wrong, Please try again",this@RateUser)
            }

            override fun onResponse(
                call: Call<ApiStatusModel>,
                response: Response<ApiStatusModel>
            ) {
                submitNotification(ratings)

            }
        })
    }

    private fun submitNotification(ratings: String) {
        RestClient.getApiInterface().submitNotificaton(preferenceManager?.userDetails?.fullName,"${ratings} star review. Tap to view",
            reviewTo, preferenceManager?.userDetails?.id, notification_type,product_id
        ).enqueue(object : Callback<ApiStatusModel>{
            override fun onResponse(
                call: Call<ApiStatusModel>,
                response: Response<ApiStatusModel>
            ) {
                sendNotification("${ratings} star review. Tap to view")
                hideLoading()
                showToast(getString(R.string.rating_posted_success),this@RateUser)
                val intent = Intent(this@RateUser, HomeActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }

            override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
                hideLoading()
            }

        })
    }

    private fun sendNotification(body: String) {
        RestClient.getApiInterface().sendNotificationFromWeb(
            preferenceManager.userDetails.fullName, body,
            notification_type, user_push_token, reviewTo, product_id, preferenceManager.userDetails.id,reviewTo
        ).enqueue(object : Callback<ApiStatusModel?> {
            override fun onResponse(
                call: Call<ApiStatusModel?>,
                response: Response<ApiStatusModel?>
            ) {
            }

            override fun onFailure(call: Call<ApiStatusModel?>, t: Throwable) {}
        })
    }

    private fun getValue() {
        product_id = intent.getBundleExtra("data")?.getString("product_id")?:""
        val clckAction = intent.getBundleExtra("data")?.getString("click_action")?:""
        if (clckAction.equals("2")) notification_type = "3"
        reviewTo =  intent.getBundleExtra("data")?.getString("user_id").toString()
        val profileImage = intent.getBundleExtra("data")?.getString("user_img")?.replace("http://","https://")
       Glide.with(this).load(profileImage)
           .placeholder(R.drawable.default_user).error(R.drawable.default_user).into(imageUsers!!)
        nameSellString?.setText(getString(R.string.would_you_like_to_sell,intent.getBundleExtra("data")?.getString("user_name")))
        if (intent.getBundleExtra("data")?.getBoolean("fromNotificationScreen", false) == true) {
            productDetailUI?.visibility = View.VISIBLE
            nameSellString?.setText(getString(R.string.would_you_like_to_buy,intent.getBundleExtra("data")?.getString("user_name")))
            setProductImage(intent.getBundleExtra("data")?.getString("product_image")?:"")
            notMyProduct?.setOnClickListener { finish() }
        } else {
            user_push_token =  intent.getBundleExtra("data")?.getString("user_push_token")?:""
        }
        if (user_push_token.isEmpty()) {
            getProfileDetails()
        }
        getProductDetails(product_id)
    }

    private fun getProductDetails(product_id: String) {
        RestClient.getApiInterface()
            .getProductDetails(product_id, preferenceManager.userDetails.token)
            .enqueue(object : Callback<ProductModel?> {
                override fun onResponse(
                    call: Call<ProductModel?>,
                    response: Response<ProductModel?>
                ) {
                    if (response.body() != null && response.body()!!.status == 200 && response.body()!!
                            .data != null && response.body()!!.data.size > 0
                    ) {
                        setProductImage(response.body()?.data?.get(0)?.images?.get(0)?.imageUrl?:"")
                    }
                }

                override fun onFailure(call: Call<ProductModel?>, t: Throwable) {
                }
            })
    }

    private fun setProductImage(url: String) {
        Glide.with(this).load(url).error(R.drawable.product_placeholder).placeholder(R.drawable.product_placeholder).into(productImage!!)

    }

    private fun getProfileDetails() {
        showLoading(this)
        RestClient.getApiInterface().getUserDetails(intent.getBundleExtra("data")?.getString("notifcation_from")?:"").enqueue(object : Callback<com.bingalollc.com.users.Users>{
            override fun onResponse(
                call: Call<com.bingalollc.com.users.Users>,
                response: Response<com.bingalollc.com.users.Users>
            ) {
                hideLoading()
                if (response.body()?.status == 200){
                    user_push_token = response.body()?.data?.pushToken?:""
                    reviewTo = response.body()?.data?.id?:""
                    Glide.with(this@RateUser).load(response.body()?.data?.image?:"")
                        .placeholder(R.drawable.default_user).error(R.drawable.default_user).into(imageUsers!!)
                    productDetailUI?.visibility = View.VISIBLE
                    nameSellString?.setText(getString(R.string.would_you_like_to_buy, response.body()?.data?.fullName?:""))

                }
            }

            override fun onFailure(call: Call<Users>, t: Throwable) {
                hideLoading()
            }

        })
    }

    private fun initViews() {
        productImage = findViewById(R.id.productImage) as ImageView?
        back_icon = findViewById(R.id.back_icon) as ImageView?
        submitRatingBtn = findViewById(R.id.submitRatingBtn) as AppCompatButton?
        productDetailUI = findViewById(R.id.productDetailUI) as LinearLayout?
        ratingBar = findViewById(R.id.ratingBar) as RatingBar?
        header = findViewById(R.id.header) as TextView?
        header?.setText(getString(R.string.rate_user))
        back_icon?.setOnClickListener { finish() }

        imageUsers= findViewById(R.id.imageUsers) as ImageView?
        nameSellString= findViewById(R.id.nameSellString) as TextView?
        notMyProduct= findViewById(R.id.notMyProduct) as TextView?
    }
}