package com.bingalollc.com.product_details

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.*
import android.util.DisplayMetrics
import android.util.Log
import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.doOnLayout
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.bingalollc.com.DetailImageViewAdapter
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.base.bottom_action_sheet.ActionSheet
import com.bingalollc.com.bump_product.BumpProduct
import com.bingalollc.com.chat.ChatMessages
import com.bingalollc.com.chat.model.ChatBox
import com.bingalollc.com.homeactivity.HomeActivity
import com.bingalollc.com.homeactivity.ViewPager.*
import com.bingalollc.com.homeactivity.homefragment.adapter.ProductAdapter
import com.bingalollc.com.homeactivity.model.AdsModel
import com.bingalollc.com.homeactivity.model.ProductModel
import com.bingalollc.com.homeactivity.postproduct.PostProduct
import com.bingalollc.com.homeactivity.profilefragment.ProfileFrag
import com.bingalollc.com.model.ApiStatusModel
import com.bingalollc.com.network.RestClient
import com.bingalollc.com.preference.Common
import com.bingalollc.com.report.ReportItem
import com.bingalollc.com.report.ReportUser
import com.bumptech.glide.Glide
import com.daimajia.easing.linear.Linear
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
//import kotlinx.android.synthetic.main.activity_product_details.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class ProductDetails : HomeActivity(), OnMapReadyCallback, ProductAdapter.OnItemChoosed,
    ActionSheet.onActionClick, DialogInterface.OnClickListener {
    private val BUMP_PRODUCT_SCREEN = 1000;
    var fragmentList: ArrayList<ImagesFragment>? = null
    var fragmentListAds: ArrayList<ImagesFragmentProductDetailsAds>? = null
    private var cipTutorial: CircularPageIndicator? = null
    private var cipTutorialAdds: CircularPageIndicator? = null
    var fragmentListBottomSheet: ArrayList<ImagesFragmentViewImages>? = null
    private var vpImages: ViewPager? = null
    private var vpImagesAdds: ViewPager? = null
    private var viewPagerContainer: LinearLayout? = null
    private var back_icon: ImageView? = null
    private var prof_name: TextView? = null
    private var prod_name: TextView? = null
    private var prod_price: TextView? = null
    private var prod_desc: TextView? = null
    private var tvPosted: TextView? = null
    private val currencySymbol = "₪ "
    private var makeAnOffer: TextView? = null
    private var askAQuestion: TextView? = null
    private var tvSepcification: TextView? = null
    private var tvCategory: TextView? = null
    private var tvCondition: TextView? = null
    private var profileImg: ImageView? = null
    private var adsLayout: LinearLayout? = null
    private var profileImgLayout: RelativeLayout? = null
    private var leftArrow: ImageView? = null
    private var rightArrow: ImageView? = null
    private var btn_fav: ImageView? = null
    private var btn_common_share: ImageView? = null
    private var btn_more: ImageView? = null
    private var bottomViewOfAppBar: View? = null
    private var header: TextView? = null
    private var rvSimiliarProducts: RecyclerView? = null
    private var detailImageRecyclerView: RecyclerView? = null
    private var appBarLayout: AppBarLayout? = null
    private var collapsing_toolbar: CollapsingToolbarLayout? = null
    private var similiar_prod_label: TextView? = null
    private var currentPage = 0
    private var googleMap: GoogleMap? = null
    private var productModeldata: ArrayList<ProductModel.Datum>? = null
    private var testDialog: AlertDialog? = null
    private var layoutOffer: LinearLayout? = null
    private var markAsSold: TextView? = null
    private var bumpBtn: ImageView? = null
    private var btn_cross: ImageView? = null
    private var bumpData: RelativeLayout? = null
    private var baseActivity: BaseActivity? = null
    private var productAdapter: ProductAdapter? = null
    private var isFav = false
    private var listOfImages = ArrayList<String>()
    private lateinit var data: ArrayList<String>
    private var mDb: FirebaseFirestore? = null
    private var other_id = ""
    private var isChatDonePreviously = false
    private var isOwnProduct = false
    private var limit = 10
    private var offset = 0



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_details)


        if (preferenceManager.isEnglish == 2) {
            val language = "hi" //Hindi language!.

            Locale.setDefault(Locale("hi"))
            val locale = Locale(language)
            Locale.setDefault(locale)
            val config = Configuration()
            config.locale = locale
            baseContext.resources.updateConfiguration(
                config,
                baseContext.resources.displayMetrics
            )

        }

        mDb = FirebaseFirestore.getInstance()
        val settings = FirebaseFirestoreSettings.Builder()
            .build()
        mDb!!.firestoreSettings = settings
        initViews()
        initClicks()
        setValues()
        searchNearByProducts(false)
        importSwipeBack()
        setSwipeBackEnable(true)

        profileImgLayout?.setOnClickListener {
            val profileFrag = ProfileFrag()
            val manager = supportFragmentManager
            profileFrag.setUserDetails(Common.selectedProductDetails.uploadedByUserId.id ?: "")
            val transaction = manager.beginTransaction()
            transaction.setCustomAnimations(
                R.anim.slide_in_bottom,
                R.anim.slide_out_top,
                R.anim.slide_in_top,
                R.anim.slide_out_bottom
            )
//            Handler().postDelayed({
//                appBarLayout?.visibility = View.GONE
//            }, 200)

            transaction.replace(R.id.fragment_container, profileFrag, "productDetails")
            transaction.addToBackStack("productDetails")
            transaction.commit()
        }

        Common.getInstance().onClickedViewPager.observe(this, androidx.lifecycle.Observer {
            run {
                if (!it.isNullOrEmpty()) {
                    showImage(it)
                    Common.getInstance().onClickedViewPager.value = null
                }
            }
        })

        if(preferenceManager.isUserLoggedIn){
            other_id = Common.selectedProductDetails.uploadedByUserId.id + "-" + preferenceManager.userDetails.id + "-" + Common.selectedProductDetails.id

            if (Common.selectedProductDetails.uploadedByUserId.id?.toInt() ?: 0 > preferenceManager.userDetails.id.toInt()) {
                other_id =
                    preferenceManager.userDetails.id + "-" + Common.selectedProductDetails.uploadedByUserId.id + "-" + Common.selectedProductDetails.id
            }

            val chatroomsCollection = mDb!!
                .collection("Conversations").document(other_id).collection("Messages")
            chatroomsCollection.addSnapshotListener { queryDocumentSnapshots, e ->
                if (queryDocumentSnapshots!!.documents.size != 0) {
                    isChatDonePreviously = true
                }
            }
        }
        getAds()
    }

    private fun initClicks() {
        makeAnOffer?.setOnClickListener({
            if(preferenceManager.isUserLoggedIn) {

                showNormalEditTextField(
                    this,
                    getString(R.string.make_a_offer),
                    getString(R.string.asking_price),
                    getString(R.string.send_offer),
                    getString(R.string.cancel)
                )
            }else{
                instance.showSocialLoginDialog(this)
            }
        })
        btn_fav?.setOnClickListener({
            if(preferenceManager.isUserLoggedIn) {
                baseActivity?.preventDoubleTap(btn_fav)
                updateFavouriteList()
                Common.isProductRefreshNeeded = true
            }else{
                instance.showSocialLoginDialog(this)
            }
        })
        btn_common_share?.setOnClickListener({
            val url = "${Common.SHARE_BASE_URL}${Common.selectedProductDetails.id}"
            val sendString = url + " " +
                getString(R.string.sell_text_tool_bar) + " " + url
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, resources.getString(R.string.app_name))
            shareIntent.putExtra(Intent.EXTRA_TEXT, sendString)
            startActivity(Intent.createChooser(shareIntent, "Share"))
        })
        bumpBtn?.setOnClickListener {
            showBumpDialog()
        }
        btn_cross?.setOnClickListener { bumpData?.visibility = View.GONE }

        btn_more?.setOnClickListener { showBottomSheet() }

        askAQuestion?.setOnClickListener {
            if(preferenceManager.isUserLoggedIn) {
                opeChatBar("")
            }else{
                instance.showSocialLoginDialog(this)
            }
        }
        markAsSold?.setOnClickListener {
            startActivity(Intent(this@ProductDetails, UserListingOnProduct::class.java))
        }
    }

    private fun opeChatBar(offer: String) {
        if (isChatDonePreviously) {
            launchChatScreen(offer)
        } else {
            buildNewChatroom(offer)
        }
    }

    private fun launchChatScreen(offerPrice: String) {
        val intent = Intent(this, ChatMessages::class.java)
        intent.putExtra("id", preferenceManager.userDetails.id)
        intent.putExtra("fromDetails", true)
        intent.putExtra("other_id", other_id)
        intent.putExtra("other_user_id", Common.selectedProductDetails.uploadedByUserId.id)
        intent.putExtra("offerPrice", offerPrice)
        if (!TextUtils.isEmpty(Common.selectedProductDetails.uploadedByUserId.image))
            intent.putExtra(
                "prof_img",
                Common.selectedProductDetails.uploadedByUserId.image
            )
        else
            intent.putExtra("prof_img", "")
        intent.putExtra(
            "friendname",
            Common.selectedProductDetails.uploadedByUserId.fullName
        )
        intent.putExtra(
            "friendToken",
            Common.selectedProductDetails.uploadedByUserId.pushToken
        )
        hideLoading()
        startActivity(intent)
    }

    private fun buildNewChatroom(offer: String) {
        baseActivity?.showLoading(this)
        val settings: FirebaseFirestoreSettings = FirebaseFirestoreSettings.Builder()
            .build()
        mDb!!.firestoreSettings = settings
        val newChatroomRef = mDb?.collection("Conversations")?.document(other_id)
        val tsLong = System.currentTimeMillis() / 1000
        val timestamp = tsLong.toString()
        val isReadContent =
            HashMap<String, Boolean>()
        isReadContent[Common.selectedProductDetails.uploadedByUserId.id ?: ""] = false
        isReadContent[preferenceManager.userDetails.id] = false
        val data = java.util.ArrayList<String>()
        data.add(Common.selectedProductDetails.uploadedByUserId.id ?: "")
        data.add(preferenceManager.userDetails.id)
        val chatBox = ChatBox()
        chatBox.setId(other_id)
        chatBox.setUserIDs(data)
        chatBox.setTimestamp(timestamp.toInt())
        chatBox.setLastMessage("")
        chatBox.setIsRead(isReadContent)
        newChatroomRef?.set(chatBox)?.addOnCompleteListener { task ->
            Handler().postDelayed({
                val node: String
                if (task.isSuccessful) {
                    isChatDonePreviously = true
                    launchChatScreen(offer)
                } else {
                    val parentLayout = findViewById(android.R.id.content) as View
                    Snackbar.make(
                        parentLayout,
                        resources.getString(R.string.something_went_wrong),
                        Snackbar.LENGTH_SHORT
                    ).show()
                }
            }, 1000)
        }?.addOnFailureListener {
            System.out.println(">>>>>>>>>> "+it?.message?:"")
            val parentLayout = findViewById(android.R.id.content) as View
            Snackbar.make(
                parentLayout,
                it.message?:"",
                Snackbar.LENGTH_SHORT
            ).show()
        }
    }

    private fun showBottomSheet() {
        data = ArrayList()
        data.add(getString(R.string.share_product))
        if (isOwnProduct) {
            if (!Common.selectedProductDetails.productIsSold.equals("1")) {
                data.add(getString(R.string.edit_product))
                if (bumpData?.visibility == View.VISIBLE)
                    data.add(getString(R.string.bump_your_item))
                data.add(getString(R.string.MarkAsSold))
            }
            if (!Common.selectedProductDetails.productIsSold.equals("1")) {
                markAsSold?.visibility = View.VISIBLE
                checkBumpLayout()
            }
            data.add(getString(R.string.delete_product))
        } else {
            data.add(getString(R.string.report_posting))
            data.add(getString(R.string.flag_seller))
        }
        this?.let {
            ActionSheet(it, data, this@ProductDetails)
                .setCancelTitle(getString(R.string.cancel))
                .setColorTitle(resources.getColor(R.color.dim_gray))
                .setColorBackground(resources.getColor(R.color.white)) //                        .hideTitle()
                //                        .setFontData(R.font.meryana_script)
                //                        .setFontCancelTitle(R.font.meryana_script)
                //                        .setFontTitle(R.font.meryana_script)
                //                        .setSizeTextCancel(30)
                //                        .setSizeTextData(30)
                //                        .setSizeTextTitle(30)
                .setColorTitleCancel(resources.getColor(R.color.basecolor))
                .setColorData(resources.getColor(R.color.basecolor))
                .setColorSelected(resources.getColor(R.color.colorAccent))
                .create()
        }

    }


    private fun updateFavouriteList() {
        if (!isFav) {
            Glide.with(this).load(R.drawable.fav_selected2x).into(btn_fav!!)
            addToFavourite()
        } else {
            Glide.with(this).load(R.drawable.fav_unselected).into(btn_fav!!)
            removeFromFavourite()
        }
    }

    private fun showBumpDialog() {
        val intent = Intent(this@ProductDetails, BumpProduct::class.java)
        intent.putExtra("product_id", Common.selectedProductDetails.id)
        startActivityForResult(intent, BUMP_PRODUCT_SCREEN)

        /* val alertDialog =
             AlertDialog.Builder(this)
         val inflater = layoutInflater
         val add_menu_layout = inflater.inflate(R.layout.bump_data_dialog, null)
         val imageBump = add_menu_layout.findViewById<ImageView>(R.id.imageBump)
         val cancelBtn = add_menu_layout.findViewById<TextView>(R.id.cancelBtn)
         val bumpBtnDialog = add_menu_layout.findViewById<TextView>(R.id.bumpBtnDialog)
         alertDialog.setView(add_menu_layout)
         if (Common.selectedProductDetails!=null && Common.selectedProductDetails.images.size>0)
             Glide.with(this).load(Common.selectedProductDetails.images.get(0).imageUrl).into(imageBump)
         bumpBtnDialog.setOnClickListener {
             testDialog!!.dismiss()
             setBumpDataToServer()

         }

         cancelBtn.setOnClickListener {
             testDialog!!.dismiss()

         }

         alertDialog.setView(add_menu_layout)
         alertDialog.setCancelable(false)
         testDialog = alertDialog.create()
         testDialog!!.show()*/
    }


    private fun addToFavourite() {
        RestClient.getApiInterface().addToFavourite(
            preferenceManager.userDetails.token,
            Common.selectedProductDetails.images.get(0).postId,
            preferenceManager.userDetails.id
        ).enqueue(object : Callback<ApiStatusModel> {
            override fun onResponse(
                call: Call<ApiStatusModel>,
                response: Response<ApiStatusModel>
            ) {
                if (response.isSuccessful && response.body()?.getStatus() == 200) {
                    baseActivity?.showToast(response.body()?.getMessage(), this@ProductDetails)
                    isFav = true
                }
            }

            override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
                baseActivity?.hideLoading()
            }

        })
    }

    private fun removeFromFavourite() {
        RestClient.getApiInterface().removeFromFavourite(
            preferenceManager.userDetails.token,
            Common.selectedProductDetails.images.get(0).postId,
            preferenceManager.userDetails.id
        ).enqueue(object : Callback<ApiStatusModel> {
            override fun onResponse(
                call: Call<ApiStatusModel>,
                response: Response<ApiStatusModel>
            ) {
                if (response.isSuccessful && response.body()?.getStatus() == 200) {
                    isFav = false
                    baseActivity?.showToast(response.body()?.getMessage(), this@ProductDetails)
                }
            }

            override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
                baseActivity?.hideLoading()
            }

        })
    }

    fun showNormalEditTextField(
        context: Context?,
        headerOne: String?,
        headerTwo: String?,
        button1: String?,
        button2: String?
    ) {
        val alertDialog = AlertDialog.Builder(context!!)
        val inflater = layoutInflater
        val add_menu_layout = inflater.inflate(R.layout.product_spec_dialog_ui, null)
        val header_one = add_menu_layout.findViewById<TextView>(R.id.header_one)
        val header_two = add_menu_layout.findViewById<TextView>(R.id.header_two)
        var edit_text = add_menu_layout.findViewById<EditText>(R.id.edit_text)
        val cancelBtn = add_menu_layout.findViewById<TextView>(R.id.cancelBtn)
        val yesBtn = add_menu_layout.findViewById<TextView>(R.id.yesBtn)
        header_one.text = headerOne
        header_two.text = headerTwo + ": ₪" + Common.selectedProductDetails.productPrice
        cancelBtn.text = button2
        yesBtn.text = button1
        alertDialog.setView(add_menu_layout)
        Handler(Looper.myLooper()!!).postDelayed({
            if (edit_text != null) {
                edit_text.requestFocus()
                baseActivity?.showKeyBoardOfView(context, edit_text)
            }

        }, 200)
        yesBtn.setOnClickListener {
            Handler(Looper.myLooper()!!).postDelayed({
                edit_text.requestFocus()
                baseActivity?.hideKeyBoardOfView(this, edit_text)
            }, 200)
            testDialog?.dismiss()
            if (TextUtils.isEmpty(edit_text?.text?.toString()).not()) {
                showLoading(this)
                opeChatBar(edit_text.text.toString())
/*                specificationString?.add(edit_text?.text?.toString() ?: "")
                postProductSpecificationTextAdapter?.updateText(specificationString)*/
            }


        }
        cancelBtn.setOnClickListener {
            Handler(Looper.myLooper()!!).postDelayed({
                edit_text.requestFocus()
                baseActivity?.hideKeyBoardOfView(this, edit_text)
            }, 200)
            testDialog?.dismiss()
            baseActivity?.hideKeyboard(this)
        }

        edit_text?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                val pp = edit_text?.text
                if (pp?.length == 1) {
                    edit_text?.setText(currencySymbol + pp)
                    edit_text?.setSelection(edit_text!!.length())
                } else if (edit_text?.text?.contains(currencySymbol) == true && edit_text?.text?.replace(
                        currencySymbol.toRegex(),
                        ""
                    )?.length == 0
                ) {
                    edit_text?.setText("")
                }

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, start: Int, before: Int, count: Int) {

            }


        })
        alertDialog.setView(add_menu_layout)
        alertDialog.setCancelable(false)
        testDialog = alertDialog.create()
        testDialog?.show()
        testDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    private fun setValues() {
        initMap()
        if (Common.selectedProductDetails == null) {
            startActivity(
                Intent(
                    this,
                    HomeActivity::class.java
                )
            )
        }
        Common.selectedProductDetails?.images?.forEachIndexed { index, image ->
            if (image != null) {
                fragmentList?.add(addFrag(image.imageUrl)!!)
                listOfImages.add(image.imageUrl)
            }
        }
        detailImageRecyclerView!!.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        detailImageRecyclerView!!.adapter = DetailImageViewAdapter(listOfImages) { position ->
            // do something with user on click
            vpImages!!.setCurrentItem(position, true)
        }


        setUpViewPager()
        if (Common.selectedProductDetails.productTitle != null) {
            header?.text = Common.selectedProductDetails.productTitle
            prod_name?.text = Common.selectedProductDetails.productTitle
        }
        if (Common.selectedProductDetails.productPrice.isNullOrEmpty().not()) {
            prod_price?.text = "₪ " + Common.selectedProductDetails.productPrice.replace("?", "")
        }
        if (Common.selectedProductDetails.productDescription.isNullOrEmpty().not()) {
            prod_desc?.text = Common.selectedProductDetails.productDescription
            if (preferenceManager.isEnglish == 2)
                prod_desc?.gravity = Gravity.LEFT
                prod_desc?.textDirection = View.TEXT_DIRECTION_LTR
        } else {
            prod_desc?.visibility = View.GONE
        }

        var speci = getString(R.string.specifications) + " " + getString(R.string.not_mentioned)
        if (Common.selectedProductDetails.productSpecifications.isNullOrEmpty().not()) {
            speci =
                getString(R.string.specifications) + " " + Common.selectedProductDetails.productSpecifications
        }
        baseActivity?.setBoldViewAbleSpannableString(
            this,
            speci,
            tvSepcification,
            0,
            15,
            getResources().getColor(R.color.basecolor)
        )

        var prodCat = getString(R.string.categories_label) + " " + getString(R.string.not_mentioned)
        if (Common.selectedProductDetails.productCategory.isNullOrEmpty().not()) {
            if (Common.categoryNameEng.size > 0 && preferenceManager.isEnglish == 2) {
                val index =
                    Common.categoryNameEng.indexOf(Common.selectedProductDetails.productCategory)
                if (index != -1) {
                    prodCat = getString(R.string.categories_label) + " " + Common.categoryNameHeb
                }
            } else {
                prodCat = getString(R.string.categories_label) + " " + Common.selectedProductDetails.productCategory
            }
        }
        baseActivity?.setBoldViewAbleSpannableString(
            this,
            prodCat,
            tvCategory,
            0,
            8,
            getResources().getColor(R.color.basecolor)
        )

        val typeface1 = ResourcesCompat.getFont(applicationContext, R.font.avenir_next_semibold)
        val typeface2 = ResourcesCompat.getFont(applicationContext, R.font.avenir_next_font)
        var text =getString(R.string.condition)

        val conditionString = Common.selectedProductDetails.productCondition
        if (Common.selectedProductDetails.productCondition.isNullOrEmpty().not()) {

            if (preferenceManager.isEnglish == 2) {
                var intString =""
                if(Common.selectedProductDetails.productCondition.contains("#")) {
                     intString = Common.selectedProductDetails.productCondition.substring(
                        0,
                        Common.selectedProductDetails.productCondition.indexOf("#")
                    )

                }
                var firstString = intString
                System.out.println(">>>>>>>>..First "+firstString)
                if (intString.equals("Brand New",ignoreCase = true)) {
                    firstString = "חדש לגמרי"
                } else if (intString.equals("New",ignoreCase = true)){
                    firstString = "חָדָשׁ"
                }else if (intString.equals("Used",ignoreCase = true)){
                    firstString = "בשימוש"
                }
                val endString = Common.selectedProductDetails.productCondition.substring(Common.selectedProductDetails.productCondition.indexOf("#")+1)
                var lastString = endString
                System.out.println(">>>>>>>>..endString "+endString)
                if (endString.contains("Not Touched", ignoreCase = true)) {
                    lastString =  "לא נגע. באריזה מקורית"
                } else if (endString.contains("Open Box", ignoreCase = true)) {
                    lastString =  "תיבה פתוחה (האריזה אינה פעילה, אף פעם לא בשימוש)"
                }else if (endString.contains("Like-New", ignoreCase = true)) {
                    lastString =  "כמו חדש (מצב מצוין. סימני שימוש קלים)"
                }else if (endString.contains("Very Good", ignoreCase = true)) {
                    lastString =  "טוב מאוד (סימני שימוש משמעותיים. פונקציות נכונות)"
                }else if (endString.contains("Acceptable", ignoreCase = true)) {
                    lastString =  "מקובל (יכול להיות שיש שריטות ושקעים קלים. עדיין עובד טוב)"
                }
                text =  getString(R.string.condition) + " " + firstString +" - "+lastString
            } else {
                text =  getString(R.string.condition) + " " + Common.selectedProductDetails.productCondition?.replace(
                    "#",
                    " - "
                )
            }

        }

        if (Common.selectedProductDetails.productIsSold.equals("1")) {
//            soldLabel.visibility = View.VISIBLE
        }
        baseActivity?.setBoldViewAbleSpannableString(
            this,
            text,
            tvCondition,
            0,
            getString(R.string.condition).length,
            getResources().getColor(R.color.basecolor)
        )

        var posted = getString(R.string.posted) + " " + getString(R.string.not_mentioned)
        if (Common.selectedProductDetails.time_diff.isNullOrEmpty().not()) {
            if (preferenceManager.isEnglish == 2) {
                var newDate = Common.selectedProductDetails.time_diff.toString()
                    .replace("years ago", "לפני שנים")
                    .replace("months ago", "לפני חודשים")
                    .replace("days ago", "לפני מספר ימים")
                    .replace("hours ago", "לפני שעות")
                    .replace("minutes ago", "לפני דקות")
                    .replace("seconds ago", "לפני שניות")
                posted = getString(R.string.posted) + " " + newDate
            } else {
                posted = getString(R.string.posted) + " " + Common.selectedProductDetails.time_diff
            }

        }

        // val date  = getString(R.string.posted) + " " + getDateDiff(Calendar.getInstance().time, convertStringToDate(Common.selectedProductDetails.createdAt))

        baseActivity?.setBoldViewAbleSpannableString(this, posted, tvPosted, 0, 8, getResources().getColor(R.color.basecolor))

        if(preferenceManager.isUserLoggedIn) {
            if (Common.selectedProductDetails.uploadedByUserId != null && !Common.selectedProductDetails.uploadedByUserId.id.equals(
                    preferenceManager.userDetails.id
                ))
            {
                prof_name?.text = Common.selectedProductDetails.uploadedByUserId.fullName
                Glide.with(this).load(Common.selectedProductDetails.uploadedByUserId.image)
                    .error(R.drawable.default_user_pic).placeholder(R.drawable.default_user_pic)
                    .into(profileImg!!)

            }else
            {
                prof_name?.visibility = View.GONE
                profileImg?.visibility = View.GONE
            }
        }else
        {
            prof_name?.text = Common.selectedProductDetails.uploadedByUserId.fullName
            Glide.with(this).load(Common.selectedProductDetails.uploadedByUserId.image)
                .error(R.drawable.default_user_pic).placeholder(R.drawable.default_user_pic)
                .into(profileImg!!)
        }
        if (isOwnProduct)
        {
            layoutOffer?.visibility = View.GONE
            btn_fav?.visibility = View.GONE
            btn_common_share?.visibility = View.GONE
            if (!Common.selectedProductDetails.productIsSold.equals("1")) {
                markAsSold?.visibility = View.VISIBLE
                checkBumpLayout()
            }

        } else
        {
            layoutOffer?.visibility = View.VISIBLE
            markAsSold?.visibility = View.GONE
            bumpData?.visibility = View.GONE

        }

    }

    private fun checkBumpLayout() {
        //
        if (Common.selectedProductDetails.getBumpData()!=null && Common.selectedProductDetails.getBumpData().equals("").not()) {
            //Product is Bumped
            bumpData?.visibility = View.GONE
        } else {
            bumpData?.visibility = View.VISIBLE
        }

    }

    private fun initViews() {
        baseActivity = BaseActivity();
        appBarLayout = findViewById(R.id.appBarLayout) as AppBarLayout?
        vpImages = findViewById(R.id.vpImages) as ViewPager?
        vpImagesAdds = findViewById(R.id.vpImagesAdds) as ViewPager?
        viewPagerContainer = findViewById(R.id.viewPagerContainer) as LinearLayout?
        cipTutorialAdds = findViewById(R.id.cipTutorialAdds) as CircularPageIndicator?
        tvSepcification = findViewById(R.id.tvSepcification) as TextView?
        tvCategory = findViewById(R.id.tvCategory) as TextView?
        tvCondition = findViewById(R.id.tvCondition) as TextView?
        cipTutorial = findViewById(R.id.cipTutorial) as CircularPageIndicator?
        back_icon = findViewById(R.id.back_icon) as ImageView?
        prof_name = findViewById(R.id.prof_name) as TextView?
        profileImg = findViewById(R.id.profileImg) as ImageView?
        profileImgLayout = findViewById(R.id.profileImgLayout) as RelativeLayout?
        prod_name = findViewById(R.id.prod_name) as TextView?
        prod_price = findViewById(R.id.prod_price) as TextView?
        prod_desc = findViewById(R.id.prod_desc) as TextView?
        tvPosted = findViewById(R.id.tvPosted) as TextView?
        btn_fav = findViewById(R.id.btn_fav) as ImageView?
        btn_common_share = findViewById(R.id.btn_common_share) as ImageView?
        btn_more = findViewById(R.id.btn_more) as ImageView?
        makeAnOffer = findViewById(R.id.makeAnOffer) as TextView?
        askAQuestion = findViewById(R.id.askAQuestion) as TextView?
        adsLayout = findViewById(R.id.adsLayout) as LinearLayout?
        bottomViewOfAppBar = findViewById(R.id.bottomViewOfAppBar)
        bottomViewOfAppBar?.visibility = View.GONE
        back_icon?.setOnClickListener({ this.finish() })
        header = findViewById(R.id.header) as TextView?
        layoutOffer = findViewById(R.id.layoutOffer) as LinearLayout?
        markAsSold = findViewById(R.id.markAsSold) as TextView?
        bumpData = findViewById(R.id.bumpData) as RelativeLayout?
        bumpBtn = findViewById(R.id.bumpBtn) as ImageView?
        rightArrow = findViewById(R.id.rightArrow) as ImageView?
        leftArrow = findViewById(R.id.leftArrow) as ImageView?
        btn_cross = findViewById(R.id.button_crossed) as ImageView?
        rvSimiliarProducts = findViewById(R.id.rvSimiliarProducts) as RecyclerView?
        detailImageRecyclerView = findViewById(R.id.detailImageRecyclerView) as RecyclerView?
        collapsing_toolbar = findViewById(R.id.collapsing_toolbar) as CollapsingToolbarLayout?
        similiar_prod_label = findViewById(R.id.similiar_prod_label) as TextView?
        fragmentList = ArrayList()
        isFav = Common.selectedProductDetails.favourite ?: false
        if (isFav) {
            Glide.with(this).load(R.drawable.fav_selected2x).into(btn_fav!!)
        } else {
            Glide.with(this).load(R.drawable.fav_unselected).into(btn_fav!!)
        }
        if(preferenceManager.isUserLoggedIn) {
            if (Common.selectedProductDetails.uploadedByUserId != null && Common.selectedProductDetails.uploadedByUserId.id != null) {
                if (Common.selectedProductDetails.uploadedByUserId.id.equals(preferenceManager.userDetails.id)) {
                    isOwnProduct = true
                }
            }
        }

        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels
        var width = displayMetrics.widthPixels
        val params = LinearLayoutCompat.LayoutParams(width, width)
        viewPagerContainer?.layoutParams = params




    }


    private fun initMap() {
        val mapFragment =
            this.supportFragmentManager.findFragmentById(R.id.mapView) as SupportMapFragment?
        try {
            mapFragment?.getMapAsync(this)
        } catch (e: Exception) {
            baseActivity?.showToast(e.message, this)
        }


    }

    private fun searchNearByProducts(isLoadMore: Boolean) {

        RestClient.getApiInterface().old_search_product(
            "",
            "",
            Common.currentLat,
            Common.currentLng,
            "",
            "",
            "ASC",
            "0",
            "100000",
            Common.selectedProductDetails.parentCategory,
            "1000", preferenceManager?.userDetails?.id,
            limit, offset,
            baseActivity?.currentDateTime,
            preferenceManager?.isEnglish
        ).enqueue(object : Callback<ProductModel> {
            override fun onFailure(call: Call<ProductModel>, t: Throwable) {
                baseActivity?.hideLoading()
            }

            override fun onResponse(call: Call<ProductModel>, response: Response<ProductModel>) {
                if (!isLoadMore) {
                    productModeldata = ArrayList()
                    if (response.body()?.data != null) {
                        response.body()?.data?.forEachIndexed { index, datum ->
                            if (preferenceManager.isUserLoggedIn) {
                                if (!datum.uploadedByUserId.id.equals(
                                        preferenceManager.userDetails.id) && Common.blockedUserId.contains(datum.uploadedByUserId.id).not()
                                    && Common.selectedProductDetails.id.equals(datum.id).not()) {
                                    addSimilarProducts(datum)
                                }
                            } else {
                                addSimilarProducts(datum)
                            }
                        }

                    }
                    if (productModeldata?.size == 0) {
                        baseActivity?.hideLoading()
                        //NO Product Found Label
                        rvSimiliarProducts?.visibility = View.GONE
//                        dummyView?.visibility = View.VISIBLE
                        similiar_prod_label?.visibility = View.GONE
                        disableScroll()
                    }
                    initRecyclerView()
                } else {
                    baseActivity?.hideLoading()
                    productAdapter?.addMoreData(productModeldata)
                }

            }

        })
    }

    private fun addSimilarProducts(datum: ProductModel.Datum?) {
        if (datum!=null){
            if (datum.productIsSold.equals("1").not())
                productModeldata?.add(datum)
            else if (BaseActivity.getNoOfDays(
                    Calendar.getInstance().time,
                    BaseActivity.convertStringToDate(datum.updated_at)
                ) < 24
            ) {
                productModeldata?.add(datum)
            }
        }
    }

    private fun disableScroll() {
      /*  val params = collapsing_toolbar?.getLayoutParams() as AppBarLayout.LayoutParams
        params.scrollFlags = AppBarLayout.LayoutParams.SCROLL_FLAG_NO_SCROLL
        collapsing_toolbar?.setLayoutParams(params)*/
    }

    private fun initRecyclerView() {
        if (productModeldata?.size == 0) {
            //NO Product Found Label
            rvSimiliarProducts?.visibility = View.GONE
//            dummyView?.visibility = View.VISIBLE
            similiar_prod_label?.visibility = View.GONE
            disableScroll()
        } else {
            //NO Product Found Label
            rvSimiliarProducts?.visibility = View.VISIBLE
            similiar_prod_label?.visibility = View.VISIBLE
//            dummyView?.visibility = View.GONE
        }
        productAdapter = ProductAdapter(productModeldata, this)
        val linearLayoutManager = GridLayoutManager(this, 2)
        rvSimiliarProducts?.adapter = productAdapter
        rvSimiliarProducts?.layoutManager = linearLayoutManager
        baseActivity?.hideLoading()

        rvSimiliarProducts?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                //1 for down
                if (!recyclerView.canScrollVertically(1))
                    if (productModeldata != null && (productModeldata?.size
                            ?: 0) > 0 && productModeldata?.get(
                            (productModeldata?.size ?: 0) - 1
                        )?.load_more == true) {
                        offset += limit
                        searchNearByProducts(true)
                    }
            }
        })
    }

    private fun getAds() {
        RestClient.getApiInterface().fetchMobileAds("8").enqueue(object : Callback<AdsModel> {
            override fun onFailure(call: Call<AdsModel>, t: Throwable) {

            }

            override fun onResponse(call: Call<AdsModel>, response: Response<AdsModel>) {
                fragmentListAds = ArrayList()
                response.body()?.data?.forEachIndexed { index, datum ->
                    // fragmentList?.add(datum.image?:"")

                    if(datum.image != null) {
                        adsLayout?.visibility = View.VISIBLE
                        fragmentListAds?.add(addFragAds(datum.image, datum.link ?: "")!!)
                    }
                }
                fragmentListAds?.shuffle()
                setUpAddsViewPager()
            }
        })
    }

    private fun setUpAddsViewPager() {
        val basePagerAdapter = ProductDetailsAdsViewPagerAdapter(
            this.supportFragmentManager,
            fragmentListAds,
            dpToPixels(this),
          this
        )
        //  vpImages!!.offscreenPageLimit = 3
        vpImagesAdds!!.adapter = basePagerAdapter
        basePagerAdapter.notifyDataSetChanged()
        cipTutorialAdds!!.setViewPager(vpImagesAdds)
        vpImagesAdds!!.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                currentPage = position
            }

            override fun onPageSelected(position: Int) {
                // selectedPageIndex = position;
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })
        val h = Handler(Looper.getMainLooper())
        val r: Runnable = object : Runnable {
            override fun run() {
                if (currentPage >= (fragmentList?.size ?: 0)) currentPage = -1
                vpImages?.setCurrentItem(currentPage++, true)
                h.postDelayed(this, 3000)
            }
        }
        h.postDelayed(r, 1000)

    }

    private fun setUpViewPager() {
        if (fragmentList?.size?:0 > 0) {
            rightArrow?.visibility = View.VISIBLE
        }
        val basePagerAdapter = ViewPagerAdapter(
            this.supportFragmentManager,
            fragmentList,
            dpToPixels(this),
            this
        )
        vpImages!!.offscreenPageLimit = 2
        vpImages!!.adapter = basePagerAdapter
        basePagerAdapter.notifyDataSetChanged()
        cipTutorial!!.setViewPager(vpImages)
        vpImages!!.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                if (position > 0) {
                    leftArrow?.visibility = View.VISIBLE
                } else {
                    leftArrow?.visibility = View.GONE
                }

                if (position+1 == fragmentList?.size) {
                    rightArrow?.visibility = View.GONE
                } else {
                    rightArrow?.visibility = View.VISIBLE
                }
            }

            override fun onPageSelected(position: Int) {
            }

            override fun onPageScrollStateChanged(state: Int) {}

        })
        val handler = Handler()
        val runnable = Runnable {
            if (currentPage >= (fragmentList?.size ?: 0)) currentPage = -1
            vpImages!!.setCurrentItem(currentPage++, true)
        }
        rightArrow?.setOnClickListener {
            vpImages!!.setCurrentItem(currentPage++, true)
        }
        leftArrow?.setOnClickListener {
            vpImages!!.setCurrentItem(currentPage--, true)
        }

        /*timer = new Timer(); // This will create a new Thread

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(runnable);
            }
        }, 1000, 3000);*/
        vpImages!!.setOnTouchListener(
            object : View.OnTouchListener {
                private var moved = false
                override fun onTouch(
                    view: View,
                    motionEvent: MotionEvent
                ): Boolean {
                    if (motionEvent.action == MotionEvent.ACTION_DOWN) {
                        moved = false
                    }
                    if (motionEvent.action == MotionEvent.ACTION_MOVE) {
                        moved = true
                    }
                    if (motionEvent.action == MotionEvent.ACTION_UP) {
                        if (!moved) {
                            view.performClick()
                        }
                    }
                    return false
                }
            }
        )
    }


    private fun showImage(url: String) {
        val viewImageWithFrag = ViewImageWithZoomFrag()
        viewImageWithFrag?.setFragmentImages(listOfImages, listOfImages.indexOf(url))
        val manager: FragmentManager = this.supportFragmentManager
        val transaction = manager.beginTransaction()
        transaction.setCustomAnimations(
            R.anim.slide_in_bottom,
            R.anim.slide_out_top,
            R.anim.slide_in_top,
            R.anim.slide_out_bottom
        )
        transaction.add(R.id.fragment_container, viewImageWithFrag!!, "viewimage")
        transaction.addToBackStack("viewimage")
        transaction.commit()

    }


    private fun addFrag(tutsImage: String): ImagesFragment? {
        val bundle = Bundle()
        bundle.putString("tutImages", tutsImage)
        bundle.putBoolean("showTransparentLayout", true)
        val imagesFragment = ImagesFragment()
        imagesFragment.arguments = bundle
        return imagesFragment
    }

    private fun addFragAds(tutsImage: String,tutsImageUrl: String): ImagesFragmentProductDetailsAds? {
        val bundle = Bundle()
        bundle.putString("tutImages", tutsImage)
        bundle.putString("tutsImageUrl", tutsImageUrl)
        bundle.putBoolean("showTransparentLayout", true)
        val imagesFragment = ImagesFragmentProductDetailsAds()
        imagesFragment.arguments = bundle
        return imagesFragment
    }

    private fun dpToPixels(context: Context): Float {
        return 2 * context.resources.displayMetrics.density
    }

    override fun onItemClicked(postion: Int) {
        Common.selectedProductDetails = productModeldata?.get(postion)
        startActivity(Intent(this, ProductDetails::class.java))
        finish()
    }

    override fun onBackPressed() {
        if (appBarLayout?.visibility == View.GONE) {
            appBarLayout?.visibility = View.VISIBLE
        }
        super.onBackPressed()

    }

    override fun onBottomSheetClicked(title: ArrayList<String>, position: Int) {
        if (position == 0) {
            val url = "${Common.SHARE_BASE_URL}${Common.selectedProductDetails.id}"
            val sendString =
                getString(R.string.sell_text) + Common.selectedProductDetails.productTitle + "... \n" + url
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, resources.getString(R.string.app_name))
            shareIntent.putExtra(Intent.EXTRA_TEXT, sendString)
            startActivity(Intent.createChooser(shareIntent, "Share"))
        } else if (title.get(position).equals(getString(R.string.report_posting))) {
            if(preferenceManager.isUserLoggedIn) {
                startActivity(Intent(this, ReportItem::class.java))
            }else{
                instance.showSocialLoginDialog(this)
            }
        } else if (title.get(position).equals(getString(R.string.flag_seller))) {
            if(preferenceManager.isUserLoggedIn) {
                val intent = Intent(this, ReportUser::class.java)
                intent.putExtra("usedId", Common.selectedProductDetails.uploadedByUserId.id)
                intent.putExtra("fullName", Common.selectedProductDetails.uploadedByUserId.fullName)
                intent.putExtra("userImage", Common.selectedProductDetails.uploadedByUserId.image)
                intent.putExtra("productId", Common.selectedProductDetails.id)
                startActivity(intent)
            }else{
                // (this as HomeActivity).showSocialLoginDialog()
                instance.showSocialLoginDialog(this)

                //  showSocialLoginDialog(this@ProductDetails)
            }
        } else if (title.get(position).equals(getString(R.string.edit_product))) {
            val postProduct = PostProduct()
            postProduct?.setAllFieldsForEdit(true)
            val manager: FragmentManager = this.supportFragmentManager
            val transaction = manager.beginTransaction()
            transaction.add(R.id.fragment_container, postProduct!!, "postProduct")
            transaction.addToBackStack("postProduct")
            transaction.commit()
        } else if (title.get(position).equals(getString(R.string.delete_product))) {
            showMessageOKCancel(
                getString(R.string.delete_confirmation),
                "",
                this,
                this,
                getString(R.string.delete),
                getString(R.string.cancel),
                true
            )
        } else if (title.get(position).equals(getString(R.string.MarkAsSold))) {
            startActivity(Intent(this@ProductDetails, UserListingOnProduct::class.java))
        }
        else if (title.get(position).equals(getString(R.string.bump_your_item))) {
           // showBumpDialog()
        }
    }

    override fun onClick(p0: DialogInterface?, p1: Int) {
        p0?.dismiss()
        showLoading(this)
        RestClient.getApiInterface().deleteProduct(
            preferenceManager.userDetails.token,
            Common.selectedProductDetails.id
        ).enqueue(object : Callback<ApiStatusModel> {
            override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
                hideLoading()
            }

            override fun onResponse(
                call: Call<ApiStatusModel>,
                response: Response<ApiStatusModel>
            ) {
                hideLoading()
                showToast("Product Deleted", this@ProductDetails)
                finish()
            }

        })

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == BUMP_PRODUCT_SCREEN) {
            if (resultCode === RESULT_OK) {
                val paymentStatus = data!!.getStringExtra("paymentStatus")
                if (paymentStatus.equals("success")) {
                    bumpData?.visibility = View.GONE
                    Common.isProductRefreshNeeded = true
                }
            }
        }
    }

    override fun onMapReady(googleMaps: GoogleMap) {
        googleMap = googleMaps
        googleMap?.uiSettings?.setAllGesturesEnabled(false)
        var lat: Double
        var lng: Double
        var name: String
        val latti: String = Common.selectedProductDetails.productLat
        val longi: String = Common.selectedProductDetails.productLng
        if (!latti.isEmpty() && !longi.isEmpty()
        ) {
            lat = latti.toDouble()
            lng = longi.toDouble()
            Log.d(
                "VIEWPRODUCT",
                "loadMap: $lat   lng  $lng"
            )

            try {
                googleMap?.addCircle(
                    CircleOptions()
                        .center(LatLng(latti.toFloat().toDouble(), longi.toFloat().toDouble()))
                        .radius(800.0)
                        .strokeColor(getColor(R.color.basecolor))
                        .fillColor(0x201976d2)
                        .strokeWidth(5f)
                )!!
                googleMaps?.moveCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(
                            latti.toFloat().toDouble(),
                            longi.toFloat().toDouble()
                        ), 14.0f
                    )
                )


            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}

