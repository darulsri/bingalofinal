package com.bingalollc.com.product_details

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.ViewPager
import com.bingalollc.com.R
import com.bingalollc.com.databinding.FragmentFilterBinding
import com.bingalollc.com.databinding.FragmentViewImageWithZoomBinding
import com.bingalollc.com.homeactivity.ViewPager.ImagesFragmentViewImages
import com.bingalollc.com.homeactivity.ViewPager.ViewPagerAdapterViewImages
import com.bingalollc.com.preference.Common
import com.bingalollc.com.utils.ViewBindingActivity
import com.bingalollc.com.utils.ViewBindingDialogFragment
//import com.github.chrisbanes.photoview.PhotoView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.liuguangqiang.swipeback.SwipeBackLayout


class ViewImageWithZoomActivity : ViewBindingActivity<FragmentViewImageWithZoomBinding>(){
    var fragmentList: ArrayList<ImagesFragmentViewImages>? = null
    var currentPage : Int=0
//    var imageView : PhotoView? = null
    var crossBtn : ImageView? = null
    var shareImg : ImageView? = null
    var vpImages : ViewPager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews()
        setFragmentImages(getIntent().getStringArrayListExtra("data"), intent.getIntExtra("position",0))

        crossBtn?.setOnClickListener {
           finish()
        }

        shareImg?.setOnClickListener {
            shareImage()
        }
        setUpViewPager()
        val swipeBackLayout = findViewById(R.id.swipeBackLayout) as SwipeBackLayout
        //swipeBackLayout.setOnFinishListener(this)
        swipeBackLayout.setEnableFlingBack(false)
        swipeBackLayout.setOnPullToBackListener { fractionAnchor, fractionScreen ->
            if (fractionScreen>=0.99){
                finish()
            }
        }
    }
    private fun initViews() {
//        imageView = findViewById(R.id.imageView) as PhotoView?
        shareImg = findViewById(R.id.shareImg) as ImageView?
        crossBtn = findViewById(R.id.crossBtn) as ImageView?
        vpImages = findViewById(R.id.vpImages) as ViewPager?
    }


    private fun shareImage() {
        val url ="${Common.PRODUCT_BASE_URL}${Common.selectedProductDetails.id}"
     //   val sendString = url + " " + "${getString(R.string.hi_i_want_to_sell)} "+ Common.selectedProductDetails.productTitle+" \n"+url
        val sendString = "${getString(R.string.hi_i_want_to_sell)} "+ " \n" +url
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.type = "text/plain"
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, resources.getString(R.string.app_name))
        shareIntent.putExtra(Intent.EXTRA_TEXT,sendString)
        startActivity(Intent.createChooser(shareIntent, getString(R.string.share)))
    }

    fun setFragmentImages(imageStringArray: ArrayList<String>?, position: Int){
        fragmentList = ArrayList()
        imageStringArray?.forEachIndexed { index, image ->
            if (!image.isEmpty())
                fragmentList?.add(addFrag(image)!!)
        }
        this.currentPage = position
    }
    private fun addFrag(tutsImage: String): ImagesFragmentViewImages? {
        val bundle = Bundle()
        bundle.putString("tutImages", tutsImage)
        val imagesFragment = ImagesFragmentViewImages()
        imagesFragment.arguments = bundle
        return imagesFragment
    }

    private fun dpToPixels(context: Context): Float {
        return 2 * context.resources.displayMetrics.density
    }

    private fun setUpViewPager() {
        val basePagerAdapter = ViewPagerAdapterViewImages(
            supportFragmentManager,
            fragmentList,
            dpToPixels(this),
            this
        )
        vpImages?.offscreenPageLimit = 2
        vpImages?.adapter = basePagerAdapter
        basePagerAdapter.notifyDataSetChanged()

        vpImages?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                currentPage = position
            }

            override fun onPageSelected(position: Int) {
            }

            override fun onPageScrollStateChanged(state: Int) {}

        })
        vpImages?.setCurrentItem(currentPage, true)
    }

    override fun provideBinding(inflater: LayoutInflater): FragmentViewImageWithZoomBinding {
        return FragmentViewImageWithZoomBinding.inflate(inflater)
    }

    override fun onBackPressed() {

        super.onBackPressed()
        overridePendingTransition(R.anim.slide_in_top,
            R.anim.slide_out_bottom);
    }

}
