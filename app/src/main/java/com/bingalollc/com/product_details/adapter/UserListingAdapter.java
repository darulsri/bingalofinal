package com.bingalollc.com.product_details.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bingalollc.com.R;
import com.bingalollc.com.homeactivity.adapter.CategoriesFilterAdapter;
import com.bingalollc.com.users.UsersArrayModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class UserListingAdapter extends RecyclerView.Adapter<UserListingAdapter.Viewholder> {
    private Context context;
    private ArrayList<UsersArrayModel.Data> username;
    private OnNameClicked onNameClicked;

    public UserListingAdapter(ArrayList<UsersArrayModel.Data> username, OnNameClicked onNameClicked) {
        this.username = username;
        this.onNameClicked = onNameClicked;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.user_listing_item,parent,false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
            holder.userName.setText(username.get(position).getFullName());
            Glide.with(context).load(username.get(position).getImage()).placeholder(R.drawable.default_user).error(R.drawable.default_user).into(holder.imageUsers);
    }

    @Override
    public int getItemCount() {
        return username.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        private final TextView userName,selectBtn;
        private final ImageView imageUsers;
        public Viewholder(@NonNull View itemView) {
            super(itemView);
            userName = itemView.findViewById(R.id.userName);
            imageUsers = itemView.findViewById(R.id.imageUsers);
            selectBtn = itemView.findViewById(R.id.selectBtn);
            selectBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onNameClicked.onClicked(getAdapterPosition());
                }
            });
        }
    }
    public interface  OnNameClicked{
        void onClicked(int position);
    }
}
