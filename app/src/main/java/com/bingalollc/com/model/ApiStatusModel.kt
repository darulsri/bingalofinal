package com.bingalollc.com.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ApiStatusModel {

    @SerializedName("message")
    @Expose
    private var message: String? = null

    @SerializedName("status")
    @Expose
    private var status: Int? = null

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String?) {
        this.message = message
    }

    fun withMessage(message: String?): ApiStatusModel? {
        this.message = message
        return this
    }

    fun getStatus(): Int? {
        return status
    }

    fun setStatus(status: Int?) {
        this.status = status
    }

    fun withStatus(status: Int?): ApiStatusModel? {
        this.status = status
        return this
    }

}