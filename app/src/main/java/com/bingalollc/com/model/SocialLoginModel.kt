package com.bingalollc.com.model

import com.bingalollc.com.users.Users
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SocialLoginModel {

    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("status")
    @Expose
    private var status: Int? = null

    @SerializedName("data")
    @Expose
    var data: Data? = null



    fun withMessage(message: String?): SocialLoginModel? {
        this.message = message
        return this
    }

    fun getStatus(): Int? {
        return status
    }

    fun setStatus(status: Int?) {
        this.status = status
    }

    fun withStatus(status: Int?): SocialLoginModel? {
        this.status = status
        return this
    }

    fun withData(data: Data?): SocialLoginModel? {
        this.data = data
        return this
    }

    class Data {
        @SerializedName("id")
        @Expose
        var id: String? = null

        @SerializedName("full_name")
        @Expose
        var fullName: String? = null

        @SerializedName("email")
        @Expose
        var email: String? = null

        @SerializedName("phone")
        @Expose
        var phone: String? = null

        @SerializedName("password")
        @Expose
        var password: String? = null

        @SerializedName("lat")
        @Expose
        var lat: String? = null

        @SerializedName("lng")
        @Expose
        var lng: String? = null

        @SerializedName("updated_at")
        @Expose
        var updatedAt: String? = null

        @SerializedName("created_at")
        @Expose
        var createdAt: String? = null

        @SerializedName("facebook_id")
        @Expose
        var facebookId: String? = null

        @SerializedName("google_id")
        @Expose
        var googleId: String? = null

        @SerializedName("image")
        @Expose
        var image: String? = null

        @SerializedName("image_name")
        @Expose
        var imageName: String? = null

        @SerializedName("language")
        @Expose
        var language: String? = null

        @SerializedName("token")
        @Expose
        var token: String? = null

        @SerializedName("twitter_id")
        @Expose
        var twitterId: String? = null

        @SerializedName("apple_id")
        @Expose
        var appleId: String? = null

        @SerializedName("push_token")
        @Expose
        var pushToken: String? = null

        @SerializedName("profile_verified")
        @Expose
        var profile_verified: String? = null

        @SerializedName("phone_verified")
        @Expose
        var phone_verified: String? = null

        @SerializedName("verification")
        @Expose
        var verification = ArrayList<Users.Verification>()


        fun withId(id: String?): Data {
            this.id = id
            return this
        }

        fun withFullName(fullName: String?): Data {
            this.fullName = fullName
            return this
        }

        fun withEmail(email: String?): Data {
            this.email = email
            return this
        }

        fun withPhone(phone: String?): Data {
            this.phone = phone
            return this
        }

        fun withPassword(password: String?): Data {
            this.password = password
            return this
        }

        fun withLat(lat: String?): Data {
            this.lat = lat
            return this
        }

        fun withLng(lng: String?): Data {
            this.lng = lng
            return this
        }

        fun withUpdatedAt(updatedAt: String?): Data {
            this.updatedAt = updatedAt
            return this
        }

        fun withCreatedAt(createdAt: String?): Data {
            this.createdAt = createdAt
            return this
        }

        fun withFacebookId(facebookId: String?): Data {
            this.facebookId = facebookId
            return this
        }

        fun withGoogleId(googleId: String?): Data {
            this.googleId = googleId
            return this
        }

        fun withImage(image: String?): Data {
            this.image = image
            return this
        }

        fun withImageName(imageName: String?): Data {
            this.imageName = imageName
            return this
        }

        fun withLanguage(language: String?): Data {
            this.language = language
            return this
        }

        fun withToken(token: String?): Data {
            this.token = token
            return this
        }

        fun withTwitterId(twitterId: String?): Data {
            this.twitterId = twitterId
            return this
        }

        fun withAppleId(appleId: String?): Data {
            this.appleId = appleId
            return this
        }

        fun withPushToken(pushToken: String?): Data {
            this.pushToken = pushToken
            return this
        }
        fun withProfileVerified(profile_verified: String?): Data {
            this.profile_verified = profile_verified
            return this
        }
    }
}