package com.bingalollc.com.model

import com.google.gson.annotations.*


class BlockedUserModel {

    @SerializedName("message")
    @Expose
    private var message: String? = null

    @SerializedName("status")
    @Expose
    private var status: Int? = null

    @SerializedName("data")
    @Expose
    private var data: List<Datum?>? = null

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String?) {
        this.message = message
    }

    fun withMessage(message: String?): BlockedUserModel? {
        this.message = message
        return this
    }

    fun getStatus(): Int? {
        return status
    }

    fun setStatus(status: Int?) {
        this.status = status
    }

    fun withStatus(status: Int?): BlockedUserModel? {
        this.status = status
        return this
    }

    fun getData(): List<Datum?>? {
        return data
    }

    fun setData(data: List<Datum?>?) {
        this.data = data
    }

    fun withData(data: List<Datum?>?): BlockedUserModel? {
        this.data = data
        return this
    }

    class Datum {
        @SerializedName("id")
        @Expose
        var id: String? = null

        @SerializedName("block_to")
        @Expose
        var blockTo: String? = null

        @SerializedName("block_by")
        @Expose
        var blockBy: String? = null

        @SerializedName("block_reason")
        @Expose
        var blockReason: String? = null

        @SerializedName("date")
        @Expose
        var date: String? = null

        @SerializedName("user_det")
        @Expose
        var userDet: Any? = null

        fun withId(id: String?): Datum {
            this.id = id
            return this
        }

        fun withBlockTo(blockTo: String?): Datum {
            this.blockTo = blockTo
            return this
        }

        fun withBlockBy(blockBy: String?): Datum {
            this.blockBy = blockBy
            return this
        }

        fun withBlockReason(blockReason: String?): Datum {
            this.blockReason = blockReason
            return this
        }

        fun withDate(date: String?): Datum {
            this.date = date
            return this
        }

        fun withUserDet(userDet: Any?): Datum {
            this.userDet = userDet
            return this
        }
    }
}