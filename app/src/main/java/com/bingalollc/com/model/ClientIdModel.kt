package com.bingalollc.com.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ClientIdModel {

    @SerializedName("message")
    @Expose
    private var message: String? = null

    @SerializedName("status")
    @Expose
    private var status: Int? = null

    @SerializedName("data")
    @Expose
    private var data: String? = null

    fun getData(): String? {
        return data
    }

    fun setData(data: String?) {
        this.data = data
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String?) {
        this.message = message
    }

    fun withMessage(message: String?): ClientIdModel? {
        this.message = message
        return this
    }

    fun getStatus(): Int? {
        return status
    }

    fun setStatus(status: Int?) {
        this.status = status
    }

    fun withStatus(status: Int?): ClientIdModel? {
        this.status = status
        return this
    }

}