package com.bingalollc.com.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
//import kotlinx.android.parcel.Parcelize

//@Parcelize
class Users(
    @SerializedName("id")
    var id: String? = null,

    @SerializedName("full_name")

    val fullName: String? = null,

    @SerializedName("email")

    val email: String? = null,

    @SerializedName("phone")

    val phone: String? = null,

    @SerializedName("password")

    val password: String? = null,

    @SerializedName("lat")

    val lat: String? = null,

    @SerializedName("lng")

    val lng: String? = null,

    @SerializedName("updated_at")

    val updatedAt: String? = null,

    @SerializedName("created_at")

    val createdAt: String? = null,

    @SerializedName("facebook_id")

    val facebookId: String? = null,

    @SerializedName("google_id")

    val googleId: String? = null,

    @SerializedName("image")

    val image: String? = null,

    @SerializedName("image_name")

    val imageName: String? = null,

    @SerializedName("language")

    val language: String? = null,

    @SerializedName("token")

    val token: String? = null,

    @SerializedName("twitter_id")

    val twitterId: String? = null,

    @SerializedName("push_token")

    val pushToken: String? = null

) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(fullName)
        parcel.writeString(email)
        parcel.writeString(phone)
        parcel.writeString(password)
        parcel.writeString(lat)
        parcel.writeString(lng)
        parcel.writeString(updatedAt)
        parcel.writeString(createdAt)
        parcel.writeString(facebookId)
        parcel.writeString(googleId)
        parcel.writeString(image)
        parcel.writeString(imageName)
        parcel.writeString(language)
        parcel.writeString(token)
        parcel.writeString(twitterId)
        parcel.writeString(pushToken)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Users> {
        override fun createFromParcel(parcel: Parcel): Users {
            return Users(parcel)
        }

        override fun newArray(size: Int): Array<Users?> {
            return arrayOfNulls(size)
        }
    }
}