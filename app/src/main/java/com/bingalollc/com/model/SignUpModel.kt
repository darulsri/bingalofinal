package com.bingalollc.com.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SignUpModel {

    @SerializedName("message")
    @Expose
    private var message: String? = null

    @SerializedName("status")
    @Expose
    private var status: Int? = null

    @SerializedName("data")
    @Expose
    private var data: Data? = null

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String?) {
        this.message = message
    }

    fun withMessage(message: String?): SignUpModel? {
        this.message = message
        return this
    }

    fun getStatus(): Int? {
        return status
    }

    fun setStatus(status: Int?) {
        this.status = status
    }

    fun withStatus(status: Int?): SignUpModel? {
        this.status = status
        return this
    }

    fun getData(): Data? {
        return data
    }

    fun setData(data: Data?) {
        this.data = data
    }

    fun withData(data: Data?): SignUpModel? {
        this.data = data
        return this
    }

    class Data {
        @SerializedName("id")
        @Expose
        var id: String? = null

        @SerializedName("full_name")
        @Expose
        var fullName: String? = null

        @SerializedName("email")
        @Expose
        var email: String? = null

        @SerializedName("phone")
        @Expose
        var phone: String? = null

        @SerializedName("password")
        @Expose
        var password: String? = null

        @SerializedName("lat")
        @Expose
        var lat: String? = null

        @SerializedName("lng")
        @Expose
        var lng: String? = null

        @SerializedName("updated_at")
        @Expose
        var updatedAt: String? = null

        @SerializedName("created_at")
        @Expose
        var createdAt: String? = null

        @SerializedName("facebook_id")
        @Expose
        var facebookId: String? = null

        @SerializedName("google_id")
        @Expose
        var googleId: String? = null

        @SerializedName("image")
        @Expose
        var image: String? = null

        @SerializedName("image_name")
        @Expose
        var imageName: String? = null

        @SerializedName("language")
        @Expose
        var language: String? = null

        @SerializedName("token")
        @Expose
        var token: String? = null

        @SerializedName("twitter_id")
        @Expose
        var twitterId: String? = null

        @SerializedName("apple_id")
        @Expose
        var appleId: String? = null

        @SerializedName("push_token")
        @Expose
        var pushToken: String? = null

        @SerializedName("rating")
        @Expose
        private var rating: Rating? = null
        fun withId(id: String?): Data {
            this.id = id
            return this
        }

        fun withFullName(fullName: String?): Data {
            this.fullName = fullName
            return this
        }

        fun withEmail(email: String?): Data {
            this.email = email
            return this
        }

        fun withPhone(phone: String?): Data {
            this.phone = phone
            return this
        }

        fun withPassword(password: String?): Data {
            this.password = password
            return this
        }

        fun withLat(lat: String?): Data {
            this.lat = lat
            return this
        }

        fun withLng(lng: String?): Data {
            this.lng = lng
            return this
        }

        fun withUpdatedAt(updatedAt: String?): Data {
            this.updatedAt = updatedAt
            return this
        }

        fun withCreatedAt(createdAt: String?): Data {
            this.createdAt = createdAt
            return this
        }

        fun withFacebookId(facebookId: String?): Data {
            this.facebookId = facebookId
            return this
        }

        fun withGoogleId(googleId: String?): Data {
            this.googleId = googleId
            return this
        }

        fun withImage(image: String?): Data {
            this.image = image
            return this
        }

        fun withImageName(imageName: String?): Data {
            this.imageName = imageName
            return this
        }

        fun withLanguage(language: String?): Data {
            this.language = language
            return this
        }

        fun withToken(token: String?): Data {
            this.token = token
            return this
        }

        fun withTwitterId(twitterId: String?): Data {
            this.twitterId = twitterId
            return this
        }

        fun withAppleId(appleId: String?): Data {
            this.appleId = appleId
            return this
        }

        fun withPushToken(pushToken: String?): Data {
            this.pushToken = pushToken
            return this
        }

        fun getRating(): Rating? {
            return rating
        }

        fun setRating(rating: Rating?) {
            this.rating = rating
        }

        fun withRating(rating: Rating?): Data {
            this.rating = rating
            return this
        }
    }

    class Rating {
        @SerializedName("ratings")
        @Expose
        var ratings: String? = null

        fun withRatings(ratings: String?): Rating {
            this.ratings = ratings
            return this
        }
    }
}