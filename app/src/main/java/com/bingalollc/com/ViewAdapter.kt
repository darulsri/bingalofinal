package com.bingalollc.com

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.card.MaterialCardView

class DetailImageViewAdapter(val imageDataModelList: ArrayList<String>, val myListener: (Int) -> Unit): RecyclerView.Adapter<DetailImageViewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.detail_images_items, parent, false))
    }
    private var lastCheckedPosition = 0
    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(imageDataModelList[position])


        if(lastCheckedPosition != position){
            holder.card_view!!.setContentPadding(0,0,0,0)
        }else{
            holder.card_view!!.setContentPadding(5,5,5,5)
        }

    }

    override fun getItemCount(): Int {
        return imageDataModelList.size
    }

  inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
      var card_view : CardView? = null
        @SuppressLint("ResourceAsColor")
        fun bindItems(imageDataModel: String) = with(itemView) {
            val imageView = itemView.findViewById<ImageView>(R.id.imageView)
            card_view = itemView.findViewById<CardView>(R.id.card_view)



            Glide.with(itemView.context).load(imageDataModel).into(imageView)

            setOnClickListener {
                myListener(adapterPosition)
                lastCheckedPosition = adapterPosition
                notifyDataSetChanged()
            }
        }

    }

}