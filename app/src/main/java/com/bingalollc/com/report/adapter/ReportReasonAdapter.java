package com.bingalollc.com.report.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bingalollc.com.R;
import com.bingalollc.com.chat.adapter.ChatAdapter;

import java.util.ArrayList;

public class ReportReasonAdapter extends RecyclerView.Adapter<ReportReasonAdapter.Viewholder> {
    private Context context;
    private ArrayList<String> nameArray;
    private OnReasonChoosed onReasonChoosed;
    private int selectedPosition = -1;

    public ReportReasonAdapter(ArrayList<String> nameArray, OnReasonChoosed onReasonChoosed) {
        this.nameArray = nameArray;
        this.onReasonChoosed = onReasonChoosed;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context=parent.getContext();
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.report_reason_item,parent,false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        holder.textReasons.setText(nameArray.get(position));
        if (selectedPosition==position){
            holder.selectedName.setVisibility(View.VISIBLE);
        }else {
            holder.selectedName.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return nameArray.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        private final TextView textReasons;
        private final RelativeLayout clickLayout;
        private final ImageView selectedName;
        public Viewholder(@NonNull View itemView) {
            super(itemView);
            textReasons=itemView.findViewById(R.id.textReasons);
            clickLayout=itemView.findViewById(R.id.clickLayout);
            selectedName=itemView.findViewById(R.id.selectedName);
            clickLayout.setOnClickListener(view -> {
                selectedPosition = getAdapterPosition();
                onReasonChoosed.onChoose(textReasons.getText().toString());
                notifyDataSetChanged();
            });
        }
    }
    public interface OnReasonChoosed{
        void onChoose(String reason);
    }
}
