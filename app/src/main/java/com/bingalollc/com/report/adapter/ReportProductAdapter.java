package com.bingalollc.com.report.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bingalollc.com.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class ReportProductAdapter extends RecyclerView.Adapter<ReportProductAdapter.Viewholder> {
    private Context context;
    private ArrayList<String> nameArray;
    private ArrayList<Integer> imageArray;
    private OnReasonChoosed onReasonChoosed;
    private int selectedPosition = -1;

    public ReportProductAdapter(ArrayList<String> nameArray, ArrayList<Integer> imageArray, OnReasonChoosed onReasonChoosed) {
        this.nameArray = nameArray;
        this.imageArray = imageArray;
        this.onReasonChoosed = onReasonChoosed;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context=parent.getContext();
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.report_product_item,parent,false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        holder.reportReasons.setText(nameArray.get(position));
        Glide.with(context).load(imageArray.get(position)).into(holder.reportImage);
        if (selectedPosition==position){
            holder.reportSelected.setVisibility(View.VISIBLE);
        }else {
            holder.reportSelected.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return imageArray.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        private final TextView reportReasons;
        private final LinearLayout clickLayout;
        private final ImageView reportImage,reportSelected;
        public Viewholder(@NonNull View itemView) {
            super(itemView);
            reportReasons=itemView.findViewById(R.id.reportReasons);
            clickLayout=itemView.findViewById(R.id.clickLayout);
            reportImage=itemView.findViewById(R.id.reportImage);
            reportSelected=itemView.findViewById(R.id.reportSelected);
            clickLayout.setOnClickListener(view -> {
                selectedPosition = getAdapterPosition();
                onReasonChoosed.onChoose(reportReasons.getText().toString());
                notifyDataSetChanged();
            });
        }
    }
    public interface OnReasonChoosed{
        void onChoose(String reason);
    }
}
