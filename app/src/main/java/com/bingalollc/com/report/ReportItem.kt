package com.bingalollc.com.report

import android.content.DialogInterface
import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.model.ApiStatusModel
import com.bingalollc.com.network.RestClient
import com.bingalollc.com.preference.Common
import com.bingalollc.com.preference.PreferenceManager
import com.bingalollc.com.report.adapter.ReportProductAdapter
import com.bingalollc.com.report.adapter.ReportReasonAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ReportItem : BaseActivity(), ReportProductAdapter.OnReasonChoosed,
    DialogInterface.OnClickListener {
    private var topLayer: LinearLayout? = null
    private var header: TextView? = null
    private var reportedDesc: EditText? = null
    private var back_icon: ImageView? = null
    private var rvReportReasons: RecyclerView? = null
    private var submitBtn: Button? = null
    private var arrayName:ArrayList<String>? = null
    private var arrayImage:ArrayList<Int>? = null
    private var reasonSelected = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report_item)
        initViews()
        initValue()
        importSwipeBack()
    }

    private fun initValue() {
        arrayName= arrayListOf("Prohibited item", "Counterfeit item", "Advertising/soliciting", "Trade/offline transaction", "Inappropriate content", "Hate/offensive","Digital item","Stolen good","Keyword stuffing","Wrong brand/ category","No intent to sell","Other");
        arrayImage?.add(R.drawable.report_item_one)
        arrayImage?.add(R.drawable.report_item_two)
        arrayImage?.add(R.drawable.report_item_three)
        arrayImage?.add(R.drawable.report_item_four)
        arrayImage?.add(R.drawable.report_item_five)
        arrayImage?.add(R.drawable.report_item_six)
        arrayImage?.add(R.drawable.report_item_seven)
        arrayImage?.add(R.drawable.report_item_eight)
        arrayImage?.add(R.drawable.report_item_nine)
        arrayImage?.add(R.drawable.report_item_ten)
        arrayImage?.add(R.drawable.report_item_eleven)
        arrayImage?.add(R.drawable.report_item_twelve)

        val reportAdapter = ReportProductAdapter(arrayName,arrayImage,this)
        val linearLayoutManager = GridLayoutManager(this,3)
        rvReportReasons?.adapter = reportAdapter
        rvReportReasons?.layoutManager = linearLayoutManager
        rvReportReasons?.isNestedScrollingEnabled = false
        submitBtn?.setOnClickListener {   if (!TextUtils.isEmpty(reasonSelected)) uploadDataToServer()}
    }

    private fun initViews() {
        arrayName = ArrayList()
        arrayImage = ArrayList()
        reportedDesc = findViewById(R.id.reportedDesc) as EditText?
        rvReportReasons = findViewById(R.id.rvReportReasons) as RecyclerView?
        topLayer = findViewById(R.id.topLayer) as LinearLayout?
        back_icon = findViewById(R.id.back_icon) as ImageView?
        header = findViewById(R.id.header) as TextView?
        submitBtn = findViewById(R.id.submitBtn) as Button?
        header?.text = getString(R.string.report_item)
        back_icon?.setOnClickListener({ onBackPressed() })
    }

    private fun uploadDataToServer() {
        showLoading(this)
        RestClient.getApiInterface().reportUser(
            Common.selectedProductDetails.id, Common.selectedProductDetails.uploadedByUserId.id?:"",
            reasonSelected,reportedDesc?.text?.toString()?:"",preferenceManager?.userDetails?.id).enqueue(object :
            Callback<ApiStatusModel> {
            override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
                hideLoading()
            }

            override fun onResponse(
                call: Call<ApiStatusModel>,
                response: Response<ApiStatusModel>
            ) {
                hideLoading()
                showConfirmationToast()
            }
        })
    }

    private fun showConfirmationToast() {
        showMessageOKCancel(getString(R.string.product_reported_successfully), "",this,this,"Ok","",false);
    }

    override fun onChoose(reason: String?) {
        reasonSelected = reason?:""
        submitBtn?.alpha = 1.0f
    }

    override fun onClick(p0: DialogInterface?, p1: Int) {
        p0?.dismiss()
        finish()
    }
}