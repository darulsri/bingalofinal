package com.bingalollc.com.report

import android.content.DialogInterface
import android.os.Bundle
import android.text.TextUtils
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.model.ApiStatusModel
import com.bingalollc.com.network.RestClient
import com.bingalollc.com.preference.Common
import com.bingalollc.com.preference.PreferenceManager
import com.bingalollc.com.report.adapter.ReportReasonAdapter
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ReportUser : BaseActivity(), ReportReasonAdapter.OnReasonChoosed,
    DialogInterface.OnClickListener {
    private var topLayer: LinearLayout? = null
    private var header: TextView? = null
    private var reportedDesc: EditText? = null
    private var username: TextView? = null
    private var back_icon: ImageView? = null
    private var userImg: ImageView? = null
    private var submitBtn: Button? = null
    private var arrayName:ArrayList<String>? = null
    private var rvReportReasons:RecyclerView? = null
    private var reasonSelected = ""
    private var usedId = "";
    private var fullName = "";
    private var userImage = "";
    private var productId = "";

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report_user)
        initViews()
        readData()
        setDataValue()
        importSwipeBack()
    }

    private fun readData() {
       usedId = intent.getStringExtra("usedId")?:""
       fullName = intent.getStringExtra("fullName")?:""
       productId = intent.getStringExtra("productId")?:""
       userImage = intent.getStringExtra("userImage")?:""
    }

    private fun setDataValue() {
        username?.text = fullName
        Glide.with(this).load(userImage).error(R.drawable.default_user)
            .placeholder(R.drawable.default_user).into(userImg!!)
        initRecyclerView()
        submitBtn?.setOnClickListener {
            if (!TextUtils.isEmpty(reasonSelected))
                uploadDataToServer()}
    }

    private fun uploadDataToServer() {
        showLoading(this)
        RestClient.getApiInterface().reportUser(productId,usedId?:"",
        reasonSelected,reportedDesc?.text?.toString()?:"",preferenceManager?.userDetails?.id).enqueue(object : Callback<ApiStatusModel>{
            override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
                hideLoading()
            }

            override fun onResponse(
                call: Call<ApiStatusModel>,
                response: Response<ApiStatusModel>
            ) {
                hideLoading()
               showConfirmationToast()
            }
        })
    }

    private fun showConfirmationToast() {
        showMessageOKCancel(getString(R.string.user_reported_successfully),"",this,this,"Ok","",false);
    }

    private fun initRecyclerView() {
        var mOptionAry= arrayListOf("They missed our meeting", "Trouble at meetup","Rude or inappropriate","Problem with items","Messaging probelms","Made a low offer","Shipping problems","None of the above")
        if (preferenceManager.isEnglish == 2)
            mOptionAry= arrayListOf("הם פספסו את הפגישה שלנו", "בעיה במפגש\",\"גס או לא הולם","בעיה בפריטים","בעיות בהודעות","הציע הצעה נמוכה","בעיות משלוח","אף אחד מן הנזכרים מעלה")

        val reportAdapter = ReportReasonAdapter(mOptionAry,this)
        val linearLayoutManager = LinearLayoutManager(this)
        rvReportReasons?.adapter = reportAdapter
        rvReportReasons?.layoutManager = linearLayoutManager
        rvReportReasons?.isNestedScrollingEnabled = false
    }

    private fun initViews() {
        arrayName = ArrayList()
        reportedDesc = findViewById(R.id.reportedDesc) as EditText?
        topLayer = findViewById(R.id.topLayer) as LinearLayout?
        submitBtn = findViewById(R.id.submitBtn) as Button?
        back_icon = findViewById(R.id.back_icon) as ImageView?
        header = findViewById(R.id.header) as TextView?
        userImg = findViewById(R.id.userImg) as ImageView?
        username = findViewById(R.id.username) as TextView?
        rvReportReasons = findViewById(R.id.rvReportReasons) as RecyclerView?
        topLayer?.setBackgroundColor(resources.getColor(R.color.basecolor))
        header?.text = getString(R.string.report_user)
        back_icon?.setOnClickListener({ onBackPressed() })

    }

    override fun onChoose(reason: String?) {
        reasonSelected = reason?:""
        submitBtn?.alpha = 1.0f
    }

    override fun onClick(p0: DialogInterface?, p1: Int) {
        p0?.dismiss()
        finish()
    }
}