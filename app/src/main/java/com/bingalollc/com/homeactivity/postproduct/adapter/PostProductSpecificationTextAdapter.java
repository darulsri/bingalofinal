package com.bingalollc.com.homeactivity.postproduct.adapter;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bingalollc.com.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class PostProductSpecificationTextAdapter extends RecyclerView.Adapter<PostProductSpecificationTextAdapter.Viewholder> {
    private Context context;
    private ArrayList<String> spectString;
    private OnClickedCalled onClickedCalled;

    public PostProductSpecificationTextAdapter(ArrayList<String> spectString, OnClickedCalled onClickedCalled) {
        this.spectString = spectString;
        this.onClickedCalled = onClickedCalled;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.specification_items, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        if (!TextUtils.isEmpty(spectString.get(position))){
            holder.specText.setText(spectString.get(position));
        }

    }

    public void updateText(ArrayList<String> spectString){
        this.spectString = spectString;
        notifyDataSetChanged();

    }

    @Override
    public int getItemCount() {
        return spectString.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        private final TextView specText;
        private final ImageView crossBtn;
        public Viewholder(@NonNull View itemView) {
            super(itemView);
            specText = itemView.findViewById(R.id.specText);
            crossBtn = itemView.findViewById(R.id.crossBtn);
            crossBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickedCalled.onDeleteClicked(getAdapterPosition());
                }
            });
        }
    }

    public interface OnClickedCalled{
        void onDeleteClicked(int position);
    }
}
