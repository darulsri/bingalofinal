package com.bingalollc.com.homeactivity.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bingalollc.com.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class CategoriesFilterAdapter extends RecyclerView.Adapter<CategoriesFilterAdapter.Viewholder> {
    private ArrayList<String> nameArray;
    private ArrayList<String> imageDrawable;
    private Context context;
    private ArrayList<String> selectedString ;
    private OnCatSelected onCatSelected;

    public CategoriesFilterAdapter(ArrayList<String> nameArray, ArrayList<String> imageDrawable, ArrayList<String> selectedString
            , OnCatSelected onCatSelected) {
        this.nameArray = nameArray;
        this.imageDrawable = imageDrawable;
        this.onCatSelected = onCatSelected;
        this.selectedString = selectedString;

    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.categories_items,parent,false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        holder.name.setText(nameArray.get(position));
        Glide.with(context).load(imageDrawable.get(position)).into(holder.imageIcon);

        try{
            for (int i = 0; i < selectedString.size(); i++) {
                if(nameArray.get(position).contains(selectedString.get(i))  && !selectedString.get(i).equals("")){
                    holder.imageIcon.setColorFilter(ContextCompat.getColor(context,
                            R.color.basecolor));
                    holder.name.setTextColor(context.getResources().getColor(R.color.basecolor));
                }

            }

        }catch (Exception e){}

    }

    @Override
    public int getItemCount() {
        return nameArray.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        private final ImageView imageIcon;
        private final TextView name;
        private final LinearLayout layoutCat;
        public Viewholder(@NonNull View itemView) {
            super(itemView);
            imageIcon = itemView.findViewById(R.id.imageIcon);
            name = itemView.findViewById(R.id.name);
            layoutCat = itemView.findViewById(R.id.layoutCat);
            layoutCat.setOnClickListener(view -> {
                if (!selectedString.contains(name.getText().toString())){
                    imageIcon.setColorFilter(ContextCompat.getColor(context,
                            R.color.basecolor));
                    name.setTextColor(context.getResources().getColor(R.color.basecolor));
                    selectedString.add(name.getText().toString());
                }else {
                    imageIcon.setColorFilter(null);
                    name.setTextColor(context.getResources().getColor(R.color.black));
                    selectedString.remove(name.getText().toString());
                }
                onCatSelected.onCategoriesSelected(selectedString);
            });
        }
    }

    public interface OnCatSelected{
        void onCategoriesSelected(ArrayList<String> cat_name);
    }
}
