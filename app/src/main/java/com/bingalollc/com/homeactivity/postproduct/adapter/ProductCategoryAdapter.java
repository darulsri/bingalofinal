package com.bingalollc.com.homeactivity.postproduct.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bingalollc.com.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class ProductCategoryAdapter extends RecyclerView.Adapter<ProductCategoryAdapter.Viewholder> {
    private Context context;
    private ArrayList<String> headerString;
    private ArrayList<String> imageString;
    private OnClickedCalled onClickedCalled;
    private int selectedPos = 0;

    public ProductCategoryAdapter(ArrayList<String> headerString,ArrayList<String> imageString, OnClickedCalled onClickedCalled) {
        this.headerString = headerString;
        this.imageString = imageString;
        this.onClickedCalled = onClickedCalled;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.list_cat_items, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        if (!headerString.isEmpty()) {
            holder.cat_header.setVisibility(View.VISIBLE);
            holder.cat_header.setText(headerString.get(position));
        }

        if (imageString.get(position) != null)
        Glide.with(context).load(imageString.get(position)).placeholder(R.drawable.product_placeholder).error(R.drawable.product_placeholder)
                .into(holder.cat_image);

        if (selectedPos == position) {
            holder.underView.setVisibility(View.VISIBLE);
        } else {
            holder.underView.setVisibility(View.GONE);
        }
    }

    public void updateText(ArrayList<String> spectString){
        this.headerString = spectString;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return headerString.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        private TextView cat_header;
        private ImageView cat_image;
        private View underView;
        private android.widget.LinearLayout layoutItems;
        public Viewholder(@NonNull View itemView) {
            super(itemView);
            cat_header = itemView.findViewById(R.id.cat_header);
            cat_image = itemView.findViewById(R.id.cat_image);
            underView = itemView.findViewById(R.id.underView);
            layoutItems = itemView.findViewById(R.id.layoutItems);
            layoutItems.setOnClickListener(view -> {
                onClickedCalled.onCategorySelected(cat_header.getText().toString(), getAdapterPosition());
                selectedPos = getAdapterPosition();
                notifyDataSetChanged();
            });
        }
    }

    public interface OnClickedCalled{
        void onCategorySelected(String text, int pos);
    }
}
