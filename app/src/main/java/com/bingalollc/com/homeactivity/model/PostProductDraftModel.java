package com.bingalollc.com.homeactivity.model;

import android.net.Uri;

import java.util.ArrayList;

public class PostProductDraftModel {

    private String title;
    private String price;
    private ArrayList<String> specification;
    private ArrayList<String> images;
    private String condition;
    private String choose_a_cat;
    private String choose_a_parent_cat;
    private String description;
    private String lat;
    private String lng;
    private String address;
    private String timeStamp;

    public PostProductDraftModel() {
    }

    public PostProductDraftModel(String title, String price, ArrayList<String> specification, ArrayList<String> images, String condition, String choose_a_cat, String choose_a_parent_cat, String description, String lat, String lng, String address, String timeStamp) {
        this.title = title;
        this.price = price;
        this.specification = specification;
        this.images = images;
        this.condition = condition;
        this.choose_a_cat = choose_a_cat;
        this.choose_a_parent_cat = choose_a_parent_cat;
        this.description = description;
        this.lat = lat;
        this.lng = lng;
        this.address = address;
        this.timeStamp = timeStamp;
    }

    public String getChoose_a_parent_cat() {
        return choose_a_parent_cat;
    }

    public void setChoose_a_parent_cat(String choose_a_parent_cat) {
        this.choose_a_parent_cat = choose_a_parent_cat;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public  ArrayList<String> getSpecification() {
        return specification;
    }

    public void setSpecification( ArrayList<String> specification) {
        this.specification = specification;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getChoose_a_cat() {
        return choose_a_cat;
    }

    public void setChoose_a_cat(String choose_a_cat) {
        this.choose_a_cat = choose_a_cat;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
