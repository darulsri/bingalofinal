package com.bingalollc.com.homeactivity.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Typeface
import android.graphics.drawable.BitmapDrawable
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.preference.Common
import com.bingalollc.com.utils.AppLocationService
import com.bumptech.glide.Glide
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.gms.tasks.Task
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import pub.devrel.easypermissions.EasyPermissions
import java.io.IOException
import java.util.*


class MapFragment : DialogFragment(), OnMapReadyCallback {
    private var topLayer: LinearLayout? = null
    private var LOCATION_PERMISSION = 1000
    private var header: TextView? = null
    private var setLocation: TextView? = null
    private var layoutClickSetLoc: RelativeLayout? = null
    private var mapLayout: RelativeLayout? = null
    private var back_icon: ImageView? = null
    private var imgSatellite: ImageView? = null
    private var showCurrentLocation: ImageView? = null
    private var onMapChoosed: OnMapChoosed? = null
    private var googleMap: GoogleMap? = null
    var latdata: String? = null
    var lngdata: String? = null
    var latti: Double? = null
    var longi: Double? = null
    var mapName: String? = null
        var currentMarker: Marker? = null
    var views: View? = null
    var mapFragment: SupportMapFragment? = null
    var autocompleteFragment: AutocompleteSupportFragment? = null
    private lateinit var baseActivity: BaseActivity
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private var gpsLocation: Location? = null
    var appLocationService: AppLocationService? = null
    var permissionsArrayLocation = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.FullScreenDialogStyle)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (view != null) {
            val parent = view!!.parent as ViewGroup
            parent?.removeView(view)
        }
        views = inflater.inflate(R.layout.fragment_map, container, false)
        baseActivity = BaseActivity()
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context!!);
        mapFragment = childFragmentManager
            .findFragmentById(R.id.mapView) as SupportMapFragment?

        initViews(views)
        initClicks()
        arguments.let { argu ->
            latdata = argu?.getString("latdata")
            lngdata = argu?.getString("lngdata")
            mapName = "My Location"
        }
        mapFragment?.getMapAsync(this)
        if (!Places.isInitialized()) {
                Places.initialize(
                requireContext().getApplicationContext(),
                    getString(R.string.google_android_places_api_key)
              //  "AIzaSyBO2Bc1WAevAJYwlYuZA9vb3phcw-1hB30"
            )
        }
        val placesClient =
            Places.createClient(requireContext())

// Initialize the AutocompleteSupportFragment.
         autocompleteFragment = childFragmentManager.findFragmentById(R.id.place_autocomplete_fragment) as? AutocompleteSupportFragment // Make casting of 'as' to nullable cast 'as?

        autocompleteFragment?.setPlaceFields(
            Arrays.asList(
                Place.Field.ID,
                Place.Field.NAME,
                Place.Field.ADDRESS,
                Place.Field.LAT_LNG
            )
        )


        autocompleteFragment?.setOnPlaceSelectedListener(object :
            PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {
                // TODO: Get info about the selected place.
                autocompleteFragment?.view?.findViewById<EditText>(R.id.places_autocomplete_search_input)?.setText(place.address)
                Log.i("fffff", "Place: " + place.name + ", " + place.id)
                val cityName = place.name.toString()
                val latLng = place.latLng
                latti = latLng!!.latitude
                longi = latLng!!.longitude
                Common.productLocationLat = latti.toString()
                Common.productLocationLng = longi.toString()
                createMarker(latti!!,longi!!,place.address)
                moveCamera(latti!!,longi!!,place.address?:"")
                currentMarker?.showInfoWindow()
                setLocation?.visibility = View.VISIBLE
            }

            override fun onError(status: Status) {
                // TODO: Handle the error.
                Log.i("asas", "An error occurred: $status")
            }
        })
        autocompleteFragment?.view?.background = resources.getDrawable(R.drawable.curved_background_grey)
        autocompleteFragment?.setHint(getString(R.string.enter_city_town_or_address))
        autocompleteFragment?.view?.findViewById<EditText>(R.id.places_autocomplete_search_input)?.textSize = 16.0f
        autocompleteFragment?.view?.findViewById<EditText>(R.id.places_autocomplete_search_input)?.setPadding(0,0,20,0)


        val typeface: Typeface = ResourcesCompat.getFont(requireContext(), R.font.avenir_next_font)!!
        autocompleteFragment?.view?.findViewById<EditText>(R.id.places_autocomplete_search_input)?.setTypeface(typeface)



        Handler(Looper.myLooper()!!).postDelayed({
            getLocation()
        }, 1000)

        setLocation?.setOnClickListener {
/*            Common.lat = latti.toString()
            Common.lng = longi.toString()*/
            onMapChoosed?.onMapChoosed()
            dismiss()
        }
        layoutClickSetLoc?.setOnClickListener {
            onMapChoosed?.onMapChoosed()
            dismiss()
        }
        return views
    }


    private fun initClicks() {
        showCurrentLocation?.setOnClickListener({
            baseActivity?.preventDoubleTap(showCurrentLocation)
            if (EasyPermissions.hasPermissions(requireActivity(), *permissionsArrayLocation)) {
                baseActivity?.showLoading(requireContext())
                if (gpsLocation!=null) {
                    var addresss =
                        baseActivity?.getAddress(gpsLocation?.latitude ?: 0.0, gpsLocation?.longitude ?: 0.0,context,false)
                    Common.productLocationLat = gpsLocation?.latitude.toString()
                    Common.productLocationLng = gpsLocation?.longitude.toString()
                    createMarker(
                        gpsLocation?.latitude ?: 0.0,
                        gpsLocation?.longitude ?: 0.0,
                        addresss
                    )
                    currentMarker?.showInfoWindow()
                    moveCamera(latti!!, longi!!, addresss ?: "")
                    autocompleteFragment?.view?.findViewById<EditText>(R.id.places_autocomplete_search_input)
                        ?.setText(addresss)
                }else{
                  getLocation()
                }

            } else {
                EasyPermissions.requestPermissions(
                    this, resources.getString(R.string.please_allow_permissions),
                    101, *permissionsArrayLocation
                )
            }
        })

        imgSatellite?.setOnClickListener({
            if(googleMap?.mapType == GoogleMap.MAP_TYPE_NORMAL){
                Glide.with(this).load(R.drawable.sattelite_selected2x).into(imgSatellite!!)
                googleMap?.setMapType(GoogleMap.MAP_TYPE_SATELLITE)
            }else{
                Glide.with(this).load(R.drawable.sattelite_unselected2x).into(imgSatellite!!)
                googleMap?.setMapType(GoogleMap.MAP_TYPE_NORMAL)

            }
        })

    }

    private fun initViews(view: View?) {
        topLayer = view?.findViewById(R.id.topLayer)
        back_icon = view?.findViewById(R.id.back_icon)
        imgSatellite = view?.findViewById(R.id.imgSatellite)
        setLocation = view?.findViewById(R.id.setLocation)
        layoutClickSetLoc = view?.findViewById(R.id.layoutClickSetLoc)
        mapLayout = view?.findViewById(R.id.mapLayout)
        header = view?.findViewById(R.id.header)
        showCurrentLocation = view?.findViewById(R.id.currentLocation)

        imgSatellite?.visibility = View.VISIBLE
        topLayer?.setBackgroundColor(requireContext().resources.getColor(R.color.basecolor))
        header?.text = getString(R.string.edit_your_loc)
        back_icon?.setOnClickListener({ dismiss() })
    }

    private fun getLocation() {
        checkGpsOn()
    }


    @SuppressLint("MissingPermission")
    private fun checkGpsOn() {
        if (context?.getSystemService(BaseActivity.LOCATION_SERVICE) is LocationManager){
            val locationManager =
                context?.getSystemService(BaseActivity.LOCATION_SERVICE) as LocationManager
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                displayLocationSettingsRequest(requireContext())
            } else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                try {
                    val task: Task<Location> = fusedLocationProviderClient.lastLocation
                    task.addOnSuccessListener({ location ->
                        if (location != null) {
                            gpsLocation = location
                            Common.productLocationLat = gpsLocation?.latitude.toString()
                            Common.productLocationLng = gpsLocation?.longitude.toString()
                        }
                    })
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        }

        baseActivity?.hideLoading()
    }

    private fun displayLocationSettingsRequest(context: Context) {
        val googleApiClient = GoogleApiClient.Builder(context)
            .addApi(LocationServices.API).build()
        googleApiClient.connect()
        val locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 10000
        locationRequest.fastestInterval = 10000 / 2.toLong()
        val builder =
            LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        builder.setAlwaysShow(true)
        val result =
            LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build())
        result.setResultCallback { result ->
            val status = result.status
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> Log.i(
                    "TAG1",
                    "All location settings are satisfied."
                )
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                    Log.i(
                        "TAG2",
                        "Location settings are not satisfied. Show the user a dialog to upgrade location settings "
                    )
                    try {
                        // Show the dialog by calling startResolutionForResult(), and check the result
                        // in onActivityResult().
                        status.startResolutionForResult(
                            requireActivity(),
                            LOCATION_PERMISSION
                        )
                    } catch (e: IntentSender.SendIntentException) {
                        Log.i("TAG3", "PendingIntent unable to execute request.")
                    }
                }
                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> Log.i(
                    "Tag4",
                    "Location settings are inadequate, and cannot be fixed here. Dialog not created."
                )
            }
        }
    }

    companion object {
        @JvmStatic
        fun show(context: FragmentActivity, onMapChoosed: OnMapChoosed) {
            val fragment = MapFragment()
            fragment.onMapChoosed = onMapChoosed
            val bundle = Bundle()
            bundle.putString("latdata", Common.productLocationLat)
            bundle.putString("lngdata", Common.productLocationLng)
            fragment.arguments = bundle
            val transaction = context.supportFragmentManager.beginTransaction()
            fragment.show(transaction, "dialog")
        }

    }

    interface OnMapChoosed{
        fun onMapChoosed()
    }



    private fun moveCamera(lat: Double, lng:Double,name:String) {
        val builder = LatLngBounds.Builder();
       // builder.include(createMarker(lat, lng,name)?.getPosition());
      //  val bounds = builder.build();
        //val cu = CameraUpdateFactory.newLatLngBounds(LatLng(lat, lng), 5);
        val cu =
            CameraUpdateFactory.newLatLngZoom(LatLng(lat, lng), 10f)

        googleMap?.moveCamera(cu)
//        googleMap?.moveCamera(cu)
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        mapLayout?.visibility = View.VISIBLE
        baseActivity?.hideLoading()
        googleMap?.isMyLocationEnabled=true
    }


    protected fun createMarker(
        latitude: Double,
        longitude: Double,
        name: String?
    ): Marker? {
        val height = 80;
        val width = 40;
        val bitmapdraw = getResources().getDrawable(R.drawable.map_pin) as BitmapDrawable;
        val b = bitmapdraw.getBitmap();
        val smallMarker = Bitmap.createScaledBitmap(b, width, height, false)
        val latLng = LatLng(latitude, longitude)
        currentMarker?.remove()
         currentMarker= googleMap!!.addMarker(
            MarkerOptions()
                .position(LatLng(latitude, longitude))
             //   .anchor(1.5f, 1.5f)
                .title(name)
                .draggable(true)
                .icon(BitmapDescriptorFactory.fromBitmap(smallMarker))
         )
        latti = latitude
        longi = longitude
        Common.currentLat = latti.toString()
        Common.currentLng = longi.toString()
        return currentMarker
    }
    override fun onDestroyView() {
        super.onDestroyView()
        if (Places.isInitialized()){
            Places.deinitialize();
        }
   /*     if (mapFragment != null)
            childFragmentManager?.beginTransaction()?.remove(mapFragment!!)?.commit();
        if (autocompleteFragment != null)
           childFragmentManager?.beginTransaction()?.remove(autocompleteFragment!!)?.commit();*/
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        if (Places.isInitialized()){
            Places.deinitialize();
        }
        googleMap?.clear()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode==LOCATION_PERMISSION ){
            getLocation()
        }
    }

    override fun onMapReady(googleMaps: GoogleMap) {
        googleMap=googleMaps;
        val latti=latdata?.replace(":","");
        val longi=lngdata?.replace(":","");

        if (latti != null && !latti.equals("") &&
            longi != null && !longi.equals("")
        ) {

            val lat = latdata?.toDouble()
            val lng = lngdata?.toDouble()
            val name = mapName;
            //  createMarker(lat!!, lng!!,name);
            if (gpsLocation != null) {
                moveCamera(gpsLocation?.latitude!!, gpsLocation?.longitude!!, name!!)
            } else {
                moveCamera(lat!!, lng!!, name!!)
            }
        }

        googleMap?.setOnMarkerDragListener(object : GoogleMap.OnMarkerDragListener {
            override fun onMarkerDragStart(arg0: Marker) {

            }

            override fun onMarkerDrag(arg0: Marker) {
                val message = arg0!!.position.latitude.toString() + "" + arg0.position.longitude.toString()
                Log.d("TAG>>>>>>? " + "_DRAG", message)
            }

            override fun onMarkerDragEnd(arg0: Marker) {
                // googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(arg0.position, 1.0f))
                val message = arg0.position.latitude.toString() + "" + arg0.position.longitude.toString()
                // currentMarker?.remove()
                // createMarker(arg0.position.latitude,arg0.position.longitude,mapName)
                Log.d("TAG>>>>>> " + "_END", message)
            }

        })
    }
}