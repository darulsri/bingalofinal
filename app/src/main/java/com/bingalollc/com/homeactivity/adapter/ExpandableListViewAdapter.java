package com.bingalollc.com.homeactivity.adapter;


import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bingalollc.com.R;
import com.bingalollc.com.homeactivity.model.ProductModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
public class ExpandableListViewAdapter extends BaseExpandableListAdapter {
    private Context context;
    // group titles
    private List<String> listDataGroup;
    private List<String> listDataGroupImage;
    private List<String> cat_selected_array;
    // child data
    private HashMap<String, ArrayList<String>> listDataChild;
    public ExpandableListViewAdapter(Context context, List<String> listDataGroup,List<String> listDataGroupImage,
                                     List<String> cat_selected_array,
                                     HashMap<String, ArrayList<String>> listChildData) {
        this.context = context;
        this.listDataGroup = listDataGroup;
        this.cat_selected_array = cat_selected_array;
        this.listDataGroupImage = listDataGroupImage;
        this.listDataChild = listChildData;
    }
    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this.listDataChild.get(this.listDataGroup.get(groupPosition))
                .get(childPosititon);
    }
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }
    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        final String childText = (String) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.expandable_list_row_child, null);
        }
        TextView textViewChild = convertView
                .findViewById(R.id.textViewChild);
        ImageView checkMark = convertView
                .findViewById(R.id.checkMark);
        textViewChild.setText(childText);
        if (cat_selected_array.contains(childText)) {
            checkMark.setVisibility(View.VISIBLE);
        } else {
            checkMark.setVisibility(View.GONE);
        }
        return convertView;
    }
    @Override
    public int getChildrenCount(int groupPosition) {
        return this.listDataChild.get(this.listDataGroup.get(groupPosition))
                .size();
    }
    @Override
    public Object getGroup(int groupPosition) {
        return this.listDataGroup.get(groupPosition);
    }
    public Object getGroupImage(int groupPosition) {
        return this.listDataGroupImage.get(groupPosition);
    }
    @Override
    public int getGroupCount() {
        return this.listDataGroup.size();
    }
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        String groupImage = (String) getGroupImage(groupPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.expandable_list_row_group, null);
        }
        TextView textViewGroup = convertView
                .findViewById(R.id.textViewGroup);
        ImageView groupImages = convertView
                .findViewById(R.id.groupImage);
        Glide.with(parent).load(groupImage).placeholder(R.drawable.product_placeholder).error(R.drawable.product_placeholder).into(groupImages);
        textViewGroup.setText(headerTitle);
        return convertView;
    }
    @Override
    public boolean hasStableIds() {
        return false;
    }
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
