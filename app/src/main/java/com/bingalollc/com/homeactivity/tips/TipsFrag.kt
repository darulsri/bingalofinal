package com.bingalollc.com.homeactivity.tips

import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import androidx.viewpager.widget.ViewPager
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.databinding.TipsOverlayBinding
import com.bingalollc.com.homeactivity.ViewPager.ImageTipsFragment
import com.bingalollc.com.homeactivity.ViewPager.ImagesFragment
import com.bingalollc.com.homeactivity.ViewPager.ViewPagerAdapter
import com.bingalollc.com.preference.PreferenceManager
import com.bingalollc.com.utils.ViewBindingDialogFragment
import com.bumptech.glide.Glide


class TipsFrag : ViewBindingDialogFragment<TipsOverlayBinding>() {
    private var tipImagePosition = 0;
    private var preferenceManager: PreferenceManager? = null
    private var baseActivity: BaseActivity? = null
    var fragmentList: ArrayList<ImageTipsFragment>? = null
    var currentPage = 0
    private var onDismissTipsDialog: OnDismissTipsDialog?=null

    override fun provideBinding(inflater: LayoutInflater): TipsOverlayBinding {
       return TipsOverlayBinding.inflate(inflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.FullScreenTransparentDialogStyle)

    }

    override fun onDismiss(dialog: DialogInterface) {
        onDismissTipsDialog?.onTipsDialogDismissed()
        super.onDismiss(dialog)

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        preferenceManager = PreferenceManager(requireContext())
        baseActivity = BaseActivity()
        setUpViewPager()
        initClicks()
    }

    private fun initClicks() {
        with(binding) {
            rightBtn.setOnClickListener({
                if (tipImagePosition < 2) {
                    tipImagePosition++
                    changeImage(tipImagePosition)
                }
            })
            leftBtn.setOnClickListener({
                if (tipImagePosition > 0) {
                    tipImagePosition--
                    changeImage(tipImagePosition)
                }
            })
            skipBtn.setOnClickListener({
                preferenceManager?.isCameraTipsViewed = true
                dismiss()
            })
        }
    }

    private fun changeImage(position: Int) {
        with(binding) {
            vpImages.setCurrentItem(position)
            if (position == 2) {
                rightBtn.visibility = View.GONE
                skipBtn.setText(getString(R.string.done))
            } else {
                rightBtn.visibility = View.VISIBLE
                skipBtn.setText(getString(R.string.skip))
            }
            if (position > 0) {
                leftBtn.visibility = View.VISIBLE
            } else if (position == 0) {
                leftBtn.visibility = View.GONE
            }
        }

    }


    private fun setUpViewPager() {
        fragmentList = ArrayList()
        fragmentList?.add(addFrag(0)!!)
        fragmentList?.add(addFrag(1)!!)
        fragmentList?.add(addFrag(2)!!)


        val basePagerAdapter = TipsViewPagerAdapter(
            getChildFragmentManager(),
            fragmentList,
            dpToPixels(requireContext()),
            getActivity()
        )
        //  vpImages!!.offscreenPageLimit = 3
        binding.vpImages!!.adapter = basePagerAdapter
        basePagerAdapter.notifyDataSetChanged()
        binding.cipTutorial!!.setViewPager(binding.vpImages)
  /*
        binding.vpImages!!.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                currentPage = position
            }

            override fun onPageSelected(position: Int) {
                // selectedPageIndex = position;
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })*/
    /*    val h = Handler(Looper.getMainLooper())
        val r: Runnable = object : Runnable {
            override fun run() {
                if (currentPage >= (fragmentList?.size ?: 0)) currentPage = -1
                binding.vpImages?.setCurrentItem(currentPage++, true)
                h.postDelayed(this, 3000)
            }
        }
        h.postDelayed(r, 1000)*/

    }


    private fun addFrag(tipsImgPos: Int): ImageTipsFragment? {
        val bundle = Bundle()
        bundle.putInt("tipsImgPos", tipsImgPos)
        val imagesFragment = ImageTipsFragment(preferenceManager)
        imagesFragment.arguments = bundle
        return imagesFragment
    }

    private fun dpToPixels(context: Context): Float {
        return 2 * context.resources.displayMetrics.density
    }

    fun setTipsDialogDismiss(onDismissTipsDialog: OnDismissTipsDialog) {
        this.onDismissTipsDialog = onDismissTipsDialog
    }

    interface OnDismissTipsDialog {
        fun onTipsDialogDismissed()
    }

}