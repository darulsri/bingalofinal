package com.bingalo.com.homeactivity.model

import android.icu.text.CaseMap.Title
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class homebannerModel  {
    class data {
        @SerializedName("id")
        @Expose
        private var id: Int? = null

        @SerializedName("title")
        @Expose
        private var title: String? = null

        @SerializedName("image")
        @Expose
        private var image: String? = null

        @SerializedName("detail")
        @Expose
        private var detail: String? = null

        @SerializedName("link")
        @Expose
        private var link: String? = null

        @SerializedName("created_at")
        @Expose
        private var created_at: String? = null

        @SerializedName("updated_at")
        @Expose
        private var updated_at: String? = null


        fun setId(): Int? {
            return id
        }
            fun getId(): Int? {
                return id

        }

        fun withId(id: Int): data? {
            this.id = id
            return this
        }

        fun getTitle(): String? {
            return title
        }

        fun setTitle(): String? {
            return title
        }

        fun withTitle(title: String): data? {
            this.title = title
            return this
        }

        fun getImage(): String? {
            return image
        }

        fun setImage(): String? {
            return image
        }

        fun withImage(image: String): data? {
            this.image = image
            return this
        }

        fun getDetail(): String? {
            return detail
        }

        fun setDetail(): String? {
            return detail
        }

        fun withDetails(detail: String): data {
            this.detail = detail
            return this
        }

        fun getLink(): String? {
            return link
        }

        fun setLink(): String? {
            return link
        }

        fun withLink(link: String): data {
            this.link = link
            return this
        }

        fun getCreated_at(): String? {
            return created_at
        }

        fun setCreated(): String? {
            return created_at
        }

        fun withCreated(created_at: String):data {
            this.created_at = created_at
            return this
        }

        fun getUpdated_at(): String? {
            return updated_at
        }

        fun setUpdated_at(): String? {
            return updated_at
        }

        fun withUpdated_at(updated_at: String): data {
            this.updated_at = updated_at
            return this
        }
    }

    @SerializedName("message")
    @Expose
    private var message: String? =null

    @SerializedName("status")
    @Expose
    private var status: Int? =null

    @SerializedName("data")
    @Expose
    private var Data: List<data> = emptyList()

    fun getMessage():String?{
        return message
    }

    fun setMessage():String?{
        return message
    }

    fun withMessage(message:String):homebannerModel?{
        this.message=message
        return this
    }
    fun getStatus():Int?{
        return status
    }
    fun setStatus():Int?{
        return status
    }
    fun withStatus(status:Int):homebannerModel?{
        this.status=status
        return this
    }

    fun getData():List<data>?{
        return Data
    }
    fun setData():List<data>?{
        return Data
    }
    fun withData(data:List<data>):homebannerModel?{
        this.Data=data
        return this
    }


}

