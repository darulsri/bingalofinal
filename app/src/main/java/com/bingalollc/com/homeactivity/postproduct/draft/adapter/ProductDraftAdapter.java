package com.bingalollc.com.homeactivity.postproduct.draft.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bingalollc.com.R;
import com.bingalollc.com.homeactivity.model.PostProductDraftModel;
import com.bingalollc.com.utils.Cache;
import com.bumptech.glide.Glide;

import java.io.File;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ProductDraftAdapter extends RecyclerView.Adapter<ProductDraftAdapter.Viewholder> {
    private Context context;
    private ArrayList<PostProductDraftModel> productDraftModels;
    private OnClickedCalled onClickedCalled;
    private com.bingalollc.com.utils.Cache cache;

    public ProductDraftAdapter(ArrayList<PostProductDraftModel> productDraftModels, OnClickedCalled onClickedCalled) {
        this.productDraftModels = productDraftModels;
        this.onClickedCalled = onClickedCalled;
    }

    public void refreshData(ArrayList<PostProductDraftModel> productDraftModels){
        this.productDraftModels = productDraftModels;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        cache = new Cache();
        View view = LayoutInflater.from(context).inflate(R.layout.product_draft_item, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        if (!TextUtils.isEmpty(productDraftModels.get(position).getTitle()))
            holder.titleName.setText(productDraftModels.get(position).getTitle());

        if (!TextUtils.isEmpty(productDraftModels.get(position).getTimeStamp()))
            holder.timeOfSaving.setText(productDraftModels.get(position).getTimeStamp());

        if (productDraftModels.get(position) != null && productDraftModels.get(position).getImages() != null &&
                productDraftModels.get(position).getImages().size() > 0) {
        //    File file1 = cache.getFileByFileName(productDraftModels.get(position).getImages().get(0), context);
         //   String filePath = file1.getAbsolutePath();
            String filePath = productDraftModels.get(position).getImages().get(0);
            System.out.println(">>>>>>>>>>FILEPATH "+filePath);
            Bitmap bitmap = BitmapFactory.decodeFile(filePath);
            System.out.println(">>>>>>>>>>FILEPATHbitmap "+bitmap);
            Glide.with(context).load(bitmap).into(holder.savedImage);
        }
    }


    @Override
    public int getItemCount() {
        return productDraftModels.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        private TextView titleName,timeOfSaving;
        private ImageView savedImage;
        private RelativeLayout draftLayout;
        public Viewholder(@NonNull View itemView) {
            super(itemView);
            savedImage = itemView.findViewById(R.id.savedImage);
            titleName = itemView.findViewById(R.id.titleName);
            timeOfSaving = itemView.findViewById(R.id.timeOfSaving);
            draftLayout = itemView.findViewById(R.id.draftLayout);
            draftLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickedCalled.onItemClicked(getAdapterPosition());
                }
            });
        }
    }

    public interface OnClickedCalled{
        void onItemClicked(int pos);
    }
}
