package com.bingalo.com.homeactivity.homefragment.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
//import com.bingalo.com.R
import com.bingalo.com.homeactivity.model.homebannerModel
import com.bingalollc.com.R
import com.bumptech.glide.Glide

class homapageadapter(
    val context: Context, val homebanner: ArrayList<homebannerModel.data>?
) : RecyclerView.Adapter<homapageadapter.homeViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): homeViewHolder {
        val itemView =LayoutInflater.from(parent.context).inflate(R.layout.homebanner,parent,false)
        return homeViewHolder(itemView)

    }

    override fun getItemCount(): Int {
        return homebanner?.size!!
    }


    override fun onBindViewHolder(holder: homeViewHolder, position: Int) {
        val currentBanner = homebanner?.get(position)


        Glide.with(context).load(currentBanner?.getImage()).placeholder(R.drawable.product_placeholder).
        error(R.drawable.product_placeholder).into(holder.banner)
        holder.title.text= currentBanner?.getTitle()
        holder.detail.text = currentBanner?.getDetail()

//        Glide.with(context).load(shop_a.store_image).placeholder(R.drawable.product_placeholder).
//        error(R.drawable.product_placeholder).into(holder.Icon)

    }

    class homeViewHolder(itemView:View):RecyclerView.ViewHolder(itemView){
        val banner:ImageView = itemView.findViewById(R.id.banner)
        val title :TextView = itemView.findViewById(R.id.title)
        val detail : TextView = itemView.findViewById(R.id.detail)
    }
}


