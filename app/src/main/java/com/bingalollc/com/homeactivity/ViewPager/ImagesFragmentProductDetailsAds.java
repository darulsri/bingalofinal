package com.bingalollc.com.homeactivity.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;

import com.bingalollc.com.R;
import com.bingalollc.com.preference.Common;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

/**
 * A simple {@link Fragment} subclass.
 */
public class ImagesFragmentProductDetailsAds extends Fragment implements View.OnClickListener {

    //private AppCompatImageView ivTutorial;
    private AppCompatImageView imageButton;
    private RelativeLayout product_ad;
    private View vpImageView;
    private ProgressBar ivProgressBar;
    private int idHeading, idSubHeading, idDescription, idBoldStr;
    private Context mContext;
    private String idTutsBg;
    private String idTutsBgUrl;
    private boolean showTransparentLayout=false;


    /**
     * constructor
     */
    public ImagesFragmentProductDetailsAds() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_images_product_details_ads, container, false);
        init(view);
        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * initialising components
     */
    private void init(final View view) {
        //ivTutorial = view.findViewById(R.id.ivTutorial);
        imageButton = view.findViewById(R.id.ivTutorial);
        vpImageView = view.findViewById(R.id.vpImageView);
        ivProgressBar = view.findViewById(R.id.ivProgressBar);
        product_ad = view.findViewById(R.id.product_ad);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        requireActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
       // product_ad.setLayoutParams(new RelativeLayout.LayoutParams(width, width));



        getBundleData();
    }

    /**
     * get Bundle Data
     */
    private void getBundleData() {

        Bundle bundle = getArguments();

        if (getArguments() != null) {
            idTutsBg = bundle.getString("tutImages");
            idTutsBgUrl = bundle.getString("tutsImageUrl");
            if ( bundle.containsKey("showTransparentLayout"))
                showTransparentLayout = bundle.getBoolean("showTransparentLayout");
        }

        setData();
    }


    /**
     * set Data
     */
    private void setData() {
       /* try {
            imageButton.setImageResource(idTutsBg);
        }catch (Exception e){}*/
        if (showTransparentLayout){
            vpImageView.setVisibility(View.VISIBLE);
        }else {
            vpImageView.setVisibility(View.GONE);
        }
        ivProgressBar.setVisibility(View.VISIBLE);
         Glide.with(mContext).load(idTutsBg).listener(new RequestListener<Drawable>() {
             @Override
             public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                 imageButton.setImageDrawable(mContext.getDrawable(R.drawable.placeholder));
                 return false;
             }

             @Override
             public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                 ivProgressBar.setVisibility(View.GONE);
                 return false;
             }
         }).placeholder(R.drawable.placeholder).error(R.drawable.placeholder)
                 .into(imageButton);
        imageButton.setOnClickListener(this);
    }

    @Override
    public void onClick(final View view) {
        if (!showTransparentLayout) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(idTutsBgUrl));
// Always use string resources for UI text. This says something like "Share this photo with"
            String title = getResources().getText(R.string.chooser_title).toString();
// Create and start the chooser
            Intent chooser = Intent.createChooser(intent, title);
            startActivity(chooser);
        }else {
            Common.getInstance().onClickedViewPager.setValue(idTutsBg);
        }
    }

    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        mContext = context;
    }
}
