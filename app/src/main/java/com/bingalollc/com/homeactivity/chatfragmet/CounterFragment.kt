package com.bingalollc.com.homeactivity.chatfragmet

import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.databinding.FragmentCounterBinding
import com.bingalollc.com.utils.ViewBindingDialogFragment


class CounterFragment(private val onCounterSendClick: OnCounterSendClick) : ViewBindingDialogFragment<FragmentCounterBinding>() {
    private var baseActivity: BaseActivity?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        baseActivity = BaseActivity()
        with(binding){
            crossBtn.setOnClickListener {
                baseActivity?.hideKeyBoardOfView(requireActivity(), priceText)
                dismiss() }

            sendBtn.setOnClickListener {
                baseActivity?.hideKeyBoardOfView(requireActivity(), priceText)
                onCounterSendClick.sendCounterOffer(binding.priceText.text.toString())
                dismiss()
            }

            priceText.addTextChangedListener {
                if (priceText.text.toString().length < 1){
                    sendBtn.alpha = 0.6f
                    sendBtn.isEnabled = false
                } else {
                    sendBtn.alpha = 1f
                    sendBtn.isEnabled = true
                }
            }
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        baseActivity?.hideKeyboard(requireActivity())
        super.onDismiss(dialog)
    }

    override fun provideBinding(inflater: LayoutInflater): FragmentCounterBinding {
       return FragmentCounterBinding.inflate(inflater)
    }


    interface OnCounterSendClick{
        fun sendCounterOffer(counterText: String)
    }
}