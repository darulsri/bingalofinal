package com.bingalollc.com.homeactivity.postproduct.draft

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.databinding.ActivityDraftBinding
import com.bingalollc.com.homeactivity.model.PostProductDraftModel
import com.bingalollc.com.homeactivity.postproduct.PostProduct
import com.bingalollc.com.homeactivity.postproduct.draft.adapter.ProductDraftAdapter
import com.bingalollc.com.preference.PreferenceManager
import com.bingalollc.com.report.ReportUser
import com.bingalollc.com.utils.SwipeHelper

class DraftActivity : BaseActivity(), ProductDraftAdapter.OnClickedCalled {
    private lateinit var binding: ActivityDraftBinding
    private var postProductDraftModel: ArrayList<PostProductDraftModel>? = null
    private var productDraftAdapter: ProductDraftAdapter? = null
    private var swipeHelper: SwipeHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_draft)
        binding.appBar.header.setText(getString(R.string.saved_draft))
        binding.appBar.backIcon.setOnClickListener { finish() }
        postProductDraftModel = preferenceManager?.postProductFromDraft
        initRecyclerView()
        importSwipeBack()

    }

    private fun initRecyclerView() {
        productDraftAdapter = ProductDraftAdapter(postProductDraftModel, this)
        val linearLayoutManager = LinearLayoutManager(this)
        binding.rvSavedDraft.adapter = productDraftAdapter
        binding.rvSavedDraft.layoutManager = linearLayoutManager
        //        recyclerView.setNestedScrollingEnabled(false);
        setSwipeForRecyclerView()
    }

    private fun setSwipeForRecyclerView() {
        swipeHelper = object : SwipeHelper(this, binding.rvSavedDraft, false) {
            override fun instantiateUnderlayButton(
                viewHolder: RecyclerView.ViewHolder,
                underlayButtons: MutableList<UnderlayButton>
            ) {
                var deleteIcon =
                    BitmapFactory.decodeResource(resources, R.drawable.btn_delete_white)
                deleteIcon = Bitmap.createScaledBitmap(deleteIcon!!, 100, 120, false)
                underlayButtons.add(UnderlayButton(
                    "Delete",
                    deleteIcon,
                    getResources().getColor(R.color.dark_red)
                ) { pos ->
                    val data = preferenceManager?.postProductFromDraft
                    data?.removeAt(pos)
                    preferenceManager?.saveDraft(data)
                    postProductDraftModel = preferenceManager?.postProductFromDraft
                    productDraftAdapter?.refreshData(postProductDraftModel)
                })
            }
        }
    }

    override fun onItemClicked(pos: Int) {
        val postProduct = PostProduct()
        postProduct?.setAllFieldsFromDraft(true, pos)
        val manager: FragmentManager = this.supportFragmentManager
        val transaction = manager.beginTransaction()
        transaction.add(R.id.fragment_container, postProduct!!, "postProduct")
        transaction.addToBackStack("postProduct")
        transaction.commit()

    }
}