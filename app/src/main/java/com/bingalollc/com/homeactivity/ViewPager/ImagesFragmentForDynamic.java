package com.bingalollc.com.homeactivity.ViewPager;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;

import com.bingalollc.com.R;
import com.bumptech.glide.Glide;

/**
 * A simple {@link Fragment} subclass.
 */
public class ImagesFragmentForDynamic extends Fragment implements View.OnClickListener {

    //private AppCompatImageView ivTutorial;
    private AppCompatImageView imageButton;
    private int idHeading, idSubHeading, idDescription, idBoldStr;
    private Context mContext;
    private String idTutsBg;


    /**
     * constructor
     */
    public ImagesFragmentForDynamic() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_images, container, false);
        init(view);
        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * initialising components
     */
    private void init(final View view) {


        //ivTutorial = view.findViewById(R.id.ivTutorial);
        imageButton = view.findViewById(R.id.ivTutorial);


        getBundleData();
    }

    /**
     * get Bundle Data
     */
    private void getBundleData() {

        Bundle bundle = getArguments();

        if (getArguments() != null) {
            idTutsBg = bundle.getString("tutImages");
        }

        setData();
    }


    /**
     * set Data
     */
    private void setData() {
       /* try {
            imageButton.setImageResource(idTutsBg);
        }catch (Exception e){}*/

         Glide.with(mContext).load(idTutsBg).into(imageButton);
    }

    @Override
    public void onClick(final View view) {

    }

    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        mContext = context;
    }
}
