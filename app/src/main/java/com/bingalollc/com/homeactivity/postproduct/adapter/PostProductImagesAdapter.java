package com.bingalollc.com.homeactivity.postproduct.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bingalollc.com.R;
import com.bumptech.glide.Glide;
import com.ernestoyaquello.dragdropswiperecyclerview.DragDropSwipeAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class PostProductImagesAdapter extends DragDropSwipeAdapter<Uri, PostProductImagesAdapter.PostProductImagesViewholder> {
    private final Context context;
    private int imageSelected;
    private final int totalImages;
    private ArrayList<Uri> arrayListImages;
    private final OnImageClicked onImageClicked;
    List<Uri> actualList = new ArrayList<>();

    public PostProductImagesAdapter(Context context, int imageSelected, int totalImages, ArrayList<Uri> arrayListImages, OnImageClicked onImageClicked) {
        this.context = context;
        this.imageSelected = imageSelected;
        this.onImageClicked = onImageClicked;
        this.totalImages = totalImages;
        this.arrayListImages = arrayListImages;
        setDataSet();
    }

    private void setDataSet() {
        actualList.clear();
        if (!arrayListImages.isEmpty()) {
            actualList.addAll(arrayListImages);
        }
        if (actualList.size() < totalImages) {
            actualList.addAll(Arrays.asList(new Uri[totalImages - actualList.size()]));
        } else if (actualList.size() > totalImages) {
            actualList = actualList.subList(0, totalImages);
        }
        setDataSet(actualList);
    }

    public static class PostProductImagesViewholder extends DragDropSwipeAdapter.ViewHolder {
        private final ImageView add_image_icon, deleteImage, selectedImages ;
        private final TextView textViewNumber;
        private final RelativeLayout numberLayout, addProductImageLayout;

        public PostProductImagesViewholder(@NonNull View itemView) {
            super(itemView);
            add_image_icon = itemView.findViewById(R.id.add_image_icon);
            deleteImage = itemView.findViewById(R.id.deleteImage);
            selectedImages = itemView.findViewById(R.id.selectedImages);
            textViewNumber = itemView.findViewById(R.id.textViewNumber);
            numberLayout = itemView.findViewById(R.id.numberLayout);
            addProductImageLayout = itemView.findViewById(R.id.addProductImageLayout);
        }
    }

    @NonNull
    @Override
    protected PostProductImagesViewholder getViewHolder(@NonNull View view) {
        return new PostProductImagesViewholder(view);
    }

    @Override
    protected void onBindViewHolder(Uri uri, @NonNull PostProductImagesViewholder holder, int position) {
        if (imageSelected == position) {
            holder.add_image_icon.setVisibility(View.VISIBLE);
            holder.textViewNumber.setVisibility(View.GONE);
            holder.numberLayout.setVisibility(View.GONE);
        } else {
            holder.add_image_icon.setVisibility(View.GONE);
            holder.textViewNumber.setVisibility(View.VISIBLE);
            holder.numberLayout.setVisibility(View.VISIBLE);
        }
        holder.textViewNumber.setText((position + 1) + "");
        if (arrayListImages != null && arrayListImages.size() - 1 >= position) {
            Glide.with(context).load(arrayListImages.get(position)).into(holder.selectedImages);
            holder.selectedImages.setVisibility(View.VISIBLE);
            holder.deleteImage.setVisibility(View.VISIBLE);
            holder.textViewNumber.setVisibility(View.GONE);
            holder.add_image_icon.setVisibility(View.GONE);
        }

        holder.addProductImageLayout.setOnClickListener(view -> onImageClicked.onImageSelectedClicked(position));
        holder.deleteImage.setOnClickListener(v -> onImageClicked.onImageDeleteClicked(position));
    }

    @Override
    public int getItemCount() {
        return totalImages;
    }

    @Nullable
    @Override
    protected View getViewToTouchToStartDraggingItem(Uri uri, @NonNull PostProductImagesViewholder viewholder, int i) {
        return null;
    }

    @Override
    protected boolean canBeDragged(Uri item, @NonNull PostProductImagesViewholder viewHolder, int position) {
        return position<arrayListImages.size();
    }

    @Override
    protected boolean canBeDroppedOver(Uri item, @NonNull PostProductImagesViewholder viewHolder, int position) {
        return position<arrayListImages.size();
    }

    @Override
    protected boolean canBeSwiped(Uri item, @NonNull PostProductImagesViewholder viewHolder, int position) {
        return false;
    }

    public void updateImages(ArrayList<Uri> arrayListImages, int imageSelected) {
        this.arrayListImages = arrayListImages;
        this.imageSelected = imageSelected;
        setDataSet();
    }

    public interface OnImageClicked {
        void onImageSelectedClicked(int postion);

        void onImageDeleteClicked(int position);
    }
}

