package com.bingalollc.com.homeactivity.fragment

import android.content.Context
import android.content.DialogInterface
import android.content.IntentSender
import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.MeasureSpec
import android.view.ViewGroup
import android.widget.*
import android.widget.ExpandableListView.*
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.databinding.FragmentFilterBinding
import com.bingalollc.com.homeactivity.adapter.CategoriesFilterAdapter
import com.bingalollc.com.homeactivity.adapter.ExpandableListViewAdapter
import com.bingalollc.com.homeactivity.model.CategoriesModel
import com.bingalollc.com.homeactivity.model.FilterModel
import com.bingalollc.com.homeactivity.postproduct.adapter.PostProductNormalTextAdapter
import com.bingalollc.com.network.RestClient
import com.bingalollc.com.preference.Common
import com.bingalollc.com.preference.PreferenceManager
import com.bingalollc.com.utils.ViewBindingDialogFragment
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsStatusCodes
import com.liuguangqiang.swipeback.SwipeBackLayout
import com.warkiz.tickseekbar.OnSeekChangeListener
import com.warkiz.tickseekbar.SeekParams
import com.warkiz.tickseekbar.TickSeekBar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class FilterFragment(var categoriesData: MutableList<CategoriesModel.Datum>?) : ViewBindingDialogFragment<FragmentFilterBinding>(),
    PostProductNormalTextAdapter.OnClickedCalled,
    MapFragment.OnMapChoosed, CategoriesFilterAdapter.OnCatSelected {
    private var topLayer: LinearLayout? = null
    private var seekBar: TickSeekBar? = null
    private var header: TextView? = null
    private var seekText: String? = null
    private var reset: TextView? = null
    private var addressText: TextView? = null
    private var LOCATION_PERMISSION_FOR_LAUNCH_MAP = 1001
    private var min_price: EditText? = null
    private var max_price: EditText? = null
    private var apply_filter: Button? = null
    private var ChangeLocLayout: RelativeLayout? = null
    private var changeCond: TextView? = null
    private var conditionName: TextView? = null
    private var back_icon: ImageView? = null
    private var rvCategories: RecyclerView? = null
    private var imageArray: ArrayList<String>? = null
    private var nameArray: ArrayList<String>? = null
    private var nameArrayOnlyEnglish: ArrayList<String>? = null
    private var subCategoryArray: ArrayList<ArrayList<String>>? = null
    private var subCategoryArrayEnglish: ArrayList<ArrayList<String>>? = null
    private var cat_selectedArray: ArrayList<String>?=null
    private var setOnClickMethod: SetOnClickMethod? = null
    private var testDialog: AlertDialog? = null
    private var closedSort: TextView? = null
    private var newestSort: TextView? = null
    private var hightolowSort: TextView? = null
    private var lowToHighSort: TextView? = null
    private var closed_tick: ImageView? = null
    private var newest_tick: ImageView? = null
    private var highToLow_tick: ImageView? = null
    private var lowToHigh_tick: ImageView? = null
    private var sort_type: String? = null
    private var sort_type_int: Int? = 0
    private var filterModel: FilterModel? = null
    private var baseActivity: BaseActivity? = null
    private var preferenceManager: PreferenceManager? = null

    private var headerTextCondition: ArrayList<String>? = null
    private var headerTextConditionEng: ArrayList<String>? = null
    private var headerTextTwoCondition: ArrayList<String>? = null
    private var headerTextTwoConditionEng: ArrayList<String>? = null
    private var headerTextCategory: ArrayList<String>? = null
    private var mCategoryHeader: Array<String>? = null
    private var blackColor: Int = 0
    private var baseColor: Int = 0

    private var expandableListViewAdapter: ExpandableListViewAdapter? = null
    private var listDataChild: HashMap<String, ArrayList<String>>? = null
    private var listDataChildEnglish: HashMap<String, ArrayList<String>>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.FullScreenDialogStyle)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            initViews(view)
            setAllUiToScreen()
            swipeBackLayout.setEnableFlingBack(false)
            swipeBackLayout.setDragEdge(SwipeBackLayout.DragEdge.LEFT)
            swipeBackLayout.setOnPullToBackListener { fractionAnchor, fractionScreen ->
                if (fractionScreen>=0.99){
                    setOnClickMethod?.onFragmentDismissed()
                    dismiss()
                }
            }
            dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        }
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                setOnClickMethod?.onFragmentDismissed()
                dismiss()
                // in here you can do logic when backPress is clicked
            }
        })
    }

    private fun setAllUiToScreen() {
        imageArray = ArrayList()
        nameArray = ArrayList()
        nameArrayOnlyEnglish = ArrayList()
        subCategoryArray = ArrayList()
        subCategoryArrayEnglish = ArrayList()
        headerTextCondition = ArrayList()
        headerTextConditionEng = ArrayList()
        headerTextTwoCondition = ArrayList()
        headerTextTwoConditionEng = ArrayList()
        headerTextCategory = ArrayList()
        reset?.visibility = View.VISIBLE
        setDataToUI()
        initClicks()
        addAllConditionValues()
        initListeners()
        initExpandableObjects()
        if (categoriesData.isNullOrEmpty())
            setData()
        else
            setAllData()
        setAddress()
    }

    fun setDynamicHeightExp(mListview: ExpandableListView?) {
        val mlistAdapter = mListview?.adapter
        if (mListview == null) return
        var height = 0
        val desireWidth: Int =
            MeasureSpec.makeMeasureSpec(mListview.width, MeasureSpec.UNSPECIFIED)
        for (i in 0 until mListview.count) {
            val listItem: View = mlistAdapter?.getView(i, null, mListview)!!
            listItem.measure(desireWidth, MeasureSpec.UNSPECIFIED as Int)
            height += listItem.measuredHeight
        }
        val parmas: ViewGroup.LayoutParams = mListview.layoutParams
        parmas.height = height + mListview.dividerHeight * (mlistAdapter!!.count - 1)
        mListview.layoutParams = parmas
        mListview.requestLayout()
    }
    private fun initExpandableObjects() {
        // initializing the list of child
        listDataChild = HashMap()
        listDataChildEnglish = HashMap()
        // initializing the adapter object
        expandableListViewAdapter = ExpandableListViewAdapter(
            requireContext(),
            nameArray,
            imageArray,
            cat_selectedArray,
            listDataChild
        )
        // setting list adapter
        binding.expandableListView.setAdapter(expandableListViewAdapter)
    }

    private fun initListeners() {
        // ExpandableListView on child click listener
        binding.expandableListView.setOnChildClickListener { parent, v, groupPosition, childPosition, id ->
            if (cat_selectedArray?.contains(listDataChild!![nameArray!![groupPosition]]!![childPosition])
                    ?: false
            ) {
                cat_selectedArray?.remove(listDataChild!![nameArray!![groupPosition]]!![childPosition])
            } else {
                cat_selectedArray?.add(listDataChild!![nameArray!![groupPosition]]!![childPosition])
            }
            expandableListViewAdapter?.notifyDataSetChanged()
            false
        }
        // ExpandableListView Group expanded listener
        binding.expandableListView.setOnGroupExpandListener(object : OnGroupExpandListener {
            var previousGroup = -1
            override fun onGroupExpand(groupPosition: Int) {
                if (groupPosition != previousGroup) binding.expandableListView.collapseGroup(
                    previousGroup
                )
                previousGroup = groupPosition
            }
        })
        // ExpandableListView Group collapsed listener
        binding.expandableListView.setOnGroupCollapseListener { groupPosition ->

        }
    }
    private fun initListData() {

        nameArray?.forEachIndexed { index, s ->
            listDataChild?.put(s, subCategoryArray?.get(index) ?: ArrayList())
        }
        nameArrayOnlyEnglish?.forEachIndexed { index, s ->
            listDataChildEnglish?.put(s, subCategoryArrayEnglish?.get(index) ?: ArrayList())
        }

        // Adding child data
       // listDataChild!![listDataGroup!![0]] = alcoholList

        // notify the adapter
        if (this.isVisible) {
            expandableListViewAdapter!!.notifyDataSetChanged()
            setDynamicHeightExp(binding.expandableListView)
        }
    }

    private fun setDataToUI() {
        min_price?.setText(filterModel?.min_price)
        max_price?.setText(filterModel?.max_price)
        conditionName?.setText(filterModel?.conditionName)
        System.out.println(">>>>>>>>>>sss "+filterModel?.distance)
        val distance = arrayOf(
            "5",
            "10",
            "25",
            "50",
            "100",
            "250",
            "400",
            "500"
        )
        seekBar?.customTickTexts(distance)

        if (filterModel?.distance != null && !filterModel?.distance.equals("")) {
            val value = Integer.valueOf(filterModel?.distance).toFloat()
            seekBar?.setProgress(value)
            seekText = filterModel?.distance
        } else {
            seekBar?.setProgress(30.0F)
        }
        if (filterModel?.addressText.isNullOrEmpty().not())
            addressText?.setText(filterModel?.addressText)
        filterModel?.sort_by?.let {
            setSortDataUI(
                it
            )
        }
    }

    private fun checkGpsOn():Boolean {
        val locationManager =
            context?.getSystemService(BaseActivity.LOCATION_SERVICE) as LocationManager
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            displayLocationSettingsRequest(requireContext())
            return false
        }
        return true
    }

    private fun displayLocationSettingsRequest(context: Context) {
        val googleApiClient = GoogleApiClient.Builder(context)
            .addApi(LocationServices.API).build()
        googleApiClient.connect()
        val locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 10000
        locationRequest.fastestInterval = 10000 / 2.toLong()
        val builder =
            LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        builder.setAlwaysShow(true)
        val result =
            LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build())
        result.setResultCallback { result ->
            val status = result.status
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> Log.i(
                    "TAG1",
                    "All location settings are satisfied."
                )
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                    Log.i(
                        "TAG2",
                        "Location settings are not satisfied. Show the user a dialog to upgrade location settings "
                    )
                    try {
                        // Show the dialog by calling startResolutionForResult(), and check the result
                        // in onActivityResult().
                        status.startResolutionForResult(
                            requireActivity(),
                            LOCATION_PERMISSION_FOR_LAUNCH_MAP
                        )
                    } catch (e: IntentSender.SendIntentException) {
                        Log.i("TAG3", "PendingIntent unable to execute request.")
                    }
                }
                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> Log.i(
                    "Tag4",
                    "Location settings are inadequate, and cannot be fixed here. Dialog not created."
                )
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode==LOCATION_PERMISSION_FOR_LAUNCH_MAP ){
            launchMap()
        }
    }

    private fun initClicks() {
        changeCond?.setOnClickListener({
            showListDialog(true)
        })
        ChangeLocLayout?.setOnClickListener({
            if (checkGpsOn()) {
                launchMap()
            }

        })
        binding.closedLayout?.setOnClickListener {
            //closedSort?.text.toString()
            setSortDataUI(
                getLocaleStringResource(Locale("en"), R.string.closest, requireContext()).toString());
         }
        closedSort?.setOnClickListener {
            binding.closedLayout.performClick()
        }
        binding.newestLayout?.setOnClickListener {
            setSortDataUI(
                getLocaleStringResource(Locale("en"), R.string.newest, requireContext()).toString());
        }
        newestSort?.setOnClickListener {
            binding.newestLayout.performClick()
        }
        binding.hightolowLayout?.setOnClickListener {
            setSortDataUI(
                getLocaleStringResource(Locale("en"), R.string.price_high_to_low, requireContext()).toString());
        }
        hightolowSort?.setOnClickListener {
            binding.hightolowLayout.performClick()
        }
        binding.lowtohightLayout?.setOnClickListener {
            setSortDataUI(
                getLocaleStringResource(Locale("en"), R.string.price_low_to_high, requireContext()).toString());
        }
        lowToHighSort?.setOnClickListener {
            binding.lowtohightLayout.performClick()
        }
        apply_filter?.setOnClickListener {
            setOnClickMethod?.onFragmentDismissed()
            dismiss()
            applyFilter()
        }

        seekBar?.setOnSeekChangeListener(object : OnSeekChangeListener {
            override fun onSeeking(seekParams: SeekParams) {
                seekText = seekParams?.tickText
            }

            override fun onStartTrackingTouch(seekBar: TickSeekBar) {}
            override fun onStopTrackingTouch(seekBar: TickSeekBar) {}
        })
        reset?.setOnClickListener {
            android.app.AlertDialog.Builder(requireContext())
                .setMessage(getString(R.string.filter_reset_text))
                .setPositiveButton(
                    getString(R.string.reset)
                ) { dialog, which ->
                    dialog?.dismiss()
                    refreshUI()
                    setOnClickMethod?.resetClicked()
                }
                .setNegativeButton(getString(R.string.cancel), null)
                .create()
                .show()
        }


    }

    private fun applyFilter() {
        var conText = conditionName?.text?.toString();
        if (conditionName?.text?.toString()?.contains("#")?:false) {
            conText  = conditionName?.text?.toString()?.substring(
                0, conditionName?.text?.indexOf(
                    "#"
                ) ?: 0
            )
        }

        System.out.println(">>>>>>>"+ conText)
        System.out.println(">>>>>>>cat_selectedArray"+ cat_selectedArray)
        System.out.println(">>>>>>>min_price?.text.toString()"+ min_price?.text.toString())
        System.out.println(">>>>>>>max?.text.toString()"+ max_price?.text.toString())
        System.out.println(">>>>>>>seekText"+ seekText)
        System.out.println(">>>>>>>sort_type"+ sort_type)
        System.out.println(">>>>>>>sort_type_int"+ sort_type_int)
        System.out.println(">>>>>>>addressText?.text.toString()"+ addressText?.text.toString())
        if (conText.isNullOrEmpty() && cat_selectedArray?.size == 0 && min_price?.text.toString().isEmpty() &&
            max_price?.text.toString().isEmpty() && seekText.isNullOrEmpty() && sort_type.isNullOrEmpty() && sort_type_int == 0) {
            setOnClickMethod?.onResetLocation(
                addressText?.text.toString(), cat_selectedArray!!, min_price?.text.toString(),
                max_price?.text.toString(), conText ?: "", seekText ?: "", sort_type ?: "",sort_type_int?:0
            )
            return
        }
        setOnClickMethod?.onApplyFilterClick(
            addressText?.text.toString(), cat_selectedArray!!, min_price?.text.toString(),
            max_price?.text.toString(), conText ?: "", seekText ?: "", sort_type ?: "",sort_type_int?:0
        )
    }

    fun getLocaleStringResource(
        requestedLocale: Locale,
        resourceId: Int,
        context: Context
    ): String? {
        val result: String
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) { // use latest api
            val config = Configuration(context.resources.configuration)
            config.setLocale(requestedLocale)
            result = context.createConfigurationContext(config).getText(resourceId).toString()
        } else { // support older android versions
            val resources: Resources = context.resources
            val conf: Configuration = resources.getConfiguration()
            val savedLocale: Locale = conf.locale
            conf.locale = requestedLocale
            resources.updateConfiguration(conf, null)

            // retrieve resources from desired locale
            result = resources.getString(resourceId)

            // restore original locale
            conf.locale = savedLocale
            resources.updateConfiguration(conf, null)
        }
        return result
    }
    private fun launchMap() {
        MapFragment.show(requireActivity(), this)
    }

    private fun setSortDataUI(
        sortString: String
    ) {
        sort_type = sortString
        if (sortString.equals(getLocaleStringResource(Locale("en"), R.string.closest, requireContext()).toString())) {
            sort_type_int = 1
            closedSort?.setTextColor(baseColor)
            newestSort?.setTextColor(blackColor)
            hightolowSort?.setTextColor(blackColor)
            lowToHighSort?.setTextColor(blackColor)
            closed_tick?.visibility = View.VISIBLE
            newest_tick?.visibility = View.GONE
            highToLow_tick?.visibility = View.GONE
            lowToHigh_tick?.visibility = View.GONE

        } else if (sortString.equals(getLocaleStringResource(Locale("en"), R.string.newest, requireContext()).toString())) {
            sort_type_int = 2
            closedSort?.setTextColor(blackColor)
            newestSort?.setTextColor(baseColor)
            hightolowSort?.setTextColor(blackColor)
            lowToHighSort?.setTextColor(blackColor)
            closed_tick?.visibility = View.GONE
            newest_tick?.visibility = View.VISIBLE
            highToLow_tick?.visibility = View.GONE
            lowToHigh_tick?.visibility = View.GONE
        } else if (sortString.equals(getLocaleStringResource(Locale("en"), R.string.price_high_to_low, requireContext()).toString())) {
            sort_type_int = 3
            closedSort?.setTextColor(blackColor)
            newestSort?.setTextColor(blackColor)
            hightolowSort?.setTextColor(baseColor)
            lowToHighSort?.setTextColor(blackColor)
            closed_tick?.visibility = View.GONE
            newest_tick?.visibility = View.GONE
            highToLow_tick?.visibility = View.VISIBLE
            lowToHigh_tick?.visibility = View.GONE
        } else if (sortString.equals(getLocaleStringResource(Locale("en"), R.string.price_low_to_high, requireContext()).toString())) {
            sort_type_int = 4
            closedSort?.setTextColor(blackColor)
            newestSort?.setTextColor(blackColor)
            hightolowSort?.setTextColor(blackColor)
            lowToHighSort?.setTextColor(baseColor)
            closed_tick?.visibility = View.GONE
            newest_tick?.visibility = View.GONE
            highToLow_tick?.visibility = View.GONE
            lowToHigh_tick?.visibility = View.VISIBLE
        }
    }

    private fun refreshUI() {
        sort_type = ""
        closedSort?.setTextColor(requireContext().resources.getColor(R.color.black))
        newestSort?.setTextColor(requireContext().resources.getColor(R.color.black))
        hightolowSort?.setTextColor(requireContext().resources.getColor(R.color.black))
        lowToHighSort?.setTextColor(requireContext().resources.getColor(R.color.black))
        min_price?.setText("")
        max_price?.setText("")
        conditionName?.setText("")
        addressText?.text=""
        cat_selectedArray = ArrayList()
        initRecyclerViews()
        setAllUiToScreen()
        seekBar?.setProgress(0.0F)
        seekText = ""
    }

    private fun addAllConditionValues() {
        val s = arrayOf("def", "abc")
        var mConditionHeaderArray = emptyArray<String>()
        var mConditionHeaderTwoArray = emptyArray<String>()
        mConditionHeaderArray = arrayOf("Brand New", "New", "Used", "Used", "Used")
        mConditionHeaderTwoArray = arrayOf(
            "Not Touched. In Original Packaging",
            "Open Box (Packaging Is Off, Never Used)",
            "Like-New (Excellent Condition. Signs Of Minor Use)",
            "Very Good (Signs Of Significant Use. Functions Properly)",
            "Acceptable (May Have Minor Scratches & Dents. Still Works Well)"
        )
        headerTextConditionEng?.addAll(mConditionHeaderArray)
        headerTextTwoConditionEng?.addAll(mConditionHeaderTwoArray)
        if (preferenceManager?.isEnglish == 2) {
            mConditionHeaderArray = arrayOf("חדש לגמרי", "חָדָשׁ\n", "בשימוש", "בשימוש", "בשימוש")
            mConditionHeaderTwoArray = arrayOf(
                "לא נגע. באריזה מקורית",
                "תיבה פתוחה (האריזה אינה פעילה, אף פעם לא בשימוש)",
                "כמו חדש (מצב מצוין. סימני שימוש קלים)",
                "טוב מאוד (סימני שימוש משמעותיים. פונקציות נכונות)",
                "מקובל (יכול להיות שיש שריטות ושקעים קלים. עדיין עובד טוב)"
            )
        }
        headerTextCondition?.addAll(mConditionHeaderArray)
        headerTextTwoCondition?.addAll(mConditionHeaderTwoArray)
        mCategoryHeader = arrayOf(
            "Antiques:Other",
            "Appliances:Electronics",
            "Arts & Crafts:Sport and hobby",
            "Audio Equipment:Music & Movies",
            "Auto Parts:Transport",
            "Baby & Kids:Children's world",
            "Beauty & Health:Fashion and style",
            "Bicycles & Scooters:Transport",
            "Bicycles - Electric:Transport",
            "Boats:Transport",
            "Sefarim, Books & Magazines:Other",
            "Business Equipment:Other",
            "Campers & RVs:Other",
            "Cars & Trucks:Transport",
            "CDs & DVDS:Music & Movies",
            "Cell Phones:Electronics",
            "Women’s Clothing & Shoes:Fashion and style",
            "Men’s Clothing & Shoes:Fashion and style",
            "Tichel’s & Beanies:Other",
            "Collectibles:Other",
            "Computer Equipment:Electronics",
            "Computer Software:Other",
            "Electronics:Electronics",
            "Farming Furniture:Other",
            "Games & Toys:Games",
            "General:Games",
            "Home & Garden:Home and garden",
            "Jewelry & Accessories:Fashion and style",
            "Motorcycles:Transport",
            "Musical Instruments:Music & Movies",
            "Pet Supplies:Other",
            "Photography:Other",
            "Sports & Outdoors:Sport and hobby",
            "Tickets:Other",
            "Tools & Machinery:Other",
            "TVs:Electronics",
            "Video Equipment:Electronics",
            "Video Games:Games"
        )
        headerTextCategory?.addAll(mCategoryHeader!!)
    }

    private fun initViews(view: View) {
        preferenceManager = PreferenceManager(requireContext())
        blackColor = requireContext().resources.getColor(R.color.black)
        baseColor = requireContext().resources.getColor(R.color.basecolor)
        baseActivity = BaseActivity()
        reset = view.findViewById(R.id.reset)
        addressText = view.findViewById(R.id.addressText)
        min_price = view.findViewById(R.id.min_price)
        max_price = view.findViewById(R.id.max_price)
        apply_filter = view.findViewById(R.id.apply_filter)
        topLayer = view.findViewById(R.id.topLayer)
        closedSort = view.findViewById(R.id.closedSort)
        newestSort = view.findViewById(R.id.newestSort)
        lowToHighSort = view.findViewById(R.id.lowToHighSort)
        closed_tick = view.findViewById(R.id.closed_tick)
        newest_tick = view.findViewById(R.id.newest_tick)
        highToLow_tick= view.findViewById(R.id.highToLow_tick)
        lowToHigh_tick= view.findViewById(R.id.lowToHigh_tick)
        hightolowSort = view.findViewById(R.id.hightolowSort)
        seekBar = view.findViewById(R.id.seekBar)
        ChangeLocLayout = view.findViewById(R.id.ChangeLocLayout)
        header = view.findViewById(R.id.header)
        changeCond = view.findViewById(R.id.changeCond)
        conditionName = view.findViewById(R.id.conditionName)
        back_icon = view.findViewById(R.id.back_icon)
        rvCategories = view.findViewById(R.id.rvCategories)
        topLayer?.setBackgroundColor(requireContext().resources.getColor(R.color.basecolor))
        header?.text = getString(R.string.filter)
        back_icon?.setOnClickListener {
            setOnClickMethod?.onFragmentDismissed()
            dismiss()
        }


    }


    private fun setData() {
        baseActivity?.showLoading(requireContext())
        RestClient.getApiInterface().categories.enqueue(object : Callback<CategoriesModel> {
            override fun onFailure(call: Call<CategoriesModel>, t: Throwable) {
                binding.catHeader.visibility = View.GONE
                baseActivity?.hideLoading()
            }

            override fun onResponse(
                call: Call<CategoriesModel>,
                response: Response<CategoriesModel>
            ) {
                binding.catHeader.visibility = View.VISIBLE
                baseActivity?.hideLoading()
                if (response.body() != null && response.body()?.status == 200 && response.body()?.data != null) {
                   // val datta = response.body()?.data?.sortedWith(compareBy { it.position.toInt() })
                    categoriesData = response.body()?.data!!
                    setAllData()
                    //  initRecyclerViews()
                }

            }
        })

    }

    private fun setAllData() {
        (categoriesData?.forEachIndexed { index, datum ->
            if (datum?.subCategories?.size ?: 0 > 0) {
                imageArray?.add(datum?.categoryImage ?: "")
                nameArrayOnlyEnglish?.add(datum?.categoryName ?: "")
                if (preferenceManager?.isEnglish == 1)
                    nameArray?.add(datum?.categoryName ?: "")
                else
                    nameArray?.add(datum?.herbewCategoryName ?: "")
                val s = ArrayList<String>()
                val s_eng = ArrayList<String>()
                datum?.subCategories?.forEach {
                    s_eng.add(it.name)
                    if (preferenceManager?.isEnglish == 1)
                        s.add(it.name)
                    else
                        s.add(it.herbewName)
                }
                subCategoryArray?.add(s)
                subCategoryArrayEnglish?.add(s_eng)
            }
        })

        initListData()
    }

    private fun setAddress() {
        if (!TextUtils.isEmpty(Common.productLocationLat)) {
            val adddress = baseActivity?.getAddress(
                Common.productLocationLat.toDouble(),
                Common.productLocationLng.toDouble(),
                requireContext(), false
            )
            addressText?.text = adddress
        }
    }

    private fun initRecyclerViews() {
        val categoriesFilterAdapter =
            CategoriesFilterAdapter(nameArray, imageArray, cat_selectedArray, this)
        val linearLayoutManager = GridLayoutManager(requireContext(), 2)
        rvCategories?.layoutManager = linearLayoutManager
        rvCategories?.adapter = categoriesFilterAdapter
    }



    fun showListDialog(isCondition: Boolean) {
        val alertDialog = AlertDialog.Builder(requireContext())
        val inflater = layoutInflater
        val add_menu_layout = inflater.inflate(R.layout.alert_dialog_header, null)
        val rvListView: RecyclerView = add_menu_layout.findViewById(R.id.rvListView)
        val crossBtn: ImageView = add_menu_layout.findViewById(R.id.crossBtn)
        alertDialog.setView(add_menu_layout)
        alertDialog.setCancelable(false)
        initNameRecyclerView(rvListView, isCondition)
        crossBtn?.setOnClickListener({ testDialog?.dismiss() })
        testDialog = alertDialog.create()
        testDialog!!.show()
    }


    private fun initNameRecyclerView(rvListView: RecyclerView, condition: Boolean) {
        var postProductNormalTextAdapter: PostProductNormalTextAdapter? = null
        if (condition)
            postProductNormalTextAdapter =
                PostProductNormalTextAdapter(headerTextCondition, headerTextTwoCondition, this)
        else
            postProductNormalTextAdapter =
                PostProductNormalTextAdapter(headerTextCategory, null, this)
        val linearLayoutManager = LinearLayoutManager(requireContext())
        rvListView.adapter = postProductNormalTextAdapter;
        rvListView.layoutManager = linearLayoutManager

    }

    interface SetOnClickMethod {
        fun onApplyFilterClick(
            addressText: String,
            cat_selectedArray: ArrayList<String>,
            min_price: String,
            max_price: String,
            conditionName: String,
            distance: String,
            sort_by: String,
            sort_by_int: Int

        )

        fun onResetLocation(
            addressText: String,
            cat_selectedArray: ArrayList<String>,
            min_price: String,
            max_price: String,
            conditionName: String,
            distance: String,
            sort_by: String,
            sort_by_int: Int

        )

        fun resetClicked()

        fun onFragmentDismissed()
    }

    override fun onItemClicked(text: String?, pos: Int,  completeStringForServer: String) {
        testDialog?.dismiss()
        if (text.toString().contains("#"))
            conditionName?.setText(text.toString().substring(0, text.toString().indexOf("#")))
        else
            conditionName?.setText(text)
    }


    override fun onMapChoosed() {
        setAddress()
        applyFilter()
    }

    override fun onCategoriesSelected(cat_name: java.util.ArrayList<String>) {
        cat_selectedArray = cat_name
    }

    override fun provideBinding(inflater: LayoutInflater): FragmentFilterBinding {
        return FragmentFilterBinding.inflate(inflater)
    }

    fun listenerCatcher(setOnClickMethod: FilterFragment.SetOnClickMethod) {
        this.setOnClickMethod = setOnClickMethod
    }

    fun filterData(filterModel: FilterModel?) {
        this.cat_selectedArray = filterModel?.categories
        this.filterModel = filterModel
    }
}