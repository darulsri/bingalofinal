package com.bingalollc.com.homeactivity.profilefragment.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductCountOfUser {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    public class Datum {

        @SerializedName("total_product")
        @Expose
        private Integer totalProduct;
        @SerializedName("unsold_product")
        @Expose
        private Integer unsoldProduct;
        @SerializedName("sold_product")
        @Expose
        private Integer soldProduct;
        @SerializedName("total_favourites")
        @Expose
        private Integer totalFavourites;
        @SerializedName("total_purchased")
        @Expose
        private Integer totalPurchased;

        public Integer getTotalProduct() {
            return totalProduct;
        }

        public void setTotalProduct(Integer totalProduct) {
            this.totalProduct = totalProduct;
        }

        public Integer getUnsoldProduct() {
            return unsoldProduct;
        }

        public void setUnsoldProduct(Integer unsoldProduct) {
            this.unsoldProduct = unsoldProduct;
        }

        public Integer getSoldProduct() {
            return soldProduct;
        }

        public void setSoldProduct(Integer soldProduct) {
            this.soldProduct = soldProduct;
        }

        public Integer getTotalFavourites() {
            return totalFavourites;
        }

        public void setTotalFavourites(Integer totalFavourites) {
            this.totalFavourites = totalFavourites;
        }

        public Integer getTotalPurchased() {
            return totalPurchased;
        }

        public void setTotalPurchased(Integer totalPurchased) {
            this.totalPurchased = totalPurchased;
        }

    }
}