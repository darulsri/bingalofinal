package com.bingalollc.com.homeactivity.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.bingalollc.com.homeactivity.chatfragmet.ChatScreenFragment;
import com.bingalollc.com.homeactivity.postproduct.PostProduct;
import com.bingalollc.com.homeactivity.homefragment.HomeFragment;
import com.bingalollc.com.homeactivity.notification.NotificationFragment;
import com.bingalollc.com.homeactivity.profilefragment.ProfileFrag;

public class TabAdapter extends FragmentPagerAdapter {
    Context context;
    int totalTabs;
    HomeFragment homeOne;
    ChatScreenFragment homeTwo;
    PostProduct homeThree;
    NotificationFragment homeFour;
    ProfileFrag homeFive;

    public TabAdapter(Context c, FragmentManager fm, int totalTabs) {
        super(fm);
        context = c;
        this.totalTabs = totalTabs;
    }
    @NonNull
    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                if (homeOne==null) {
                    homeOne = new HomeFragment();
                    return homeOne;
                }
            case 1:
                if (homeTwo==null) {
                    homeTwo = new ChatScreenFragment();
                    return homeTwo;
                }
            case 2:
                if (homeThree==null) {
                    homeThree = new PostProduct();
                    return homeThree;
                }
            case 3:
                if (homeFour==null) {
                    homeFour = new NotificationFragment();
                    return homeFour;
                }
            case 4:
                if (homeFive==null) {
                    homeFive = new ProfileFrag();
                    return homeFive;
                }
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return totalTabs;
    }

    public Fragment getCurrentItem(int pos) {
        if (pos == 0) {
            return homeOne;
        } else
            return homeOne;
    }


}
