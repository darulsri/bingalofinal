package com.bingalollc.com.homeactivity.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bingalollc.com.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class FilterStringAdapter extends RecyclerView.Adapter<FilterStringAdapter.Viewholder> {
    private ArrayList<String> nameArray;
    private Context context;
    private OnDeleteFilterSelected OnDeleteFilterSelected;

    public FilterStringAdapter(ArrayList<String> nameArray, OnDeleteFilterSelected OnDeleteFilterSelected) {
        this.nameArray = nameArray;
        this.OnDeleteFilterSelected = OnDeleteFilterSelected;

    }

    public void refreshData(ArrayList<String> nameArray){
        this.nameArray = nameArray;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.filter_text_item,parent,false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        holder.filterString.setText(nameArray.get(position));
        if (nameArray.get(position).equalsIgnoreCase("")){
            holder.filterString.setText(nameArray.get(position));
            holder.completeLayout.setVisibility(View.GONE);
        }else {
            holder.completeLayout.setVisibility(View.VISIBLE
            );
        }
    }

    @Override
    public int getItemCount() {
        return nameArray.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        private final ImageView crossBtnFilter;
        private final TextView filterString;
        private final LinearLayout completeLayout;
        public Viewholder(@NonNull View itemView) {
            super(itemView);
            crossBtnFilter = itemView.findViewById(R.id.crossBtnFilter);
            filterString = itemView.findViewById(R.id.filterString);
            completeLayout = itemView.findViewById(R.id.completeLayout);
            filterString.setOnClickListener(view -> {
                OnDeleteFilterSelected.onFilterStringData(filterString.getText().toString());
            });

        }
    }

    public interface OnDeleteFilterSelected{
        void onFilterStringData(String filterString);
    }
}
