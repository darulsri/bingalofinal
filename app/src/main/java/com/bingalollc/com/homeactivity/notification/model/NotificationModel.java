package com.bingalollc.com.homeactivity.notification.model;

import java.util.List;
import com.bingalollc.com.model.Users;
import com.bingalollc.com.users.UsersArrayModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationModel {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class NotificationBy {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("full_name")
        @Expose
        private String fullName;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("lat")
        @Expose
        private String lat;
        @SerializedName("lng")
        @Expose
        private String lng;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("facebook_id")
        @Expose
        private String facebookId;
        @SerializedName("google_id")
        @Expose
        private String googleId;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("image_name")
        @Expose
        private String imageName;
        @SerializedName("language")
        @Expose
        private String language;
        @SerializedName("token")
        @Expose
        private String token;
        @SerializedName("twitter_id")
        @Expose
        private String twitterId;
        @SerializedName("apple_id")
        @Expose
        private String appleId;
        @SerializedName("push_token")
        @Expose
        private String pushToken;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getFacebookId() {
            return facebookId;
        }

        public void setFacebookId(String facebookId) {
            this.facebookId = facebookId;
        }

        public String getGoogleId() {
            return googleId;
        }

        public void setGoogleId(String googleId) {
            this.googleId = googleId;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getImageName() {
            return imageName;
        }

        public void setImageName(String imageName) {
            this.imageName = imageName;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getTwitterId() {
            return twitterId;
        }

        public void setTwitterId(String twitterId) {
            this.twitterId = twitterId;
        }

        public String getAppleId() {
            return appleId;
        }

        public void setAppleId(String appleId) {
            this.appleId = appleId;
        }

        public String getPushToken() {
            return pushToken;
        }

        public void setPushToken(String pushToken) {
            this.pushToken = pushToken;
        }

    }
    public class Image {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("image_name")
        @Expose
        private String imageName;
        @SerializedName("image_url")
        @Expose
        private String imageUrl;
        @SerializedName("post_id")
        @Expose
        private String postId;
        @SerializedName("uploaded_at")
        @Expose
        private String uploadedAt;
        @SerializedName("user_id")
        @Expose
        private String userId;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getImageName() {
            return imageName;
        }

        public void setImageName(String imageName) {
            this.imageName = imageName;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public String getPostId() {
            return postId;
        }

        public void setPostId(String postId) {
            this.postId = postId;
        }

        public String getUploadedAt() {
            return uploadedAt;
        }

        public void setUploadedAt(String uploadedAt) {
            this.uploadedAt = uploadedAt;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

    }

    public static class Datum {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("notification_title")
        @Expose
        private String notificationTitle;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("notification_description")
        @Expose
        private String notificationDescription;
        @SerializedName("notification_type")
        @Expose
        private String notificationType;
        @SerializedName("notification_by")
        @Expose
        private UsersArrayModel.Data notificationBy;
        @SerializedName("product_id")
        @Expose
        private ProductModel product_id;
        @SerializedName("notification_link")
        @Expose
        private String notification_link;
        @SerializedName("is_read")
        @Expose
        private String isRead;
        @SerializedName("notification_date")
        @Expose
        private String notificationDate;
        @SerializedName("time_diff")
        @Expose
        private String time_diff;

        public String getNotification_link() {
            return notification_link;
        }


        public ProductModel getProduct_id() {
            return product_id;
        }

        public void setProduct_id(ProductModel product_id) {
            this.product_id = product_id;
        }

        public void setNotification_link(String notification_link) {
            this.notification_link = notification_link;
        }


        public UsersArrayModel.Data getNotificationBy() {
            return notificationBy;
        }

        public void setNotificationBy(UsersArrayModel.Data notificationBy) {
            this.notificationBy = notificationBy;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getTime_diff() {
            return time_diff;
        }

        public void setTime_diff(String time_diff) {
            this.time_diff = time_diff;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getNotificationTitle() {
            return notificationTitle;
        }

        public void setNotificationTitle(String notificationTitle) {
            this.notificationTitle = notificationTitle;
        }

        public String getNotificationDescription() {
            return notificationDescription;
        }

        public void setNotificationDescription(String notificationDescription) {
            this.notificationDescription = notificationDescription;
        }

        public String getNotificationType() {
            return notificationType;
        }

        public void setNotificationType(String notificationType) {
            this.notificationType = notificationType;
        }

        public String getIsRead() {
            return isRead;
        }

        public void setIsRead(String isRead) {
            this.isRead = isRead;
        }

        public String getNotificationDate() {
            return notificationDate;
        }

        public void setNotificationDate(String notificationDate) {
            this.notificationDate = notificationDate;
        }

    }

    public class ProductId {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("product_category")
        @Expose
        private String productCategory;
        @SerializedName("product_condition")
        @Expose
        private String productCondition;
        @SerializedName("product_description")
        @Expose
        private String productDescription;
        @SerializedName("product_is_sold")
        @Expose
        private String productIsSold;
        @SerializedName("product_lat")
        @Expose
        private String productLat;
        @SerializedName("product_lng")
        @Expose
        private String productLng;
        @SerializedName("product_location")
        @Expose
        private String productLocation;
        @SerializedName("product_city")
        @Expose
        private String productCity;
        @SerializedName("product_state")
        @Expose
        private String productState;
        @SerializedName("product_country")
        @Expose
        private String productCountry;
        @SerializedName("product_price")
        @Expose
        private String productPrice;
        @SerializedName("product_specifications")
        @Expose
        private String productSpecifications;
        @SerializedName("product_title")
        @Expose
        private String productTitle;
        @SerializedName("uploaded_by_user_id")
        @Expose
        private String uploadedByUserId;
        @SerializedName("parent_category")
        @Expose
        private String parentCategory;
        @SerializedName("bump_data")
        @Expose
        private String bumpData;
        @SerializedName("rank")
        @Expose
        private String rank;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("images")
        @Expose
        private List<Image> images = null;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProductCategory() {
            return productCategory;
        }

        public void setProductCategory(String productCategory) {
            this.productCategory = productCategory;
        }

        public String getProductCondition() {
            return productCondition;
        }

        public void setProductCondition(String productCondition) {
            this.productCondition = productCondition;
        }

        public String getProductDescription() {
            return productDescription;
        }

        public void setProductDescription(String productDescription) {
            this.productDescription = productDescription;
        }

        public String getProductIsSold() {
            return productIsSold;
        }

        public void setProductIsSold(String productIsSold) {
            this.productIsSold = productIsSold;
        }

        public String getProductLat() {
            return productLat;
        }

        public void setProductLat(String productLat) {
            this.productLat = productLat;
        }

        public String getProductLng() {
            return productLng;
        }

        public void setProductLng(String productLng) {
            this.productLng = productLng;
        }

        public String getProductLocation() {
            return productLocation;
        }

        public void setProductLocation(String productLocation) {
            this.productLocation = productLocation;
        }

        public String getProductCity() {
            return productCity;
        }

        public void setProductCity(String productCity) {
            this.productCity = productCity;
        }

        public String getProductState() {
            return productState;
        }

        public void setProductState(String productState) {
            this.productState = productState;
        }

        public String getProductCountry() {
            return productCountry;
        }

        public void setProductCountry(String productCountry) {
            this.productCountry = productCountry;
        }

        public String getProductPrice() {
            return productPrice;
        }

        public void setProductPrice(String productPrice) {
            this.productPrice = productPrice;
        }

        public String getProductSpecifications() {
            return productSpecifications;
        }

        public void setProductSpecifications(String productSpecifications) {
            this.productSpecifications = productSpecifications;
        }

        public String getProductTitle() {
            return productTitle;
        }

        public void setProductTitle(String productTitle) {
            this.productTitle = productTitle;
        }

        public String getUploadedByUserId() {
            return uploadedByUserId;
        }

        public void setUploadedByUserId(String uploadedByUserId) {
            this.uploadedByUserId = uploadedByUserId;
        }

        public String getParentCategory() {
            return parentCategory;
        }

        public void setParentCategory(String parentCategory) {
            this.parentCategory = parentCategory;
        }

        public String getBumpData() {
            return bumpData;
        }

        public void setBumpData(String bumpData) {
            this.bumpData = bumpData;
        }

        public String getRank() {
            return rank;
        }

        public void setRank(String rank) {
            this.rank = rank;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public List<Image> getImages() {
            return images;
        }

        public void setImages(List<Image> images) {
            this.images = images;
        }

    }

    public static class ProductModel {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("product_category")
        @Expose
        private String productCategory;
        @SerializedName("product_condition")
        @Expose
        private String productCondition;
        @SerializedName("product_description")
        @Expose
        private String productDescription;
        @SerializedName("product_is_sold")
        @Expose
        private String productIsSold;
        @SerializedName("product_lat")
        @Expose
        private String productLat;
        @SerializedName("product_lng")
        @Expose
        private String productLng;
        @SerializedName("product_location")
        @Expose
        private String productLocation;
        @SerializedName("product_city")
        @Expose
        private String productCity;
        @SerializedName("product_state")
        @Expose
        private String productState;
        @SerializedName("product_country")
        @Expose
        private String productCountry;
        @SerializedName("product_price")
        @Expose
        private String productPrice;
        @SerializedName("product_specifications")
        @Expose
        private String productSpecifications;
        @SerializedName("product_title")
        @Expose
        private String productTitle;
        @SerializedName("uploaded_by_user_id")
        @Expose
        private String uploadedByUserId;
        @SerializedName("parent_category")
        @Expose
        private String parentCategory;
        @SerializedName("bump_data")
        @Expose
        private Object bumpData;
        @SerializedName("isBumped")
        @Expose
        private Integer isBumped;
        @SerializedName("rank")
        @Expose
        private String rank;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("distance")
        @Expose
        private String distance;
        @SerializedName("isFavourite")
        @Expose
        private Boolean isFavourite;
        @SerializedName("time_diff")
        @Expose
        private String time_diff;
        @SerializedName("images")
        @Expose
        private List<ProductModelImage> images = null;
        @SerializedName("load_more")
        @Expose
        private Boolean load_more;
        @SerializedName("is_deleted")
        @Expose
        private String is_deleted;

        public String getIs_deleted() {
            return is_deleted;
        }

        public void setIs_deleted(String is_deleted) {
            this.is_deleted = is_deleted;
        }

        public Integer getIsBumped() {
            return isBumped;
        }

        public void setIsBumped(Integer isBumped) {
            this.isBumped = isBumped;
        }

        public Boolean getLoad_more() {
            return load_more;
        }

        public void setLoad_more(Boolean load_more) {
            this.load_more = load_more;
        }

        public Boolean getFavourite() {
            return isFavourite;
        }

        public void setFavourite(Boolean favourite) {
            isFavourite = favourite;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProductCategory() {
            return productCategory;
        }

        public void setProductCategory(String productCategory) {
            this.productCategory = productCategory;
        }

        public String getProductCondition() {
            return productCondition;
        }

        public void setProductCondition(String productCondition) {
            this.productCondition = productCondition;
        }

        public String getProductDescription() {
            return productDescription;
        }

        public void setProductDescription(String productDescription) {
            this.productDescription = productDescription;
        }

        public String getProductIsSold() {
            return productIsSold;
        }

        public void setProductIsSold(String productIsSold) {
            this.productIsSold = productIsSold;
        }

        public String getProductLat() {
            return productLat;
        }

        public void setProductLat(String productLat) {
            this.productLat = productLat;
        }

        public String getProductLng() {
            return productLng;
        }

        public void setProductLng(String productLng) {
            this.productLng = productLng;
        }

        public String getProductLocation() {
            return productLocation;
        }

        public void setProductLocation(String productLocation) {
            this.productLocation = productLocation;
        }

        public String getProductCity() {
            return productCity;
        }

        public void setProductCity(String productCity) {
            this.productCity = productCity;
        }

        public String getProductState() {
            return productState;
        }

        public void setProductState(String productState) {
            this.productState = productState;
        }

        public String getProductCountry() {
            return productCountry;
        }

        public void setProductCountry(String productCountry) {
            this.productCountry = productCountry;
        }

        public String getProductPrice() {
            return productPrice;
        }

        public void setProductPrice(String productPrice) {
            this.productPrice = productPrice;
        }

        public String getProductSpecifications() {
            return productSpecifications;
        }

        public void setProductSpecifications(String productSpecifications) {
            this.productSpecifications = productSpecifications;
        }

        public String getProductTitle() {
            return productTitle;
        }

        public void setProductTitle(String productTitle) {
            this.productTitle = productTitle;
        }

        public String getUploadedByUserId() {
            return uploadedByUserId;
        }

        public void setUploadedByUserId(String uploadedByUserId) {
            this.uploadedByUserId = uploadedByUserId;
        }

        public String getParentCategory() {
            return parentCategory;
        }

        public void setParentCategory(String parentCategory) {
            this.parentCategory = parentCategory;
        }

        public Object getBumpData() {
            return bumpData;
        }

        public void setBumpData(Object bumpData) {
            this.bumpData = bumpData;
        }

        public String getRank() {
            return rank;
        }

        public void setRank(String rank) {
            this.rank = rank;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public String getTime_diff() {
            return time_diff;
        }

        public void setTime_diff(String time_diff) {
            this.time_diff = time_diff;
        }

        public List<ProductModelImage> getImages() {
            return images;
        }

        public void setImages(List<ProductModelImage> images) {
            this.images = images;
        }

    }
    public class ProductModelImage {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("image_name")
        @Expose
        private String imageName;
        @SerializedName("image_url")
        @Expose
        private String imageUrl;
        @SerializedName("post_id")
        @Expose
        private String postId;
        @SerializedName("uploaded_at")
        @Expose
        private String uploadedAt;
        @SerializedName("user_id")
        @Expose
        private String userId;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getImageName() {
            return imageName;
        }

        public void setImageName(String imageName) {
            this.imageName = imageName;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public String getPostId() {
            return postId;
        }

        public void setPostId(String postId) {
            this.postId = postId;
        }

        public String getUploadedAt() {
            return uploadedAt;
        }

        public void setUploadedAt(String uploadedAt) {
            this.uploadedAt = uploadedAt;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

    }
}
