package com.bingalollc.com.homeactivity.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;

import com.bingalollc.com.R;
import com.bingalollc.com.preference.Common;
import com.bingalollc.com.preference.PreferenceManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;


public class ImageTipsFragment extends Fragment implements View.OnClickListener {

    private ImageView mainImg;
    private TextView headerText;
    private TextView subHeaderText;
    private Context mContext;
    private PreferenceManager preferenceManager;
    private int tipsImgPos;
    private boolean showTransparentLayout=false;


    /**
     * constructor
     */
    public ImageTipsFragment(PreferenceManager preferenceManager) {
        // Required empty public constructor
        this.preferenceManager = preferenceManager;
    }


    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.tips_overlay_item, container, false);
        init(view);
        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * initialising components
     */
    private void init(final View view) {


        //ivTutorial = view.findViewById(R.id.ivTutorial);
        mainImg = view.findViewById(R.id.mainImg);
        headerText = view.findViewById(R.id.headerText);
        subHeaderText = view.findViewById(R.id.subHeaderText);


        getBundleData();
    }

    /**
     * get Bundle Data
     */
    private void getBundleData() {

        Bundle bundle = getArguments();

        if (getArguments() != null) {
            tipsImgPos = bundle.getInt("tipsImgPos");
        }

        setData();
    }


    /**
     * set Data
     */
    private void setData() {

        Drawable drawable = mContext.getDrawable(R.drawable.img_tip1);
        if (preferenceManager.getIsEnglish() == 2) {
            drawable = mContext.getDrawable(R.drawable.img_tip1_hebrew);
        }

        String header = mContext.getString(R.string.tips_one);
        String subHeader = mContext.getString(R.string.tip_desc_one);
        if (tipsImgPos == 1) {
            if (preferenceManager.getIsEnglish() == 2) {
                drawable = mContext.getDrawable(R.drawable.img_tip2_hebrew);
            } else {
                drawable = mContext.getDrawable(R.drawable.img_tip2);
            }
            header = mContext.getString(R.string.tips_two);
            subHeader = mContext.getString(R.string.tip_desc_two);
        } else if (tipsImgPos == 2) {
            drawable = mContext.getDrawable(R.drawable.img_tip3);
            header = mContext.getString(R.string.tips_three);
            subHeader = mContext.getString(R.string.tip_desc_three);
        }
        mainImg.setImageDrawable(drawable);
        headerText.setText(header);
        subHeaderText.setText(subHeader);
    }

    @Override
    public void onClick(final View view) {

    }

    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        mContext = context;
    }
}
