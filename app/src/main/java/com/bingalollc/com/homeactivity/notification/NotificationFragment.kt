package com.bingalollc.com.homeactivity.notification

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.camerax.CameraPostProduct
import com.bingalollc.com.homeactivity.HomeActivity
import com.bingalollc.com.homeactivity.notification.adapter.HomeNotificationAdapter
import com.bingalollc.com.homeactivity.notification.model.NotificationModel
import com.bingalollc.com.model.ApiStatusModel
import com.bingalollc.com.network.RestClient
import com.bingalollc.com.preference.PreferenceManager
import com.bingalollc.com.report.ReportUser
import com.bingalollc.com.utils.SwipeHelper
import com.bingalollc.com.utils.convertStringToDate
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.NullPointerException
import java.util.*
import kotlin.collections.ArrayList


class NotificationFragment : Fragment() {
    private var topLayer: LinearLayout? = null
    private var header: TextView? = null
    private var back_icon: ImageView? = null
    private var imgNotificationPopup: CardView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var reset: TextView? = null
    private var noNotificationText: TextView? = null
    private var  rvNotifications: RecyclerView? = null
    private var  baseActivity:BaseActivity?=null
    private var  preferenceManager:PreferenceManager?=null
    private var  notificationModel: ArrayList<NotificationModel.Datum>? = null
    private var  notificationId: ArrayList<String>? = null
    private var  homeNotificationAdapter: HomeNotificationAdapter? = null
    private var swipeHelper: SwipeHelper? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        resetNotificationCount()

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view=inflater.inflate(R.layout.fragment_notification, container, false)

//        GlobalScope.launch(Dispatchers.Main) {
//            // Switch to a background (IO) thread
//            val retval = withContext(Dispatchers.IO) {
//                Log.e("TASK", "Started background task")
//
//                val retval = "The value from background"
//                Thread.sleep(3000)
//                Log.e("TASK", "Finished background task with result: " + retval)
//                retval
//            }
//            // Now you're back the main thread
//            Log.e("TASK", "Started task in Main thread with result from Background: " + retval)
//        }

        initViews(view)

        if(preferenceManager?.userDetails?.id.isNullOrEmpty().not()) {
            fetchNotication()
        }
        baseActivity?.hideLoading()
        reset?.setOnClickListener {
            baseActivity?.showNormalDialogVerticalButton(requireActivity(),
                getString(R.string.clear_notification),
                getString(
                    R.string.clear_notification_confirm
                ),
                getString(R.string.cancel),
                getString(R.string.clear_all),
                BaseActivity.OnOkClicked {
                    if (it) {
                        deleteNotifications(true, -1)
                    }
                }, true)
        }
        swipeRefreshLayout?.setOnRefreshListener(OnRefreshListener {
            fetchNotication()
            //resetNotificationCount()
            updateNotiReadStatus()
        })
        return view
    }


    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)

        if (isVisibleToUser) {
           if(preferenceManager?.userDetails?.id.isNullOrEmpty().not()){
               if(!preferenceManager!!.notificationCount){
                   fetchNotication()
               }
               //resetNotificationCount()

           }
        }
    }


    override fun onPause() {
        super.onPause()

        deleteNotifications(true, -1)
    }


    override fun onDestroyView() {
        super.onDestroyView()

        deleteNotifications(true, -1)

    }



    private fun deleteNotifications(deleteAll: Boolean, position: Int) {
        baseActivity?.showLoading(requireContext())
        var ids = ""
        if (deleteAll) {
            ids = notificationId.toString().replace("[", "").replace("]", "")
        } else {
            ids = notificationId?.get(position)?:""
        }
        RestClient.getApiInterface().readNotification(ids, preferenceManager?.userDetails?.id).enqueue(
            object : Callback<ApiStatusModel> {
                override fun onResponse(
                    call: Call<ApiStatusModel>,
                    response: Response<ApiStatusModel>
                ) {
                    baseActivity?.hideLoading()
                    if (response.body()?.getStatus() == 200) {
                        if (deleteAll) {
                            notificationModel = ArrayList()
                            //addDefaultDatToModel()
                            updateNotiReadStatus()
                            initRecyclerViewForNotification()
                            //resetNotificationCount()
                        }else {
                            notificationModel?.removeAt(position)
                            homeNotificationAdapter?.refreshData()
                            if(notificationModel?.size==0){
                                fetchNotication()
                            }
                        }
                    } else if (response.body()?.getMessage().isNullOrEmpty().not()) {
                        baseActivity?.showToast(response.body()?.getMessage(), requireContext())
                    }
                }

                override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {

                }

            })
    }

    private fun resetNotificationCount() {
      //  baseActivity?.showLoading(requireContext())
        RestClient.getApiInterface().resetNotificationCount(preferenceManager?.userDetails?.id).enqueue(
            object : Callback<ApiStatusModel> {
                override fun onResponse(
                    call: Call<ApiStatusModel>,
                    response: Response<ApiStatusModel>
                ) {
                 //   baseActivity?.hideLoading()
                    if (response.body()?.getStatus() == 200) {
                        //addDefaultDatToModel()
                      //  updateNotiReadStatus()
                        //initRecyclerViewForNotification()
                        (activity as HomeActivity).notificationCount?.visibility = View.GONE
                        if(preferenceManager!!.notificationCount){
                            preferenceManager!!.setNotificationCount(false)
                        }
                    } else if (response.body()?.getMessage().isNullOrEmpty().not()) {
                        baseActivity?.showToast(response.body()?.getMessage(), requireContext())
                    }
                }

                override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {

                }

            })
    }

    private fun fetchNotication() {
        notificationModel = ArrayList()
        notificationId = ArrayList()

        requireActivity().runOnUiThread {
            baseActivity?.showLoading(requireContext())
        }


        RestClient.getApiInterface().getNotifications(preferenceManager?.userDetails?.id).enqueue(
            object : Callback<NotificationModel> {
                override fun onFailure(call: Call<NotificationModel>, t: Throwable) {
                    baseActivity?.hideLoading()
                    if (swipeRefreshLayout!!.isRefreshing) {
                        swipeRefreshLayout!!.isRefreshing = false
                    }
                    System.out.print(">>>>>>>>NOTTMT "+t.message)
                }

                override fun onResponse(
                    call: Call<NotificationModel>,
                    response: Response<NotificationModel>
                ) {
                    if (swipeRefreshLayout!!.isRefreshing) {
                        swipeRefreshLayout!!.isRefreshing = false
                    }
                    baseActivity?.hideLoading()
                    System.out.print(">>>>>>>>NOTTT ")
                    if (response.body()?.status == 201) {
                        val data = preferenceManager?.deletedNotifications
                        notificationModel = ArrayList()
                        notificationId = ArrayList()
                        if(preferenceManager!!.notificationCount){
                            addDefaultDatToModel()
                        }


                        response.body()?.data?.forEachIndexed { index, datum ->
                            val date1 = convertStringToDate(datum.notificationDate)
                            val date2 = Calendar.getInstance().time
                         //   if (date2 < date1) {

                                if (data != null && data.size > 0) {
                                    if (!data.contains(response.body()?.data!!.get(index).id)) {
                                        notificationId?.add(response.body()?.data!!.get(index).id)
                                        notificationModel?.add(response.body()?.data!!.get(index))
                                        if (response.body()?.data?.get(index)?.isRead == "1") {

                                        }
                                    }
                                } else {
                                    notificationId?.add(response.body()?.data!!.get(index).id)
                                    notificationModel?.add(response.body()?.data!!.get(index))

                                }
                         //   }


                        }

                    }
                    initRecyclerViewForNotification()
                }
            })
    }

    private fun updateNotiReadStatus() {
        try {
            (activity as HomeActivity?)!!.fetchNotificationCount(preferenceManager?.userDetails?.id.toString(), preferenceManager?.userDetails?.token.toString())
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }
    private fun addDefaultDatToModel() {
        val notiModel = NotificationModel.Datum()
        notiModel.notificationTitle = getString(R.string.welcome_to_bingalo)
        notiModel.notificationDescription = getString(R.string.lorem_epsum)
        notiModel.notificationType = "12345"
        notificationModel?.add(notiModel)
    }

    private fun initRecyclerViewForNotification() {
        if ((notificationModel?.size?:0)>1){
            reset?.visibility = View.VISIBLE
        }else {
            reset?.visibility = View.GONE
        }
        System.out.println(">>>>>>>>>>>>>.SSSS "+notificationModel?.size)
        if ((notificationModel?.size?:0)==0) {
            noNotificationText?.visibility = View.VISIBLE
            imgNotificationPopup?.visibility = View.VISIBLE
        }else {
            noNotificationText?.visibility = View.GONE
            if(preferenceManager!!.notificationCount){
                if(notificationModel?.size!! == 1){
                    imgNotificationPopup?.visibility = View.VISIBLE
                }
            }else {
                imgNotificationPopup?.visibility = View.GONE
            }
        }

        val linearLayoutManager = LinearLayoutManager(requireContext())
        homeNotificationAdapter = HomeNotificationAdapter(notificationModel, preferenceManager)
        rvNotifications?.layoutManager = linearLayoutManager
        rvNotifications?.adapter = homeNotificationAdapter
       setSwipeForRecyclerView()
    }


    private fun setSwipeForRecyclerView() {
        swipeHelper = object : SwipeHelper(requireContext(), rvNotifications, false) {
            override fun instantiateUnderlayButton(
                viewHolder: RecyclerView.ViewHolder,
                underlayButtons: MutableList<UnderlayButton>
            ) {
                var deleteIcon =
                    BitmapFactory.decodeResource(resources, R.drawable.btn_delete_white)
                deleteIcon = Bitmap.createScaledBitmap(deleteIcon!!, 100, 120, false)
                underlayButtons.add(UnderlayButton(
                    getString(R.string.delete),
                    deleteIcon,
                    requireContext().resources.getColor(R.color.dark_red)
                ) { pos ->
                    deleteNotifications(false, pos)
                })
            }
        }
    }

    private fun initViews(view: View) {
        baseActivity = BaseActivity()
        preferenceManager = PreferenceManager(requireContext())
        topLayer = view.findViewById(R.id.topLayer)
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout)
        header = view.findViewById(R.id.header)
        back_icon = view.findViewById(R.id.back_icon)
        reset = view.findViewById(R.id.reset)
        rvNotifications = view.findViewById(R.id.rvNotifications)
        noNotificationText = view.findViewById(R.id.noNotificationText)
        imgNotificationPopup = view.findViewById(R.id.imgNotificationPopup)
        topLayer?.setBackgroundColor(requireContext().resources.getColor(R.color.basecolor))
        header?.text = getString(R.string.notification)
        reset?.text = getString(R.string.clear_all)
        back_icon?.visibility = View.GONE

        imgNotificationPopup!!.setOnClickListener({
            if(preferenceManager!!.isUserLoggedIn){
                startActivity(
                    Intent(requireContext(), CameraPostProduct::class.java)
                        .putExtra("screen", "")
                )
            }else{
                activity?.let { it1 -> (activity as HomeActivity?)?.showSocialLoginDialog(it1) }
            }
        })
    }

}