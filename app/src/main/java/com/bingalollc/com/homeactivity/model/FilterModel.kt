package com.bingalollc.com.homeactivity.model

import com.google.gson.annotations.SerializedName
import java.util.*
import kotlin.collections.ArrayList


data class FilterModel(
        @SerializedName("search_name")
        var search_name:String? = "",
        @SerializedName("categories")
        var categories:ArrayList<String>? = ArrayList(),
        @SerializedName("addressText")
        var addressText:String? = "",
        @SerializedName("min_price")
        var min_price:String? = "",
        @SerializedName("display_min_price")
        var display_min_price:String? = "",
        @SerializedName("display_max_price")
        var display_max_price:String? = "",
        @SerializedName("max_price")
        var max_price:String? = "",
        @SerializedName("conditionName")
        var conditionName:String? = "",
        @SerializedName("distance")
        var distance:String? = "",
        @SerializedName("distance")
        var display_distance:String? = "",
        @SerializedName("sort_by")
        var sort_by:String? = "",
        @SerializedName("sort_by_int")
        var sort_by_int:Int? = 0

)

