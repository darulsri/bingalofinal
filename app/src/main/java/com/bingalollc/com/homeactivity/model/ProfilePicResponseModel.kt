package com.bingalollc.com.homeactivity.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class ProfilePicResponseModel {

    @SerializedName("message")
    @Expose
    private var message: String? = null

    @SerializedName("status")
    @Expose
    private var status: Int? = null

    @SerializedName("data")
    @Expose
    private var data: Data? = null

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String?) {
        this.message = message
    }

    fun withMessage(message: String?): ProfilePicResponseModel? {
        this.message = message
        return this
    }

    fun getStatus(): Int? {
        return status
    }

    fun setStatus(status: Int?) {
        this.status = status
    }

    fun withStatus(status: Int?): ProfilePicResponseModel? {
        this.status = status
        return this
    }

    fun getData(): Data? {
        return data
    }

    fun setData(data: Data?) {
        this.data = data
    }

    fun withData(data: Data?): ProfilePicResponseModel? {
        this.data = data
        return this
    }

    class Data {
        @SerializedName("image")
        @Expose
        var image: String? = null

        fun withImage(image: String?): Data {
            this.image = image
            return this
        }
    }
}
