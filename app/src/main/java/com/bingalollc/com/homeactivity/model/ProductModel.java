package com.bingalollc.com.homeactivity.model;

import com.bingalollc.com.model.Users;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductModel {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public static class Datum {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("product_category")
        @Expose
        private String productCategory;
        @SerializedName("product_condition")
        @Expose
        private String productCondition;
        @SerializedName("product_description")
        @Expose
        private String productDescription;
        @SerializedName("product_is_sold")
        @Expose
        private String productIsSold;
        @SerializedName("product_lat")
        @Expose
        private String productLat;
        @SerializedName("product_lng")
        @Expose
        private String productLng;
        @SerializedName("product_location")
        @Expose
        private String productLocation;
        @SerializedName("product_city")
        @Expose
        private String productCity;
        @SerializedName("product_state")
        @Expose
        private String productState;
        @SerializedName("product_country")
        @Expose
        private String productCountry;
        @SerializedName("product_price")
        @Expose
        private String productPrice;
        @SerializedName("product_specifications")
        @Expose
        private String productSpecifications;
        @SerializedName("product_title")
        @Expose
        private String productTitle;
        @SerializedName("uploaded_by_user_id")
        @Expose
        private Users uploadedByUserId;
        @SerializedName("parent_category")
        @Expose
        private String parentCategory;
        @SerializedName("bump_data")
        @Expose
        private Object bumpData;
        @SerializedName("isBumped")
        @Expose
        private Integer isBumped;
        @SerializedName("rank")
        @Expose
        private String rank;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updated_at;
        @SerializedName("distance")
        @Expose
        private String distance;
        @SerializedName("isFavourite")
        @Expose
        private Boolean isFavourite;
        @SerializedName("time_diff")
        @Expose
        private String time_diff;
        @SerializedName("images")
        @Expose
        private List<Image> images = null;
        @SerializedName("load_more")
        @Expose
        private Boolean load_more;
        @SerializedName("is_deleted")
        @Expose
        private String is_deleted;

        public String getIs_deleted() {
            return is_deleted;
        }

        public void setIs_deleted(String is_deleted) {
            this.is_deleted = is_deleted;
        }

        public Integer getIsBumped() {
            return isBumped;
        }

        public void setIsBumped(Integer isBumped) {
            this.isBumped = isBumped;
        }

        public Boolean getLoad_more() {
            return load_more;
        }

        public void setLoad_more(Boolean load_more) {
            this.load_more = load_more;
        }

        public Boolean getFavourite() {
            return isFavourite;
        }

        public void setFavourite(Boolean favourite) {
            isFavourite = favourite;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProductCategory() {
            return productCategory;
        }

        public void setProductCategory(String productCategory) {
            this.productCategory = productCategory;
        }

        public String getProductCondition() {
            return productCondition;
        }

        public void setProductCondition(String productCondition) {
            this.productCondition = productCondition;
        }

        public String getProductDescription() {
            return productDescription;
        }

        public void setProductDescription(String productDescription) {
            this.productDescription = productDescription;
        }

        public String getProductIsSold() {
            return productIsSold;
        }

        public void setProductIsSold(String productIsSold) {
            this.productIsSold = productIsSold;
        }

        public String getProductLat() {
            return productLat;
        }

        public void setProductLat(String productLat) {
            this.productLat = productLat;
        }

        public String getProductLng() {
            return productLng;
        }

        public void setProductLng(String productLng) {
            this.productLng = productLng;
        }

        public String getProductLocation() {
            return productLocation;
        }

        public void setProductLocation(String productLocation) {
            this.productLocation = productLocation;
        }

        public String getProductCity() {
            return productCity;
        }

        public void setProductCity(String productCity) {
            this.productCity = productCity;
        }

        public String getProductState() {
            return productState;
        }

        public void setProductState(String productState) {
            this.productState = productState;
        }

        public String getProductCountry() {
            return productCountry;
        }

        public void setProductCountry(String productCountry) {
            this.productCountry = productCountry;
        }

        public String getProductPrice() {
            return productPrice;
        }

        public void setProductPrice(String productPrice) {
            this.productPrice = productPrice;
        }

        public String getProductSpecifications() {
            return productSpecifications;
        }

        public void setProductSpecifications(String productSpecifications) {
            this.productSpecifications = productSpecifications;
        }

        public String getProductTitle() {
            return productTitle;
        }

        public void setProductTitle(String productTitle) {
            this.productTitle = productTitle;
        }

        public Users getUploadedByUserId() {
            return uploadedByUserId;
        }

        public void setUploadedByUserId(Users uploadedByUserId) {
            this.uploadedByUserId = uploadedByUserId;
        }

        public String getParentCategory() {
            return parentCategory;
        }

        public void setParentCategory(String parentCategory) {
            this.parentCategory = parentCategory;
        }

        public Object getBumpData() {
            return bumpData;
        }

        public void setBumpData(Object bumpData) {
            this.bumpData = bumpData;
        }

        public String getRank() {
            return rank;
        }

        public void setRank(String rank) {
            this.rank = rank;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public String getTime_diff() {
            return time_diff;
        }

        public void setTime_diff(String time_diff) {
            this.time_diff = time_diff;
        }

        public List<Image> getImages() {
            return images;
        }

        public void setImages(List<Image> images) {
            this.images = images;
        }

    }
    public class Image {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("image_name")
        @Expose
        private String imageName;
        @SerializedName("image_url")
        @Expose
        private String imageUrl;
        @SerializedName("post_id")
        @Expose
        private String postId;
        @SerializedName("uploaded_at")
        @Expose
        private String uploadedAt;
        @SerializedName("user_id")
        @Expose
        private String userId;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getImageName() {
            return imageName;
        }

        public void setImageName(String imageName) {
            this.imageName = imageName;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public String getPostId() {
            return postId;
        }

        public void setPostId(String postId) {
            this.postId = postId;
        }

        public String getUploadedAt() {
            return uploadedAt;
        }

        public void setUploadedAt(String uploadedAt) {
            this.uploadedAt = uploadedAt;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

    }

}