package com.bingalollc.com.homeactivity.homefragment.search

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.databinding.FragmentSearchBinding
import com.bingalollc.com.homeactivity.homefragment.adapter.AdapterSearch
import com.bingalollc.com.homeactivity.model.ProductModel
import com.bingalollc.com.preference.PreferenceManager
import com.bingalollc.com.utils.ViewBindingDialogFragment
import com.bingalollc.com.utils.afterTextChangedDebounce
import java.util.*
import kotlin.collections.ArrayList

class SearchFragment : ViewBindingDialogFragment<FragmentSearchBinding>(), AdapterSearch.OnItemSelect {
    private var adapterSearchTrending: AdapterSearch? = null
    private var adapterSearchRecent: AdapterSearch? = null
    private var adapterSearchNormal: AdapterSearch? = null
    private lateinit var filterTextArrayString: ArrayList<String>
    private lateinit var trendingSearchData: ArrayList<String>
    private lateinit var baseActivity: BaseActivity
    private lateinit var preferenceManager: PreferenceManager
    private var onItemSelected: OnItemSelected? = null
    private var onDialogClosedCalled = false;
    private var lastStringSearch = "";
    private var testDialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            preferenceManager = PreferenceManager(requireContext())
            baseActivity = BaseActivity()
           // binding.searchText.setText(lastStringSearch)
            filterTextArrayString = ArrayList()
            searchText?.requestFocus()
            baseActivity?.showKeyBoardOfView(context, searchText)
            initRecyclerview()
            initRecentSearch()
            initTrendingSearch()
            searchText.afterTextChangedDebounce(500) { newText ->
                if (!onDialogClosedCalled)
                    if (binding.searchText?.length()!! >0) {
                        rvProducts?.visibility = View.VISIBLE
                        onItemSelected?.onSearchSelected(
                            false,
                            binding.searchText?.text?.toString() ?: ""
                        )
                    } else
                        rvProducts?.visibility = View.GONE
            }
            cancelButton?.setOnClickListener({
                onItemSelected?.onCancelSelected(true, binding.searchText?.text?.toString() ?: "")
                dismiss()
            })


            binding.clearHistory.setOnClickListener {
                showNormalEditTextField(
                    context, getString(R.string.clear_search), getString(R.string.clear_search_desc),
                    getString(R.string.cancel), getString(R.string.clear)
                )

            }


            searchText.setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    onSearchItemSelected(searchText.text.toString())
                    true
                }
                false
            }
        }
    }

    fun showNormalEditTextField(
        context: Context?,
        headerOne: String?,
        headerTwo: String?,
        button1: String?,
        button2: String?
    ) {
        val alertDialog = AlertDialog.Builder(context!!)
        val inflater = layoutInflater
        val add_menu_layout = inflater.inflate(R.layout.edit_dialog_ui, null)
        val header_one = add_menu_layout.findViewById<TextView>(R.id.header_one)
        val header_two = add_menu_layout.findViewById<TextView>(R.id.header_two)
        var edit_text = add_menu_layout.findViewById<EditText>(R.id.edit_text)
        val cancelBtn = add_menu_layout.findViewById<TextView>(R.id.cancelBtn)
        val yesBtn = add_menu_layout.findViewById<TextView>(R.id.yesBtn)
        header_one.text = headerOne
        header_two.text = headerTwo
        cancelBtn.text = button1
        yesBtn.text = button2

        edit_text.visibility = View.GONE
        yesBtn.setTextColor(context.getColor(R.color.colorRed))
        header_two.setTextColor(context.getColor(R.color.black))
        alertDialog.setView(add_menu_layout)

        yesBtn.setOnClickListener {
            Handler(Looper.myLooper()!!).postDelayed({
                preferenceManager.recentSearches = ArrayList<String>()
                initRecentSearch()
            }, 200)
            testDialog?.dismiss()
        }

        cancelBtn.setOnClickListener {
            testDialog?.dismiss()
        }
        alertDialog.setView(add_menu_layout)
        alertDialog.setCancelable(false)
        testDialog = alertDialog.create()
        testDialog?.show()
        testDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))


    }


    fun trendingSearchData(trendingSearch: ArrayList<String>) {
        trendingSearchData = trendingSearch
    }

    private fun initRecentSearch() {
        val recentSearch = preferenceManager.recentSearches ?: ArrayList()
        if (recentSearch.size == 0) {
            binding.recentSearchLabel.visibility = View.GONE
            binding.rvRecentSearches.visibility = View.GONE
        } else {
            Collections.reverse(recentSearch)
            adapterSearchRecent?.refreshData(recentSearch)
        }
    }

    private fun initTrendingSearch() {
        if (trendingSearchData.size==0){
            binding.rvTrendingSearches.visibility = View.GONE
            binding.trendingSearchLabel.visibility = View.GONE
        }else {
            adapterSearchTrending?.refreshData(trendingSearchData)
        }
    }

    fun setLastStringSearch(lastStringSearch:String) {
        this.lastStringSearch = lastStringSearch

    }

    private fun initRecyclerview() {
        adapterSearchNormal = AdapterSearch(this)
        binding.rvProducts.adapter = adapterSearchNormal
         adapterSearchRecent = AdapterSearch(this)
        binding.rvRecentSearches.adapter = adapterSearchRecent
         adapterSearchTrending = AdapterSearch(this)
        binding.rvTrendingSearches.adapter = adapterSearchTrending
        binding.rvProducts.layoutManager = LinearLayoutManager(context)
        binding.rvRecentSearches.layoutManager = LinearLayoutManager(context)
        binding.rvTrendingSearches.layoutManager = LinearLayoutManager(context)
        binding.rvProducts.isNestedScrollingEnabled = false
        binding.rvRecentSearches.isNestedScrollingEnabled = false
        binding.rvTrendingSearches.isNestedScrollingEnabled = false
    }

    fun listenerCatcher(onItemSelected: OnItemSelected){
        this.onItemSelected = onItemSelected
    }
    override fun onSearchItemSelected(searchString: String) {
        binding.searchText.setText(searchString)
            val recentSearch = preferenceManager.recentSearches ?: ArrayList()
            if (recentSearch.contains(searchString)) {
                recentSearch.remove(searchString)
            }
            recentSearch.add(searchString)
            preferenceManager.recentSearches = recentSearch
        onDialogClosedCalled = true;
            onItemSelected?.onSearchSelected(true, searchString)
            dismiss()
    }

    fun initRecycler(product: ArrayList<ProductModel.Datum>){
        val productsList = ArrayList<String>()
        product.forEachIndexed { index, datum ->
            productsList.add(datum.productTitle)
        }
        adapterSearchNormal?.refreshData(productsList!!)
    }

    interface OnItemSelected {
        fun onSearchSelected(itemClicked: Boolean, searchString: String)
        fun onCancelSelected(itemClicked: Boolean, searchString: String)
    }

    override fun provideBinding(inflater: LayoutInflater): FragmentSearchBinding {
        return FragmentSearchBinding.inflate(inflater)
    }

}