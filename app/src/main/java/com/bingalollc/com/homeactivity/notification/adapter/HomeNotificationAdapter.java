package com.bingalollc.com.homeactivity.notification.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.bingalollc.com.R;
import com.bingalollc.com.homeactivity.notification.model.NotificationModel;
import com.bingalollc.com.preference.PreferenceManager;
import com.bingalollc.com.product_details.RateUser;
import com.bumptech.glide.Glide;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeNotificationAdapter extends RecyclerView.Adapter<HomeNotificationAdapter.Viewholder> {
    private Context context;
    private List<NotificationModel.Datum> notificationModel;
    private PreferenceManager preferenceManager;

    public HomeNotificationAdapter(List<NotificationModel.Datum> storiesModel, PreferenceManager preferenceManager) {
        this.notificationModel = storiesModel;
        this.preferenceManager = preferenceManager;
    }

    @NonNull
    @Override
    public HomeNotificationAdapter.Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context=parent.getContext();
        View view= LayoutInflater.from(context).inflate(R.layout.notification_item,parent,false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeNotificationAdapter.Viewholder holder, int position) {
            if (notificationModel.get(position).getNotification_link()!=null && !notificationModel.get(position).getNotification_link().isEmpty()){
                //📎
                holder.notification_title.setText(notificationModel.get(position).getNotificationTitle()+"  \uD83D\uDCCE");
            }else {
                holder.notification_title.setText(notificationModel.get(position).getNotificationTitle());
            }

        if (!notificationModel.get(position).getNotificationType().equals("12345")){
            if (notificationModel.get(position).getImage() != null){
                Glide.with(context).load(notificationModel.get(position).getImage()).error(R.drawable.admin_logo)
                        .into(holder.notification_welcome);
            } else if(notificationModel.get(position).getImage() == null){
                holder.notification_welcome.setImageDrawable(context.getResources().getDrawable(R.drawable.admin_logo));
            }
            else if (!notificationModel.get(position).getNotificationBy().getImage().isEmpty()) {
                String profileImage = notificationModel.get(position).getNotificationBy().getImage().replace("http://","https://");
                Glide.with(context).load(profileImage).error(R.drawable.admin_logo)
                        .into(holder.notification_welcome);
            }else {
                holder.notification_welcome.setImageDrawable(context.getResources().getDrawable(R.drawable.admin_logo));
            }
        }

        if (!notificationModel.get(position).getNotificationType().equals("12345")) {

            if (notificationModel.get(position).getNotificationDescription() != null) {
                if (notificationModel.get(position).getNotificationDescription().length() > 49) {
                    String text = notificationModel.get(position).getNotificationDescription().substring(0, 50) + " " + context.getString(R.string.read_more);
                    SpannableString content = new SpannableString(text);
                    ClickableSpan clickableSpan = new ClickableSpan() {
                        @Override
                        public void onClick(View textView) {
                            holder.descriptionDetails.setText(notificationModel.get(position).getNotificationDescription());
                        }

                        @Override
                        public void updateDrawState(@NonNull TextPaint ds) {
                            super.updateDrawState(ds);
                            ds.setColor(context.getResources().getColor(R.color.basecolor));
                            //draw underline base on true/false
                            ds.setUnderlineText(true);
                        }
                    };
                    content.setSpan(clickableSpan, text.length() - 9, text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    holder.descriptionDetails.setMovementMethod(LinkMovementMethod.getInstance());
                    holder.descriptionDetails.setHighlightColor(context.getResources().getColor(R.color.basecolor));
                    holder.descriptionDetails.setText(content);

                } else {
                    holder.descriptionDetails.setText(notificationModel.get(position).getNotificationDescription());
                }
            }
        }else{
            holder.descriptionDetails.setTextSize(12.0f);
            holder.descriptionDetails.setText(notificationModel.get(position).getNotificationDescription());
        }

        if (notificationModel.get(position).getTime_diff()!=null){
            holder.notification_time.setText(notificationModel.get(position).getTime_diff());
        }else {
            holder.notification_time.setText("");
        }


    }


    public void refreshPosition(int pos, List<NotificationModel.Datum> storiesModel){
        this.notificationModel = storiesModel;
        notifyItemChanged(pos);

    }
    public void refreshData(){
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return notificationModel.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        private TextView notification_title, descriptionDetails,notification_time;
        private CircleImageView notification_welcome;
        private final LinearLayout layoutFull;
        public Viewholder(@NonNull View itemView) {
            super(itemView);
            notification_title = itemView.findViewById(R.id.notification_title);
            descriptionDetails = itemView.findViewById(R.id.descriptionDetails);
            notification_time = itemView.findViewById(R.id.notification_time);
            notification_welcome = itemView.findViewById(R.id.notificationImage);
            layoutFull = itemView.findViewById(R.id.layoutFull);
            layoutFull.setOnClickListener(view -> {
                if ( notificationModel.get(getAdapterPosition()) != null ) {
                    if (!notificationModel.get(getAdapterPosition()).getNotificationType().equals("12345")) {
                        if (notificationModel.get(getAdapterPosition()).getNotificationType().equals("-1") &&
                                !TextUtils.isEmpty(notificationModel.get(getAdapterPosition()).getNotification_link())) {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(notificationModel.get(getAdapterPosition()).getNotification_link()));
                            context.startActivity(browserIntent);
                        } else if (notificationModel.get(getAdapterPosition()).getNotificationType().equals("2")) {
                            Intent intent = new Intent(context, RateUser.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("user_id", notificationModel.get(getAdapterPosition()).getNotificationBy().getId());
                            bundle.putString("user_push_token", notificationModel.get(getAdapterPosition()).getNotificationBy().getPushToken());
                            bundle.putString("user_img", notificationModel.get(getAdapterPosition()).getNotificationBy().getImage());
                            bundle.putString("user_name", notificationModel.get(getAdapterPosition()).getNotificationBy().getFullName());
                            if (notificationModel.get(getAdapterPosition()).getProduct_id() != null && notificationModel.get(getAdapterPosition()).getProduct_id().getImages() != null &&
                                    notificationModel.get(getAdapterPosition()).getProduct_id().getImages().size() > 0) {
                                bundle.putString("product_image", notificationModel.get(getAdapterPosition()).getProduct_id().getImages().get(0).getImageUrl());
                            } else {
                                bundle.putString("product_image", "");
                            }

                            if (notificationModel.get(getAdapterPosition()).getProduct_id() != null)
                                bundle.putString("product_id", notificationModel.get(getAdapterPosition()).getProduct_id().getImages().get(0).getId());
                            else
                                bundle.putString("product_id", "");
                            bundle.putBoolean("fromNotificationScreen", true);
                            intent.putExtra("data", bundle);
                            context.startActivity(intent);
                        }
                    }
                }
            });



        }
    }

    public void setMaxLengthOfText(int length, TextView textView){
        InputFilter[] fArray = new InputFilter[1];
        fArray[0] = new InputFilter.LengthFilter(length);
        textView.setFilters(fArray);

    }

}
