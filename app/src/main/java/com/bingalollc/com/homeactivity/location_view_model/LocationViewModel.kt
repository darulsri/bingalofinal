package com.bingalollc.com.homeactivity.location_view_model

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bingalollc.com.R
import com.bingalollc.com.homeactivity.model.ProductModel
import com.bingalollc.com.homeactivity.profilefragment.model.ProductCountOfUser
import com.bingalollc.com.model.ApiStatusModel
import com.bingalollc.com.network.RestClient
import com.bingalollc.com.preference.Common
import com.bingalollc.com.preference.PreferenceManager
import com.bingalollc.com.users.Users
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LocationViewModel:ViewModel() {


    fun updateLocation(preferenceManager: PreferenceManager, lat: String, lng: String, location: String){
        RestClient.getApiInterface().updateLocation(preferenceManager.userDetails.id,lat, lng, location).enqueue(object : Callback<ApiStatusModel>{
            override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {

            }

            override fun onResponse(call: Call<ApiStatusModel>, response: Response<ApiStatusModel>) {
                val usersData = preferenceManager.userDetails
                usersData.lat = lat
                usersData.lng = lng
                preferenceManager.userDetails = usersData
                Common.getInstance().refreshLatLong.postValue(true)
            }
        })
    }
}