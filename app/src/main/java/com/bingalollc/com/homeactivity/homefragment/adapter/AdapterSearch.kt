package com.bingalollc.com.homeactivity.homefragment.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bingalollc.com.R
import com.bingalollc.com.homeactivity.homefragment.search.SearchFragment
import com.bingalollc.com.homeactivity.model.ProductModel

class AdapterSearch(
    onItemSelect:OnItemSelect
) : RecyclerView.Adapter<AdapterSearch.ViewHolder>() {
    private lateinit var context: Context
    private var productModeldata: ArrayList<String> = ArrayList()
    private var onItemSelect : OnItemSelect = onItemSelect

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.search_layout, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textView.text = productModeldata?.get(position)

        holder.textView.setOnClickListener({
            onItemSelect.onSearchItemSelected(holder.textView.text.toString())
        })

    }

    override fun getItemCount(): Int {
        return productModeldata!!.size
    }

    fun refreshData(productModeldata: ArrayList<String>) {
        this.productModeldata = productModeldata
        notifyDataSetChanged()
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textView: TextView = itemView.findViewById(R.id.textView)


    }

    interface OnItemSelect{
        fun onSearchItemSelected(searchString: String)
    }

}
