package com.bingalollc.com.homeactivity.chatfragmet;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.bingalollc.com.R;
import com.bingalollc.com.base.BaseActivity;
import com.bingalollc.com.chat.ChatMessages;
import com.bingalollc.com.chat.SwipeUtil;
import com.bingalollc.com.chat.adapter.ChatAdapter;
import com.bingalollc.com.chat.model.BlockListModel;
import com.bingalollc.com.chat.model.ChatBox;
import com.bingalollc.com.homeactivity.HomeActivity;
import com.bingalollc.com.homeactivity.ViewPager.CircularPageIndicator;
import com.bingalollc.com.homeactivity.ViewPager.ImagesFragment;
import com.bingalollc.com.homeactivity.ViewPager.ViewPagerAdapter;
import com.bingalollc.com.homeactivity.model.AdsModel;
import com.bingalollc.com.homeactivity.model.ProductModel;
import com.bingalollc.com.network.RestClient;
import com.bingalollc.com.preference.Common;
import com.bingalollc.com.preference.PreferenceManager;
import com.bingalollc.com.report.ReportUser;
import com.bingalollc.com.users.UsersArrayModel;
import com.bingalollc.com.utils.SwipeHelper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ChatScreenFragment extends Fragment implements ChatAdapter.onClickedListen, DialogInterface.OnClickListener {
    private static final String TAG = "Conversations";
    ChatAdapter adapterServices;
    private int currentPage = 0;

    private LinearLayout topLayer;
    private TextView header;
    private ImageView back_icon;
    private BaseActivity baseActivity;
    private PreferenceManager preferenceManager;
    private RecyclerView recyclerView;
    private FirebaseFirestore mDb;
    private ImageView noConversation;
    private View parentLayout;
    private CountDownTimer detectFriendOnline;
    private FloatingActionButton floatButton;
    private AlertDialog alertDialog1;
    private EditText editText;
    private BroadcastReceiver deleteFriendReceiver;
    private ListenerRegistration mChatroomEventListener;
    private final ArrayList<ChatBox> chatBoxArrayList = new ArrayList<>();
    private final ArrayList<String> img = new ArrayList<>();
    private final ArrayList<String> names = new ArrayList<>();
    private final ArrayList<String> push_token = new ArrayList<>();
    private final ArrayList<String> userids = new ArrayList<>();
    private final ArrayList<String> productids = new ArrayList<>();
    private final ArrayList<String> prod_img = new ArrayList<>();
    private final ArrayList<String> profileImg = new ArrayList<>();
    private final ArrayList<Boolean> onlineStatus = new ArrayList<>();
    private final ArrayList<String> time = new ArrayList<>();
    private final ArrayList<String> completeKeyId = new ArrayList<>();
    private final ArrayList<String> isSold = new ArrayList<>();
    private final ArrayList<String> lastMsg = new ArrayList<>();
    private final ArrayList<Map<String, Boolean>> readStatus = new ArrayList<>();
    private ArrayList<ImagesFragment> fragmentList = new ArrayList<>();

    private boolean isThisScreen = true;
    private FirebaseAuth mAuth;
    private int clickItems = -1;
    private int clickItemsPos = -1;
    private DatabaseReference database;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ChatAdapter chatAdapter;
    private int totalReadCountNumber = 0;
    private SwipeHelper swipeHelper;
    private ImageView imgSatellite;
    private ViewPager vpImages;
    private CircularPageIndicator cipTutorial;



    public ChatScreenFragment() {
        // Required empty public constructor
    }

    public static ChatScreenFragment newInstance(String param1, String param2) {
        ChatScreenFragment fragment = new ChatScreenFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chat_screen, container, false);
        initViews(view);
        recyclerView = view.findViewById(R.id.recyclerView);
        mDb = FirebaseFirestore.getInstance();
        database = FirebaseDatabase.getInstance().getReference();
        parentLayout = view.findViewById(android.R.id.content);
        noConversation = view.findViewById(R.id.noConversation);
        vpImages = view.findViewById(R.id.vpImages);
        cipTutorial = view.findViewById(R.id.cipTutorial);
        if (preferenceManager.getIsEnglish() == 2) {
            noConversation.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.noconversation_hebrew));
        }
        floatButton = view.findViewById(R.id.fab);

        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .build();
        mDb.setFirestoreSettings(settings);

        mAuth = FirebaseAuth.getInstance();
        initClicks();

        return view;
    }

    private void initClicks() {
        imgSatellite.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.refresh));
        imgSatellite.setVisibility(View.VISIBLE);
        imgSatellite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // onViewCreated(getView(), getArguments());
                baseActivity.showToast("Refreshing", requireContext());
                isThisScreen = true;
                loadChats();
            }
        });

        swipeRefreshLayout.setOnRefreshListener(() -> {
            isThisScreen = true;
            loadChats();
        });
    }

    private void initViews(View view) {
        baseActivity = new BaseActivity();
        preferenceManager = PreferenceManager.getInstance(requireContext());
        topLayer = view.findViewById(R.id.topLayer);
        header = view.findViewById(R.id.header);
        back_icon = view.findViewById(R.id.back_icon);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        imgSatellite = view.findViewById(R.id.imgSatellite);
        topLayer.setBackgroundColor(requireContext().getResources().getColor(R.color.basecolor));
        header.setText(getString(R.string.chats));
        back_icon.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        fragmentList = new ArrayList<>();
    }

    private void loadChats() {
        CollectionReference chatroomsCollection = mDb
                .collection("Conversations");
        Query query = chatroomsCollection.whereArrayContains("userIDs", preferenceManager.getUserDetails().getId()).orderBy("timestamp", Query.Direction.DESCENDING);
        query.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@androidx.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @androidx.annotation.Nullable FirebaseFirestoreException e) {
                if(swipeRefreshLayout.isRefreshing()){
                    swipeRefreshLayout.setRefreshing(false);
                }
                if (isThisScreen) {
                    isThisScreen = false;
                    time.clear();
                    completeKeyId.clear();
                    isSold.clear();
                    lastMsg.clear();
                    readStatus.clear();
                    chatBoxArrayList.clear();
                    names.clear();
                    push_token.clear();
                    userids.clear();
                    productids.clear();
                    profileImg.clear();
                    onlineStatus.clear();
                    prod_img.clear();
                    totalReadCountNumber = 0;
                    if (queryDocumentSnapshots != null){
                        for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                            if (documentSnapshot.getData().get("lastMessage") != null && !documentSnapshot.getData().get("lastMessage").toString().equals("")) {
                                Log.d(TAG, "onEvent: " + documentSnapshot.getData());
                                ChatBox chatBox = new ChatBox();
                                chatBox.setId(documentSnapshot.getData().get("id").toString());
                                chatBox.setUserIDs((ArrayList<String>) documentSnapshot.getData().get("userIDs"));
                                chatBox.setIsRead((HashMap<String, Boolean>) (documentSnapshot.getData().get("isRead")));
                                // chatBox.setLastMessage(documentSnapshot.getData().get("lastMessage").toString());
                                //  chatBox.setTimestamp(Integer.parseInt(documentSnapshot.getData().get("timestamp").toString()));

                                ArrayList<String> arrayList = new ArrayList<>();
                                arrayList = (ArrayList<String>) documentSnapshot.getData().get("userIDs");
                                arrayList.remove(preferenceManager.getUserDetails().getId());
                                if (!Common.blockedUserId.contains(arrayList.get(0))) {
                                    userids.addAll(arrayList);
                                    time.add(documentSnapshot.getData().get("timestamp").toString());
                                    completeKeyId.add(documentSnapshot.getData().get("id").toString());
                                    lastMsg.add(documentSnapshot.getData().get("lastMessage").toString());

                                    if (((HashMap<String, Boolean>)(documentSnapshot.getData().get("isRead")) != null))
                                    {

                                        if (((HashMap<String, Boolean>) (documentSnapshot.getData().get("isRead"))).get(preferenceManager.getUserDetails().getId()) != null && ((HashMap<String, Boolean>) (documentSnapshot.getData().get("isRead"))).get(preferenceManager.getUserDetails().getId()).equals(false)) {
                                            totalReadCountNumber++;
                                        }
                                    }
                                    //readStatus.add(documentSnapshot.getData().get("readStatus").toString());
                                    chatBoxArrayList.add(chatBox);
                                    String prod_id = chatBox.getId().substring(chatBox.getId().lastIndexOf("-") + 1);
                                    productids.add(prod_id);
                                    prod_img.add("");
                                    isSold.add("");
                                    names.add("Loading...");
                                    profileImg.add("No Name");
                                    push_token.add("");
                                    onlineStatus.add(false);
                                }

                            }
                        }

                        updateReadStatus();
                }

                    if (chatBoxArrayList.size() == 0) {
                        noConversation.setVisibility(View.VISIBLE);
                        baseActivity.hideLoading();
                    } else {
                        noConversation.setVisibility(View.GONE);
                    }
                    initRecyclerView();

                    String userList = userids.toString().replace("[","").replace("]","");
                    RestClient.getApiInterface().getUsersList(userList,preferenceManager.getUserDetails().getToken()).enqueue(new Callback<UsersArrayModel>() {
                        @Override
                        public void onResponse(Call<UsersArrayModel> call, Response<UsersArrayModel> response) {
                            if (response.body()!=null && response.body().getData() != null){
                                for (int i=0; i<response.body().getData().size(); i++){
                                    names.set(i, response.body().getData().get(i).getFullName());
                                    push_token.set(i, response.body().getData().get(i).getPushToken());
                                    profileImg.set(i, response.body().getData().get(i).getImage());

                                }
                            }
                            baseActivity.hideLoading();
                            chatAdapter.updateProfileDetails(names,profileImg);
                            loadProductImages();
                            getUserStatus();
                        }

                        @Override
                        public void onFailure(Call<UsersArrayModel> call, Throwable t) {

                        }
                    });

                    /*for (int i = 0; i < userids.size(); i++) {
                        CollectionReference users = mDb
                                .collection("Users");
                        Query queryusers = users.whereEqualTo("id", userids.get(i));
                        queryusers.addSnapshotListener(new EventListener<QuerySnapshot>() {
                            @Override
                            public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                                for (QueryDocumentSnapshot documentSnapshot : value) {
                                    if (!documentSnapshot.getData().get("name").toString().equals("")) {
                                        names.add(documentSnapshot.getData().get("name").toString());
                                    } else {
                                        names.add("No Name");
                                    }
                                    profileImg.add(documentSnapshot.getData().get("profilePicLink").toString());

                                }
                                if (value.size() == 0) {
                                    names.add("No Name");
                                    profileImg.add("No Name");
                                }
                                if (names.size() == userids.size()) {
                                    baseActivity.hideLoading();
                                    loadProductImages();
                                    initRecyclerView();
                                }

                            }
                        });

                    }*/
                }


            }
        });


    }

    private void getUserStatus() {
        for (int i=0;i<userids.size();i++){
            database.child("online").child(userids.get(i)).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (onlineStatus.size() > 0) {
                        if (snapshot.exists()) {
                            onlineStatus.set(userids.indexOf(snapshot.getKey()), true);
                        } else {
                            onlineStatus.set(userids.indexOf(snapshot.getKey()), false);
                        }
                        chatAdapter.updateOnlineStatus(onlineStatus, userids.indexOf(snapshot.getKey()));
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }

    }

    private void getAds() {
        RestClient.getApiInterface().fetchMobileAds("7").enqueue(new Callback<AdsModel>() {
            @Override
            public void onResponse(@NonNull Call<AdsModel> call, @NonNull Response<AdsModel> response) {
                if (response.body() != null) {
                    for (AdsModel.Datum data : response.body().getData()) {
                        if (data.getImage() != null) {
                            fragmentList.add(addFrag(data.getImage(), data.getLink()));
                        }
                    }
                    Collections.shuffle(fragmentList);
                    setUpViewPager();
                }
            }

            @Override
            public void onFailure(Call<AdsModel> call, Throwable t) {

            }
        });
    }


    private ImagesFragment addFrag(String tutsImage, String tutsImageUrl) {
        Bundle bundle = new Bundle();
        bundle.putString("tutImages", tutsImage);
        bundle.putString("tutsImageUrl", tutsImageUrl);
        ImagesFragment imagesFragment = new ImagesFragment();
        imagesFragment.setArguments(bundle);
        return imagesFragment;
    }

    private Float dpToPixels(Context context) {
        return 2 * context.getResources().getDisplayMetrics().density;
    }

    private void setUpViewPager() {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(
                getChildFragmentManager(),
                fragmentList,
                dpToPixels(requireContext()),
                getActivity()
        );
        //  vpImages!!.offscreenPageLimit = 3
        vpImages.setAdapter(viewPagerAdapter);
        viewPagerAdapter.notifyDataSetChanged();
        cipTutorial.setViewPager(vpImages);
        vpImages.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                currentPage = position;
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        Handler h = new Handler(Looper.getMainLooper());
        Runnable r = new Runnable() {
            @Override
            public void run() {
                if (currentPage >= fragmentList.size()) currentPage = -1;
                vpImages.setCurrentItem(currentPage++,true);
                h.postDelayed(this,3000);
            }
        };
        h.postDelayed(r,1000);
    }


    private void loadProductImages() {
        String prodIds = productids.toString().replace("[","").replace("]","");
        RestClient.getApiInterface().getProductDetails(prodIds,preferenceManager.getUserDetails().getToken()).enqueue(new Callback<ProductModel>() {
            @Override
            public void onResponse(Call<ProductModel> call, Response<ProductModel> response) {
                if (response.body() != null && response.body().getData() != null){
                    for (int i = 0; i<response.body().getData().size(); i++){
                        if (response.body().getData()!= null && response.body().getData().get(i).getImages() !=null  && response.body().getData().get(i).getImages().size()>0)
                        prod_img.set(i,response.body().getData().get(i).getImages().get(0).getImageUrl());
                        isSold.set(i,response.body().getData().get(i).getProductIsSold());
                    }
                    chatAdapter.updateProImages(prod_img);
                }
            }

            @Override
            public void onFailure(Call<ProductModel> call, Throwable t) {

            }
        });
    }


    private void initRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(requireContext());
         chatAdapter = new ChatAdapter(onlineStatus,prod_img,profileImg, names, time, lastMsg, chatBoxArrayList, preferenceManager, this, isSold);
        recyclerView.setAdapter(chatAdapter);
        recyclerView.setLayoutManager(linearLayoutManager);
//        recyclerView.setNestedScrollingEnabled(false);
        setSwipeForRecyclerView();


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                isThisScreen = true;
            }
        }, 1000);

    }

    private void  setSwipeForRecyclerView() {
         swipeHelper = new SwipeHelper(requireContext(),recyclerView, false) {
            @Override
            public void instantiateUnderlayButton(RecyclerView.ViewHolder viewHolder, List<UnderlayButton> underlayButtons) {
                Bitmap deleteIcon = BitmapFactory.decodeResource(getResources(), R.drawable.btn_delete_white);
                deleteIcon = Bitmap.createScaledBitmap(deleteIcon, 100, 120, false);
                Bitmap reportIcon = BitmapFactory.decodeResource(getResources(), R.drawable.btn_report_white);
                reportIcon = Bitmap.createScaledBitmap(reportIcon, 100, 120, false);
                Bitmap blockIcon = BitmapFactory.decodeResource(getResources(), R.drawable.btn_block);
                blockIcon = Bitmap.createScaledBitmap(blockIcon, 100, 120, false);
                underlayButtons.add(new SwipeHelper.UnderlayButton(
                        getString(R.string.delete),
                        deleteIcon,
                        requireContext().getResources().getColor(R.color.dark_red),
                        pos -> {
                            chatAdapter.refreshItem(pos);
                            clickItems = 0;
                            clickItemsPos = pos;
                            baseActivity.showMessageOKCancel(getString(R.string.delete_confirmation_chat), "", ChatScreenFragment.this,requireContext(),"Delete","Cancel",false);
                        }
                ));


                underlayButtons.add(new SwipeHelper.UnderlayButton(
                        getString(R.string.report),
                        reportIcon,
                        Color.parseColor("#FF9502"),
                        pos -> {
                            chatAdapter.refreshItem(pos);
                            clickItems = 1;
                            clickItemsPos = pos;
                            Intent intent = new Intent(requireContext(), ReportUser.class);
                            intent.putExtra("usedId", userids.get(clickItemsPos));
                            intent.putExtra("fullName",names.get(clickItemsPos));
                            intent.putExtra("userImage",profileImg.get(clickItemsPos));
                            intent.putExtra("productId",productids.get(clickItemsPos));
                            clickItemsPos = -1;
                            startActivity(intent);
                        }
                ));


                underlayButtons.add(new SwipeHelper.UnderlayButton(
                        getString(R.string.block),
                        blockIcon,
                        getResources().getColor(R.color.basecolor),
                        new SwipeHelper.UnderlayButtonClickListener() {
                            @Override
                            public void onClick(int pos) {
                                chatAdapter.refreshItem(pos);
                                clickItems = 2;
                                clickItemsPos = pos;
                                baseActivity.showMessageOKCancel(getString(R.string.are_you_sure_want_to_block),"", ChatScreenFragment.this,requireContext(),getString(R.string.block),
                                        getString(R.string.cancel),false);
                            }
                        }
                ));

            }
        };
    }


    private void findEmailID(String email) {
        CollectionReference chatroomsCollection = mDb
                .collection("Users");
        Query query = chatroomsCollection.whereEqualTo("email", email);
        mChatroomEventListener = query.addSnapshotListener(new com.google.firebase.firestore.EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                for (QueryDocumentSnapshot doc : queryDocumentSnapshots) {
                    Log.d("TAG", doc.getId());
                    String id = doc.getData().get("id").toString();
                    buildNewChatroom(id);


                }
            }
        });
    }

    private void buildNewChatroom(String id) {
        String s = "";
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .build();
        mDb.setFirestoreSettings(settings);
        if (Integer.parseInt(preferenceManager.getUserDetails().getId()) < Integer.parseInt(id)) {
            s = preferenceManager.getUserDetails().getId() + "-" + id;
        } else {
            s = id + "-" + preferenceManager.getUserDetails().getId();
        }


        DocumentReference newChatroomRef = mDb
                .collection("Conversations")
                .document(s);
        Long tsLong = System.currentTimeMillis() / 1000;
        String timestamp = tsLong.toString();

        HashMap<String, Boolean> isReadContent = new HashMap<>();
        isReadContent.put(id, false);
        isReadContent.put(preferenceManager.getUserDetails().getId(), false);

        ArrayList<String> as = new ArrayList<>();
        as.add(id);
        as.add(preferenceManager.getUserDetails().getId());
        ChatBox chatBox = new ChatBox();
        chatBox.setId(s);
        chatBox.setUserIDs(as);
        chatBox.setTimestamp(Integer.parseInt(timestamp));
        chatBox.setLastMessage("");
        chatBox.setIsRead(isReadContent);
        newChatroomRef.set(chatBox).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {


                if (task.isSuccessful()) {
                    alertDialog1.dismiss();

                } else {
                    alertDialog1.dismiss();
                    Snackbar.make(parentLayout, "Something went wrong.", Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }


    @Override
    public void onLayoutClick(int position) {
        try {
            if (chatBoxArrayList == null || chatBoxArrayList.get(position) == null) return;
        }catch (Exception e) {return;}

        String newid = chatBoxArrayList.get(position).getId();
       // String newid_other = chatBoxArrayList.get(position).getId().replace(preferenceManager.getUserDetails().getId(), "").replace("-", "");
        ArrayList<String> newid_other_array = chatBoxArrayList.get(position).getUserIDs();
        newid_other_array.remove(preferenceManager.getUserDetails().getId());
        String newid_other = newid_other_array.get(0);

        DocumentReference newChatroomRef = mDb
                .collection("Conversations")
                .document(newid);
        HashMap<String, Boolean> isReadContent = new HashMap<>();
        isReadContent.put(newid_other, true);
        isReadContent.put(preferenceManager.getUserDetails().getId(), true);
        HashMap<String, Object> hs = new HashMap<>();
        hs.put("isRead", isReadContent);
        newChatroomRef.update(hs);

        database.child(newid).child(preferenceManager.getUserDetails().getId()).setValue(System.currentTimeMillis()/1000);

        Intent intent = new Intent(requireContext(), ChatMessages.class);
        intent.putExtra("id", preferenceManager.getUserDetails().getId());
        intent.putExtra("other_id", chatBoxArrayList.get(position).getId());
        intent.putExtra("other_user_id", newid_other);
        intent.putExtra("prof_img", profileImg.get(position));
        intent.putExtra("fromDetails", false);
        intent.putExtra("friendname", names.get(position));
        intent.putExtra("friendToken", push_token.get(position));
        startActivity(intent);
    }

    @Override
    public void listPosition(int position) {
        String newid = chatBoxArrayList.get(position).getId();
        System.out.println(">>>>..newID "+newid);
        String newid_other = chatBoxArrayList.get(position).getId().replace(preferenceManager.getUserDetails().getId(), "").replace("-", "");

        DocumentReference newChatroomRef = mDb
                .collection("Conversations")
                .document(newid);
        newChatroomRef.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
            }
        });
    }

    @Override
    public void onErrorCalled() {
        // finish();
        //startActivity(getIntent());
    }

    private void updateReadStatus() {
        try {
            ((HomeActivity)getActivity()).updateChatCount(totalReadCountNumber);
        }catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onDetach() {
        isThisScreen = false;
        super.onDetach();

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getAds();
        isThisScreen = true;
        try {
            baseActivity.showLoading(requireContext());
            loadChats();
        } catch (Exception e) {
        }
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        if (clickItems==0){
            // DELETE
           // baseActivity.showLoading(requireContext());
            //completeKeyId
            chatAdapter.removeItem(clickItemsPos);
            listPosition(clickItemsPos);
            clickItemsPos = -1;

        }else if (clickItems == 2){
           //BLOCK
            baseActivity.showLoading(requireContext());
            uploadBlockedListToFirebase();
        }
    }

    private void uploadBlockedListToFirebase() {
        String completeId = chatBoxArrayList.get(clickItemsPos).getId();
        String otherUserId= userids.get(clickItemsPos);
        DocumentReference newChatroomRef = mDb
                .collection("BlockedUsers")
                .document(completeId);

        Long tsLong = System.currentTimeMillis() / 1000;
        String timestamp = tsLong.toString();
        ArrayList<String> as = new ArrayList<>();
        as.add(otherUserId);
        Common.blockedUserId.add(otherUserId);
        as.add(preferenceManager.getUserDetails().getId());
        BlockListModel blockListModel = new BlockListModel();
        blockListModel.setId(completeId);
        blockListModel.setUserIDs(as);
        blockListModel.setTimestamp(Integer.parseInt(timestamp));
        blockListModel.setBlockedBy(preferenceManager.getUserDetails().getId());
        newChatroomRef.set(blockListModel).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                baseActivity.hideLoading();
                if (!task.isSuccessful()) {
                    Snackbar.make(parentLayout, "Something went wrong.", Snackbar.LENGTH_SHORT).show();
                }else {
                    imgSatellite.performClick();
                   // chatAdapter.removeItem(clickItemsPos);
                }
                clickItems = -1;
                clickItemsPos = -1;
            }
        });
    }
}