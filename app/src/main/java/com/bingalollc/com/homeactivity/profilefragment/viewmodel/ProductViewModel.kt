package com.bingalollc.com.homeactivity.profilefragment.viewmodel

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bingalollc.com.R
import com.bingalollc.com.homeactivity.model.ProductModel
import com.bingalollc.com.homeactivity.profilefragment.model.ProductCountOfUser
import com.bingalollc.com.network.RestClient
import com.bingalollc.com.preference.Common
import com.bingalollc.com.preference.PreferenceManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProductViewModel:ViewModel() {


    fun getOwnProduct(preferenceManager: PreferenceManager){
        RestClient.getApiInterface().getProductByUserId("","-1").enqueue(object : Callback<ProductModel>{
            override fun onFailure(call: Call<ProductModel>, t: Throwable) {

            }

            override fun onResponse(call: Call<ProductModel>, response: Response<ProductModel>) {
                if (response.body()?.status==200 && response.body()?.data!=null && response.body()?.data?.size!!>0){
                    Common.getInstance().allProductOfUser.postValue(response.body()!!.data as ArrayList<ProductModel.Datum>?)
                }
            }
        })
    }
}