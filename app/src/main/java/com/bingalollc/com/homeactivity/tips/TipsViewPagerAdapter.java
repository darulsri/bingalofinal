package com.bingalollc.com.homeactivity.tips;


import android.content.Context;
import android.view.ViewGroup;

import androidx.annotation.Keep;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.bingalollc.com.homeactivity.ViewPager.ImageTipsFragment;
import com.bingalollc.com.homeactivity.ViewPager.ImagesFragment;

import java.util.List;

@Keep
public class TipsViewPagerAdapter extends FragmentStatePagerAdapter {
    private List<ImageTipsFragment> fragments;
    private FragmentManager fm;
    private float baseElevation;
    private Context mContext;

    /**
     * Constructor
     *
     * @param fm            instance of fragment manager
     * @param fragments     list for fragments
     * @param baseElevation base elevation
     * @param context       context
     */
    public TipsViewPagerAdapter(final FragmentManager fm, final List<ImageTipsFragment> fragments, final float baseElevation, final Context context) {
        super(fm);
        this.fragments = fragments;
        this.fm = fm;
        this.baseElevation = baseElevation;
        mContext = context;
    }

    @Override
    public Fragment getItem(final int position) {
        return this.fragments.get(position);

    }

    @Override
    public int getCount() {
        return fragments == null ? 0 : fragments.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        Object fragment = super.instantiateItem(container, position);
        fragments.set(position, (ImageTipsFragment) fragment);


        return fragment;
    }

    /**
     * Clear data.
     */
    public void clearData() {
        fragments.clear();
    }

}