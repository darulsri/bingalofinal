package com.bingalollc.com.homeactivity.profilefragment

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.os.Bundle
import android.text.InputFilter
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import androidx.fragment.app.DialogFragment
import com.bingalollc.com.R
import com.bingalollc.com.databinding.ActivityVerifyOtpBinding
import com.bingalollc.com.utils.ViewBindingDialogFragment
//import kotlinx.android.synthetic.main.app_bar.*

class VerifyOtpActivity : ViewBindingDialogFragment<ActivityVerifyOtpBinding>() {

    var onVerifiedClicked: OnVerifiedClicked? = null
    private var from = ""

    override fun provideBinding(inflater: LayoutInflater): ActivityVerifyOtpBinding {
        return ActivityVerifyOtpBinding.inflate(inflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isCancelable = false
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle)
    }

    public fun setFrom(from: String) {
        this.from = from
    }

    fun setKetListener(onVerifiedClicked: OnVerifiedClicked) {
        this.onVerifiedClicked = onVerifiedClicked
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if(from == "HomeFragment"){
//            back_icon.visibility = View.GONE
        }

        with(binding) {
            ccp?.setCountryForNameCode("IL")
            ccp?.setCustomMasterCountries("il,gb,us,in")
            ccp?.setOnCountryChangeListener { checkValidNumber() }
            appBar.backIcon.setOnClickListener { dismiss() }
            appBar.header.text = getString(R.string.verifyOtp)



            submitBtn.setOnClickListener {
                if (etPhone.text?.length!! > 0) {
                    onVerifiedClicked?.OnOtpSendClicked("+"+binding.ccp.selectedCountryCode+  etPhone.text.toString())
                    headerIcon.setImageDrawable(requireContext().getDrawable(R.drawable.icon_otp))
                    headerText.setText(getString(R.string.we_have_sent_otp))
                    headerTextTwo.setText(ccp?.selectedCountryCode.toString()+" "+etPhone.text.toString())
                    otpLayout.requestFocus()
                    headerTextTwo.visibility = View.VISIBLE
                    otpLayout.visibility = View.VISIBLE
                    clickBtn.visibility = View.VISIBLE
                    phoneLayout.visibility = View.GONE
                    submitBtn.visibility = View.GONE
                }else{
                    etPhone.setError(getString(R.string.enter_phone_number))
                }
            }

            verify.setOnClickListener {
                if (binding.otpLayout.getOtpEntered(requireContext()).length > 5) {
                    onVerifiedClicked?.OnVerifiedClicked(binding.otpLayout.getOtpEntered(requireContext()))
                }

            }

            resendOtp?.setOnClickListener {  onVerifiedClicked?.OnOtpSendClicked(etPhone.text.toString()) }
        }
    }



  /*  override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (binding.clickBtn.visibility == View.VISIBLE) {
            if (binding.otpLayout.isOtpValid()) {
                binding.verify.background = getDrawable(R.drawable.curve6dp_basecolor)
            } else {
                binding.verify.background = getDrawable(R.drawable.curve6dp_grey)
            }
        }
        return super.onKeyDown(keyCode, event)
    }*/

    private fun checkValidNumber() {
        with(binding) {
            val countryCode = ccp?.selectedCountryCode.toString()
            var MAX = 12
            if (countryCode == "972") {
                MAX = 9
            } else if (countryCode == "91") {
                MAX = 10
            }
            etPhone?.setFilters(arrayOf<InputFilter>(InputFilter.LengthFilter(MAX)))
            if (etPhone?.length()?:0 > MAX) {
                etPhone?.setText(etPhone?.text?.toString()?.substring(0,MAX))
                etPhone?.setSelection(etPhone?.text?.length?:0)
            }
        }

    }

    interface OnVerifiedClicked{
        fun OnVerifiedClicked(otp: String)
        fun OnOtpSendClicked(number: String)
    }

    override fun onResume() {
        super.onResume()

        dialog!!.setOnKeyListener { dialog, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                // To dismiss the fragment when the back-button is pressed.
                if(from == "HomeFragment"){

                    false
                }else {
                    dismiss()
                        true
                }
            } else false
        }
    }

}