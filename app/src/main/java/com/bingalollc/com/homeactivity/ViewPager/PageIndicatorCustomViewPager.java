package com.bingalollc.com.homeactivity.ViewPager;

import androidx.annotation.Keep;
import androidx.viewpager.widget.ViewPager;

import com.bingalollc.com.utils.CustomViewPager;


/**
 * A PageIndicator is responsible to show an visual indicator on the total views
 * number and the current visible view.
 */
@Keep
public interface PageIndicatorCustomViewPager extends ViewPager.OnPageChangeListener {
    /**
     * Bind the indicator to a ViewPager.
     *
     * @param view the view
     */
    void setViewPager(final CustomViewPager view);

    /**
     * Bind the indicator to a ViewPager.
     *
     * @param view            the view
     * @param initialPosition the initial position
     */
    void setViewPager(final CustomViewPager view, final int initialPosition);

    /**
     * <p>Set the current page of both the ViewPager and indicator.</p>
     * <p>
     * <p>This <strong>must</strong> be used if you need to set the page before
     * the views are drawn on screen (e.g., default start page).</p>
     *
     * @param item the item
     */
    void setCurrentItem(final int item);

    /**
     * Set a page change listener which will receive forwarded events.
     *
     * @param listener the listener
     */
    void setOnPageChangeListener(final CustomViewPager.OnPageChangeListener listener);

    /**
     * Notify the indicator that the fragment list has changed.
     */
    void notifyDataSetChanged();
}


