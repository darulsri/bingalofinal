package com.bingalollc.com.homeactivity.notification.model;

import com.google.gson.annotations.SerializedName;

public class NotificationCountModel{

	@SerializedName("data")
	private String data;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public String getData(){
		return data;
	}

	public String getMessage(){
		return message;
	}

	public int getStatus(){
		return status;
	}
}