package com.bingalollc.com.homeactivity.homefragment

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentSender.SendIntentException
import android.content.pm.PackageManager
import android.location.LocationManager
import android.net.Uri
import android.os.*
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.*
import android.widget.AbsListView.OnScrollListener.SCROLL_STATE_IDLE
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getColor
import androidx.core.content.res.ResourcesCompat
import androidx.core.text.isDigitsOnly
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewpager.widget.ViewPager
import co.lujun.androidtagview.TagContainerLayout
import co.lujun.androidtagview.TagView
import com.bingalo.com.homeactivity.homefragment.adapter.homapageadapter
import com.bingalo.com.homeactivity.homefragment.adapter.shoplisting_adapter
import com.bingalo.com.homeactivity.model.ShopListingModel
import com.bingalo.com.homeactivity.model.data
import com.bingalo.com.homeactivity.model.homebannerModel
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.base.BaseActivity.LOCATION_PERMISSION
import com.bingalollc.com.bottom_sheet.AddPhoneNumberPopUpDialog
import com.bingalollc.com.camerax.CameraPostProduct
import com.bingalollc.com.homeactivity.HomeActivity
import com.bingalollc.com.homeactivity.ViewPager.CircularPageIndicator
import com.bingalollc.com.homeactivity.ViewPager.ImagesFragment
import com.bingalollc.com.homeactivity.ViewPager.ViewPagerAdapter
import com.bingalollc.com.homeactivity.fragment.FilterFragment
import com.bingalollc.com.homeactivity.homefragment.adapter.ProductAdapter
import com.bingalollc.com.homeactivity.homefragment.search.SearchFragment
import com.bingalollc.com.homeactivity.location_view_model.LocationViewModel
import com.bingalollc.com.homeactivity.model.*
import com.bingalollc.com.homeactivity.profilefragment.VerifyOtpActivity
import com.bingalollc.com.homeactivity.profilefragment.viewmodel.ProductViewModel
import com.bingalollc.com.model.ApiStatusModel
import com.bingalollc.com.network.RestClient
import com.bingalollc.com.preference.Common
import com.bingalollc.com.preference.PreferenceManager
import com.bingalollc.com.product_details.ProductDetails
import com.bingalollc.com.stores.storehome
import com.bingalollc.com.stores.storepage
import com.bingalollc.com.users.Users
import com.bingalollc.com.utils.AppLocationService
import com.bingalollc.com.utils.showDialogFragment
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsStatusCodes
import com.google.android.material.card.MaterialCardView
import com.google.android.material.tabs.TabLayout
import com.google.firebase.messaging.FirebaseMessaging
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.NullPointerException
import java.util.*
import kotlin.collections.ArrayList


class HomeFragment : Fragment(), ProductAdapter.OnItemChoosed, View.OnKeyListener, VerifyOtpActivity.OnVerifiedClicked{
    private val PRODUCT_DETAILS_PAGE = 1001
    private var limit = 1000
    private var offset = 0
    private var constantDistance = 50

    var locationManager: LocationManager? = null
    var fragmentList: ArrayList<ImagesFragment>? = null
    private var cipTutorial: CircularPageIndicator? = null
    private var vpImages: ViewPager? = null
    private var currentPage = 0
    private var isDataLoading = false
    private var baseActivity: BaseActivity? = null
    private var adminMessageLayout: LinearLayout? = null
    private var difference_km: MaterialCardView? = null
    private var distanceText: TextView? = null
    private var rv_products: RecyclerView? = null
    private var filter_btn: ImageView? = null
    private var lociconOne: ImageView? = null
    private var lociconTwo: ImageView? = null
    private var progress_bar: ProgressBar? = null
    private var no_prod_available: LinearLayout? = null
    private var no_search_data_image: ImageView? = null
    private var no_search_data_text: TextView? = null
    private var searchText: TextView? = null
    private var report_issue: RelativeLayout? = null
    private var searchCrossBtn: ImageView? = null
    private var productModeldata: ArrayList<ProductModel.Datum>? = null
    private var filterModel: FilterModel? = FilterModel()
    private var rankWiseProduct: ArrayList<String>? = null
    private var productAdapter: ProductAdapter? = null
    private var linearLayoutManager: GridLayoutManager? = null
    private var pullToRefresh: SwipeRefreshLayout? = null
    private var scrollView: NestedScrollView? = null
    private var tabLayout: TabLayout? = null
    private var toolbar: RelativeLayout? = null
    private var searchFragment: SearchFragment? = null
    private var filterFragment: FilterFragment? = null
    private var mTagContainerLayout: TagContainerLayout? = null
    private var timer: Timer? = null
    private var preferenceManager: PreferenceManager?=null
    private var productViewModel: ProductViewModel? =null
    private var locationViewModel: LocationViewModel? =null
    private var adminCurrentMsg: String? =null
    private var adminCategory  = ""
    private var currentLatitude  = ""
    private var currentLongitude  = ""
    private var adminMessages: TextView? =null
    private var categoriesData: MutableList<CategoriesModel.Datum>? = null
    private var verifyOtpActivity: VerifyOtpActivity? = null

    private var seeall : TextView? = null

    private var recycler_shop:RecyclerView? = null
    private  var shopadapter: shoplisting_adapter?=null
    private var shopicon:List<data>?=null

    private  var homapageadapter:homapageadapter?=null
    private var homebanner:RecyclerView?=null
    private var homepagebanner:ArrayList<homebannerModel.data>?=null

    private  var linearLayoutManager_1: LinearLayoutManager?=null

    companion object {
        @JvmStatic lateinit var instance: HomeFragment
    }

    init {
        instance = this
    }

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//
//    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        FirebaseMessaging.getInstance().subscribeToTopic("News")
        initView(view)
        initClicks()

        fetchStores()
        fetchTopBanner()

        if (!Common.productLocationLat.equals("") && !Common.productLocationLng.equals("")) {
            searchNearByProducts(false)
        }
        else {
            getPermission(true)
        }

        recycler_shop?.layoutManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
        shopicon = ArrayList()
        shopadapter = context?.let { shoplisting_adapter(it, shopicon as ArrayList<data>) }
        recycler_shop?.adapter = shopadapter

        shopadapter?.onItemClick = {
            val intent = Intent (context,storehome::class.java)
            intent.putExtra("store_id",it)
            startActivity(intent)
        }

        seeall?.setOnClickListener {

            requireActivity().run{
                startActivity(Intent(this, storepage::class.java))
                finish()
            }

        }




        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        productViewModel = ViewModelProvider(this).get(ProductViewModel::class.java)
        locationViewModel = ViewModelProvider(this).get(LocationViewModel::class.java)
        searchCrossBtn?.visibility = View.GONE

//        GlobalScope.launch(Dispatchers.Main) {
//            // Switch to a background (IO) thread
//            val retval = withContext(Dispatchers.IO) {
//                Log.e("TASK", "Started background task")
//
//
//
//
//
//                val retval = "The value from background"
//                Thread.sleep(1000)
//                Log.e("TASK", "Finished background task with result: " + retval)
//                retval
//            }
//            // Now you're back the main thread
//            Log.e("TASK", "Started task in Main thread with result from Background: " + retval)
//        }

        pullToRefresh!!.setOnRefreshListener {
                getProductData()
                getTrendingproducts()
                searchAdminMessage()
                updateNotiReadStatus()
                getFilterCategories()

        }
     //   startTimer()
/*
        if(preferenceManager!!.isUserLoggedIn){
            if(preferenceManager!!.userDetails.phone_verified == "0") {

                verifyOtpActivity = showDialogFragment<VerifyOtpActivity>()
                verifyOtpActivity?.setFrom("HomeFragment")
                verifyOtpActivity?.setKetListener(this)
            }
        }*/
        startTimer()
        getAds()

        productViewModel?.getOwnProduct(preferenceManager!!)


    }


    private fun fetchTopBanner(){
        RestClient.getApiInterface().fetchTopBanner().enqueue(object : Callback<homebannerModel>{
            override fun onResponse(
                call: Call<homebannerModel>,
                response: Response<homebannerModel>
            ) {
                homepagebanner = ArrayList()

                if (response.body()?.getData()!=null)
                {
                    homepagebanner?.addAll(response.body()?.getData()!!)
                    homapageadapter = context?.let { homapageadapter(it,homepagebanner) }
                    linearLayoutManager_1 = LinearLayoutManager(context)
                    homebanner?.adapter = homapageadapter
                    homebanner?.layoutManager = linearLayoutManager_1

                }

                Log.e("Success_topbanner", response.body()?.getMessage().toString())
            }

            override fun onFailure(call: Call<homebannerModel>, t: Throwable) {

            }

        })
    }


    private fun fetchStores(){
        RestClient.getApiInterface().fetchStores().enqueue(object : Callback<ShopListingModel> {
            override fun onResponse(
                call: Call<ShopListingModel>,
                response: Response<ShopListingModel>
            ) {
                if (response.isSuccessful){
                    val shopListingModel = response.body()
                    if (shopListingModel!=null){
                        val data =shopListingModel.data
                        shopadapter?.updateData(data)


                    }

                    Log.e("Success", shopListingModel?.message.toString())
                }

            }

            override fun onFailure(call: Call<ShopListingModel>, t: Throwable) {

                Log.e("Error", t.message.toString())

            }

        })
    }


    private fun updateNotiReadStatus() {
        try {
            (activity as HomeActivity?)!!.fetchNotificationCount(preferenceManager?.userDetails?.id.toString(), preferenceManager?.userDetails?.token.toString())
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    var cTimer: CountDownTimer? = null

    //start timer function
    fun startTimer() {
        cTimer = object : CountDownTimer(5000, 1000) {
            override fun onTick(millisUntilFinished: Long) {}
            override fun onFinish() {
                Log.d("home fragment", "home fragment after 10 sec")
                getTrendingproducts()
                searchAdminMessage()
                getFilterCategories()
            }
        }
        (cTimer as CountDownTimer).start()
    }

    class doAsync(val handler: () -> Unit) : AsyncTask<Void, Void, Void>() {
        override fun doInBackground(vararg params: Void?): Void? {
            handler()
            return null
        }
    }

    fun getFilterCategories() {
        RestClient.getApiInterface().categories.enqueue(object : Callback<CategoriesModel> {
            override fun onFailure(call: Call<CategoriesModel>, t: Throwable) {
            }

            override fun onResponse(
                call: Call<CategoriesModel>,
                response: Response<CategoriesModel>
            ) {
                if (response.body() != null && response.body()?.status == 200 && response.body()?.data != null) {
                    categoriesData = response.body()?.data!!
                }
            }
        })
    }

    fun getProductData() {
        offset =0;
        if (!Common.currentLat.isNullOrEmpty()) {
            findProducts(false)
            pullToRefresh!!.isRefreshing = false
        }else{
            pullToRefresh!!.isRefreshing = false
            getPermission(true)
        }
    }

    private fun searchAdminMessage() {
        adminMessageLayout?.visibility = View.GONE
        RestClient.getApiInterface().getAdminMessages().enqueue(object :
            Callback<AdminMessageModel> {
            override fun onFailure(call: Call<AdminMessageModel>, t: Throwable) {

            }

            override fun onResponse(
                call: Call<AdminMessageModel>,
                response: Response<AdminMessageModel>
            ) {
                if (response.body()?.status != 200) {
                    return
                }
                val hasmap = HashMap<String, Int>()
                val localHashMap = preferenceManager?.adminMessage
                var endIndex = 4
                if (response.body()?.data?.message.isNullOrEmpty().not()){
                    var str = "NEW! "+response.body()?.data?.message
                    if (preferenceManager?.isEnglish == 2) {
                        endIndex = 7
                        str = "חָדָשׁ! "+response.body()?.data?.hebrew_message
                    }

                    baseActivity?.setSpannableString(
                        requireContext(), str, adminMessages, 0,
                        endIndex,
                        getResources().getColor(R.color.colorRed)
                    )
                }
                // adminMessages?.text = response.body()?.data?.message
                adminCurrentMsg = response.body()?.data?.message
                if (preferenceManager?.getIsEnglish() == 1) {
                    adminCategory = response.body()?.data?.category?.categoryName?:""
                }else {
                    adminCategory = response.body()?.data?.category?.herbewCategoryName?:""
                }
                val keyValue = response.body()?.data?.created?.replace(" ","")
                if (TextUtils.isEmpty(keyValue)) return
                if (localHashMap != null && localHashMap.containsKey(keyValue)) {
                    val value = (localHashMap.get(keyValue)?:0) + 1
                    if (value < 4) {
                        adminMessageLayout?.visibility = View.VISIBLE
                    } else {
                        adminMessageLayout?.visibility = View.GONE
                    }
                    hasmap.put(keyValue ?: "", value)
                    preferenceManager?.setAdminMessgae(hasmap)
                } else {
                    hasmap.put(keyValue ?: "", 1)
                    adminMessageLayout?.visibility = View.VISIBLE
                    preferenceManager?.setAdminMessgae(hasmap)
                }
                adminMessages?.setOnClickListener { adminMessageLayout?.performClick() }

                adminMessageLayout?.setOnClickListener {
                    adminMessageLayout?.visibility = View.GONE
                    if(response.body()?.data?.type?.contains("website")!!){
                        var url = ""
                        if(!response.body()?.data?.link.toString().contains("http")){
                            url = "https://www." +response.body()?.data?.link.toString()
                        }else{
                            url = response.body()?.data?.link.toString()
                        }
                        val browserIntent = Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse(url)
                        )
                        requireActivity().startActivity(browserIntent)
                      //  (activity as HomeActivity?)!!.openUrl(response.body()?.data?.link.toString(), requireContext(), getString(R.string.app_name))
                    }else {
                        val arra = ArrayList<String>()
                        arra.add(adminCategory ?: "")
                        updateDataToFilterModel(
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            0,
                            arra
                        );
                        initRecyclerViewForFilter()
                        searchDataFromFilter(false)
                    }
                }

            }
        })
    }

    private fun findProducts(isLoadMore: Boolean) {
        if (mTagContainerLayout?.size() ?: 0 > 0 || searchText?.text?.length!!>0 || (filterFragment != null && filterModel?.addressText.toString().length>1)) {
            if (!isLoadMore){
                offset = 0;
            }
            searchDataFromFilter(isLoadMore)
        } else {
            searchNearByProducts(isLoadMore)
        }
    }

    private fun getTrendingproducts() {
        RestClient.getApiInterface().getProductRankWise()
            .enqueue(object : Callback<TrendingSearches> {
                override fun onFailure(call: Call<TrendingSearches>, t: Throwable) {
                    System.out.println(">>>>>>>>>>>>>>>> " + t.message)
                }

                override fun onResponse(
                    call: Call<TrendingSearches>,
                    response: Response<TrendingSearches>
                ) {
                    if (response.body() != null) {
                        rankWiseProduct = ArrayList()
                        response.body()?.data?.forEachIndexed { index, datum ->
                            rankWiseProduct?.add(datum.product_title)
                        }
                    }
                }
            })
    }

    private fun initClicks() {
        searchText?.setOnClickListener {
            searchFragment = showDialogFragment<SearchFragment>()
            searchFragment?.trendingSearchData(trendingSearch = rankWiseProduct ?: ArrayList())
            searchFragment?.setLastStringSearch(searchText?.text.toString())
            searchFragment?.listenerCatcher(object : SearchFragment.OnItemSelected {
                override fun onSearchSelected(itemClicked: Boolean, searchString: String) {
                    if (itemClicked) {
                        resetToDefault()
                        searchText?.setText(searchString)
                    } else {
                        offset = 0
                        limit = Common.commonLimit
                    }
                    if (searchText?.text?.length!! > 0)
                        searchCrossBtn?.visibility = View.VISIBLE
                    else
                        searchCrossBtn?.visibility = View.GONE
                    filterModel?.search_name = searchString
                    initRecyclerViewForFilter()
                    if (searchString.length > 0) {
                        searchDataFromFilter(false)
                    } else {
                        searchNearByProducts(false)
                    }
                }

                override fun onCancelSelected(itemClicked: Boolean, searchString: String) {
                    resetToDefault()
                    if (searchString.length > 0) {
                        searchDataFromFilter(false)
                    } else {
                        searchNearByProducts(false)
                    }
                }

            })
        }
        filter_btn?.setOnClickListener {
            progress_bar?.visibility = View.GONE
            filterFragment = FilterFragment(categoriesData)
            filterFragment?.filterData(filterModel)
            filterFragment?.listenerCatcher(object : FilterFragment.SetOnClickMethod {
                override fun onApplyFilterClick(
                    addressText: String,
                    cat_selectedArray: ArrayList<String>,
                    min_price: String,
                    max_price: String,
                    conditionName: String,
                    distance: String,
                    sort_by: String,
                    sort_by_int: Int
                ) {
                    offset = 0;
                    updateDataToFilterModel(
                        addressText,
                        min_price,
                        max_price,
                        conditionName,
                        distance,
                        sort_by,
                        sort_by_int,
                        cat_selectedArray
                    );
                    var dis = Common.commonDistance
                    if (!distance.isEmpty()) {
                        dis = distance
                    }
                    //setDistance(dis.toInt())
                    initRecyclerViewForFilter()
                    searchDataFromFilter(false)
                }

                override fun onResetLocation(
                    addressText: String,
                    cat_selectedArray: ArrayList<String>,
                    min_price: String,
                    max_price: String,
                    conditionName: String,
                    distance: String,
                    sort_by: String,
                    sort_by_int: Int
                ) {
                    searchNearByProducts(false)
                }

                override fun resetClicked() {
                    val arra = ArrayList<String>()
                    arra.add(adminCategory ?: "")
                    updateDataToFilterModel(
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        0,
                        arra
                    )
                    //setDistance(Common.commonDistance.toInt())
                    initRecyclerViewForFilter()
                    searchDataFromFilter(false)
                }

                override fun onFragmentDismissed() {
                    report_issue?.visibility = View.VISIBLE
                }
            })
            val manager: FragmentManager = childFragmentManager
            val transaction = manager.beginTransaction()
            transaction.setCustomAnimations(
                R.anim.right_to_left,
                R.anim.left_to_right,
                R.anim.left_to_right,
                R.anim.right_to_left
            )
            transaction.add(R.id.fragment_filter, filterFragment!!, "filter")
            transaction.addToBackStack(null)
            transaction.commit()
            report_issue?.visibility = View.GONE
        }
        report_issue?.setOnClickListener {
            if(preferenceManager!!.isUserLoggedIn){
                startActivity(
                    Intent(requireContext(), CameraPostProduct::class.java)
                        .putExtra("screen", "")
                )
            }else{
                activity?.let { it1 -> (activity as HomeActivity?)?.showSocialLoginDialog(it1) }
            }


        }

        searchCrossBtn?.setOnClickListener {
            searchCrossBtn?.visibility = View.GONE
            searchNearByProducts(false)
            searchText?.text = ""
            filterModel = FilterModel()
            initRecyclerViewForFilter()
        }

    }

    private fun resetToDefault() {
        offset = 0;
        limit = Common.commonLimit;
    }

    private fun setDistance(currentVisibleItemPosition: Int) {
        if (currentLatitude.isEmpty()) {
            distanceText?.text =   "$constantDistance " + getString(R.string.km_from_you)
            getPermission(false)
            return
        }
        if (currentVisibleItemPosition < (productModeldata?.size ?: 0) &&
            productModeldata?.get(currentVisibleItemPosition)?.productLat?.isEmpty()?.not()?:false) {
            val distance = calculateDistance(currentLatitude.toDouble(), currentLongitude.toDouble(), productModeldata?.get(currentVisibleItemPosition)?.productLat?.toDouble()?:0.0,
                productModeldata?.get(currentVisibleItemPosition)?.productLng?.toDouble()?:0.0)
            distanceText?.text =  distance.toString() +" " + getString(R.string.km_from_you)
        }
    }

    private fun updateDataToFilterModel(
        addressText: String,
        minPrice: String,
        maxPrice: String,
        conditionName: String,
        distance: String,
        sortBy: String,
        sort_by_int: Int,
        catSelectedarray: ArrayList<String>
    ) {
        filterModel = FilterModel()
        filterModel?.addressText = addressText
        filterModel?.search_name = searchText?.text.toString()
        filterModel?.conditionName = conditionName
        filterModel?.min_price = minPrice
        filterModel?.max_price = maxPrice
        filterModel?.sort_by = sortBy
        filterModel?.sort_by_int = sort_by_int
        filterModel?.distance = distance
        if (distance.isEmpty().not())
            filterModel?.display_distance = distance + " Km"
        else
            filterModel?.display_distance =  filterModel?.distance
        filterModel?.display_min_price = "Min: ₪ $minPrice"
        filterModel?.display_max_price = "Max: ₪ $maxPrice"
        filterModel?.categories = catSelectedarray

    }

    private fun getPermission(updateAllData: Boolean) {
        if (requireActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                requireContext() as Activity,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION
            )
        } else {
            if (checkGpsOn()) {
                val appLocationService = AppLocationService(requireContext())
                var gpsLocation = appLocationService
                    .getLocation(LocationManager.GPS_PROVIDER)
                if (gpsLocation == null) {
                    gpsLocation = appLocationService
                        .getLocation(LocationManager.NETWORK_PROVIDER)
                    if (gpsLocation == null) {
                        gpsLocation = appLocationService
                            .getLocation(LocationManager.PASSIVE_PROVIDER)
                    }
                }

                if (gpsLocation != null) {
                    currentLatitude = gpsLocation.getLatitude().toString()
                    currentLongitude = gpsLocation.getLongitude().toString()
                    if (updateAllData) {
                        Common.productLocationLat = gpsLocation.getLatitude().toString()
                        Common.currentLat = gpsLocation.getLatitude().toString()
                        Common.productLocationLng = gpsLocation.getLongitude().toString()
                        Common.currentLng = gpsLocation.getLongitude().toString()
                        findProducts(false)
                        if(preferenceManager?.userDetails !=null) {
                            updateLocation()
                        }
                    }
                } else {
                    baseActivity?.showToast(
                        "Current location not found. Please refresh.",
                        requireContext()
                    )
                }
            }
        }
    }

    private fun updateLocation() {
        locationViewModel?.updateLocation(preferenceManager!!, Common.currentLat, Common.currentLng, baseActivity?.getAddress(
            Common.productLocationLat.toDouble(),
            Common.productLocationLng.toDouble(), requireContext(),true
        )?:"")
    }

    fun checkGpsOn(): Boolean {
        if (!locationManager?.isProviderEnabled(LocationManager.GPS_PROVIDER)!!) {
            displayLocationSettingsRequest(requireContext())
            return false
        } else {
            return true
        }
    }

    private fun displayLocationSettingsRequest(context: Context) {
        val googleApiClient = GoogleApiClient.Builder(context)
            .addApi(LocationServices.API).build()
        googleApiClient.connect()
        val locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 10000
        locationRequest.fastestInterval = 10000 / 2.toLong()
        val builder =
            LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        builder.setAlwaysShow(true)
        val result =
            LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build())
        result.setResultCallback { result ->
            val status = result.status
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> Log.i(
                    "TAG1",
                    "All location settings are satisfied."
                )
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                    Log.i(
                        "TAG2",
                        "Location settings are not satisfied. Show the user a dialog to upgrade location settings "
                    )
                    try {
                        // Show the dialog by calling startResolutionForResult(), and check the result
                        // in onActivityResult().
                        status.startResolutionForResult(
                            requireActivity(),
                            LOCATION_PERMISSION
                        )
                    } catch (e: SendIntentException) {
                        Log.i("TAG3", "PendingIntent unable to execute request.")
                    }
                }
                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> Log.i(
                    "Tag4",
                    "Location settings are inadequate, and cannot be fixed here. Dialog not created."
                )
            }
        }
    }

    private fun searchNearByProducts(isLoadMore: Boolean) {
        if (!isLoadMore){
            offset = 0;
        }
        filterModel = FilterModel()
        progress_bar?.visibility = View.VISIBLE
        RestClient.getApiInterface().searchNearbyProducts(
            Common.currentLat,
            Common.currentLng,
            Common.commonDistance,
            preferenceManager?.userDetails?.id,
            limit,
            offset,
            baseActivity?.getCurrentDateTime(),
            preferenceManager?.isEnglish
        )
            .enqueue(object : Callback<ProductModel> {
                override fun onFailure(call: Call<ProductModel>, t: Throwable) {
                    Log.d("sd", t.message.toString());
                    progress_bar?.visibility = View.GONE
                    baseActivity?.showToast("Please refresh again", context)
                }

                override fun onResponse(
                    call: Call<ProductModel>,
                    response: Response<ProductModel>
                ) {
                    if (!isLoadMore) {
                        productModeldata = ArrayList()
                        no_prod_available?.visibility = View.GONE
                        if (response.body()?.data != null) {
                            addDataTpProduct(response.body()?.data!!)
                            difference_km?.visibility = View.VISIBLE
                        } else {
                            no_prod_available?.visibility = View.VISIBLE
                            //NO Product Found Label
                        }
                        initRecyclerView()
                    } else {
                        if (response.body()?.data != null)
                            addDataTpProduct(response.body()?.data!!)
                        progress_bar?.visibility = View.GONE
                        productAdapter?.addMoreData(productModeldata)
                    }
                    isDataLoading = false

                }

            })
    }

    private fun addDataTpProduct(data: List<ProductModel.Datum>) {
        data?.forEachIndexed { index, datum ->
            if (Common.blockedUserId.contains(datum.uploadedByUserId.id).not()){
                if (datum.productIsSold.equals("1").not())
                    productModeldata?.add(datum)
                else if (BaseActivity.getNoOfDays(Calendar.getInstance().time, BaseActivity.convertStringToDate(datum.updated_at)) < 24) {
                    productModeldata?.add(datum)
                }
            }
        }
        if (filterModel?.sort_by_int == 1) {
            productModeldata?.sortBy { it.distance.first() }
        } else if (filterModel?.sort_by_int == 2) {
            productModeldata?.sortBy { it.createdAt.first() }
        } else if (filterModel?.sort_by_int == 3) {
            productModeldata?.sortWith { o1, o2 ->
                o2.productPrice.compareTo(o1.productPrice)
            }
        } else if (filterModel?.sort_by_int == 4) {
            productModeldata?.sortWith { o2, o1 ->
                o2.productPrice.compareTo(o1.productPrice)
            }
        }
    }

    private fun searchProducts(
        name: String,
        product_condition: String,
        lat: String,
        lng: String,
        sort_type: String,
        min_price: String,
        max_price: String,
        parent_category: String,
        max_distance: String,
        isLoadMore: Boolean
    ) {
        var maximumPrice = max_price
        if (maximumPrice.isNullOrEmpty()) {
            maximumPrice = "100000"
        }
        if (searchFragment != null && searchFragment?.isVisible ?: false) {
            progress_bar?.visibility = View.GONE
        } else {
            progress_bar?.visibility = View.VISIBLE
        }


        if (!isLoadMore) {
            showDataLoading()
        }
        RestClient.getApiInterface().search_product(
            name,
            product_condition,
            lat,
            lng,
            Common.currentLat,
            Common.currentLng,
            sort_type,
            min_price,
            maximumPrice,
            parent_category,
            max_distance, preferenceManager?.userDetails?.id,
            limit, offset,
            baseActivity?.currentDateTime,
            preferenceManager?.isEnglish
        ).enqueue(object : Callback<ProductModel> {
            override fun onFailure(call: Call<ProductModel>, t: Throwable) {
                progress_bar?.visibility = View.GONE
                baseActivity?.showToast("Network Error", requireContext())
            }

            override fun onResponse(call: Call<ProductModel>, response: Response<ProductModel>) {
                if (!isLoadMore)
                    hideDataLoading()
                if (searchFragment?.isVisible ?: false) {
                    val productModeldata = ArrayList<ProductModel.Datum>()
                    if (response.body()?.data != null) {
                        addDataTpProduct(response.body()?.data!!)
                    }
                    searchFragment?.initRecycler(productModeldata)
                } else {
                    no_prod_available?.visibility = View.GONE
                    if (offset == 0)
                        productModeldata = ArrayList()
                    if (response.body()?.data != null && response.body()?.data?.size?:0 > 0) {
                        addDataTpProduct(response.body()?.data!!)
                    } else {
                        no_prod_available?.visibility = View.VISIBLE
                        //NO Product Found Label
                    }
                    if (offset == 0)
                        productAdapter?.refreshData(productModeldata)
                    else
                        productAdapter?.addMoreData(productModeldata)

                    difference_km?.visibility = View.VISIBLE
                }
                progress_bar?.visibility = View.GONE
                isDataLoading = false
            }

        })
    }

    private fun initRecyclerView() {
        productAdapter = ProductAdapter(productModeldata, this)
        linearLayoutManager = GridLayoutManager(requireContext(), 2)

        rv_products!!.viewTreeObserver.addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
            override fun onPreDraw(): Boolean {
                rv_products!!.getViewTreeObserver().removeOnPreDrawListener(this);

                for (i in 0 until rv_products!!.getChildCount()) {
                    var v = rv_products!!.getChildAt(i);
                    v.setAlpha(0.0f);
                    v.animate().alpha(1.0f)
                        .setDuration(500)
                        .setStartDelay((i * 50).toLong())
                        .start();
                }

                return true
            }


        })
        
        rv_products?.adapter = productAdapter
        rv_products?.layoutManager = linearLayoutManager
        rv_products?.isNestedScrollingEnabled = true
        progress_bar?.visibility = View.GONE

        rv_products!!
            .addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(
                    recyclerView: RecyclerView,
                    dx: Int, dy: Int
                ) {
                    super.onScrolled(recyclerView!!, dx, dy)
                    if (dy > 0) {
                        //Scrolling down
                        (activity as HomeActivity?)?.hideBottomBar(false)

                    } else if (dy < 0) {
                        //Scrolling up
                        (activity as HomeActivity?)?.hideBottomBar(true)
                    }
                    val totalItemCount = linearLayoutManager!!.itemCount
                    if (totalItemCount > 0) {

                        val findFirstVisibleItemPosition = linearLayoutManager!!
                            .findLastVisibleItemPosition()
                        System.out.println(
                            ">>>>>>>>>>>>>>>>> " + productModeldata?.get(
                                findFirstVisibleItemPosition
                            )?.distance ?: 0
                        )
                        var dis = productModeldata?.get(findFirstVisibleItemPosition)?.distance
                            ?: 0.toString()
                        var dis_text = math(dis.toFloat())
                        if (dis_text < 1) {
                            dis_text = 1;
                        }
                        System.out.println(
                            ">>>>>>>>>>>>>>findLastVisibleItemPosition " + linearLayoutManager!!
                                .findLastVisibleItemPosition()
                        )
                        if (!recyclerView.canScrollVertically(1)) { //1 for down

                            if (productModeldata != null && productModeldata?.size ?: 0 > 0 && productModeldata?.get(
                                    (productModeldata?.size ?: 0) - 1
                                )?.load_more ?: false && !isDataLoading
                            ) {
                                isDataLoading = true
                                offset = offset + limit + 1
                                findProducts(true)
                            }
                        }
                        setDistance(linearLayoutManager!!
                            .findLastVisibleItemPosition())
                    }
                }

                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                    if (newState == SCROLL_STATE_IDLE) {
                        (activity as HomeActivity?)?.hideBottomBar(true)
                    }

                }
            })


    }
    private fun calculateDistance(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Int {
        val theta = lon1 - lon2
        var dist = (Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + (Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta))))
        dist = Math.acos(dist)
        dist = rad2deg(dist)
        dist = dist * 60 * 1.1515 * 1.60934
        var finalDist = Math.round(dist).toInt()
        if (finalDist < constantDistance) finalDist = constantDistance
        return finalDist
    }

    private fun deg2rad(deg: Double): Double {
        return deg * Math.PI / 180.0
    }

    private fun rad2deg(rad: Double): Double {
        return rad * 180.0 / Math.PI
    }

    fun math(f: Float): Int {
        val c = (f + 0.5f).toInt()
        val n = f + 0.5f
        return if ((n - c) % 2 == 0f) f.toInt() else c
    }

    private fun getAds() {
        RestClient.getApiInterface().fetchMobileAds("1").enqueue(object : Callback<AdsModel> {
            override fun onFailure(call: Call<AdsModel>, t: Throwable) {

            }

            override fun onResponse(call: Call<AdsModel>, response: Response<AdsModel>) {
                fragmentList = ArrayList()
                response.body()?.data?.forEachIndexed { index, datum ->
                    // fragmentList?.add(datum.image?:"")

                    if (datum.image != null)
                        fragmentList?.add(addFrag(datum.image, datum.link ?: "")!!)
                }
                fragmentList?.shuffle()
                setUpViewPager()
            }
        })
    }

    private fun initView(view: View) {
        recycler_shop = view.findViewById(R.id.recycler_shop)
        homebanner = view?.findViewById(R.id.homebanner)

        preferenceManager = PreferenceManager(requireContext())
        locationManager =
            requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        tabLayout = requireActivity().findViewById(R.id.tabLayout)
        baseActivity = BaseActivity()
        toolbar = view?.findViewById(R.id.toolbar)
        mTagContainerLayout = view?.findViewById(R.id.tagcontainerLayout)
        adminMessages = view?.findViewById(R.id.adminMessages)
        pullToRefresh = view?.findViewById(R.id.pullToRefresh)
        scrollView = view.findViewById(R.id.scrollView)
        vpImages = view.findViewById(R.id.vpImages)
        cipTutorial = view.findViewById(R.id.cipTutorial)
        rv_products = view.findViewById(R.id.rv_products)
        difference_km = view.findViewById(R.id.difference_km)
        adminMessageLayout = view.findViewById(R.id.adminMessageLayout)
        distanceText = view.findViewById(R.id.distanceText)
        distanceText?.text = "$constantDistance " + getString(R.string.km_from_you)
        no_prod_available = view.findViewById(R.id.no_prod_available)
        no_search_data_text = view.findViewById(R.id.no_search_data_text)
        no_search_data_image = view.findViewById(R.id.no_search_data_image)
        filter_btn = view.findViewById(R.id.filter_btn)
        lociconTwo = view.findViewById(R.id.lociconTwo)
        lociconOne = view.findViewById(R.id.lociconOne)
        progress_bar = view.findViewById(R.id.progress_bar)
        searchText = view.findViewById(R.id.searchText)
        report_issue = view.findViewById(R.id.report_issue)
        searchCrossBtn = view.findViewById(R.id.searchCrossBtn)
        seeall = view.findViewById(R.id.seeall)

        if (preferenceManager?.isEnglish == 2) {
            lociconOne?.visibility = View.GONE
            lociconTwo?.visibility = View.VISIBLE
        }

        searchText?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

//                searchDataFromFilter()
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {

            }
        })

        initRecyclerViewForFilter()
    }

    private fun initRecyclerViewForFilter() {
        val copyData = ArrayList<String>()
        val colors = ArrayList<IntArray>()
        val color1 = intArrayOf(
            getColor(
                requireContext(),
                R.color.basecolor
            ), // TagBackgroundColor
            getColor(
                requireContext(),
                R.color.basecolor
            ),  //TabBorderColor
            getColor(
                requireContext(),
                R.color.white
            ), //TagTextColor
            getColor(
                requireContext(),//TagSelectedBackgroundColor
                R.color.basecolor
            )
        )

        filterModel?.min_price?.let {
            if (!it.isEmpty()) {
                copyData.add(filterModel?.display_min_price ?: "")
                colors.add(color1)
            }
        }
        filterModel?.max_price?.let {
            if (!it.isEmpty()) {
                copyData.add(filterModel?.display_max_price ?: "")
                colors.add(color1)
            }
        }
        filterModel?.conditionName?.let {
            if (!it.isEmpty()) {
                copyData.add(it)
                colors.add(color1)
            }
        }
        filterModel?.display_distance?.let {
            if (!it.isEmpty()) {
                copyData.add(it)
                colors.add(color1)
            }
        }
        filterModel?.sort_by?.let {
            if (!it.isEmpty()) {
                copyData.add(it)
                colors.add(color1)
            }
        }
        filterModel?.categories?.let {
            it.forEachIndexed { index, s ->
                if (!it.get(index).isEmpty()) {
                    copyData.add(it.get(index))
                    colors.add(color1)
                }

            }
        }
        mTagContainerLayout?.crossColor = getColor(
            requireContext(),
            R.color.white
        )
        mTagContainerLayout?.backgroundColor = ContextCompat.getColor(
            requireContext(),
            R.color.white
        )
        val typeface = ResourcesCompat.getFont(requireContext(), R.font.avenir_next_font);
        mTagContainerLayout?.tagTypeface = typeface
        // mTagContainerLayout?.tagTextSize = 15F
        mTagContainerLayout?.isEnableCross = true
        mTagContainerLayout?.setTags(copyData, colors)
        mTagContainerLayout!!.setOnTagClickListener(object : TagView.OnTagClickListener {
            override fun onTagClick(position: Int, text: String?) {
                // ...
            }

            override fun onTagLongClick(position: Int, text: String?) {
                // ...
            }

            override fun onSelectedTagDrag(position: Int, text: String?) {
                // ...
            }

            override fun onTagCrossClick(position: Int) {
                // ...
                checkDataPositionInFilter(mTagContainerLayout?.getTagText(position))
                mTagContainerLayout?.removeTag(position)
                // initRecyclerViewForFilter()
                if (mTagContainerLayout?.size() ?: 0 > 0) {
                    searchDataFromFilter(false)
                } else {
                    searchNearByProducts(false)
                }
            }
        })

    }

    private fun setUpViewPager() {
        val basePagerAdapter = ViewPagerAdapter(
            getChildFragmentManager(),
            fragmentList,
            dpToPixels(requireContext()),
            getActivity()
        )
        //  vpImages!!.offscreenPageLimit = 3
        vpImages!!.adapter = basePagerAdapter
        basePagerAdapter.notifyDataSetChanged()
        cipTutorial!!.setViewPager(vpImages)
        vpImages!!.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                currentPage = position
            }

            override fun onPageSelected(position: Int) {
                // selectedPageIndex = position;
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })
        val h = Handler(Looper.getMainLooper())
        val r: Runnable = object : Runnable {
            override fun run() {
                if (currentPage >= (fragmentList?.size ?: 0)) currentPage = -1
                vpImages?.setCurrentItem(currentPage++, true)
                h.postDelayed(this, 3000)
            }
        }
        h.postDelayed(r, 1000)

    }

    private fun addFrag(tutsImage: String, tutsImageUrl: String): ImagesFragment? {
        val bundle = Bundle()
        bundle.putString("tutImages", tutsImage)
        bundle.putString("tutsImageUrl", tutsImageUrl)
        val imagesFragment = ImagesFragment()
        imagesFragment.arguments = bundle
        return imagesFragment
    }

    private fun dpToPixels(context: Context): Float {
        return 2 * context.resources.displayMetrics.density
    }
    override fun onItemClicked(postion: Int) {
        if (preferenceManager?.isUserLoggedIn == true && preferenceManager?.userDetails?.phone.isNullOrEmpty()) {
            showAddPhoneDialog()
        } else {
            Common.selectedProductDetails = productModeldata?.get(postion)
            startActivityForResult(
                Intent(requireContext(), ProductDetails::class.java),
                PRODUCT_DETAILS_PAGE
            )
        }
    }

    fun showAddPhoneDialog() {
        val bottomSheetDialog = AddPhoneNumberPopUpDialog()
        bottomSheetDialog.isCancelable = true
        bottomSheetDialog.show(childFragmentManager, "phone_bottom")
    }

    private fun searchDataFromFilter(isLoadMore: Boolean) {
        var cat_array = ""
        filterModel?.categories?.forEachIndexed { index, s ->
            if (!s.isEmpty()) {
                if (cat_array.length==0) {
                    cat_array = filterModel?.categories?.get(index).toString()
                } else
                    cat_array =
                        cat_array + "," + filterModel?.categories?.get(index).toString()
            }
        }

        searchProducts(
            name = filterModel?.search_name ?: "",
            product_condition = filterModel?.conditionName ?: "",
            lat = Common.productLocationLat.toString(),
            lng = Common.productLocationLng.toString(),
            sort_type = filterModel?.sort_by_int.toString(),
            min_price = filterModel?.min_price ?: "",
            max_price = filterModel?.max_price ?: "",
            parent_category = cat_array,
            max_distance = getDistance(),
            isLoadMore

        )
    }

    private fun showDataLoading() {
        productModeldata = ArrayList()
        initRecyclerView()
        no_search_data_image?.setImageDrawable(requireContext().getDrawable(R.drawable.download_data))
        no_search_data_text?.text = getString(R.string.loading_data)
        no_prod_available?.visibility = View.VISIBLE
    }
    private fun hideDataLoading() {
        no_prod_available?.visibility = View.GONE
        no_search_data_image?.setImageDrawable(requireContext().getDrawable(R.drawable.no_search_data))
        no_search_data_text?.text = getString(R.string.no_products_available)
    }

    private fun getDistance():String{
        return if (!filterModel?.distance.isNullOrEmpty())
            filterModel?.distance.toString()
        else if (searchText?.text?.length == 0)
            Common.commonDistance
        else
            ""

    }

    private fun checkDataPositionInFilter(filterString: String?) {
        if (!filterString.isNullOrEmpty()) {
            if (filterModel?.search_name.equals(filterString)) {
                filterModel?.search_name = ""
            } else if (filterModel?.addressText.equals(filterString)) {
                filterModel?.addressText = ""
                Common.productLocationLat = Common.currentLat
                Common.productLocationLng = Common.currentLng
            } else if (filterModel?.min_price.equals(filterString) ||  filterModel?.display_min_price.equals(
                    filterString
                )) {
                filterModel?.min_price = ""
                filterModel?.display_min_price = ""
            } else if (filterModel?.max_price.equals(filterString) || filterModel?.display_max_price.equals(
                    filterString
                )) {
                filterModel?.max_price = ""
                filterModel?.display_max_price = ""
            } else if (filterModel?.conditionName.equals(filterString)) {
                filterModel?.conditionName = ""
            } else if (filterModel?.distance.equals(filterString) || filterModel?.display_distance.equals(
                    filterString
                )
            ) {
                filterModel?.distance = ""
                filterModel?.display_distance = ""
            } else if (filterModel?.sort_by.equals(filterString)) {
                filterModel?.sort_by = ""
                filterModel?.sort_by_int = 0
            } else {
                filterModel?.categories?.forEachIndexed { index, s ->
                    if (filterModel?.categories?.get(index).equals(filterString)) {
                        filterModel?.categories?.set(index, "")
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode==PRODUCT_DETAILS_PAGE &&  Common.isProductRefreshNeeded){
            findProducts(false)
        }
    }

    override fun onDestroy() {
        timer?.cancel()
        super.onDestroy()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode ==LOCATION_PERMISSION) {
            if (Build.VERSION.SDK_INT >= 23) {
                getPermission(true)
            }
        }
    }

    override fun onKey(p0: View?, p1: Int, p2: KeyEvent?): Boolean {
        return false


    }

    override fun OnVerifiedClicked(otp: String) {
        verifYOtp(otp)
    }

    override fun OnOtpSendClicked(number: String) {
        sendOtpToEmailPhone(number.trim(), 1)
    }
    private var verifyingNumberOrEmail  = ""
    private var isEmailVerifying = false
    private fun sendOtpToEmailPhone(text: String, otpSendType: Int) {
        //0-Email 1-Phone
        baseActivity!!.showLoading(requireContext())
        verifyingNumberOrEmail = text
        var emailID = text
        var phoneNum = text
        if (otpSendType == 1) {
            emailID = ""
        } else {
            phoneNum = ""
        }

        RestClient.getApiInterface().sendOTP(emailID, phoneNum).enqueue(object : Callback<ApiStatusModel> {
            override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
                baseActivity!!.hideLoading()
            }

            override fun onResponse(
                call: Call<ApiStatusModel>,
                response: Response<ApiStatusModel>
            ) {
                baseActivity!!.hideLoading()
                if (response.body()?.getStatus() == 200) {
                    if (otpSendType == 0)
                        setUiForOtp(otpSendType)
                }
            }
        })

        /*  if (otpSendType == 0) {

          }else{
              baseActivity.hideLoading()
              setUiForOtp(otpSendType)
          }*/
    }

    private fun setUiForOtp(otpSendType: Int) {
        //0-Email 1-Phone
        if (otpSendType == 0){
            isEmailVerifying = true
        }
//        header_one?.setText(getString(R.string.enter_otp))
//        enterTextField?.setText("")
//        enterTextField?.setHint(getString(R.string.enter_otp))
//        enterTextField?.inputType = InputType.TYPE_CLASS_NUMBER
//        cancelBtn?.setText(getString(R.string.verify))
    }

    private fun verifYOtp(otp: String) {
        baseActivity!!.showLoading(requireContext())
        var email = ""
        var phone = ""
        if (verifyingNumberOrEmail.isDigitsOnly()) {
            phone = verifyingNumberOrEmail
        } else {
            email = verifyingNumberOrEmail
        }

        baseActivity?.showLoading(requireContext())
        RestClient.getApiInterface().verifyOtp("", verifyingNumberOrEmail, "",otp).enqueue(object : retrofit2.Callback<ApiStatusModel> {
            override fun onResponse(
                call: Call<ApiStatusModel>,
                response: Response<ApiStatusModel>
            ) {
                baseActivity?.hideLoading()
                if (response.body()?.getStatus() == 200) {
                    update_verificationForEmailPhone("phone")
                } else {
                    baseActivity?.showToast(response.body()?.getMessage()?:"", requireContext())
                }


            }

            override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
                baseActivity?.hideLoading()
                baseActivity?.showToast(t.message.toString()?:"", requireContext())

            }

        })

    }

    private fun update_verificationForEmailPhone(verify_type : String) {
        RestClient.getApiInterface().update_verification(
            preferenceManager?.userDetails?.id,
            verifyingNumberOrEmail,
            verify_type
        ).enqueue(object : Callback<ApiStatusModel> {
            override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
                baseActivity!!.hideLoading()
            }

            override fun onResponse(
                call: Call<ApiStatusModel>,
                response: Response<ApiStatusModel>
            ) {
                baseActivity!!.hideLoading()
                if (response.body()?.getStatus() == 200) {
                   // testDialog?.dismiss()
                    val v = Users.Verification()
                    v.id = ""
                    v.userId = preferenceManager?.userDetails?.id
                    v.verifiedBy = verify_type
                    v.createdAt = ""
                    v.otp = ""
                    v.verifiedStatus = ""
                    preferenceManager?.userDetails?.verification?.add(v)

                } else {
                    baseActivity?.showToast(response?.body()?.getMessage(), requireContext())
                    verifyOtpActivity?.dismiss()
                }

            }
        })

    }
}