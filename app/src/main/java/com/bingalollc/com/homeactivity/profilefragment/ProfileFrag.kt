package com.bingalollc.com.homeactivity.profilefragment

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.*
import android.text.style.ImageSpan
import android.util.Log
import android.view.*
import android.widget.*
import android.widget.TextView.BufferType
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.text.isDigitsOnly
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.base.bottom_action_sheet.ActionSheet
import com.bingalollc.com.camerax.CameraPostProduct
import com.bingalollc.com.camerax.CameraXDisplayImage
import com.bingalollc.com.chat.model.BlockListModel
import com.bingalollc.com.databinding.FragmentProfileNewBinding
import com.bingalollc.com.homeactivity.homefragment.adapter.ProductAdapter
import com.bingalollc.com.homeactivity.model.ProductModel
import com.bingalollc.com.homeactivity.model.ProfilePicResponseModel
import com.bingalollc.com.homeactivity.profilefragment.model.ProductCountOfUser
import com.bingalollc.com.model.ApiStatusModel
import com.bingalollc.com.network.MultipartParams
import com.bingalollc.com.network.RestClient
import com.bingalollc.com.preference.Common
import com.bingalollc.com.preference.PreferenceManager
import com.bingalollc.com.product_details.ProductDetails
import com.bingalollc.com.report.ReportUser
import com.bingalollc.com.setting.SettingActivity
import com.bingalollc.com.splash.viewmodel.UpdateUserViewModel
import com.bingalollc.com.users.Users.Verification
import com.bingalollc.com.utils.PickerUtils
import com.bingalollc.com.utils.showDialogFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.kbeanie.multipicker.api.FilePicker
import com.kbeanie.multipicker.api.Picker
import com.kbeanie.multipicker.api.callbacks.FilePickerCallback
import com.kbeanie.multipicker.api.entity.ChosenFile
import com.twitter.sdk.android.core.*
import com.twitter.sdk.android.core.identity.TwitterAuthClient
import com.twitter.sdk.android.core.models.User
//import kotlinx.android.synthetic.main.fragment_profile_new.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import pub.devrel.easypermissions.EasyPermissions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.*


class ProfileFrag : Fragment(),View.OnClickListener, ProductAdapter.OnItemChoosed,
    FilePickerCallback,
    ActionSheet.onActionClick, VerifyOtpActivity.OnVerifiedClicked,
    DialogInterface.OnClickListener {
    val Req_Code: Int = 10000
    private val REQUEST_CAMERA = 1001
    private val PERMISSION_CAMERA_REQUEST_CODE = 1003
    private val REQUEST_READ_EXTERNAL_STORAGE_CHOOSE_IMG = 1004
    private val REQUEST_SETTINGS_PAGE = 2002
    private lateinit var binding: FragmentProfileNewBinding
    private lateinit var preferenceManager: PreferenceManager
    private lateinit var baseActivity: BaseActivity
    private var productModeldata: ArrayList<ProductModel.Datum>? = null
    private var productAdapter: ProductAdapter? = null
    private var selectedTab = 1
    private var testDialog: AlertDialog? = null
    lateinit var callbackManager: CallbackManager
    private var client: TwitterAuthClient? = null
    lateinit var mGoogleSignInClient: GoogleSignInClient
    var firebaseAuth = FirebaseAuth.getInstance()
    private var header_one: TextView?=null
    private var enterTextField: EditText?=null
    private var cancelBtn: TextView?=null
    private var isEmailVerifying = false
    private var isOtherUser = false
    private var verifyingNumberOrEmail  = ""
    private var currentStatusSelected = "0"
    var updateUserViewModel: UpdateUserViewModel?=null
    private var userDetails : com.bingalollc.com.users.Users.Data?=null
    private var userId : String?=null
    private var mArrayFile: java.util.ArrayList<File>? = ArrayList()
    private lateinit var data: ArrayList<String>
    private var filePicker: FilePicker? = null
    private var mDb: FirebaseFirestore? = null
    private var imageUrl = ""
    private var verifyOtpActivity: VerifyOtpActivity? = null
    private val permissionsGALLERY = arrayOf(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE
    )


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
    private fun initSocialLogin() {
        callbackManager = CallbackManager.Factory.create()
        val config = TwitterConfig.Builder(requireContext())
            .logger(DefaultLogger(Log.DEBUG)) //enable logging when app is in debug mode
            .twitterAuthConfig(
                TwitterAuthConfig(
                    resources.getString(R.string.twitter_api_key),
                    resources.getString(R.string.twitter_api_secret_key)
                )
            ) //pass the created app Consumer KEY and Secret also called API Key and Secret
            .debug(true) //enable debug mode
            .build()
        Twitter.initialize(config)
        client = TwitterAuthClient()

    }

    override fun onResume() {
        super.onResume()
        if(preferenceManager?.userDetails?.id.isNullOrEmpty().not()) {
            if (EasyPermissions.hasPermissions(requireContext(), *permissionsGALLERY)) {
            } else {
                EasyPermissions.requestPermissions(
                    requireActivity(), resources.getString(R.string.please_allow_permissions),
                    1004, *permissionsGALLERY
                )
            }
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        updateUserViewModel = ViewModelProvider(this).get(UpdateUserViewModel::class.java)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile_new, container, false)
        return binding.root
    }

    fun setUserDetails(userId: String) {
        this.userId = userId
        isOtherUser = true
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        preferenceManager = PreferenceManager(requireContext())
        mDb = FirebaseFirestore.getInstance()
        val settings = FirebaseFirestoreSettings.Builder()
            .build()
        mDb!!.firestoreSettings = settings
        baseActivity = BaseActivity()

        binding.cardLayout.visibility = View.GONE

//        baseActivity.showFullScreenBaseColor(requireContext().getColor(R.color.blue_bg))
        if(preferenceManager.userDetails?.id.isNullOrEmpty().not()) {
            if (!isOtherUser) {
                userDetails = preferenceManager.userDetails

                loadData()
//                updateVerifiedBy()
            } else {
//                getProfileDetails()
            }
        }

        binding.moreButton.setOnClickListener {
//            showBottomSheet()
        }

        binding.reportIssue.setOnClickListener {
            startActivity(
                Intent(requireContext(), CameraPostProduct::class.java)
                    .putExtra("screen", "")
            )
        }

        Common.getInstance().refreshLatLong.observe(requireActivity()) {
            run {
                if (!it) {
                    binding.userAddress.setText(
                        baseActivity?.getBasicAddress(
                            userDetails?.lat?.toDouble(),
                            userDetails?.lng?.toDouble(),
                            context
                        )
                    )
                    binding.userAddress.visibility = View.VISIBLE
                }
            }
        }

        binding.swipeRefreshLayout.setOnRefreshListener(OnRefreshListener {
            if (!isOtherUser) {
                userDetails = preferenceManager.userDetails
                loadData()
            }else {
//                getProfileDetails()
            }
        })

    }

    private fun loadData() {
        binding.cardLayout.visibility = View.VISIBLE
        imageUrl = userDetails?.image ?: ""
        getproductFromServer(currentStatusSelected);
        initValue()
        initRecyclerView()
        initClicks()
        initSocialLogin()
        if (userDetails?.rating?.rating != null)
            binding.starRatings.setStarRating(
                (userDetails?.rating?.rating ?: "0").toString().toFloat(),
                requireContext()
            )

        if (binding.swipeRefreshLayout.isRefreshing()) {
            binding.swipeRefreshLayout.setRefreshing(false)
        }
        if (isOtherUser) {
//            addImage.visibility = View.GONE
        }

    }
    private fun getProfileDetails() {
        if (userId == null) {return}
        baseActivity?.showLoading(requireContext())
        RestClient.getApiInterface().getUserDetails(userId).enqueue(object : Callback<com.bingalollc.com.users.Users>{
            override fun onResponse(
                call: Call<com.bingalollc.com.users.Users>,
                response: Response<com.bingalollc.com.users.Users>
            ) {
                baseActivity?.hideLoading()
                if (response.body()?.status == 200){
                    userDetails = response.body()?.data
                    binding.cardLayout.visibility = View.VISIBLE

                    loadData()

                } else {
                    baseActivity?.showToast(response.body()?.message?:"", requireContext())
                }
            }

            override fun onFailure(call: Call<com.bingalollc.com.users.Users>, t: Throwable) {
                baseActivity?.hideLoading()
                baseActivity?.showToast("Network Error", requireContext())
            }

        })
    }

    private fun getproductFromServer(status: String) {
        currentStatusSelected = status
        RestClient.getApiInterface().getProductByUserId(userDetails?.id, status).enqueue(
            object : Callback<ProductModel> {
                override fun onFailure(call: Call<ProductModel>, t: Throwable) {
                    binding.noProductFound.visibility = View.VISIBLE
                    binding.noProductFound.text =
                        requireContext().getString(R.string.not_able_to_load)
                }

                override fun onResponse(
                    call: Call<ProductModel>,
                    response: Response<ProductModel>
                ) {
                    productModeldata = ArrayList()
                    if (response.body()?.status == 200 && response.body()?.data != null && response.body()?.data?.size!! > 0) {
                        binding.noProductFound.visibility = View.GONE
                        response?.body()?.data?.forEachIndexed { index, datum ->
                            if (Common.blockedUserId.contains(datum.uploadedByUserId.id).not()) {
                                productModeldata?.add(datum)
                            }
                        }
                    } else {
                        binding.noProductFound.visibility = View.VISIBLE
                        binding.noProductFound.text =
                            requireContext().getString(R.string.no_product_found)
                    }
                    productAdapter?.refreshData(productModeldata)
                }
            })
    }

    private fun initRecyclerView() {
        productAdapter= ProductAdapter(productModeldata, this)
        val linearLayoutManager= GridLayoutManager(requireContext(), 2)


        binding.rvProducts?.adapter=productAdapter
        binding.rvProducts?.layoutManager=linearLayoutManager

//        rv_products!!.viewTreeObserver.addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
//            override fun onPreDraw(): Boolean {
//                rv_products!!.getViewTreeObserver().removeOnPreDrawListener(this);
//
//                for (i in 0 until rv_products!!.getChildCount()) {
//                    var v = rv_products!!.getChildAt(i);
//                    v.setAlpha(0.0f);
//                    v.animate().alpha(1.0f)
//                        .setDuration(500)
//                        .setStartDelay((i * 50).toLong())
//                        .start();
//                }
//
//                return true
//            }
//
//
//        })
    }

    private fun getCountFromServer() {
        if (Common.currentTab == 4)
        RestClient.getApiInterface().getProductCountByUserId(userDetails?.id).enqueue(
            object : Callback<ProductCountOfUser> {
                override fun onFailure(call: Call<ProductCountOfUser>, t: Throwable) {
                    baseActivity.hideLoading()
                }

                override fun onResponse(
                    call: Call<ProductCountOfUser>,
                    response: Response<ProductCountOfUser>
                ) {
                    baseActivity.hideLoading()
                    if (response.body()?.status == 200) {
//                        binding.rvItems.sellingCounter.setText(response.body()?.data?.get(0)?.unsoldProduct.toString())
//                        binding.rvItems.favouriteCounter.setText(response.body()?.data?.get(0)?.totalFavourites.toString())
//                        binding.rvItems.boughtCounter.setText(response.body()?.data?.get(0)?.totalPurchased.toString())
//                        binding.rvItems.soldCounter.setText(response.body()?.data?.get(0)?.soldProduct.toString())
                    }

                }
            })
    }

    private fun initClicks() {
//        binding.rvItems.sellingLayout.setOnClickListener(this)
//        binding.rvItems.favouriteLayout.setOnClickListener(this)
//        binding.rvItems.boughtLayout.setOnClickListener(this)
//        binding.rvItems.soldLayout.setOnClickListener(this)
        binding.settingIcon.setOnClickListener(this)
        binding.userImage.setOnClickListener(this)
        binding.addImage.setOnClickListener(this)

    }

    private fun initValue() {
        productModeldata = ArrayList()
        setImage(null)
        binding.userName.setText(userDetails?.fullName)
        if (Common.currentLat.isNullOrEmpty().not()) {
            binding.userAddress.visibility = View.VISIBLE
            binding.userAddress.setText(baseActivity?.getBasicAddress(Common.currentLat?.toDouble(), Common.currentLng?.toDouble(), context))

        } else {
            binding.userAddress.visibility = View.VISIBLE
            binding.userAddress.setText(baseActivity?.getBasicAddress(userDetails?.lat?.toDouble(), userDetails?.lng?.toDouble(), context))
        }
        if (!isOtherUser) {
            binding.buildTrustBtn.visibility = View.VISIBLE
            binding.backBtn.visibility = View.GONE
            binding.buildTrustBtn.setOnClickListener {
                showBuildTrustDialog()
            }

            if (preferenceManager?.isEnglish == 2)
                binding.buildTrustBtn.setImageDrawable(context?.getDrawable(R.drawable.btn_build_trust_he))
        } else {
            binding.buildTrustBtn.visibility = View.INVISIBLE
            binding.settingIcon.setImageDrawable(requireContext().getDrawable(R.drawable.more_btn))
            binding.reportIssue.visibility = View.GONE
//            binding.rvItems.favouriteLayout.visibility = View.GONE
//            binding.rvItems.boughtLayout.visibility = View.GONE
//            binding.rvItems.tabBarParentLayout.weightSum = 2f
//            binding.backBtn.visibility = View.VISIBLE
        }
        updateVerifiedBy()

        binding.backBtn.setOnClickListener {
            val manager = requireActivity().supportFragmentManager
            manager.beginTransaction().remove(this).commit()
        }
    }

    private fun setImage(uri: Uri?) {
        if (uri == null) {
            System.out.println(">>>>>>>vv "+imageUrl)
            Glide.with(this)
                .load(imageUrl)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .placeholder(R.drawable.default_user)
                .error(R.drawable.default_user)
                .into(binding.userImage)
        } else {
            binding.userImage?.let {
                Glide.with(this).load(uri).into(it)
            }

        }

    }

    private fun showBuildTrustDialog() {
        val alertDialog = AlertDialog.Builder(requireContext(), R.style.AlertDialogShrinked_Dialog)
        val inflater = layoutInflater
        val add_menu_layout = inflater.inflate(R.layout.build_trust_layout, null)
        val facebookBtn = add_menu_layout.findViewById<ImageView>(R.id.facebookBtn)
        val gmailBtn = add_menu_layout.findViewById<ImageView>(R.id.gmailBtn)
        val twitterBtn = add_menu_layout.findViewById<ImageView>(R.id.twitterBtn)
        val emailBtn = add_menu_layout.findViewById<ImageView>(R.id.emailBtn)
        val phoneBtn = add_menu_layout.findViewById<ImageView>(R.id.phoneBtn)
         enterTextField = add_menu_layout.findViewById<EditText>(R.id.enterTextField)
        val socialLoginList = add_menu_layout.findViewById<LinearLayout>(R.id.socialLoginList)
         header_one = add_menu_layout.findViewById<TextView>(R.id.header_one)
         cancelBtn = add_menu_layout.findViewById<TextView>(R.id.cancelBtn)

        alertDialog.setView(add_menu_layout)

        facebookBtn.setOnClickListener {
            testDialog?.dismiss()
             if (binding.psFacebookButton.visibility == View.VISIBLE) {
                showErrorMessage(" facebook")
            } else {
                baseActivity?.showLoading(requireContext())
                 LoginManager.getInstance()
                     .logInWithReadPermissions(this, Arrays.asList("public_profile"))
                 facebookLogin() }

        }
        gmailBtn.setOnClickListener {
            testDialog?.dismiss()
            if (binding.psGoogleButton.visibility == View.VISIBLE) {
                showErrorMessage(" gmail")
            } else {
                baseActivity?.showLoading(requireContext())
                googleLogin() }

        }
        twitterBtn.setOnClickListener {
            testDialog?.dismiss()
            if (binding.psTwitterButton.visibility == View.VISIBLE) {
                showErrorMessage(" twitter")
            } else {
                baseActivity?.showLoading(requireContext())
                twitterLogin() }
        }
        emailBtn.setOnClickListener {
            if (binding.psEmailButton.visibility == View.VISIBLE) {
                showErrorMessage(" email")
                return@setOnClickListener
            }
            socialLoginList.visibility = View.GONE
            enterTextField?.visibility = View.VISIBLE
            header_one?.setText(getString(R.string.email_veri))
            cancelBtn?.setText("Submit")
        }
        phoneBtn.setOnClickListener {
            if (binding.psPhoneButton.visibility == View.VISIBLE){
                showErrorMessage(" mobile")
                return@setOnClickListener
            }
            verifyOtpActivity = showDialogFragment<VerifyOtpActivity>()
            verifyOtpActivity?.setKetListener(this)
           /* if (binding.psPhoneButton.visibility == View.VISIBLE) {
                showErrorMessage(" phone")
                return@setOnClickListener
            }
            socialLoginList.visibility = View.GONE
            enterTextField?.visibility = View.VISIBLE
            header_one?.setText(getString(R.string.phone_veri))
            enterTextField?.setText("")
            PhoneNumberUtils.formatNumber(enterTextField?.text, 1);
            enterTextField?.inputType = InputType.TYPE_CLASS_NUMBER
            enterTextField?.setHint(getString(R.string.phone_veri))
            cancelBtn?.setText("Send OTP")*/
        }

        cancelBtn?.setOnClickListener {
            if (socialLoginList.visibility == View.VISIBLE)
                testDialog?.dismiss()
            else if (header_one?.text.toString().equals(getString(R.string.phone_veri))){
                //SEND MOBILE
                if (enterTextField?.text?.length!! >0)
                    sendOtpToEmailPhone(enterTextField?.text.toString().trim(), 1)
                else
                    baseActivity.showToast("Enter Valid Mobile", requireContext())

            }else if (header_one?.text.toString().equals(getString(R.string.email_veri))){
                //EMAIL MOBILE
                if (enterTextField?.text?.length!! >0 && enterTextField?.text.toString().contains("@"))
                    sendOtpToEmailPhone(enterTextField?.text.toString().trim(), 0)
                else
                    baseActivity.showToast("Enter Valid Email", requireContext())

            }else if (cancelBtn?.text.toString().equals(getString(R.string.verify))){
                if (verifyingNumberOrEmail.isDigitsOnly()) {
                    if (enterTextField?.text?.toString() == "1234") {
                        update_verificationForEmailPhone("phone")
                    } else {
                        baseActivity.showToast("Enter Valid OTP", requireContext())
                    }
                } else {
                    if (enterTextField?.text?.toString()?.length!! > 1) {
                        baseActivity.showLoading(requireContext())
                        verifYOtp(enterTextField?.text.toString())
                    } else {
                        baseActivity.showToast("Enter Valid OTP", requireContext())
                    }
                }

            }

        }
        testDialog?.setView(add_menu_layout)
        testDialog?.setCancelable(true)
        testDialog = alertDialog.create()
        testDialog?.show()
    }

    private fun showErrorMessage(type: String) {
        baseActivity?.showNormalDialogVerticalButton(requireActivity(),
            getString(R.string.already_verified),
            getString(
                R.string.you_are_already_verified, type
            ),
            "",
            getString(R.string.ok),
            BaseActivity.OnOkClicked {
                if (it) {

                }
            }, false)
    }

    private fun verifYOtp(otp: String) {
        baseActivity.showLoading(requireContext())
        var email = ""
        var phone = ""
        if (verifyingNumberOrEmail.isDigitsOnly()) {
            phone = verifyingNumberOrEmail
        } else {
            email = verifyingNumberOrEmail
        }
        if (isEmailVerifying) {
            RestClient.getApiInterface().verifyOtp("", phone,
                email,
                otp
            ).enqueue(object : Callback<ApiStatusModel> {
                override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
                    baseActivity.hideLoading()
                }

                override fun onResponse(
                    call: Call<ApiStatusModel>,
                    response: Response<ApiStatusModel>
                ) {
                    baseActivity.hideLoading()
                    if (response.body()?.getStatus() == 200) {
                        baseActivity.showLoading(requireContext())
                        if (verifyingNumberOrEmail.isDigitsOnly()) {
                            update_verificationForEmailPhone("phone")
                        } else {
                            update_verificationForEmailPhone("email")
                        }

                    } else {
                        baseActivity.showToast(
                            response.body()?.getMessage() ?: "",
                            requireContext()
                        )
                    }

                }
            })
        }else{
            baseActivity.hideLoading()
            update_verificationForEmailPhone("phone")
        }

    }

    private fun update_verificationForEmailPhone(verify_type : String) {
        RestClient.getApiInterface().update_verification(
            userDetails?.id,
            verifyingNumberOrEmail,
            verify_type
        ).enqueue(object : Callback<ApiStatusModel> {
            override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
                baseActivity.hideLoading()
            }

            override fun onResponse(
                call: Call<ApiStatusModel>,
                response: Response<ApiStatusModel>
            ) {
                baseActivity.hideLoading()
                if (response.body()?.getStatus() == 200) {
                    testDialog?.dismiss()
                    val v = Verification()
                    v.id = ""
                    v.userId = userDetails?.id
                    v.verifiedBy = verify_type
                    v.createdAt = ""
                    v.otp = ""
                    v.verifiedStatus = ""
                    userDetails?.verification?.add(v)
                    if (verify_type == "email") {
                        binding.psEmailButton.visibility = View.VISIBLE
                    } else if (verify_type == "phone"){
                        binding.psPhoneButton.visibility = View.VISIBLE
                        showComfirmationofPhone()
                    }else if (verify_type == "google") {
                        binding.psGoogleButton.visibility = View.VISIBLE
                    } else if (verify_type == "facebook") {
                        binding.psFacebookButton.visibility = View.VISIBLE
                    } else if (verify_type == "twitter") {
                        binding.psTwitterButton.visibility = View.VISIBLE
                    }


                } else {
                    baseActivity?.showToast(response?.body()?.getMessage(), requireContext())
                    verifyOtpActivity?.dismiss()
                }

            }
        })

    }


    private fun showComfirmationofPhone() {
        baseActivity?.showMessageOKCancel(getString(R.string.phone_number_verified), getString(R.string.verification_successfull),this,requireContext(),"Ok","",false);

    }

    private fun sendOtpToEmailPhone(text: String, otpSendType: Int) {
        //0-Email 1-Phone
        baseActivity.showLoading(requireContext())
        verifyingNumberOrEmail = text
        var emailID = text
        var phoneNum = text
        if (otpSendType == 1) {
            emailID = ""
        } else {
            phoneNum = ""
        }

        RestClient.getApiInterface().sendOTP(emailID, phoneNum).enqueue(object : Callback<ApiStatusModel> {
            override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
                baseActivity.hideLoading()
            }

            override fun onResponse(
                call: Call<ApiStatusModel>,
                response: Response<ApiStatusModel>
            ) {
                baseActivity.hideLoading()
                if (response.body()?.getStatus() == 200) {
                    if (otpSendType == 0)
                     setUiForOtp(otpSendType)
                }
            }
        })

      /*  if (otpSendType == 0) {

        }else{
            baseActivity.hideLoading()
            setUiForOtp(otpSendType)
        }*/
    }

    private fun setUiForOtp(otpSendType: Int) {
        //0-Email 1-Phone
        if (otpSendType == 0){
            isEmailVerifying = true
        }
        header_one?.setText(getString(R.string.enter_otp))
        enterTextField?.setText("")
        enterTextField?.setHint(getString(R.string.enter_otp))
        enterTextField?.inputType = InputType.TYPE_CLASS_NUMBER
        cancelBtn?.setText(getString(R.string.verify))
    }

    override fun onClick(view: View?) {
        when (requireView().id) {
            R.id.sellingLayout -> {
                if (selectedTab != 1)
                    actionOnTab(1)
            }
            R.id.favouriteLayout -> {
                if (selectedTab != 2)
                    actionOnTab(2)
            }
            R.id.boughtLayout -> {
                if (selectedTab != 3)
                    actionOnTab(3)
            }
            R.id.soldLayout -> {
                if (selectedTab != 4)
                    actionOnTab(4)
            }
            R.id.settingIcon -> {
                if (!isOtherUser)
                    startActivityForResult(Intent(context, SettingActivity::class.java), REQUEST_SETTINGS_PAGE)
                else {
                    showMoreBottomSheet()
                }
            }
            R.id.userImage -> {
                showCameraPopupMenu()
            }
            R.id.addImage -> {
                showCameraPopupMenu()
            }
        }
    }

    private fun showCameraPopupMenu() {
        if (userId != null && preferenceManager.userDetails.id != userId){
            CameraXDisplayImage.fullImageDisplay(
                requireActivity(),
                imageUrl?:""
            )
            return
        }


        val popup = PopupMenu(
            context,
            binding.userImage,
            Gravity.RIGHT
        )
        popup.inflate(R.menu.camera_popup_menu)
        if (userId == null || preferenceManager.userDetails.id == userId)
            popup.menu.add(getString(R.string.edit))
        popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
            override fun onMenuItemClick(item: MenuItem?): Boolean {
                if (item!!.title?.equals(getString(R.string.view)) == true) {
                    CameraXDisplayImage.fullImageDisplay(
                        requireActivity(),
                        imageUrl?:""
                    )
                    return true
                } else if (item.title?.equals(getString(R.string.edit)) == true) {
//                    startActivityForResult()
                    showBottomSheet()
                    return true
                } else {
                    return false
                }
            }

        })
        popup.show()

    }


    private fun showBottomSheet() {
        data = ArrayList()
        data.add(getString(R.string.take_photo))
        data.add(getString(R.string.choose_from_library))
        context?.let {
            ActionSheet(it, data, this)
                .setTitle(getString(R.string.upload_picture))
                .setCancelTitle("Cancel")
                .setColorTitle(resources.getColor(R.color.dim_gray))
                .setColorBackground(resources.getColor(R.color.white)) //                        .hideTitle()
                //                        .setFontData(R.font.meryana_script)
                //                        .setFontCancelTitle(R.font.meryana_script)
                //                        .setFontTitle(R.font.meryana_script)
                //                        .setSizeTextCancel(30)
                //                        .setSizeTextData(30)
                //                        .setSizeTextTitle(30)
                .setColorTitleCancel(resources.getColor(R.color.basecolor))
                .setColorData(resources.getColor(R.color.basecolor))
                .setColorSelected(resources.getColor(R.color.colorAccent))
                .create()
        }

    }


    private fun showMoreBottomSheet() {
        data = ArrayList()
        data.add(getString(R.string.report_user))
        data.add(getString(R.string.block_user))
        context?.let {
            ActionSheet(it, data, this)
               // .setTitle(getString(R.string.upload_picture))
                .setCancelTitle("Cancel")
                .setColorTitle(resources.getColor(R.color.dim_gray))
                .setColorBackground(resources.getColor(R.color.white)) //                        .hideTitle()
                //                        .setFontData(R.font.meryana_script)
                //                        .setFontCancelTitle(R.font.meryana_script)
                //                        .setFontTitle(R.font.meryana_script)
                //                        .setSizeTextCancel(30)
                //                        .setSizeTextData(30)
                //                        .setSizeTextTitle(30)
                .setColorTitleCancel(resources.getColor(R.color.basecolor))
                .setColorData(resources.getColor(R.color.basecolor))
                .setColorSelected(resources.getColor(R.color.colorAccent))
                .create()
        }

    }

    override fun onBottomSheetClicked(data: ArrayList<String>, position: Int) {
        if (data.get(position).equals(getString(R.string.take_photo))) {
            if (checkPermissionCAMERA(requireContext())) {
                openCamera()
            }
        } else if (data.get(position).equals(getString(R.string.choose_from_library))) {
            if (checkPermissionREAD_EXTERNAL_STORAGE(
                    requireContext(),
                    REQUEST_READ_EXTERNAL_STORAGE_CHOOSE_IMG
                )
            ) {
                choosephoto()
            }
        } else if (data.get(position).equals(getString(R.string.report_user))) {
            val intent = Intent(requireContext(), ReportUser::class.java)
            intent.putExtra("usedId", userDetails?.id)
            intent.putExtra("fullName", userDetails?.fullName)
            intent.putExtra("userImage", imageUrl)
            intent.putExtra("productId","")
            startActivity(intent)
        } else if (data.get(position).equals(getString(R.string.block_user))) {
            baseActivity.showNormalDialog(requireActivity(), "", getString(R.string.are_you_sure_want_to_block),getString(R.string.cancel), getString(R.string.block), BaseActivity.OnOkClicked {
                if (it) {
                    uploadBlockedListToFirebase()
                }

            })
        }
    }

    private fun uploadBlockedListToFirebase() {
        val completeId: String = preferenceManager.userDetails?.getId()+"-"+userDetails?.id
        val otherUserId: String = userDetails?.id?:""
        val newChatroomRef: DocumentReference = mDb?.collection("BlockedUsers")?.document(completeId)!!
        val tsLong = System.currentTimeMillis() / 1000
        val timestamp = tsLong.toString()
        val `as` = java.util.ArrayList<String>()
        `as`.add(otherUserId)
        `as`.add(preferenceManager.userDetails.id)
        val blockListModel = BlockListModel()
        blockListModel.id = completeId
        blockListModel.userIDs = `as`
        blockListModel.timestamp = timestamp.toInt()
        blockListModel.blockedBy = preferenceManager.userDetails.id
        newChatroomRef.set(blockListModel).addOnCompleteListener { task ->
            baseActivity.hideLoading()
            if (!task.isSuccessful) {
                baseActivity?.showToast(getString(R.string.something_went_wring), requireContext())
            }
        }
    }

    private fun checkPermissionCAMERA(contexts: Context?): Boolean {
        val currentAPIVersion = Build.VERSION.SDK_INT
        return if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    contexts!!,
                    Manifest.permission.CAMERA
                ) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        (requireContext() as Activity?)!!,
                        Manifest.permission.CAMERA
                    )) {
                    //Toast.makeText(PostAdPage.this, "Do itititi", Toast.LENGTH_SHORT).show();
                    showDialog(
                        "Camera", requireContext(),
                        Manifest.permission.CAMERA, PERMISSION_CAMERA_REQUEST_CODE
                    )
                } else {
                    requestPermissions(
                        arrayOf(Manifest.permission.CAMERA),
                        PERMISSION_CAMERA_REQUEST_CODE
                    )
                }
                false
            } else {
                true
            }
        } else {
            true
        }
    }

    fun checkPermissionREAD_EXTERNAL_STORAGE(
        context: Context?, requestCode: Int
    ): Boolean {
        val currentAPIVersion = Build.VERSION.SDK_INT
        return if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    requireContext()!!,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        requireActivity(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )) {
                    //Toast.makeText(PostAdPage.this, "Do itititi", Toast.LENGTH_SHORT).show();
                    showDialog(
                        "External storage", context,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE, requestCode
                    )
                } else {
                    requestPermissions(
                        arrayOf(
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        ),
                        requestCode
                    )
                }
                false
            } else {
                true
            }
        } else {
            true
        }
    }

    fun showDialog(
        msg: String, context: Context?,
        permission: String, statusCode: Int
    ) {
        val alertBuilder = android.app.AlertDialog.Builder(context)
        alertBuilder.setCancelable(false)
        alertBuilder.setTitle("Permission necessary")
        alertBuilder.setMessage("$msg permission is necessary")
        alertBuilder.setPositiveButton(
            android.R.string.yes
        ) { dialog, which ->
            requestPermissions(
                arrayOf(permission),
                statusCode
            )
        }
        val alert = alertBuilder.create()
        alert.show()
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_READ_EXTERNAL_STORAGE_CHOOSE_IMG) {
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                choosephoto()
            }
        } else if (requestCode == PERMISSION_CAMERA_REQUEST_CODE) {
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openCamera()
            }
        }
    }


    fun getRealPathFromURI(uri: Uri?): String? {
        var path = ""
        if (requireActivity().getContentResolver() != null) {
            val cursor: Cursor? = requireActivity().getContentResolver().query(uri!!, null, null, null, null)
            if (cursor != null) {
                cursor.moveToFirst()
                val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                path = cursor.getString(idx)
                cursor.close()
            }
        }
        return path
    }
    var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            // There are no request codes
  //          val data: Intent? = result.data
//            val photo = data!!.extras!!["data"] as Bitmap?
    //        val tempUri = getImageUri(requireContext(), photo!!)
           // val files: File = File(getRealPathFromURI(tempUri))
            mArrayFile = ArrayList();
            val files = File(BaseActivity.getCameraFilePathName())
            val file = baseActivity?.getCompressed(requireContext(), files.path)
            try {
                mArrayFile?.add(file!!)
                uploadImageToServer()
                setImage(Uri.fromFile(file))

            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun openCamera() {
        val outputFileUri = Uri.fromFile(BaseActivity.setCamerFilePath())
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        cameraIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        cameraIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        resultLauncher.launch(cameraIntent)
        //EasyImage.openCamera(this, REQUEST_CAMERA)
    }

    private fun choosephoto() {
        if (EasyPermissions.hasPermissions(requireContext(), *permissionsGALLERY)) {
            pickImageMultiple("image")
        } else {
            EasyPermissions.requestPermissions(
                requireActivity(), resources.getString(R.string.please_allow_permissions),
                1003, *permissionsGALLERY
            )
        }
    }
    private fun pickImageMultiple(type: String) {
        filePicker = getFilePicker()
        filePicker?.setMimeType("image/*")
        filePicker?.pickFile()
    }
    private fun getFilePicker(): FilePicker? {
        filePicker = FilePicker(this)
        filePicker!!.setFilePickerCallback(this)
        filePicker!!.setCacheLocation(PickerUtils.getSavedCacheLocation(requireContext()))
        return filePicker
    }



    private fun actionOnTab(i: Int) {
        selectedTab = i
        productModeldata = ArrayList()
        productAdapter?.refreshData(productModeldata)
//        if (i == 1) {
//            binding.rvItems.sellingCounter.setTextColor(requireContext().resources.getColor(R.color.basecolor))
//            binding.rvItems.sellingText.setTextColor(requireContext().resources.getColor(R.color.basecolor))
//            binding.rvItems.sellingView.visibility = View.VISIBLE
//
//            binding.rvItems.favouriteCounter.setTextColor(requireContext().resources.getColor(R.color.black))
//            binding.rvItems.favouriteText.setTextColor(requireContext().resources.getColor(R.color.grey_500))
//            binding.rvItems.favouriteView.visibility = View.GONE
//
//            binding.rvItems.boughtCounter.setTextColor(requireContext().resources.getColor(R.color.black))
//            binding.rvItems.boughtText.setTextColor(requireContext().resources.getColor(R.color.grey_500))
//            binding.rvItems.boughtView.visibility = View.GONE
//
//            binding.rvItems.soldCounter.setTextColor(requireContext().resources.getColor(R.color.black))
//            binding.rvItems.soldText.setTextColor(requireContext().resources.getColor(R.color.grey_500))
//            binding.rvItems.soldView.visibility = View.GONE
//            getproductFromServer("0")
//
//        }else  if (i==2){
//            binding.rvItems.sellingCounter.setTextColor(requireContext().resources.getColor(R.color.black))
//            binding.rvItems.sellingText.setTextColor(requireContext().resources.getColor(R.color.grey_500))
//            binding.rvItems.sellingView.visibility = View.GONE
//
//            binding.rvItems.favouriteCounter.setTextColor(requireContext().resources.getColor(R.color.basecolor))
//            binding.rvItems.favouriteText.setTextColor(requireContext().resources.getColor(R.color.basecolor))
//            binding.rvItems.favouriteView.visibility = View.VISIBLE
//
//            binding.rvItems.boughtCounter.setTextColor(requireContext().resources.getColor(R.color.black))
//            binding.rvItems.boughtText.setTextColor(requireContext().resources.getColor(R.color.grey_500))
//            binding.rvItems.boughtView.visibility = View.GONE
//
//            binding.rvItems.soldCounter.setTextColor(requireContext().resources.getColor(R.color.black))
//            binding.rvItems.soldText.setTextColor(requireContext().resources.getColor(R.color.grey_500))
//            binding.rvItems.soldView.visibility = View.GONE
//            getproductFromServer("4")
//
//        }else  if (i==3){
//            binding.rvItems.sellingCounter.setTextColor(requireContext().resources.getColor(R.color.black))
//            binding.rvItems.sellingText.setTextColor(requireContext().resources.getColor(R.color.grey_500))
//            binding.rvItems.sellingView.visibility = View.GONE
//
//            binding.rvItems.favouriteCounter.setTextColor(requireContext().resources.getColor(R.color.black))
//            binding.rvItems.favouriteText.setTextColor(requireContext().resources.getColor(R.color.grey_500))
//            binding.rvItems.favouriteView.visibility = View.GONE
//
//            binding.rvItems.boughtCounter.setTextColor(requireContext().resources.getColor(R.color.basecolor))
//            binding.rvItems.boughtText.setTextColor(requireContext().resources.getColor(R.color.basecolor))
//            binding.rvItems.boughtView.visibility = View.VISIBLE
//
//            binding.rvItems.soldCounter.setTextColor(requireContext().resources.getColor(R.color.black))
//            binding.rvItems.soldText.setTextColor(requireContext().resources.getColor(R.color.grey_500))
//            binding.rvItems.soldView.visibility = View.GONE
//            getproductFromServer("5")
//
//        }else  if (i==4){
//            binding.rvItems.sellingCounter.setTextColor(requireContext().resources.getColor(R.color.black))
//            binding.rvItems.sellingText.setTextColor(requireContext().resources.getColor(R.color.grey_500))
//            binding.rvItems.sellingView.visibility = View.GONE
//
//            binding.rvItems.favouriteCounter.setTextColor(requireContext().resources.getColor(R.color.black))
//            binding.rvItems.favouriteText.setTextColor(requireContext().resources.getColor(R.color.grey_500))
//            binding.rvItems.favouriteView.visibility = View.GONE
//
//            binding.rvItems.boughtCounter.setTextColor(requireContext().resources.getColor(R.color.black))
//            binding.rvItems.boughtText.setTextColor(requireContext().resources.getColor(R.color.grey_500))
//            binding.rvItems.boughtView.visibility = View.GONE
//
//            binding.rvItems.soldCounter.setTextColor(requireContext().resources.getColor(R.color.basecolor))
//            binding.rvItems.soldText.setTextColor(requireContext().resources.getColor(R.color.basecolor))
//            binding.rvItems.soldView.visibility = View.VISIBLE
//            getproductFromServer("1")

//        }

    }

    private fun uploadImageToServer() {
        val builder = MultipartParams.Builder();
        builder.add("user_id", userDetails?.id)
        if (mArrayFile != null && mArrayFile?.size?:0 > 0) {
            builder.addFile("image", mArrayFile?.get(0))
        }
        baseActivity?.showLoading(context)
        RestClient.getApiInterface().changeProfilePic(builder.build().map)
            .enqueue(object : Callback<ProfilePicResponseModel> {
                override fun onResponse(
                    call: Call<ProfilePicResponseModel>,
                    response: Response<ProfilePicResponseModel>
                ) {
                    baseActivity?.hideLoading()
                    if (response.isSuccessful) {
                        if (response?.body() != null && response?.body()?.getStatus() == 200) {
                            baseActivity?.showToast(response?.body()?.getMessage(), context)
                            updateUserViewModel?.updateUser(preferenceManager?.userDetails?.id,preferenceManager)
                            imageUrl = response?.body()?.getData()?.image?:""
                            setImage(null)

                        }
                    }

                }

                override fun onFailure(call: Call<ProfilePicResponseModel>, t: Throwable) {
                    baseActivity?.hideLoading()
                }

            })

    }

    private fun facebookLogin() {
        LoginManager.getInstance().registerCallback(callbackManager, object :
            FacebookCallback<LoginResult> {

            override fun onCancel() {
                baseActivity?.hideLoading()
            }

            override fun onError(exception: FacebookException) {
                baseActivity?.hideLoading()
            }

            override fun onSuccess(loginResult: LoginResult) {
                baseActivity?.hideLoading()
                val request = GraphRequest.newMeRequest(
                    loginResult!!.accessToken
                ) { `object`, response ->
                    try {
                        Log.i("Response", response.toString())
                        val socail_id = response!!.jsonObject!!.getString("id")
                        uploadBuildTrust(1, socail_id)
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }
                }
                val parameters = Bundle()
                parameters.putString("fields", "id")
                request.parameters = parameters
                request.executeAsync()
            }
        })
    }

    private fun googleLogin() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(requireContext(), gso)
        firebaseAuth = FirebaseAuth.getInstance()


        val signInIntent: Intent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, Req_Code)


    }


    override fun onItemClicked(postion: Int) {
        Common.selectedProductDetails = productModeldata?.get(postion)
        startActivity(Intent(requireContext(), ProductDetails::class.java))
    }

    private fun handleGoogleLoginResponse(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account: GoogleSignInAccount? = completedTask.getResult(ApiException::class.java)
            if (account != null) {
                uploadBuildTrust(0, account.id)
            }
        } catch (e: ApiException) {
        }
    }

    private fun twitterLogin() {
        client?.cancelAuthorize()
        client!!.authorize(
            requireActivity(),
            object : com.twitter.sdk.android.core.Callback<TwitterSession?>() {
                override fun success(result: Result<TwitterSession?>) {

                    // Do something with result, which provides a TwitterSession for making API calls
                    val twitterSession: TwitterSession? = result.data
                    val twitterApiClient = TwitterCore.getInstance().apiClient
                    val call: Call<User> =
                        twitterApiClient.accountService.verifyCredentials(true, false, true)
                    call.enqueue(object : com.twitter.sdk.android.core.Callback<User>() {
                        override fun success(result: Result<User>?) {
                            val user = result!!.data
                            uploadBuildTrust(2, user.id.toString())
                            baseActivity?.hideLoading()

                        }

                        override fun failure(exception: TwitterException?) {
                            baseActivity?.hideLoading()
                        }
                    })
                }

                override fun failure(e: TwitterException?) {
                    baseActivity?.hideLoading()
                }
            })
        /* } else {
             //if user is already authenticated direct call fetch twitter email api
             Toast.makeText(this, "User already authenticated", Toast.LENGTH_SHORT).show()
         }*/
    }

    private fun uploadBuildTrust(uploadType: Int, accountId: String?) {
        //0- Google 1-Facebook 2-Twitter 3-Email 4-Phone
        var googleId = userDetails?.googleId
        var facebookId = userDetails?.facebookId
        var twitterId = userDetails?.twitterId

        if (uploadType==0){
            googleId = accountId
            update_verificationForEmailPhone("google")
        }else if (uploadType == 1){
            facebookId = accountId
            update_verificationForEmailPhone("facebook")
        }else if (uploadType==2){
            twitterId = accountId
            update_verificationForEmailPhone("twitter")
        }

     /*   RestClient.getApiInterface().updateBuildTrust(
            userDetails?.token, userDetails?.id,
            facebookId, googleId, twitterId
        ).enqueue(object : Callback<ApiStatusModel> {
            override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {

            }

            override fun onResponse(
                call: Call<ApiStatusModel>,
                response: Response<ApiStatusModel>
            ) {
                if (response.body()?.getStatus() == 200) {
                    if (uploadType == 0) {
                        userDetails?.googleId = accountId
                        binding.psGoogleButton.visibility = View.VISIBLE
                    } else if (uploadType == 1) {
                        binding.psFacebookButton.visibility = View.VISIBLE
                        userDetails?.facebookId = accountId
                    } else if (uploadType == 2) {
                        userDetails?.twitterId = accountId
                        binding.psTwitterButton.visibility = View.VISIBLE
                    }
                        // updateVerifiedBy()

                }
            }
        })*/

    }

    private fun updateVerifiedBy() {
        binding.userName.setText(userDetails!!.fullName)

        if(userDetails!!.profile_verified == "1"){
            binding.imgVerified.visibility = View.VISIBLE
        }else{
            binding.imgVerified.visibility = View.GONE
        }



        if (userDetails?.verification !=null) {
            binding.psPhoneButton.visibility = View.GONE
            binding.psEmailButton.visibility = View.GONE



            userDetails?.verification?.forEachIndexed { index, verification ->
                if (verification.verifiedBy.equals("phone", ignoreCase = true)) {
                    binding.psPhoneButton.visibility = View.VISIBLE
                }
                if (verification.verifiedBy.equals("email", ignoreCase = true)) {
                    binding.psEmailButton.visibility = View.VISIBLE
                }
                if (verification.verifiedBy.equals("facebook", ignoreCase = true)) {
                    binding.psFacebookButton.visibility = View.VISIBLE
                }
                if (verification.verifiedBy.equals("twitter", ignoreCase = true)) {
                    binding.psTwitterButton.visibility = View.VISIBLE
                }
                if (verification.verifiedBy.equals("email", ignoreCase = true)) {
                    binding.psEmailButton.visibility = View.VISIBLE
                }
                if (verification.verifiedBy.equals("google", ignoreCase = true)) {
                    binding.psGoogleButton.visibility = View.VISIBLE
                }
            }
        }else{
            if((userDetails?.verification?.size ?: 0) == 0){
                binding.verifiedText.text = getString(R.string.profile_not_verified)
            }else{
                binding.verifiedText.text = getString(R.string.profile_not_verified)
            }

        }
        isEmailVerifying = false
        verifyingNumberOrEmail = ""
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        baseActivity?.hideLoading()
        callbackManager.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_SETTINGS_PAGE) {
            userDetails?.lat = Common.currentLat
            userDetails?.lng = Common.currentLng
            Common.getInstance().refreshLatLong.postValue(false)
        }

        if (requestCode == Req_Code) { //Google Signin
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            if (task.isSuccessful) {
                handleGoogleLoginResponse(task)
            }
        } else if (requestCode == Picker.PICK_FILE && resultCode == BaseActivity.RESULT_OK) {
            filePicker!!.submit(data)
        } else if (requestCode == REQUEST_CAMERA && data != null && data.extras != null) {
            try {
                val bundle = data.extras
                val bitmap = bundle!!["data"] as Bitmap?
                val imageName = System.currentTimeMillis().toString()
                val tempUri: Uri = getImageUri(requireContext()!!, bitmap!!)!!
                val file = File(getImagePathFromURI(tempUri))
                //Compress
                mArrayFile?.add(file!!)
                uploadImageToServer()
                setImage(Uri.fromFile(file))

            } catch (e: Exception) {
                e.message
            }
        }  else if (requestCode == 140) {
            client?.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun getImagePathFromURI(uri: Uri): String {
        var path = ""
        if (requireActivity().getContentResolver() != null) {
            val cursor: Cursor? = requireActivity().getContentResolver().query(
                uri,
                null,
                null,
                null,
                null
            )
            if (cursor != null) {
                cursor.moveToFirst()
                val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                path = cursor.getString(idx)
                cursor.close()
            }
        }
        return path
    }

    fun getImageUri(inContext: Context, inImage: Bitmap): Uri? {
        val bytes = ByteArrayOutputStream()
        val path = MediaStore.Images.Media.insertImage(
            inContext.contentResolver,
            inImage,
            "Title" + Calendar.getInstance().getTime(),
            null
        )
        return Uri.parse(path)
    }

    override fun onError(p0: String?) {

    }

    override fun onFilesChosen(list: MutableList<ChosenFile>?) {
        if (list?.size ?: 0 > 0) {
            mArrayFile = ArrayList();
            for (i in 0 until list?.size!!) {
                val file1 = File(list?.get(i)?.getOriginalPath())
                val file = baseActivity?.getCompressed(requireContext(), file1.path)
                val mFile = RequestBody.create("image/*".toMediaTypeOrNull(), file!!)
                mArrayFile?.add(file!!)
                uploadImageToServer()
                setImage(Uri.fromFile(file))

                break
            }
        }
    }

    override fun OnVerifiedClicked(otp: String) {
        verifYOtp(otp)
    /*    if (otp == "123456") {
            baseActivity.showLoading(requireContext())
            update_verificationForEmailPhone()
        } else {
            baseActivity.showToast("Enter Valid OTP", requireContext())
        }*/
    }

    override fun OnOtpSendClicked(number: String) {
        sendOtpToEmailPhone(number.trim(), 1)
    }

    override fun onClick(p0: DialogInterface?, p1: Int) {
        verifyOtpActivity?.dismiss()
    }


}