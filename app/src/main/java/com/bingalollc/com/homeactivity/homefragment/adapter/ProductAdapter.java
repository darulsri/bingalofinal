package com.bingalollc.com.homeactivity.homefragment.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.preference.Preference;
import androidx.recyclerview.widget.RecyclerView;

import com.bingalollc.com.R;
import com.bingalollc.com.base.BaseActivity;
import com.bingalollc.com.homeactivity.model.ProductModel;
import com.bingalollc.com.preference.Common;
import com.bingalollc.com.preference.PreferenceManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.card.MaterialCardView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.Viewholder> {
    private Context context;
    private ArrayList<ProductModel.Datum> productModeldata;
    private OnItemChoosed onItemChoosed;
    private PreferenceManager preferenceManager;

    public ProductAdapter(ArrayList<ProductModel.Datum> productModeldata, OnItemChoosed onItemChoosed) {
        this.productModeldata = productModeldata;
        this.onItemChoosed = onItemChoosed;
    }

    public void refreshData(ArrayList<ProductModel.Datum> productModeldata){
        this.productModeldata = productModeldata;
        notifyDataSetChanged();
    }

    public void addMoreData(ArrayList<ProductModel.Datum> productModeldata){
        int pos = productModeldata.size();
        this.productModeldata = new ArrayList<>();
        this.productModeldata.addAll(productModeldata);
        notifyDataSetChanged();

    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        preferenceManager = PreferenceManager.getInstance(context);
        View view = LayoutInflater.from(context).inflate(R.layout.products_items, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        if (productModeldata.get(position).getImages().size()>0){
            Glide.with(context).load(productModeldata.get(position).getImages().get(0).getImageUrl()).placeholder(R.drawable.product_placeholder).error(R.drawable.product_placeholder)
                    .apply(RequestOptions.bitmapTransform(new RoundedCorners(10)))
                    .diskCacheStrategy(DiskCacheStrategy.ALL).dontTransform()
                    .into(holder.productImage);
        }
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.sold_image.getLayoutParams();
        if (productModeldata.get(position).getProductIsSold().equalsIgnoreCase("1")){
            if (preferenceManager.getIsEnglish() == 1) {
                params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
                Glide.with(context).load(R.drawable.item_sold).diskCacheStrategy(DiskCacheStrategy.ALL).dontTransform().into(holder.sold_image);
            }else {
                params.addRule(RelativeLayout.ALIGN_PARENT_START, RelativeLayout.TRUE);
                Glide.with(context).load(R.drawable.item_sold_he).diskCacheStrategy(DiskCacheStrategy.ALL).dontTransform().into(holder.sold_image);
            }
        } else if (productModeldata.get(position).getBumpData()!=null && !productModeldata.get(position).getBumpData().equals("")){
            if (preferenceManager.getIsEnglish() == 1) {
                params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
                Glide.with(context).load(R.drawable.item_bumped).diskCacheStrategy(DiskCacheStrategy.ALL).dontTransform().into(holder.sold_image);
            } else  {
                params.addRule(RelativeLayout.ALIGN_PARENT_START, RelativeLayout.TRUE);
                params.addRule(RelativeLayout.ALIGN_PARENT_START, RelativeLayout.TRUE);
                Glide.with(context).load(R.drawable.item_bumped_he).diskCacheStrategy(DiskCacheStrategy.ALL).dontTransform().into(holder.sold_image);
            }

        }else if (BaseActivity.isProductTodayPosted(Calendar.getInstance().getTime(),
                BaseActivity.convertStringToDate(productModeldata.get(position).getCreatedAt()))){
            if (preferenceManager.getIsEnglish() == 1) {
                params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
                Glide.with(context).load(R.drawable.item_new).diskCacheStrategy(DiskCacheStrategy.ALL).dontTransform().into(holder.sold_image);
            } else {
                params.addRule(RelativeLayout.ALIGN_PARENT_START, RelativeLayout.TRUE);
                Glide.with(context).load(R.drawable.item_new_he).diskCacheStrategy(DiskCacheStrategy.ALL).dontTransform().into(holder.sold_image);
            }
        }else {
            holder.sold_image.setImageDrawable(null);
        }
    }

    public boolean isTodayPosted(String timeDiff) {
        if (timeDiff.contains("years") || timeDiff.contains("months")) {
            return false;
        } else if (timeDiff.contains("days")) {
            if (timeDiff.contains("1 days")) {
                return true;
            } else  return false;
        } else {
            return true;
        }
    }

    public boolean compareTime(String dateString){
        Calendar calendar1 = Calendar.getInstance();
        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
        String currentDate = formatter1.format(calendar1.getTime());
        String dynamicDate = "";
        try {
             dynamicDate = dateString.substring(0,11).trim();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(currentDate.compareTo(dynamicDate)==0) {
            return true;
        }else {
            return false;
        }
    }

    @Override
    public int getItemCount() {
        return productModeldata.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        private final ImageView productImage,sold_image;
        private CardView prod_layout;
        public Viewholder(@NonNull View itemView) {
            super(itemView);
            productImage = itemView.findViewById(R.id.productImage);
            sold_image = itemView.findViewById(R.id.sold_image);
            prod_layout = itemView.findViewById(R.id.prod_layout);
            productImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemChoosed.onItemClicked(getAdapterPosition());
                }
            });
        }
    }

    public interface OnItemChoosed{
        void onItemClicked(int postion);
    }
}
