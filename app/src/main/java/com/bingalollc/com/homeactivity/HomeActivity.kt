package com.bingalollc.com.homeactivity

import android.Manifest
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.text.*
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.viewpager.widget.ViewPager
import com.bingalollc.com.AppController
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.bottom_sheet.AddPhoneNumberPopUpDialog
import com.bingalollc.com.camerax.CameraConstant
import com.bingalollc.com.google_analytics.AnalyticsEvent
import com.bingalollc.com.google_analytics.GoogleAnalytics
import com.bingalollc.com.homeactivity.ViewPager.CircularPageIndicator
import com.bingalollc.com.homeactivity.adapter.TabAdapter
import com.bingalollc.com.homeactivity.homefragment.HomeFragment
import com.bingalollc.com.homeactivity.notification.model.NotificationCountModel
import com.bingalollc.com.model.ApiStatusModel
import com.bingalollc.com.model.SignUpModel
import com.bingalollc.com.model.SocialLoginModel
import com.bingalollc.com.network.MultipartParams
import com.bingalollc.com.network.RestClient
import com.bingalollc.com.preference.Common
import com.bingalollc.com.signup.OtpScreen
import com.bingalollc.com.users.Users
import com.bingalollc.com.utils.CustomViewPager
import com.bingalollc.com.utils.PickerUtils
import com.bingalollc.com.utils.showDialogFragment
import com.bumptech.glide.Glide
import com.facebook.*
import com.facebook.FacebookSdk.sdkInitialize
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.android.material.tabs.TabLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.hbb20.CountryCodePicker
import com.kbeanie.multipicker.api.FilePicker
import com.kbeanie.multipicker.api.Picker
import com.kbeanie.multipicker.api.callbacks.FilePickerCallback
import com.kbeanie.multipicker.api.entity.ChosenFile
import com.onesignal.OneSignal
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import com.twitter.sdk.android.core.*
import com.twitter.sdk.android.core.identity.TwitterAuthClient
import com.twitter.sdk.android.core.models.User
import de.hdodenhof.circleimageview.CircleImageView
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import pub.devrel.easypermissions.EasyPermissions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.net.URL
import java.util.*


open class HomeActivity : BaseActivity(), FilePickerCallback, OtpScreen.OnClicked,
    DialogInterface.OnClickListener {
    private val REQUEST_READ_EXTERNAL_STORAGE_CHOOSE_IMG = 1000
    private var onBackButtonClicked = false
    var tabLayout: TabLayout? = null
    var numberLayoutHome: LinearLayoutCompat? = null
    var viewPager: CustomViewPager? = null
    var adapter: TabAdapter? = null
    var fragment: Fragment? = null
    private var header: TextView? = null
    var chatCount: TextView? = null
    var notificationCount: TextView? = null
    private var cipTutorial: CircularPageIndicator? = null
    private var vpImages: ViewPager? = null
    lateinit var mGoogleSignInClient: GoogleSignInClient
    val Req_Code: Int = 123
    var firebaseAuth = FirebaseAuth.getInstance()
    lateinit var callbackManager: CallbackManager
    var client: TwitterAuthClient? = null

    val REQUEST_CAMERA = 101
    var loginCode = true
    val PICK_IMAGE_REQUEST_GALLERY = 102
    private val MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 2001
    private val PERMISSION_CAMERA_REQUEST_CODE = 2002
    var fileToUpload: MultipartBody.Part? = null
    var mArrayFile: java.util.ArrayList<File>? = java.util.ArrayList()
    private var imageUrl = ""
    private var fullName = ""
    private var socialId = ""
    private var loginType = ""
    private var deviceType = ""
    private lateinit var name: String
    private lateinit var email: String
    private lateinit var phone: String
    private lateinit var password: String
    private lateinit var confirmPassword: String
    var totalUnReadNotificationCount = 0
    private var isSocialLogin = MutableLiveData(false)


    companion object {
        @JvmStatic
        lateinit var instance: HomeActivity
    }

    init {
        instance = this
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.activity_home)

        sdkInitialize(this)
//        setAutoLogAppEventsEnabled(true)
//        setIsDebugEnabled(true)
//        addLoggingBehavior(LoggingBehavior.APP_EVENTS)

        notificationCount = findViewById(R.id.notificationCount) as TextView?


        instance = this
        if (preferenceManager?.isEnglish == 1) {
            chooseLanguage(true)
        } else {
            chooseLanguage(false)
        }


        if (preferenceManager?.isUserLoggedIn == true && preferenceManager?.userDetails?.phone.isNullOrEmpty()) {
            showAddPhoneDialog()
        }


        if (preferenceManager?.userDetails?.id.isNullOrEmpty().not()) {
            System.out.println(">>>>>>>>>OneSignal.getDeviceState()?.userId " + OneSignal.getDeviceState()?.userId)
            if (preferenceManager.isUserLoggedIn) {

            }
            updateToken(OneSignal.getDeviceState()?.userId ?: "")
            getStoragePermission()
        }

        initSocialLogin()

        showFullScreenBaseColor(0)
        initViewPager()
        disableSwipe()

        if (preferenceManager?.userDetails?.id.isNullOrEmpty().not()) {
            fetchNotificationCount(
                preferenceManager?.userDetails?.id!!,
                preferenceManager?.userDetails?.token!!
            )
        } else {
            notificationCount?.visibility = View.INVISIBLE
        }

    }

    fun fetchNotificationCount(user_id: String, token: String) {


        //  showLoading(this)
        RestClient.getApiInterface().fetchNotificationBadge(user_id, token).enqueue(
            object : Callback<NotificationCountModel> {
                override fun onFailure(call: Call<NotificationCountModel>, t: Throwable) {
                    //   hideLoading()

                    System.out.print(">>>>>>>>NOTTMT " + t.message)
                }

                override fun onResponse(
                    call: Call<NotificationCountModel>,
                    response: Response<NotificationCountModel>
                ) {
                    //  hideLoading()
                    if (response.body()?.status == 200) {

                        try {
                            System.out.print(">>>>>>>>NOTTT COUNT "+response.body()?.data)
                            if (response.body()?.data != null) {
                                totalUnReadNotificationCount = response.body()?.data!!.toInt()
                                if (preferenceManager!!.notificationCount) {
                                    totalUnReadNotificationCount = totalUnReadNotificationCount + 1
                                } else {
                                    totalUnReadNotificationCount = response.body()?.data!!.toInt()
                                }
                                notificationCount?.text = totalUnReadNotificationCount.toString()
                                notificationCount?.visibility = View.VISIBLE
                            } else {
                                if (preferenceManager!!.notificationCount) {
                                    totalUnReadNotificationCount = totalUnReadNotificationCount + 1
                                    notificationCount?.text =
                                        totalUnReadNotificationCount.toString()
                                    notificationCount?.visibility = View.VISIBLE
                                } else {
                                    notificationCount?.text = "0"
                                    notificationCount?.visibility = View.GONE
                                }

                            }
                        } catch (nfe: NumberFormatException) {
                            // not a valid int
                        }

                    }
                }
            })
    }


    class doAsync(val handler: () -> Unit) : AsyncTask<Void, Void, Void>() {
        override fun doInBackground(vararg params: Void?): Void? {
            handler()
            return null
        }
    }


    fun updateChatCount(chatCountNum: Int) {
        if (chatCountNum > 0) {
            chatCount = findViewById(R.id.chatCount) as TextView?
            chatCount?.text = chatCountNum.toString()
            chatCount?.visibility = View.VISIBLE
        } else {
            chatCount?.visibility = View.INVISIBLE
        }
    }

    fun updateNotificationChatCount(chatCountNum: Int) {
        if (chatCountNum > 0) {
            notificationCount = findViewById(R.id.notificationCount) as TextView?
            notificationCount?.text = chatCountNum.toString()
            notificationCount?.visibility = View.VISIBLE
        } else {
            notificationCount?.visibility = View.INVISIBLE
        }
    }

    private fun getStoragePermission() {
        if (checkPermissionREAD_EXTERNAL_STORAGE(
                this
            )
        ) {
        }
    }

//    fun checkPermissionREAD_EXTERNAL_STORAGE(
//        context: Context?, requestCode: Int
//    ): Boolean {
//        val currentAPIVersion = Build.VERSION.SDK_INT
//        return if (currentAPIVersion >= Build.VERSION_CODES.M) {
//            if (ContextCompat.checkSelfPermission(
//                    this,
//                    Manifest.permission.WRITE_EXTERNAL_STORAGE
//                ) != PackageManager.PERMISSION_GRANTED) {
//                if (ActivityCompat.shouldShowRequestPermissionRationale(
//                        this,
//                        Manifest.permission.WRITE_EXTERNAL_STORAGE
//                    )) {
//                    //Toast.makeText(PostAdPage.this, "Do itititi", Toast.LENGTH_SHORT).show();
//                    showDialog(
//                        "External storage", context,
//                        Manifest.permission.WRITE_EXTERNAL_STORAGE, requestCode
//                    )
//                } else {
//                    requestPermissions(
//                        arrayOf(
//                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
//                            Manifest.permission.READ_EXTERNAL_STORAGE
//                        ),
//                        requestCode
//                    )
//                }
//                false
//            } else {
//                true
//            }
//        } else {
//            true
//        }
//    }


    fun checkPermissionREAD_EXTERNAL_STORAGE(
        context: Context?
    ): Boolean {
        val currentAPIVersion = Build.VERSION.SDK_INT
        return if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this!!,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        (context as Activity?)!!,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )
                ) {
                    showDialog(
                        "External storage",
                        context,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE
                    )
                } else {
                    requestPermissions(
                        arrayOf(
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        ),
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE
                    )
                }
                false
            } else {
                true
            }
        } else {
            true
        }
    }

    fun showDialog(
        msg: String, context: Context?,
        permission: String, statusCode: Int
    ) {
        val alertBuilder = android.app.AlertDialog.Builder(context)
        alertBuilder.setCancelable(false)
        alertBuilder.setTitle("Permission necessary")
        alertBuilder.setMessage("$msg permission is necessary")
        alertBuilder.setPositiveButton(
            android.R.string.yes
        ) { dialog, which ->
            requestPermissions(
                arrayOf(permission),
                statusCode
            )
        }
        val alert = alertBuilder.create()
        alert.show()
    }

    private fun updateToken(token: String) {
        RestClient.getApiInterface()
            .updateToken(
                preferenceManager.userDetails.token,
                preferenceManager.userDetails.id,
                token
            )
            .enqueue(object : Callback<ApiStatusModel?> {
                override fun onResponse(
                    call: Call<ApiStatusModel?>,
                    response: Response<ApiStatusModel?>
                ) {
                    Log.d("ad", "");
                }

                override fun onFailure(call: Call<ApiStatusModel?>, t: Throwable) {}
            })
    }

    fun hideBottomBar(visible: Boolean) {
        if (visible) {
            tabLayout!!.animate()
                .translationY(0.0f)
                .alpha(1.0f)
                .setDuration(2000)
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        super.onAnimationEnd(animation)
                        tabLayout?.visibility = View.VISIBLE
                        numberLayoutHome?.visibility = View.VISIBLE
                    }
                })
        } else {
            tabLayout!!.animate()
                .translationY(tabLayout!!.getHeight().toFloat())
                .alpha(0.0f)
                .setDuration(2000)
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        super.onAnimationEnd(animation)
                        tabLayout?.setVisibility(View.GONE)
                        numberLayoutHome?.setVisibility(View.GONE)
                    }
                })
        }
    }

    fun showAddPhoneDialog() {
        val bottomSheetDialog = AddPhoneNumberPopUpDialog()
        bottomSheetDialog.isCancelable = true
        bottomSheetDialog.show(supportFragmentManager, "bottom")
    }


    fun showSocialLoginDialog(context: Context) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.activity_login_bottom)
        dialog.setCancelable(false)
        val imgClose: RelativeLayout = dialog.findViewById(R.id.imgClose)
        val googleLogin = dialog.findViewById(R.id.googleLogin) as RelativeLayout?
        val twitterLogin = dialog.findViewById(R.id.twitterLogin) as RelativeLayout?
        val fbLogin = dialog.findViewById(R.id.fbLogin) as RelativeLayout?
        val continueWithEmail = dialog.findViewById(R.id.continueWithEmail) as RelativeLayout?
        val btnAlreadyMember = dialog.findViewById(R.id.btnAlreadyMember) as LinearLayout?
        initSocialLogin()
        googleLogin?.setOnClickListener {
            loginCode = true
            googleLogin()
            dialog.dismiss()
        }

        fbLogin?.setOnClickListener {
            dialog.dismiss()
            LoginManager.getInstance()
                .logInWithReadPermissions(this, Arrays.asList("public_profile"))
            facebookLogin()

        }

        twitterLogin?.setOnClickListener {
            dialog.dismiss()
            twitterLogin()
        }

        imgClose.setOnClickListener {
            dialog.dismiss()
        }

        btnAlreadyMember?.setOnClickListener {
            showSignUpDialog(context)
            dialog.dismiss()
        }

        continueWithEmail?.setOnClickListener {
            showLoginDialog(context)
            dialog.dismiss()
        }

        dialog.show()
        dialog.getWindow()!!
            .setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        //  dialog.getWindow()!!.getAttributes().windowAnimations = R.style.DialogAnimation
        dialog.getWindow()!!.setGravity(Gravity.BOTTOM)
    }

    var et_email: EditText? = null
    var et_username: EditText? = null
    var et_password: EditText? = null
    private var isLoginPasswordVisible = false
    fun showLoginDialog(context: Context) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.activity_login_with_mail_bottom)
        dialog.setCancelable(false)
        val tv_signup = dialog.findViewById(R.id.tv_signup) as LinearLayout?
        val tv_forgot = dialog.findViewById(R.id.tv_forgot) as TextView?
        val bt_login = dialog.findViewById(R.id.bt_login) as RelativeLayout?
        val parentLayout = dialog.findViewById(R.id.parentLayout) as RelativeLayout?
        val showHideLoginPassword = dialog.findViewById(R.id.showHideLoginPassword) as ImageView?
        et_email = dialog.findViewById(R.id.et_email) as EditText?
        et_password = dialog.findViewById(R.id.et_password) as EditText?
        val cbRememberMe = dialog.findViewById(R.id.cbRememberMe) as AppCompatCheckBox?

        val imgClose: RelativeLayout = dialog.findViewById(R.id.imgClose)
        val imgBack: RelativeLayout = dialog.findViewById(R.id.imgBack)

        // Get a hold of the inpoutMethodManager here
        //  val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager


        tv_signup?.setOnClickListener {
            showSignUpDialog(context)
            dialog.dismiss()
        }

        parentLayout?.setOnClickListener {
            //   et_email?.requestFocus()
            //  imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
        }

        tv_forgot?.setOnClickListener {
            showForgetPasswordDialog(context)
            dialog.dismiss()
        }

        showHideLoginPassword?.setOnClickListener {
            if (isLoginPasswordVisible) {
                showHideLoginPassword.setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.show_password
                    )
                )
                isLoginPasswordVisible = false
                et_password?.transformationMethod = PasswordTransformationMethod()
            } else {
                showHideLoginPassword.setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.hide_password
                    )
                )
                isLoginPasswordVisible = true
                et_password?.transformationMethod = null
            }
            et_password?.setSelection(et_password?.text?.length ?: 0)

        }
        bt_login?.setOnClickListener {
            if (validateLoginInputs()) {
                showLoading(this)
                RestClient.getApiInterface()
                    .login(et_email?.text.toString(), et_password?.text.toString())
                    .enqueue(object : Callback<Users> {
                        override fun onFailure(call: Call<Users>, t: Throwable) {
                            hideLoading()
                        }

                        override fun onResponse(call: Call<Users>, response: Response<Users>) {
                            hideLoading()
                            if (response.body()?.status == 200) {
                                dialog.dismiss()
                                if (cbRememberMe?.isChecked!!) {
                                    preferenceManager?.setUserLoggedin(true)
                                } else {
                                    preferenceManager?.setUserLoggedin(false)
                                }
                                if (response?.body()?.data?.is_activated != 2) {
                                    preferenceManager?.userDetails = response?.body()?.data
                                    AppController.getInstance().haveCalled = true
                                    preferenceManager?.setUserLoggedin(true)
                                    startActivity(
                                        Intent(this@HomeActivity, HomeActivity::class.java)
                                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                    )
                                    finish()
                                } else {
                                    showDeactivateMsg()
                                }

                            } else {
                                if (cbRememberMe?.isChecked!!) {
                                    preferenceManager?.setUserLoggedin(true)
                                } else {
                                    preferenceManager?.setUserLoggedin(false)
                                }
                                showToast(response.body()?.message, this@HomeActivity)
                            }
                        }
                    })
            }
        }

        imgBack.setOnClickListener {
            dialog.dismiss()
            showSocialLoginDialog(context)
            // Toast.makeText(this@MainActivity, "Print is Clicked", Toast.LENGTH_SHORT).show()
        }
        imgClose.setOnClickListener {
            dialog.dismiss()
            // Toast.makeText(this@MainActivity, "Print is Clicked", Toast.LENGTH_SHORT).show()
        }
        dialog.show()
        dialog.getWindow()!!
            .setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        //  dialog.getWindow()!!.getAttributes().windowAnimations = R.style.DialogAnimation
        dialog.getWindow()!!.setGravity(Gravity.BOTTOM)


    }

    private lateinit var userName: String

    fun showForgetPasswordDialog(context: Context) {
        val dialog = Dialog(context)
        dialog.setCancelable(false)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.activity_forget_password_bottom)
        val backToLogin = dialog.findViewById(R.id.backToLogin) as LinearLayout?
        val submitBtn = dialog.findViewById(R.id.submitBtn) as AppCompatButton?
        et_username = dialog.findViewById(R.id.et_username) as EditText?

        val imgClose: RelativeLayout = dialog.findViewById(R.id.imgClose)
        val imgBack: RelativeLayout = dialog.findViewById(R.id.imgBack)

        backToLogin?.setOnClickListener({
            showLoginDialog(context)
            dialog.dismiss()
        })


        submitBtn?.setOnClickListener {
            if (validateForgetPasswordInputs()) {
                showLoading(this)
                RestClient.getApiInterface().forgotPassword(userName)
                    .enqueue(object : Callback<ApiStatusModel> {
                        override fun onResponse(
                            call: Call<ApiStatusModel>,
                            response: Response<ApiStatusModel>
                        ) {
                            hideLoading()
                            if (response.body()?.getStatus() == 200) {
                                dialog.dismiss()
                                showMessageOKCancel(
                                    getString(R.string.reset_link_sent_successfully),
                                    "",
                                    this@HomeActivity,
                                    this@HomeActivity,
                                    getString(R.string.ok),
                                    "",
                                    false
                                )
                            } else {
                                showMessageOKCancel(
                                    response.body()?.getMessage(),
                                    "",
                                    this@HomeActivity,
                                    this@HomeActivity,
                                    getString(R.string.ok),
                                    "",
                                    false
                                )
                            }
                        }

                        override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
                            hideLoading()
                        }

                    })
            }
        }

        imgBack.setOnClickListener {
            dialog.dismiss()
            showLoginDialog(context)
            // Toast.makeText(this@MainActivity, "Print is Clicked", Toast.LENGTH_SHORT).show()
        }
        imgClose.setOnClickListener {
            dialog.dismiss()
            // Toast.makeText(this@MainActivity, "Print is Clicked", Toast.LENGTH_SHORT).show()
        }
        dialog.show()
        dialog.getWindow()!!
            .setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        //  dialog.getWindow()!!.getAttributes().windowAnimations = R.style.DialogAnimation
        dialog.getWindow()!!.setGravity(Gravity.BOTTOM)
    }

    override fun onClick(p0: DialogInterface?, p1: Int) {
        p0!!.dismiss();
        // onBackPressed()
    }

    private fun validateForgetPasswordInputs(): Boolean {
        userName = et_username?.text.toString()
        if (TextUtils.isEmpty(userName)) {
            et_username?.error = getString(R.string.email_or_username_required)
            et_username?.requestFocus()
            return false
        } else {
            return true
        }
    }

    private fun validateLoginInputs(): Boolean {
        if (TextUtils.isEmpty(et_email?.text.toString())) {
            et_email?.error = getString(R.string.email_required)
            et_email?.requestFocus()
            return false
        } else if (!et_email?.text.toString().contains("@")) {
            et_email?.error = getString(R.string.invalid_email)
            et_email?.requestFocus()
            return false
        } else if (TextUtils.isEmpty(et_password?.text.toString())) {
            et_password?.error = getString(R.string.password_required)
            et_password?.requestFocus()
            return false
        } else {
            return true
        }
    }


    var profilePic: CircleImageView? = null
    var et_sname: EditText? = null
    var et_semail: EditText? = null
    var et_sphone: EditText? = null
    var et_spassword: EditText? = null
    var et_sconfirm_password: EditText? = null
    var showHideSignupPassword: ImageView? = null
    var showHideConfirmPassword: ImageView? = null
    var checkbox: CheckBox? = null
    var imgUser: ImageView? = null
    var addLayout: RelativeLayout? = null
    var ccp: CountryCodePicker? = null
    private var isPasswordVisible = false
    private var isConfirmPasswordVisible = false

    fun showSignUpDialog(context: Context) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.activity_signup_bottom)
        dialog.setCancelable(false)
        et_sname = dialog.findViewById(R.id.et_name) as EditText?
        et_semail = dialog.findViewById(R.id.et_email) as EditText?
        et_sphone = dialog.findViewById(R.id.et_phone) as EditText?
        et_spassword = dialog.findViewById(R.id.et_password) as EditText?
        et_sconfirm_password = dialog.findViewById(R.id.et_confirm_password) as EditText?
        profilePic = dialog.findViewById(R.id.profilePic) as CircleImageView?
        showHideSignupPassword = dialog.findViewById(R.id.showHideSignupPassword) as ImageView?
        showHideConfirmPassword = dialog.findViewById(R.id.showHideConfirmPassword) as ImageView?
        imgUser = dialog.findViewById(R.id.imgUser) as ImageView?
        checkbox = dialog.findViewById(R.id.checkbox) as CheckBox?
        addLayout = dialog.findViewById(R.id.addLayout) as RelativeLayout?
        ccp = dialog.findViewById(R.id.ccp) as CountryCodePicker?
        val btnContinue = dialog.findViewById(R.id.btnContinue) as RelativeLayout?
        val alreadyMember = dialog.findViewById(R.id.alreadyMember) as LinearLayout?

        val fbLogin = dialog.findViewById(R.id.fbLogin) as ImageView?
        val twitterLogin = dialog.findViewById(R.id.twitterLogin) as ImageView?
        val googleLogin = dialog.findViewById(R.id.googleLogin) as ImageView?


        val imgClose: RelativeLayout = dialog.findViewById(R.id.imgClose)
        val imgBack: RelativeLayout = dialog.findViewById(R.id.imgBack)

        ccp?.setCountryForNameCode("IL")
        ccp?.setCustomMasterCountries("il,gb,us,in")
        ccp?.setOnCountryChangeListener { checkValidNumber() }

        initSocialLogin()
        googleLogin?.setOnClickListener({
            loginCode = false
            //  dialog.dismiss()
            googleLogin()
        })

        fbLogin?.setOnClickListener({
            //  dialog.dismiss()
            LoginManager.getInstance()
                .logInWithReadPermissions(this, Arrays.asList("public_profile"))
            facebookSignup()

        })

        twitterLogin?.setOnClickListener({
            // dialog.dismiss()
            twitterSignup()
        })

        profilePic?.setOnClickListener({
            showImagePickerDialog(context)

        })

        alreadyMember?.setOnClickListener({
            showLoginDialog(context)
            dialog.dismiss()
        })

        btnContinue?.setOnClickListener {
            if (validateInputs()) {
                phone = ccp?.selectedCountryCode.toString() + et_sphone?.text.toString()
                if (phone.contains("+").not()) {
                    phone = "+$phone"
                }
                //  dialog.dismiss()
                optScreen()
            }
        }


        imgBack.setOnClickListener {
            dialog.dismiss()
            showSocialLoginDialog(context)
            // Toast.makeText(this@MainActivity, "Print is Clicked", Toast.LENGTH_SHORT).show()
        }
        imgClose.setOnClickListener {
            dialog.dismiss()
            // Toast.makeText(this@MainActivity, "Print is Clicked", Toast.LENGTH_SHORT).show()
        }

        showHideSignupPassword?.setOnClickListener {
            if (isPasswordVisible) {
                showHideSignupPassword!!.setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.show_password
                    )
                )
                isPasswordVisible = false
                et_spassword?.transformationMethod = PasswordTransformationMethod()
            } else {
                showHideSignupPassword!!.setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.hide_password
                    )
                )
                isPasswordVisible = true
                et_spassword?.transformationMethod = null
            }
            et_spassword?.setSelection(et_spassword?.text?.length ?: 0)

        }

        showHideConfirmPassword?.setOnClickListener {
            if (isConfirmPasswordVisible) {
                showHideConfirmPassword!!.setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.show_password
                    )
                )
                isConfirmPasswordVisible = false
                et_sconfirm_password?.transformationMethod = PasswordTransformationMethod()
            } else {
                showHideConfirmPassword!!.setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.hide_password
                    )
                )
                isConfirmPasswordVisible = true
                et_sconfirm_password?.transformationMethod = null
            }
            et_sconfirm_password?.setSelection(et_sconfirm_password?.text?.length ?: 0)

        }

        et_sphone?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                checkValidNumber()
            }

        })

        dialog.show()
        dialog.getWindow()!!
            .setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        //  dialog.getWindow()!!.getAttributes().windowAnimations = R.style.DialogAnimation
        dialog.getWindow()!!.setGravity(Gravity.BOTTOM)
    }

    private fun checkValidNumber() {
        val countryCode = ccp?.selectedCountryCode.toString()
        var MAX = 10

        et_sphone?.setFilters(arrayOf<InputFilter>(InputFilter.LengthFilter(MAX)))
        if (et_sphone?.length() ?: 0 > MAX) {
            et_sphone?.setText(et_sphone?.text?.toString()?.substring(0, MAX))
            et_sphone?.setSelection(et_sphone?.text?.length ?: 0)
        }
    }


    private fun optScreen() {
        val frag = showDialogFragment<OtpScreen>()
        frag?.setPhoneNum(phone)
        frag?.setListner(this)
    }

    private var alertDialog: AlertDialog.Builder? = null
    var filePicker: FilePicker? = null
    private val permissionsGALLERY = arrayOf(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE
    )


    var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                // There are no request codes
                val files = File(getCameraFilePathName())
                val file = getCompressed(this, files.path)
                try {
                    com.theartofdev.edmodo.cropper.CropImage.activity(Uri.fromFile(file))
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this@HomeActivity);

                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        }

    private fun showImagePickerDialog(context: Context) {
        alertDialog = AlertDialog.Builder(context)
        val inflater: LayoutInflater = getLayoutInflater()
        val add_menu_layout: View = inflater.inflate(R.layout.image_mode_upload, null)
        val submitMode = add_menu_layout.findViewById<Button>(R.id.submitMode)
        alertDialog!!.setView(add_menu_layout)
        alertDialog!!.setCancelable(true)
        val testDialog: AlertDialog = alertDialog!!.create()
        val tv_camera = add_menu_layout.findViewById<TextView>(R.id.tv_camera)
        val tv_gallery = add_menu_layout.findViewById<TextView>(R.id.tv_gallery)
        tv_camera.setOnClickListener {
            if (checkPermissionForCamera(context)) {
                openCamera()
            } else {
                requestPermissionForCamera(this)
            }
            testDialog.dismiss()
        }
        tv_gallery.setOnClickListener {
            if (checkPermissionREAD_EXTERNAL_STORAGE(this)) {
                choosephoto()
            }
            testDialog.dismiss()
        }
        testDialog.show()

    }

    fun hideSoftKeyboard(activity: Activity) {
        val inputMethodManager: InputMethodManager = activity.getSystemService(
            INPUT_METHOD_SERVICE
        ) as InputMethodManager
        if (inputMethodManager.isAcceptingText()) {
            inputMethodManager.hideSoftInputFromWindow(
                activity.currentFocus!!.windowToken,
                0
            )
        }
    }

    fun openCamera() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val outputFileUri = Uri.fromFile(setCamerFilePath())
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri)
        cameraIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        cameraIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        resultLauncher.launch(cameraIntent)
    }

    fun choosephoto() {
        /* val intent = Intent()
         intent.type = "image/*"
         intent.action = Intent.ACTION_GET_CONTENT
         startActivityForResult(
             Intent.createChooser(intent, "Select Picture"),
             PICK_IMAGE_REQUEST_GALLERY
         )*/

         */
        if (EasyPermissions.hasPermissions(this, *permissionsGALLERY)) {
            pickImageMultiple("image")
        } else {
            EasyPermissions.requestPermissions(
                this, resources.getString(R.string.please_allow_permissions),
                1003, *permissionsGALLERY
            )
        }
    }

    fun pickImageMultiple(type: String) {
        filePicker = getFilePicker()
        filePicker?.allowMultiple()
        filePicker?.setMimeType("image/*")
        filePicker?.pickFile()
    }

    @JvmName("getFilePicker1")
    fun getFilePicker(): FilePicker? {
        filePicker = FilePicker(this)
        filePicker!!.setFilePickerCallback(this)
        filePicker!!.setCacheLocation(PickerUtils.getSavedCacheLocation(this))
        return filePicker
    }


    fun twitterLogin() {
//        if (getTwitterSession() == null) {
        //if user is not authenticated start authenticating
        client?.cancelAuthorize()
        client!!.authorize(this, object : com.twitter.sdk.android.core.Callback<TwitterSession?>() {
            override fun success(result: Result<TwitterSession?>) {

                // Do something with result, which provides a TwitterSession for making API calls
                val twitterSession: TwitterSession? = result.data
                val twitterApiClient = TwitterCore.getInstance().apiClient
                val call: Call<User> =
                    twitterApiClient.accountService.verifyCredentials(true, false, true)
                call.enqueue(object : com.twitter.sdk.android.core.Callback<User>() {
                    override fun success(result: Result<User>?) {
                        val user = result!!.data
                        socialLogin(
                            user.email,
                            user.profileImageUrl,
                            user.name,
                            user.id.toString(),
                            "2",
                            "android"
                        )

                    }

                    override fun failure(exception: TwitterException?) {
                    }
                })
            }

            override fun failure(e: TwitterException?) {
                showToast("Failed to authenticate. Please try again.", this@HomeActivity)
            }
        })
        /* } else {
             //if user is already authenticated direct call fetch twitter email api
             Toast.makeText(this, "User already authenticated", Toast.LENGTH_SHORT).show()
         }*/
    }

    fun twitterSignup() {
//        if (getTwitterSession() == null) {
        //if user is not authenticated start authenticating
        client!!.authorize(this, object : com.twitter.sdk.android.core.Callback<TwitterSession?>() {
            override fun success(result: Result<TwitterSession?>) {

                // Do something with result, which provides a TwitterSession for making API calls
                val twitterSession: TwitterSession? = result.data
                val twitterApiClient = TwitterCore.getInstance().apiClient
                val call: Call<User> =
                    twitterApiClient.accountService.verifyCredentials(true, false, true)
                call.enqueue(object : com.twitter.sdk.android.core.Callback<User>() {
                    override fun success(result: Result<User>?) {
                        val user = result!!.data
                        setDataToUI(
                            user.email,
                            user.profileImageUrl,
                            user.name,
                            user.id.toString(),
                            "twitter",
                            "android"
                        )
                        email = user.email
                        imageUrl = user.profileImageUrl
                        fullName = user.name
                        socialId = user.id.toString()
                        loginType = "twitter"
                        deviceType = "android"
                        isSocialLogin.value = true
                        //   preferenceManager?.setUserLoggedin(true)

                    }

                    override fun failure(exception: TwitterException?) {
                    }
                })
            }

            override fun failure(e: TwitterException?) {
                showToast("Failed to authenticate. Please try again.", this@HomeActivity)
            }
        })
        /* } else {
             //if user is already authenticated direct call fetch twitter email api
             Toast.makeText(this, "User already authenticated", Toast.LENGTH_SHORT).show()
         }*/
    }

    fun facebookLogin() {
        var lastName = ""
        var email = ""
        LoginManager.getInstance().registerCallback(callbackManager, object :
            FacebookCallback<LoginResult> {
            override fun onCancel() {
                showToast("Cancelled", this@HomeActivity)
            }

            override fun onError(exception: FacebookException) {
                showToast(exception.message, this@HomeActivity)
            }

            override fun onSuccess(loginResult: LoginResult) {
                val request = GraphRequest.newMeRequest(
                    loginResult.accessToken
                ) { `object`, response ->
                    try {
                        Log.i("Response", response.toString())
                        val socail_id = response!!.jsonObject!!.getString("id")
                        val profilePicture =
                            URL("https://graph.facebook.com/$socail_id/picture?width=500&height=500")
                        if (response.jsonObject!!.has("email")) {
                            email = response.jsonObject!!.getString("email")
                        }
                        val firstName = response.jsonObject!!.getString("first_name")
                        if (response!!.jsonObject!!.getString("last_name") != null) {
                            lastName = response!!.jsonObject!!.getString("last_name")
                        } else {
                            lastName = ""
                        }
                        socialLogin(
                            email,
                            profilePicture.toString(),
                            firstName + " " + lastName,
                            socail_id,
                            "0",
                            "android"
                        )
                    } catch (e: java.lang.Exception) {
                        showToast(e.message, this@HomeActivity)
                    }
                }
                val parameters = Bundle()
                parameters.putString("fields", "id,email,first_name,last_name,gender")
                request.parameters = parameters
                request.executeAsync()
            }
        })
    }


    fun facebookSignup() {
        var lastName = ""
        LoginManager.getInstance().registerCallback(callbackManager, object :
            FacebookCallback<LoginResult> {

            override fun onCancel() {
            }

            override fun onError(exception: FacebookException) {
            }

            override fun onSuccess(loginResult: LoginResult) {
                val request = GraphRequest.newMeRequest(
                    loginResult!!.accessToken
                ) { `object`, response ->
                    try {
                        Log.i("Response", response.toString())
                        val socail_id = response!!.jsonObject!!.getString("id")
                        val profilePicture =
                            URL("https://graph.facebook.com/$socail_id/picture?width=500&height=500")
                        if (response.jsonObject!!.has("email")) {
                            email = response.jsonObject!!.getString("email")
                        }
                        val firstName = response.jsonObject!!.getString("first_name")
                        if (response!!.jsonObject!!.getString("last_name") != null) {
                            lastName = response.jsonObject!!.getString("last_name")
                        } else {
                            lastName = ""
                        }
                        setDataToUI(
                            email,
                            profilePicture.toString(),
                            firstName + " " + lastName,
                            socail_id,
                            "facebook",
                            "android"
                        )
                        email = email
                        imageUrl = profilePicture.toString()
                        fullName = firstName + " " + lastName
                        socialId = socail_id
                        loginType = "facebook"
                        deviceType = "android"
                        isSocialLogin.value = true
                        // preferenceManager?.setUserLoggedin(true)
                    } catch (e: java.lang.Exception) {
                        showToast(e.message, this@HomeActivity)
                    }
                }
                val parameters = Bundle()
                parameters.putString("fields", "id,email,first_name,last_name,gender")
                request.parameters = parameters
                request.executeAsync()
            }
        })
    }

    fun setDataToUI(
        email: String?,
        imageUrl: String,
        fullName: String?,
        socialId: String?,
        loginType: String,
        deviceType: String
    ) {
        imgUser?.visibility = View.GONE
        addLayout?.visibility = View.GONE
        et_semail?.setText(email)
        profilePic?.let { Glide.with(this).load(imageUrl).into(it) }
        et_sname?.setText(fullName)

    }

    fun googleLogin() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        firebaseAuth = FirebaseAuth.getInstance()


        val signInIntent: Intent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, Req_Code)

    }

    fun initSocialLogin() {
        callbackManager = CallbackManager.Factory.create() //facebook
        val config = TwitterConfig.Builder(this)
            .logger(DefaultLogger(Log.DEBUG)) //enable logging when app is in debug mode
            .twitterAuthConfig(
                TwitterAuthConfig(
                    resources.getString(R.string.twitter_api_key),
                    resources.getString(R.string.twitter_api_secret_key)
                )
            ) //pass the created app Consumer KEY and Secret also called API Key and Secret
            .debug(true) //enable debug mode
            .build()
        Twitter.initialize(config)
        client = TwitterAuthClient()

    }

    fun handleGoogleLoginResponse(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account: GoogleSignInAccount? = completedTask.getResult(ApiException::class.java)
            if (account != null) {
                val credential = GoogleAuthProvider.getCredential(account.idToken, null)
                socialLogin(
                    account.email,
                    account.photoUrl.toString(),
                    account.givenName,
                    account.id,
                    "1",
                    "android"
                )
                isSocialLogin.value = true
            }
        } catch (e: ApiException) {
            e.printStackTrace()
        }
    }

    fun handleGoogleSignupResponse(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account: GoogleSignInAccount? = completedTask.getResult(ApiException::class.java)
            if (account != null) {
                val credential = GoogleAuthProvider.getCredential(account.idToken, null)
                setDataToUI(
                    account.email,
                    account.photoUrl.toString(),
                    account.givenName,
                    account.id,
                    "google",
                    "android"
                )
                email = account.email!!
                imageUrl = account.photoUrl.toString()
                fullName = account.givenName!!
                socialId = account.id!!
                loginType = "google"
                deviceType = "android"
                isSocialLogin.value = true
                //  preferenceManager?.setUserLoggedin(true)
            }
        } catch (e: ApiException) {
        }
    }

    fun socialLogin(
        email: String?,
        profilePic: String,
        fullName: String?,
        socialId: String?,
        loginType: String,
        deviceType: String
    ) {
        System.out.println(">>>>>>>>>>>>> " + Common.currentLat)
        showLoading(this)
        var userData = Users().Data()
        RestClient.getApiInterface().socialLogin(
            fullName,
            email,
            Common.currentLat,
            Common.currentLng,
            socialId,
            loginType,
            profilePic,
            "Android"
        )
            .enqueue(object : retrofit2.Callback<SocialLoginModel> {
                override fun onResponse(
                    call: Call<SocialLoginModel>,
                    response: Response<SocialLoginModel>
                ) {
                    hideLoading()
                    if (response.isSuccessful) {
                        if (response.body()?.getStatus() == 200) {
                            Log.d("sd", "d");
                            response.body()?.data?.also {
                                userData.email = it.email
                                userData.id = it.id
                                userData.fullName = it.fullName
                                userData.image = it.image
                                userData.googleId = it.googleId
                                userData.language = it.language
                                userData.lat = it.lat
                                userData.lng = it.lng
                                userData.phone = it.phone
                                userData.token = it.token
                                userData.twitterId = it.pushToken
                                userData.facebookId = it.facebookId
                                userData.googleId = it.googleId
                                userData.profile_verified = it.profile_verified
                                userData.phone_verified = it.phone_verified
                                userData.verification = it.verification
                            }

                            when (loginType) {
                                "0" -> {
                                    GoogleAnalytics.sentDataToGoogleAnalytics(
                                        loginType,
                                        AnalyticsEvent.LogInType.FACEBOOK.name,
                                        AnalyticsEvent.EventType.FACEBOOK_LOGIN.name
                                    )
                                }
                                "1" -> {
                                    GoogleAnalytics.sentDataToGoogleAnalytics(
                                        loginType,
                                        AnalyticsEvent.LogInType.GOOGLE.name,
                                        AnalyticsEvent.EventType.GOOGLE_LOGIN.name
                                    )
                                }
                                else -> {
                                    GoogleAnalytics.sentDataToGoogleAnalytics(
                                        loginType,
                                        AnalyticsEvent.LogInType.TWITTER.name,
                                        AnalyticsEvent.EventType.TWITTER_LOGIN.name
                                    )
                                }
                            }

                            if (userData?.is_activated != 2) {
                                preferenceManager?.userDetails = userData
                                preferenceManager?.setUserLoggedin(true)
                                AppController.getInstance().haveCalled = true
                                startActivity(
                                    Intent(this@HomeActivity, HomeActivity::class.java)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                )
                                finish()
                            } else {
                                showDeactivateMsg()
                            }

                        } else {
                            showToast(response.body()?.message, this@HomeActivity)
                        }
                    }
                }

                override fun onFailure(call: Call<SocialLoginModel>, t: Throwable) {
                    hideLoading()
                }

            })

    }

    private fun showDeactivateMsg() {
        showNormalDialogVerticalButton(this,
            getString(R.string.account_deactivated),
            getString(
                R.string.account_deactivated_desc
            ),
            getString(R.string.ok),
            "",
            BaseActivity.OnOkClicked {
                if (it) {

                }
            }, true
        )
    }


    private fun initViewPager() {
        header = findViewById(R.id.header) as TextView?
        viewPager = findViewById(R.id.viewPager) as CustomViewPager?
        tabLayout = findViewById(R.id.tabLayout) as TabLayout?
        numberLayoutHome = findViewById(R.id.numberLayoutHome) as LinearLayoutCompat?
        vpImages = findViewById(R.id.vpImages) as ViewPager?
        cipTutorial = findViewById(R.id.cipTutorial) as CircularPageIndicator?
        tabLayout?.tabGravity = TabLayout.GRAVITY_FILL
        adapter = TabAdapter(
            this, supportFragmentManager,
            tabLayout?.tabCount!!
        )
        viewPager?.adapter = adapter
        viewPager?.offscreenPageLimit = 0




        if (intent.getBooleanExtra("navigate_to_notification_screen", false)) {
            tabLayout?.getTabAt(3)?.select()
        }

        if (intent.getBooleanExtra("admin_notification", false)) {
            tabLayout?.getTabAt(3)?.select()
            viewPager?.setCurrentItem(3)
        } else {
            tabLayout?.getTabAt(0)?.select()
            viewPager?.setCurrentItem(0)
        }

        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
                Common.currentTab = tab!!.position
                if (tab!!.position != 0) {
                    if (preferenceManager?.isUserLoggedIn == true && preferenceManager?.userDetails?.phone.isNullOrEmpty()) {
                        showAddPhoneDialog()
                        tabLayout?.selectTab(tabLayout?.getTabAt(0))
                    } else if (preferenceManager?.isUserLoggedIn == true) {
                        viewPager?.currentItem = tab.position
                        if (fragment != null && fragment!!.isVisible) {
                            supportFragmentManager.beginTransaction().remove(fragment!!).commit()
                        }
                    } else {
                        showSocialLoginDialog(this@HomeActivity)
                        //  startActivity(Intent(this@HomeActivity, LoginActivity::class.java))
                    }
                } else {
                    viewPager?.currentItem = tab.position
                    if (fragment != null && fragment!!.isVisible) {
                        supportFragmentManager.beginTransaction().remove(fragment!!).commit()
                    }
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                Common.currentTab = tab!!.position
                if (tab.position != 0) {
                    if (preferenceManager?.isUserLoggedIn == true && preferenceManager?.userDetails?.phone.isNullOrEmpty()) {
                        showAddPhoneDialog()
                        tabLayout?.selectTab(tabLayout?.getTabAt(0))
                    } else if (preferenceManager?.isUserLoggedIn == true) {
                        viewPager?.currentItem = tab.position
                        if (fragment != null && fragment!!.isVisible) {
                            supportFragmentManager.beginTransaction().remove(fragment!!).commit()
                        }
                    } else {
                        showSocialLoginDialog(this@HomeActivity)
                        //  startActivity(Intent(this@HomeActivity, LoginActivity::class.java))
                    }
                } else {
                    viewPager?.currentItem = tab.position
                    if (fragment != null && fragment!!.isVisible) {
                        supportFragmentManager.beginTransaction().remove(fragment!!).commit()
                    }
                }
            }

        })

    }

    override fun onBackPressed() {
        if (!onBackButtonClicked) {
            onBackButtonClicked = true
            showToast(getString(R.string.press_again_to_exit), this)
            Handler().postDelayed({ onBackButtonClicked = false }, 2000)
        } else {
            super.onBackPressed()
        }

    }

    fun launchHomeFragment() {
        val activeFragment = adapter?.getCurrentItem(0)
        (activeFragment as HomeFragment).getProductData()
        viewPager?.setCurrentItem(0)
        Common.currentTab = 0
        if (fragment != null && fragment!!.isVisible) {
            supportFragmentManager.beginTransaction().remove(fragment!!).commit()
        }
        tabLayout?.getTabAt(0)?.select();

    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //   val tagName  = getSupportFragmentManager().getFragments().get(4).tag
        //      val activeFragment: ProfileFrag? = supportFragmentManager.findFragmentByTag(tagName) as ProfileFrag?
//        activeFragment?.onActivityResult(requestCode,resultCode,data)

        callbackManager.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Req_Code) { //Google Signin
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            if (task.isSuccessful) {
                AppController.getInstance().haveCalled = true
                if (loginCode) {
                    handleGoogleLoginResponse(task)
                } else {
                    handleGoogleSignupResponse(task)
                }

            }
        } else if (requestCode == 140) {
            client?.onActivityResult(requestCode, resultCode, data)
        }

        if (requestCode == Picker.PICK_FILE && resultCode == BaseActivity.RESULT_OK) {
            filePicker!!.submit(data)
        } else if (requestCode == REQUEST_CAMERA && data != null && data.extras != null) {
            try {
                val bundle = data.extras
                val bitmap = bundle!!["data"] as Bitmap?
                val tempUri: Uri = getImageUri(this!!, bitmap!!)!!
                com.theartofdev.edmodo.cropper.CropImage.activity(tempUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this);

            } catch (e: Exception) {
                e.message
            }
        } else if (requestCode == PICK_IMAGE_REQUEST_GALLERY && data != null) {
            val tempUri = data.data
            com.theartofdev.edmodo.cropper.CropImage.activity(tempUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);

            //  val tempUri = data.data
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && data != null) {
            val result: com.theartofdev.edmodo.cropper.CropImage.ActivityResult =
                com.theartofdev.edmodo.cropper.CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK) {
                val resultUri: Uri = result.getUri()
                val bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri)
                val file = File(resultUri.path)


                //    val file = File(getPath(resultUri))
                if (file != null)
                    mArrayFile?.add(file!!)

                val mFile = RequestBody.create("image/*".toMediaTypeOrNull(), file)
                fileToUpload = MultipartBody.Part.createFormData("image", file?.name, mFile)

                try {
                    imgUser?.visibility = View.GONE
                    addLayout?.visibility = View.GONE
                    profilePic?.setImageBitmap(bitmap)
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            } else if (resultCode === com.theartofdev.edmodo.cropper.CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error: java.lang.Exception = result.getError()
            }
        }

    }

    fun getImageUri(inContext: Context, inImage: Bitmap): Uri? {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path =
            MediaStore.Images.Media.insertImage(inContext.contentResolver, inImage, "Title", null)
        return Uri.parse(path)
    }

    private fun validateInputs(): Boolean {
        name = et_sname?.text.toString()
        email = et_semail?.text.toString()
        phone = et_sphone?.text.toString()
        password = et_spassword?.text.toString()
        confirmPassword = et_sconfirm_password?.text.toString()

        if (TextUtils.isEmpty(name)) {
            et_sname?.error = getString(R.string.name_required)
            et_sname?.requestFocus()
            return false
        } else if (TextUtils.isEmpty(email)) {
            et_semail?.error = getString(R.string.email_required)
            et_semail?.requestFocus()
            return false
        } else if (!email.contains("@")) {
            et_semail?.error = getString(R.string.invalid_email)
            et_semail?.requestFocus()
            return false
        } else if (TextUtils.isEmpty(phone)) {
            et_sphone?.error = getString(R.string.phone_required)
            et_sphone?.requestFocus()
            return false
        } else if (TextUtils.isEmpty(password)) {
            et_spassword?.error = getString(R.string.password_required)
            et_spassword?.requestFocus()
            return false
        } else if (TextUtils.isEmpty(confirmPassword)) {
            et_sconfirm_password?.error = getString(R.string.confirm_password_required)
            et_sconfirm_password?.requestFocus()
            return false
        } else if (!password.equals(confirmPassword)) {
            et_sconfirm_password?.error = getString(R.string.password_not_matched)
            et_sconfirm_password?.requestFocus()
            return false
        } else if (!checkbox?.isChecked!!) {
            checkbox?.error = getString(R.string.terms_of_conditions_required)
            checkbox?.requestFocus()
            return false
        } else {
            return true
        }
    }

    private fun signUp() {
        var builder = MultipartParams.Builder();
        builder.add("full_name", name)
        builder.add("email", email)
        builder.add("phone", phone)
        builder.add("lat", Common.productLocationLat)
        builder.add("lng", Common.productLocationLng)
        builder.add("password", password)
        builder.add("singup_from", "Android")

        if (mArrayFile != null && mArrayFile?.size ?: 0 > 0) {
            builder.addFile("image", mArrayFile?.get(0))
        } else if (imageUrl.isNullOrEmpty().not()) {
            builder.add("profile_pic_url", imageUrl)
        }
        val userData = Users().Data()

        showLoading(this)
        RestClient.getApiInterface().signUp(builder.build().map)
            .enqueue(object : retrofit2.Callback<SignUpModel> {
                override fun onResponse(call: Call<SignUpModel>, response: Response<SignUpModel>) {
                    hideLoading()
                    if (response.isSuccessful) {
                        CameraConstant.mArrayFileConstant = java.util.ArrayList()
                        if (response.body()?.getStatus() != null && response.body()
                                ?.getStatus() == 200
                        ) {
                            showToast(response?.body()?.getMessage().toString(), applicationContext)
                            val signupData: SignUpModel.Data = response?.body()?.getData()!!
                            userData.email = signupData.email
                            userData.id = signupData.id
                            userData.fullName = signupData.fullName
                            userData.image = signupData.image
                            userData.googleId = signupData.googleId
                            userData.language = signupData.language
                            userData.lat = signupData.lat
                            userData.lng = signupData.lng
                            userData.phone = signupData.phone
                            userData.token = signupData.token
                            userData.twitterId = signupData.pushToken
                            userData.facebookId = signupData.facebookId
                            userData.googleId = signupData.googleId
                            preferenceManager?.userDetails = userData
                            preferenceManager?.setUserLoggedin(true)
                            AppController.getInstance().haveCalled = true
                            startActivity(
                                Intent(this@HomeActivity, HomeActivity::class.java)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            )
                        } else {
                            showToast(response?.body()?.getMessage().toString(), applicationContext)
                        }
                    } else {
                        showToast(response?.body()?.getMessage().toString(), applicationContext)
                    }
                }

                override fun onFailure(call: Call<SignUpModel>, t: Throwable) {
                    CameraConstant.mArrayFileConstant = java.util.ArrayList()
                    hideLoading()
                    showToast(getString(R.string.something_went_wring), applicationContext)

                }

            })
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == LOCATION_PERMISSION) {
            val tagName = getSupportFragmentManager().getFragments().get(0).tag
            val activeFragment: HomeFragment? =
                supportFragmentManager.findFragmentByTag(tagName) as HomeFragment?
            activeFragment?.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun onError(p0: String?) {

    }

    override fun onFilesChosen(list: MutableList<ChosenFile>?) {
        if (list?.size ?: 0 > 0) {
            for (i in 0 until list?.size!!) {
                val file1 = File(list?.get(i)?.getOriginalPath())
                val file = getCompressed(this, file1.path)
                val tempUri = Uri.fromFile(file)
                com.theartofdev.edmodo.cropper.CropImage.activity(tempUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this);
                break
            }
        }
    }


    override fun onOtpVerified(otp: String) {
        signUp()
    }
//
//    override fun OnVerifiedClicked(otp: String) {
//        verifYOtp(otp)
//    }
//
//    override fun OnOtpSendClicked(number: String) {
//        sendOtpToEmailPhone(number.trim(), 1)
//    }
//    private var verifyingNumberOrEmail  = ""
//    private fun sendOtpToEmailPhone(text: String, otpSendType: Int) {
//        //0-Email 1-Phone
//        showLoading(this)
//        verifyingNumberOrEmail = text
//        var emailID = text
//        var phoneNum = text
//        if (otpSendType == 1) {
//            emailID = ""
//        } else {
//            phoneNum = ""
//        }
//
//        RestClient.getApiInterface().sendOTP(emailID, phoneNum).enqueue(object : Callback<ApiStatusModel> {
//            override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
//                hideLoading()
//            }
//
//            override fun onResponse(
//                call: Call<ApiStatusModel>,
//                response: Response<ApiStatusModel>
//            ) {
//                hideLoading()
//                if (response.body()?.getStatus() == 200) {
//                    if (otpSendType == 0)
//                        setUiForOtp(otpSendType)
//                }
//            }
//        })
//
//        /*  if (otpSendType == 0) {
//
//          }else{
//              baseActivity.hideLoading()
//              setUiForOtp(otpSendType)
//          }*/
//    }
//
//    private fun setUiForOtp(otpSendType: Int) {
//        //0-Email 1-Phone
//        if (otpSendType == 0){
//            isEmailVerifying = true
//        }
//        header_one?.setText(getString(R.string.enter_otp))
//        enterTextField?.setText("")
//        enterTextField?.setHint(getString(R.string.enter_otp))
//        enterTextField?.inputType = InputType.TYPE_CLASS_NUMBER
//        cancelBtn?.setText(getString(R.string.verify))
//    }
//
//    private fun verifYOtp(otp: String) {
//        showLoading(this)
//        var email = ""
//        var phone = ""
//        if (verifyingNumberOrEmail.isDigitsOnly()) {
//            phone = verifyingNumberOrEmail
//        } else {
//            email = verifyingNumberOrEmail
//        }
//        if (isEmailVerifying) {
//            RestClient.getApiInterface().verifyOtp(phone,
//                email,
//                otp
//            ).enqueue(object : Callback<ApiStatusModel> {
//                override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
//                    hideLoading()
//                }
//
//                override fun onResponse(
//                    call: Call<ApiStatusModel>,
//                    response: Response<ApiStatusModel>
//                ) {
//                    hideLoading()
//                    if (response.body()?.getStatus() == 200) {
//                        showLoading(this)
//                        if (verifyingNumberOrEmail.isDigitsOnly()) {
//                            update_verificationForEmailPhone("phone")
//                        } else {
//                            update_verificationForEmailPhone("email")
//                        }
//
//                    } else {
//                        showToast(
//                            response.body()?.getMessage() ?: "",
//                            this@HomeActivity
//                        )
//                    }
//
//                }
//            })
//        }else{
//            hideLoading()
//            update_verificationForEmailPhone("phone")
//        }
//
//    }
}