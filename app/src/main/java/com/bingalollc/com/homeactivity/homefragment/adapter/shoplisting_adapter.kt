package com.bingalo.com.homeactivity.homefragment.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bingalo.com.homeactivity.model.data
import com.bingalollc.com.R
import com.bumptech.glide.Glide

class shoplisting_adapter(val context: Context, var shopicon: List<data>):
    RecyclerView.Adapter<shoplisting_adapter.MyViewHolder>() {

    var onItemClick : ((data) -> Unit)? = null



    @SuppressLint("SuspiciousIndentation")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
    val view = LayoutInflater.from(parent.context).inflate(R.layout.home_recyler_shop,parent,false)
        return MyViewHolder(view)

    }

    override fun getItemCount(): Int {
        return shopicon?.size!!
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val shop_a = shopicon[position]
//        val storecate = storecategory.get(position)

//        Glide.with(context).load(shop?.getStore_image()).placeholder(R.drawable.download).into(holder.Icon)
//        holder.Text.text = shop_a.store_name
        Glide.with(context).load(shop_a.store_image).placeholder(R.drawable.product_placeholder).
        error(R.drawable.product_placeholder).into(holder.Icon)

        holder.itemView.setOnClickListener {
            onItemClick?.invoke(shop_a)

        }


    }

    fun updateData(newShopicon: List<data>) {
        shopicon = newShopicon
        notifyDataSetChanged()

    }

    class MyViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
        val Icon : ImageView = itemView.findViewById(R.id.icon)
//        val Text : TextView = itemView.findViewById(R.id.text)


    }

}