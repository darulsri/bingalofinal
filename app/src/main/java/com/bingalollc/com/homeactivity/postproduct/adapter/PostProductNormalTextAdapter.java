package com.bingalollc.com.homeactivity.postproduct.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bingalollc.com.R;

import java.util.ArrayList;

public class PostProductNormalTextAdapter extends RecyclerView.Adapter<PostProductNormalTextAdapter.Viewholder> {
    private Context context;
    private ArrayList<String> headerString;
    private ArrayList<String> headerTwoString;
    private OnClickedCalled onClickedCalled;

    public PostProductNormalTextAdapter(ArrayList<String> headerString, ArrayList<String> headerTwoString, OnClickedCalled onClickedCalled) {
        this.headerString = headerString;
        this.headerTwoString = headerTwoString;
        this.onClickedCalled = onClickedCalled;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.list_view_items, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        if (headerString.get(position).contains(":"))
            holder.header_text.setText(headerString.get(position).substring(0,headerString.get(position).indexOf(":")));
        else
            holder.header_text.setText(headerString.get(position));

        if (headerTwoString!=null) {
            holder.header_two_text.setText(headerTwoString.get(position));
        }else {
            holder.header_two_text.setVisibility(View.GONE);
        }

    }

    public void updateText(ArrayList<String> spectString){
        this.headerString = spectString;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return headerString.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        private TextView header_text,header_two_text;
        private RelativeLayout layoutItems;
        public Viewholder(@NonNull View itemView) {
            super(itemView);
            header_text = itemView.findViewById(R.id.header_text);
            header_two_text = itemView.findViewById(R.id.header_two_text);
            layoutItems = itemView.findViewById(R.id.layoutItems);
            layoutItems.setOnClickListener(view -> {
                if (!TextUtils.isEmpty(header_two_text.getText())) {
                    //+"#"+header_two_text.getText().toString()
                    onClickedCalled.onItemClicked(header_text.getText().toString(), getAdapterPosition(), header_text.getText().toString()+"-"+header_two_text.getText().toString());
                }else
                    onClickedCalled.onItemClicked(header_text.getText().toString(),getAdapterPosition(), "");
            });
        }
    }

    public interface OnClickedCalled{
        void onItemClicked(String text, int pos, String completeStringForServer);
    }
}
