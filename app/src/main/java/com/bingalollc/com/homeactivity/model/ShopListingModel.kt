package com.bingalo.com.homeactivity.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ShopListingModel (



//    @SerializedName("message")
//    @Expose
    val message: String,

//    @SerializedName("status")
//    @Expose
     val status: Int,

//    @SerializedName("data")
//    @Expose
    val data: List<data>

)

data class data (
//    @SerializedName("id")
//    @Expose
    val id: Int,

//    @SerializedName("store_name")
//    @Expose
    val store_name: String,

//    @SerializedName("store_detail")
//    @Expose
     val store_detail: String,

//    @SerializedName("store_image")
//    @Expose
    val store_image: String,

//    @SerializedName("position")
//    @Expose
    val position: String,

//    @SerializedName("store_category")
//    @Expose
//    val storecategory: store_category,

//    @SerializedName("store_rating")
//    @Expose
    val store_rating: Float,

//    @SerializedName("is_verified")
//    @Expose
    val is_verified: String,

//    @SerializedName("store_location")
//    @Expose
    val store_location: String,

//    @SerializedName("store_lat")
//    @Expose
    val store_lat: String,

//    @SerializedName("store_lon")
//    @Expose
    val store_lon   : String,

//    @SerializedName("store_ribbon")
//    @Expose
    val store_ribbon: String,

//    @SerializedName("store_delivery_fee")
//    @Expose
    val store_delivery_fee: String,

//    @SerializedName("created_at")
//    @Expose
    val created_at: String,

//    @SerializedName("updated_at")
//    @Expose
    val updated_at: String,

//    @SerializedName("featured_position")
//    @Expose
    val featured_position: String,

//    @SerializedName("minimum_order_amount")
//    @Expose
    val minimum_order_amount: String

): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readFloat()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(store_name)
        parcel.writeString(store_detail)
        parcel.writeString(store_image)
        parcel.writeString(position)
        parcel.writeFloat(store_rating)
        parcel.writeString(is_verified)
        parcel.writeString(store_location)
        parcel.writeString(store_lat)
        parcel.writeString(store_lon)
        parcel.writeString(store_ribbon)
        parcel.writeString(store_delivery_fee)
        parcel.writeString(created_at)
        parcel.writeString(updated_at)
        parcel.writeString(featured_position)
        parcel.writeString(minimum_order_amount)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<data> {
        override fun createFromParcel(parcel: Parcel): data {
            return data(parcel)
        }

        override fun newArray(size: Int): Array<data?> {
            return arrayOfNulls(size)
        }
    }
}


class store_category (
//    @SerializedName("id")
//    @Expose
    val id: Int,

//    @SerializedName("category_name")
//    @Expose
    val category_name: String,

//    @SerializedName("category_position")
//    @Expose
    val category_position: String,

//    @SerializedName("created_at")
//    @Expose
    val created_at: String,

//    @SerializedName("updated_at")
//    @Expose
    val updated_at: String
)