package com.bingalollc.com.bump_product;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bingalollc.com.R;
import com.bingalollc.com.chat.adapter.ChatAdapter;

import java.util.ArrayList;

public class OfferAdapter extends RecyclerView.Adapter<OfferAdapter.Viewholder> {
    private Context context;
    private ArrayList<String> daysArray;
    private ArrayList<String> priceAray;
    private ArrayList<String> offerText;
    private int selectedItem = 1;
    private OnItemSelected onItemSelected;

    public OfferAdapter(ArrayList<String> days, ArrayList<String> price, ArrayList<String> offerText, OnItemSelected onItemSelected) {
        this.daysArray = days;
        this.priceAray = price;
        this.offerText = offerText;
        this.onItemSelected = onItemSelected;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context=parent.getContext();
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.offer_layout_item,parent,false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        if (!daysArray.get(position).equals("")) {
            holder.completeLayout.setVisibility(View.VISIBLE);
            holder.extraView.setVisibility(View.GONE);
            holder.completeLayout.setVisibility(View.VISIBLE);
            holder.days.setText(daysArray.get(position));
            holder.price.setText(priceAray.get(position));
            holder.savetext.setText(offerText.get(position));
            System.out.println(">>>>>>SEelectedPos "+selectedItem);
            if (selectedItem == position) {
                holder.completeLayout.setBackground(context.getResources().getDrawable(R.drawable.curved_10dp_thickborder_blue));
            } else {
                holder.completeLayout.setBackground(context.getResources().getDrawable(R.drawable.curve10_border_thingrey));
            }
        }else {
            holder.completeLayout.setVisibility(View.GONE);
            holder.extraView.setVisibility(View.VISIBLE);
        }
        if (position == 0) {
            RelativeLayout.LayoutParams params =  new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(300,0,5,0);
            holder.layoutCell.setLayoutParams(params);
        } else if (position == 2) {
            RelativeLayout.LayoutParams params =  new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(5,0,300,0);
            holder.layoutCell.setLayoutParams(params);
        } else {
            RelativeLayout.LayoutParams params =  new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(5,0,5,0);
            holder.layoutCell.setLayoutParams(params);
        }
    }

    public void setCenterPosition(int position){
        selectedItem = position;
       /* if (selectedItem==0){
            selectedItem=1;
        } else if (selectedItem==4){
            selectedItem=3;
        }*/
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return daysArray.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        private final TextView days, price, savetext;
        private final RelativeLayout completeLayout,layoutCell;
        private final View extraView;

        public Viewholder(@NonNull View itemView) {
            super(itemView);
            days = itemView.findViewById(R.id.days);
            layoutCell = itemView.findViewById(R.id.layoutCell);
            price = itemView.findViewById(R.id.price);
            savetext = itemView.findViewById(R.id.savetext);
            completeLayout = itemView.findViewById(R.id.completeLayout);
            extraView = itemView.findViewById(R.id.extraView);
            completeLayout.setOnClickListener(view -> {
                if (!daysArray.get(getAdapterPosition()).equals("")){
                    selectedItem = getAdapterPosition();
                    onItemSelected.onItemSelected(priceAray.get(getAdapterPosition()));
                    notifyDataSetChanged();
                }
            });
        }
    }

    interface OnItemSelected{
        void onItemSelected(String price);
    }
}
