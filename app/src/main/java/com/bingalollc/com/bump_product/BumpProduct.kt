package com.bingalollc.com.bump_product

import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.os.Handler
import android.util.TypedValue
import android.view.View
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.databinding.ActivityBumpProductBinding
import com.bingalollc.com.model.ApiStatusModel
import com.bingalollc.com.model.ClientIdModel
import com.bingalollc.com.network.RestClient
import com.bingalollc.com.preference.Common
import com.bingalollc.com.utils.CenterZoomLayoutManagerHome
import com.bumptech.glide.Glide
import com.google.android.gms.wallet.PaymentData
import com.google.android.gms.wallet.TransactionInfo
import com.google.android.gms.wallet.WalletConstants
//import kotlinx.android.synthetic.main.activity_bump_product.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*


class BumpProduct : BaseActivity(), OfferAdapter.OnItemSelected{
   // DropInListener {
    private lateinit var binding:ActivityBumpProductBinding
    private var PUBLISHABLE_KEY = "pk_test_51J8PosLTp72IeRov83jjcufswcn7C2O07lVhW20pohkHBbUkGFxDJPZ16gBfjckRTMQQuTrHAtD68MJoxTGMA8oU00jvQJBHgv"
    private var AUTHORIZATION_KEY = "sandbox_8h9jmwnr_8qvhbp66n65srzv5"
    val daysArray = ArrayList<String>()
    val priceArray = ArrayList<String>()
    val offerTextArray = ArrayList<String>()
    private var price: String?= "99"
    private val GOOGLE_PAYMENT_REQUEST_CODE = 1004
//    var dropInRequest = DropInRequest()
//    var dropInClient: DropInClient? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_bump_product)

//        try {
//            dropInRequest = DropInRequest()
//            dropInClient = DropInClient(this,dropInRequest, AUTHORIZATION_KEY)
//            dropInClient?.setListener(this)
//            binding.nxtBtn.isEnabled = true
//            binding.nxtBtn.alpha = 1f
//            // mBraintreeFragment is ready to use!
//        } catch (e: InvalidArgumentException) {
//            // There was an issue with your authorization string.
//        }
        initHeader()
        initData()

    }

    private fun initData() {
        var imgUrl = ""
        if (Common.selectedProductDetails.images!=null && Common.selectedProductDetails.images.size>0) {
            imgUrl = Common.selectedProductDetails.images.get(0).imageUrl
            }
        Glide.with(this).load(imgUrl)
            .error(R.drawable.product_placeholder).placeholder(R.drawable.product_placeholder)
            .into(binding.prodImg)
        binding.prodName.setText(Common.selectedProductDetails.productTitle)
        binding.prodPrice.setText(Common.selectedProductDetails.productPrice)
        initRecyclerView()
        binding.promotionGuideLines.setOnClickListener {
            startActivity(Intent(this, BumpGuideLinesActivity::class.java))
        }
        binding.nxtBtn.setOnClickListener {
            makePayment()
        }

    }

    private fun makePayment() {
      //  dropInClient?.launchDropInForResult(this,GOOGLE_PAYMENT_REQUEST_CODE)

    }

    private fun setBumpDataToServer() {
        showLoading(this)
        val c: Date = Calendar.getInstance().getTime()
        val df = SimpleDateFormat("MMM dd, yyyy hh:mm a", Locale.getDefault())
        val formattedDate: String = df.format(c)

        RestClient.getApiInterface().addUpdatedProductBump(
            formattedDate, "1", preferenceManager?.userDetails?.id,
            Common.selectedProductDetails.productPrice, Common.selectedProductDetails.id
        ).enqueue(object : Callback<ApiStatusModel> {
            override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
                hideLoading()
                showErrorDialog()
            }

            override fun onResponse(
                call: Call<ApiStatusModel>,
                response: Response<ApiStatusModel>
            ) {
                hideLoading()
                if (response.body()?.getStatus() == 200) {
                   showNormalDialogVerticalButton(this@BumpProduct,
                        getString(R.string.success),
                        getString(
                            R.string.your_product_has_been_bump
                        ),
                       "",
                        getString(R.string.ok),
                        BaseActivity.OnOkClicked {
                            if (it) {
                                val intent =  Intent();
                                intent.putExtra("paymentStatus", "success")
                                setResult(RESULT_OK, intent);
                                finish()
                            }
                        }, false)

                } else {
                    showErrorDialog()
                }

            }
        })
    }

    private fun showErrorDialog() {
        showNormalDialogVerticalButton(this@BumpProduct,
            getString(R.string.failure),
            getString(
                R.string.bump_product_error
            ),
            "",
            getString(R.string.ok),
            BaseActivity.OnOkClicked {
                if (it) {
                    finish()
                }
            }, false)
    }

    private fun initRecyclerView(){
        setDataToArray()
        val offerAdapter = OfferAdapter(daysArray, priceArray, offerTextArray, this)
        val layoutManager = CenterZoomLayoutManagerHome(this, RecyclerView.HORIZONTAL, false)
        binding.rvOfferList.setLayoutManager(layoutManager)
        binding.rvOfferList.setAdapter(offerAdapter)
        binding.rvOfferList.visibility = View.INVISIBLE
 /*       val snapHelper: SnapHelper = PagerSnapHelper()
        snapHelper.attachToRecyclerView(binding.rvOfferList)*/
       Handler().postDelayed({
            binding.rvOfferList.smoothScrollToPosition(2)
            Handler().postDelayed({
               // binding.rvOfferList.smoothScrollToPosition(0)
               // binding.rvOfferList.scrollBy(10,0)
                binding.rvOfferList.visibility = View.VISIBLE
                val view: View = binding.rvOfferList.getChildAt(0)
                val displayWidth: Int = Resources.getSystem().getDisplayMetrics().widthPixels
                val scrollX: Int = view!!.left - displayWidth / 2 + view!!.width / 2
                binding.rvOfferList.smoothScrollBy(scrollX, 0)
                binding.rvOfferList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                        super.onScrollStateChanged(recyclerView, newState)
                        synchronized(this) {
                            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                                val positionIndexs =
                                    layoutManager.findLastCompletelyVisibleItemPosition();
                                offerAdapter.setCenterPosition(positionIndexs)
                            }
                        }
                    }

                    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                        super.onScrolled(recyclerView, dx, dy)
                    }
                })

            }, 500)
        }, 200)
    }
    fun dpToPx(dp: Int): Int {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            dp.toFloat(),
            Resources.getSystem().displayMetrics
        )
            .toInt()
    }

    private fun setDataToArray() {
        //daysArray.add("")
        daysArray.add(getString(R.string.three_days))
        daysArray.add(getString(R.string.seven_days))
        daysArray.add(getString(R.string.fifteen_days))
      //  daysArray.add("")
        //priceArray.add("")
        priceArray.add(getString(R.string.amount_one))
        priceArray.add(getString(R.string.amount_two))
        priceArray.add(getString(R.string.amount_three))
       // priceArray.add("")
        //offerTextArray.add("")
        offerTextArray.add("")
        offerTextArray.add(getString(R.string.save_text_one))
        offerTextArray.add(getString(R.string.save_text_two))
       // offerTextArray.add("")
    }

    private fun initHeader() {
        binding.appBar.header.setText(getString(R.string.bump_your_item).replace("!", ""))
        binding.appBar.backIcon.setOnClickListener { finish() }
        if (preferenceManager.isEnglish == 2) {
            binding.featuredImg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.img_featured_he))
        }

    }

    override fun onItemSelected(price: String?) {
        this.price = price?.replace("₪ ", "")?.trim()
    }


//    override fun onDropInSuccess(dropInResult: DropInResult) {
//        RestClient.getApiInterface().makePaymentApi(dropInResult.paymentMethodNonce?.string,price, "Product Purchased" ).enqueue(object : Callback<ApiStatusModel> {
//            override fun onResponse(
//                call: Call<ApiStatusModel>,
//                response: Response<ApiStatusModel>
//            ) {
//                if (response.body()?.getStatus() == 200) {
//                    setBumpDataToServer()
//                }
//
//            }
//
//            override fun onFailure(call: Call<ApiStatusModel>, t: Throwable) {
//                showToast("Please try again", this@BumpProduct)
//            }
//
//        })
//    }
//
//    override fun onDropInFailure(error: Exception) {
//        showToast("Failure: "+error.message, this)
//    }

}