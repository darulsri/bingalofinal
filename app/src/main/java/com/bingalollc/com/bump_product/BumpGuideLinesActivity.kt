package com.bingalollc.com.bump_product

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.bingalollc.com.R
import com.bingalollc.com.databinding.ActivityBumpGuideLinesBinding

class BumpGuideLinesActivity : AppCompatActivity() {
    private lateinit var binding: ActivityBumpGuideLinesBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_bump_guide_lines)
        initHeader()
    }


    private fun initHeader() {
        binding.appBar.header.setText(getString(R.string.how_does_promoting_work))
        binding.appBar.backIcon.setOnClickListener { finish() }

    }
}