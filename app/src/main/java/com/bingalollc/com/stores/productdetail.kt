package com.bingalollc.com.stores

import android.annotation.SuppressLint
import android.graphics.drawable.Icon
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.bingalollc.com.R
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialogFragment


class productdetail : BottomSheetDialogFragment() {

    var producticon :ImageView ?=null
    var productTitle : TextView ?=null
    var productrate :TextView ?=null
    var productde : TextView ?=null
    var moredelivery :TextView ?=null
    var learnmore :TextView ?=null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    @SuppressLint("MissingInflatedId")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_productdetail, container, false)
    initview(view)

        val bundle = arguments
        var productId =  bundle?.getString("productId")
        var productName = bundle?.getString("productName")
        var productRate =bundle?.getString("productRate")
        var productImage = bundle?.getString("productImage")
        var productDescr=bundle?.getString("productDescription")

        Log.e("product_name_product", productImage.toString())

        productTitle?.text = productName
        productrate?.text = productRate
        productde?.text = productDescr
        producticon?.let {
            Glide.with(this).load(productImage).placeholder(R.drawable.download).error(R.drawable.download).into(
                producticon!!
            ) }

    return view
    }

    private fun initview(view: View){
        producticon = view.findViewById(R.id.producticon)
        productTitle = view.findViewById(R.id.nameofproduct)
        productrate = view.findViewById(R.id.productrate)
        productde = view.findViewById(R.id.productdetails)
        moredelivery = view.findViewById(R.id.moreofdelivery)
        learnmore = view.findViewById(R.id.learnmore)
    }

}