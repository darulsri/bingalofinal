package com.bingalollc.com.stores

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bingalollc.com.R
import com.bingalollc.com.homeactivity.HomeActivity
import com.bingalollc.com.homeactivity.homefragment.HomeFragment
import com.bingalollc.com.network.RestClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class storepage : AppCompatActivity() {

    private var storecategory : TextView?= null
    private var storelistpage : RecyclerView?=null

    private var linearLayoutManager : LinearLayoutManager?= null
    private var storeadapter :storelistadapter ?= null
    private  var shopicon :ArrayList<stores> ?=null
    lateinit var back : ImageView

    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_storepage)
        storelistpage = findViewById(R.id.storelistpage)
        storecategory = findViewById(R.id.storecategory)
        back = findViewById(R.id.back)


        fetchStores()

        storelistpage?.layoutManager = LinearLayoutManager(baseContext,LinearLayoutManager.HORIZONTAL,false)
        shopicon = ArrayList()
        storeadapter = baseContext?.let { storelistadapter(it, shopicon as ArrayList<stores>) }
        storelistpage?.adapter = storeadapter

        back.setOnClickListener {
            intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
        }


    }

    private fun fetchStores(){
        RestClient.getApiInterface().getfetchAllStoresByCategory().enqueue(object : Callback<storecategorymodel> {
            override fun onResponse(
                call: Call<storecategorymodel>,
                response: Response<storecategorymodel>
            ) {
                if (response.isSuccessful){
                    val storecategorymodel = response.body()
                    if (storecategorymodel!=null){
                        val stores =storecategorymodel?.data
                        val store = stores?.firstOrNull()?.stores
                        store?.let { storeadapter?.updateData(it) }
                    }

                    Log.e("Success", storecategorymodel?.message.toString()+
                            storecategorymodel?.data?.firstOrNull()?.stores.toString())
                }

            }

            override fun onFailure(call: Call<storecategorymodel>, t: Throwable) {

                Log.e("Error", t.message.toString())

            }

        })
    }


}