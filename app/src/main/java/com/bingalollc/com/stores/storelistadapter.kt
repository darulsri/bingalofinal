package com.bingalollc.com.stores

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bingalollc.com.R
import com.bumptech.glide.Glide

class storelistadapter(val context: Context, var shopicon: List<stores>):
    RecyclerView.Adapter<storelistadapter.MyViewHolder>() {




    @SuppressLint("SuspiciousIndentation")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.storelist,parent,false)
        return MyViewHolder(view)

    }

    override fun getItemCount(): Int {
        Log.e("Length","count   : "+shopicon.size)
        return shopicon.size!!

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val shop_a = shopicon[position]
//        val storecate = storecategory.get(position)

//        Glide.with(context).load(shop?.getStore_image()).placeholder(R.drawable.download).into(holder.Icon)
        holder.Text.text = shop_a.store_name
        Glide.with(context).load(shop_a.store_image).placeholder(R.drawable.download).
        error(R.drawable.download).into(holder.Icon)

    }

    fun updateData(newShopicon: List<stores>) {
        shopicon = newShopicon
        notifyDataSetChanged()

    }

    class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val Icon : ImageView = itemView.findViewById(R.id.icon)
        val Text : TextView = itemView.findViewById(R.id.text)


    }


}