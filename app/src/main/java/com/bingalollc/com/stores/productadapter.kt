package com.bingalollc.com.stores

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bingalollc.com.R
import com.bumptech.glide.Glide


class productadapter(var context: Context, var productnew: List<Product>,private val onItemClickListener: onItemClickLis):
    RecyclerView.Adapter<productadapter.MyViewHolder>() {

    var onItemClick : ((Product) -> Unit)? = null



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.product_list,parent,false)
        return MyViewHolder(view,onItemClickListener)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val productlist = productnew[position]

        holder.prices.text = productlist.product_price
        holder.product_name.text = productlist.product_name
        Glide.with(context).load(productlist.product_thumb_image).placeholder(R.drawable.download).
        error(R.drawable.download).into(holder.product_image)

        holder.itemView.setOnClickListener {
            onItemClick?.invoke(productlist)
        }

        holder.addcart.setOnClickListener {

            onItemClickListener.onClick(position,productlist)

        }

    }

    override fun getItemCount(): Int {
            return productnew.size
    }


    fun updateData(newProduct: List<Product>?) {
        productnew = newProduct!!
        notifyDataSetChanged()
    }

    class MyViewHolder (itemView: View,onItemClickListener: onItemClickLis) : RecyclerView.ViewHolder(itemView) {

        val product_image : ImageView = itemView.findViewById(R.id.product_image)
        val prices : TextView = itemView.findViewById(R.id.prices)
        val product_name : TextView = itemView.findViewById(R.id.product_name)
        val addcart : ImageView = itemView.findViewById(R.id.addcart)

    }

    interface onItemClickLis{
        fun onClick(position: Int, product: Product)
    }
}