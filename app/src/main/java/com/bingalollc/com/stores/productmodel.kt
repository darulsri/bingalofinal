package com.bingalollc.com.stores

import android.os.Parcel
import android.os.Parcelable
import com.bingalo.com.homeactivity.model.data


data class productmodel(
    val message: String,
    val status: Int,
    val data: List<Product>,

    )

data class Product(
    val id: String,
    val store_id: String,
    val product_name: String,
    val product_price: String,
    val product_detail: String,
    val product_thumb_image: String,
    val created_at: String,
    val updated_at: String,
    val position: String,
) :Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(store_id)
        parcel.writeString(product_name)
        parcel.writeString(product_price)
        parcel.writeString(product_detail)
        parcel.writeString(product_thumb_image)
        parcel.writeString(created_at)
        parcel.writeString(updated_at)
        parcel.writeString(position)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Product> {
        override fun createFromParcel(parcel: Parcel): Product {
            return Product(parcel)
        }

        override fun newArray(size: Int): Array<Product?> {
            return arrayOfNulls(size)
        }
    }
}

data class Store(
    val id: String,
    val store_name: String,
    val store_detail: String,
    val store_image: String,
    val position: String,
    val storeCategory: StoreCategory,
    val store_rating: String,
    val isVerified: String,
    val storeLocation: String,
    val storeLat: String,
    val storeLon: String,
    val storeRibbon: String,
    val storeDeliveryFee: String,
    val createdAt: String,
    val updatedAt: String,
    val featuredPosition: String,
    val minimumOrderAmount: String
)

data class StoreCategory(
    val id: String,
    val categoryName: String,
    val categoryPosition: String,
    val createdAt: String,
    val updatedAt: String
)




