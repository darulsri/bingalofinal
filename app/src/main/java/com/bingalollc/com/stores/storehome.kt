package com.bingalollc.com.stores

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RatingBar
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bingalo.com.homeactivity.model.data
import com.bingalollc.com.R
import com.bingalollc.com.base.BaseActivity
import com.bingalollc.com.cart.Product_1
import com.bingalollc.com.cart.additem
import com.bingalollc.com.cart.confirmproduct
import com.bingalollc.com.cart.data_1
import com.bingalollc.com.cart.deliveryadapter
import com.bingalollc.com.cart.storeDeliverymodel
import com.bingalollc.com.homeactivity.HomeActivity
import com.bingalollc.com.network.RestClient
import com.bingalollc.com.preference.PreferenceManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.ImageHeaderParser
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class storehome : BaseActivity(),productadapter.onItemClickLis {

    var productAdapter:productadapter? = null
    var linearLayoutManager: GridLayoutManager? = null
    var preferenceManager: PreferenceManager? =null
    var product: RecyclerView? =null
    var productnew:List<Product>?=null
    var storename: TextView?=null
    var storedetails: TextView?=null
    var storerating: RatingBar?=null
    var storeicon : ImageView?=null
    var token : String = ""
    var id : Int ?=null
    var store_id:String=""
    var desc :String=""
    var ETA :String=""
    var rate :Int?= null
    var qty: TextView?=null
    var total: TextView?=null
    private var addtocart : RelativeLayout ?=null
    //    var dialog :Dialog? =null
    var deliveryadapter : deliveryadapter?=null
    var delivery :List<storeDeliverymodel>?=null
    var back : ImageView?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_store_home)
        preferenceManager = PreferenceManager(this)

        product = findViewById(R.id.product) as RecyclerView?
        storename = findViewById(R.id.storename) as TextView?
        storedetails = findViewById(R.id.storedetails) as TextView?
        storeicon = findViewById(R.id.storeicon) as ImageView?
        storerating = findViewById(R.id.rating) as RatingBar?
        qty= findViewById(R.id.qty) as TextView?
        total= findViewById(R.id.rate) as TextView?
        addtocart = (findViewById(R.id.addtocart) as RelativeLayout?)
        back = findViewById(R.id.back)as ImageView

//        dialog = Dialog(this)



        getUserCart()
        fetchStoreProducts()

        addtocart?.isInvisible

        val datamodel = intent.getParcelableExtra<data>("store_id")

        storename?.text = datamodel?.store_name
        storedetails?.text = datamodel?.store_detail
        storerating?.rating = datamodel?.store_rating!!
        storeicon?.let {
            Glide.with(this).load(datamodel.store_image).placeholder(R.drawable.product_placeholder).error(R.drawable.product_placeholder).into(
                it
            )
        }



        product?.layoutManager = GridLayoutManager(this,2)
        productnew= ArrayList()
        productAdapter = this.let { productadapter(it, productnew as ArrayList<Product>,this) }
        product?.adapter = productAdapter

        productAdapter?.onItemClick={

            val productdetail = actionbotton.newInstance()
            val bundle = Bundle()
            bundle.putString("productId", it.id)
            bundle.putString("productName",it.product_name)
            bundle.putString("productRate",it.product_price)
            bundle.putString("productImage",it.product_thumb_image)
            bundle.putString("productDescription",it.product_detail)
            productdetail.arguments = bundle
            productdetail.show(supportFragmentManager,actionbotton.TAG)
            Log.e("productname",it.product_name)

        }

        back!!.setOnClickListener {

            intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)

        }

        addtocart!!.setOnClickListener {
            bottomsheet()

        }

    }


    private fun fetchStoreProducts(){

        val data = intent.getParcelableExtra<data>("store_id")

        var store_id = data?.id
        RestClient.getApiInterface().fetchStoreProducts(store_id).enqueue(object :
            Callback<productmodel> {
            override fun onResponse(call: Call<productmodel>, response: Response<productmodel>) {

                if (response.isSuccessful)
                {
                    val productmodel =response.body()
                    if (productmodel!=null){
                        val Product = productmodel?.data
                        productAdapter?.updateData(Product)

                      }
                    Log.e("Succesful", productmodel?.message.toString())

                }

            }

            override fun onFailure(call: Call<productmodel>, t: Throwable) {
                Log.e("Error",t.message.toString())
            }

        })


    }


    private fun getUserCart(){
        RestClient.getApiInterface().getUserCart(preferenceManager?.userDetails?.token,
            preferenceManager?.userDetails?.id).enqueue(object : Callback<additem> {
            override fun onResponse(call: Call<additem>, response: Response<additem>) {
                if (response.isSuccessful)
                {
                    val ordersummary =response.body()
                    if (ordersummary!=null) {
                        val Product = ordersummary?.data
                        val ordernew = Product?.products
//                        Product_Cart = Product?.products as ArrayList<Product_1>
                        if (Product != null) {

                            var cartTotal = Product?.cart_total
                            var cartqty = Product?.cart_quantity

                            addtocart?.isVisible
                            qty?.text = cartqty.toString()
                            total?.text = cartTotal.toString()

                        } else {
                            addtocart?.isInvisible
                        }
                    }
                }
            }

            override fun onFailure(call: Call<additem>, t: Throwable) {
                Log.e("Failure", t.message.toString())
            }

        })
    }


    private fun GetStoreDeliveryMethods(){

        var datastore = intent.getParcelableExtra<data>("store_id")

        var store_Id = datastore?.id
        RestClient.getApiInterface().GetStoreDeliveryMethods(preferenceManager?.userDetails?.token,
            store_Id).enqueue(object : Callback<storeDeliverymodel> {
            override fun onResponse(
                call: Call<storeDeliverymodel>,
                response: Response<storeDeliverymodel>
            ) {
                if (response.isSuccessful){
                    var storeDeliverymodel = response.body()
                    if (storeDeliverymodel != null){
                        var delivery = storeDeliverymodel?.data
                        deliveryadapter?.updateData(delivery)

                        Log.e("Successfull", storeDeliverymodel?.data.toString())
                    }
                }
            }

            override fun onFailure(call: Call<storeDeliverymodel>, t: Throwable) {

                Log.e("Error",t.message.toString())
            }

        })

    }

    override fun onClick(position: Int, product: Product) {

        addItemToCart(product,position)
        getUserCart()
//        Toast.makeText(this,"successfull",Toast.LENGTH_SHORT).show()

    }

    private fun addItemToCart(product: Product, position: Int) {

        RestClient.getApiInterface().addItemToCart(
            preferenceManager?.userDetails?.token, product.id,
            product.store_id, preferenceManager?.userDetails?.id
        ).enqueue(object : Callback<additem> {
            override fun onResponse(call: Call<additem>, response: Response<additem>) {
                if(response.isSuccessful){
                    val additem = response.body()
                    if (additem != null){
                        var data = additem.data
                        var cartqty = data.cart_quantity
                        var carttotal = data.cart_total

//                        addtocart?.isVisible
//                        qty?.text = cartqty.toString()
//                        total?.text = carttotal.toString()


                    }else{
                        addtocart?.isInvisible
                    }
                }

                Log.e("Added Successfully",response.message())
            }

            override fun onFailure(call: Call<additem>, t: Throwable) {
                Log.e("Added Error",t.message.toString())
            }

        })

    }

//    private fun addItemToCart(product: Product, position: Int) {
//
//        RestClient.getApiInterface().addItemToCart(preferenceManager?.userDetails?.token,).enqueue(object :
//            Callback<additem> {
//            override fun onResponse(call: Call<additem>, response: Response<additem>) {
//
//                if(response.isSuccessful){
//                    val additem = response.body()
//                    if (additem != null){
//                        var data = additem.data
//                        var cartqty = data.cart_quantity
//                        var carttotal = data.cart_total
//
//                        addtocart?.isVisible
//                        qty?.text = cartqty.toString()
//                        total?.text = carttotal.toString()
//
//
//                    }else{
//                        addtocart?.isInvisible
//                    }
//                }
//
//                Log.e("Added Successfully",response.message())
//            }
//
//            override fun onFailure(call: Call<additem>, t: Throwable) {
//                Log.e("Added Error",t.message.toString())
//            }
//        })
//    }/

    private fun bottomsheet(){

        var dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.storedeliverymethod)
        dialog.show()

        var deliverypop:RecyclerView = dialog.findViewById(R.id.deliverymethod)
        var button: Button = dialog.findViewById(R.id.continueD)
        var linearLayoutManager: LinearLayoutManager
        var deliverynew : List<data_1>
        var bundle =Bundle()


        GetStoreDeliveryMethods()


        deliverypop?.layoutManager = LinearLayoutManager(this)
        deliverynew = ArrayList()
        deliveryadapter = this.let { deliveryadapter(it,deliverynew as ArrayList<data_1>) }
        deliverypop?.adapter = deliveryadapter

        deliveryadapter?.onItemClick ={
//            val intent = Intent(this,confirmproduct::class.java)
//
//            intent.putExtra("id",it.id)
//            intent.putExtra("store_id",it.store_id)
//            intent.putExtra("desc",it.description)
//            intent.putExtra("ETA",it.delievery_eta)
//            intent.putExtra("rate",it.price)
//            startActivity(intent)

            id = it.id
            store_id = it.store_id
            desc = it.description
            ETA = it.delievery_eta
            rate = it.price

        }
        button.setOnClickListener {
            val intent = Intent(this, confirmproduct::class.java)
            intent.putExtra("id",id)
            intent.putExtra("store_id",store_id)
            intent.putExtra("desc",desc)
            intent.putExtra("ETA",ETA)
            intent.putExtra("rate",rate)
            startActivity(intent)
        }

        dialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window?.setGravity(Gravity.BOTTOM)
    }





}