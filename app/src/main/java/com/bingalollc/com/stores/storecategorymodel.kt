package com.bingalollc.com.stores

data class storecategorymodel (

    val message: String,
    val status: Int,
    val data: List<data>,
)

data class data (
    val id: Int,
    val category_name: String,
    val category_position : String,
    val created_at : String,
    val updated_at:String,
    val stores: List<stores>,

        )
   data class stores (

        val id: Int,
        val store_name: String,
        val store_details : String,
        val store_image :String,
        val position :String,
        val store_category :StoreCategory_1,
        val store_rating: String,
        val isVerified: String,
        val storeLocation: String,
        val storeLat: String,
        val storeLon: String,
        val storeRibbon: String,
        val storeDeliveryFee: String,
        val createdAt: String,
        val updatedAt: String,
        val featuredPosition: String,
        val minimumOrderAmount: String

            )
data class StoreCategory_1(
    val id: String,
    val category_name: String,
    val category_position: String,
    val createdAt: String,
    val updatedAt: String
)





